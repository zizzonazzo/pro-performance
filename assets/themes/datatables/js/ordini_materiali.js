$(document).ready(
	function () {
		if($("select[name='codice_materiale_interno'] option:selected").text().indexOf('RITAGLIO')>-1){
					$("#lunghezza_ordinata_field_box").fadeTo("fast", 0.33);
					$("input[name='lunghezza_ordinata']").prop('disabled', true);
				}else{
					$("#misure_ritaglio_ordinato_field_box").fadeTo("fast", 0.33);
					$("input[name='misure_ritaglio_ordinato']").prop('disabled', true);
				}
		
		$("select[name='codice_materiale_interno']").change(function(){
				if($("select[name='codice_materiale_interno'] option:selected").text().indexOf('RITAGLIO')>-1){
					$("#misure_ritaglio_ordinato_field_box").fadeTo("fast", 1);
					$("input[name='misure_ritaglio_ordinato']").prop('disabled', false);
					$("#lunghezza_ordinata_field_box").fadeTo("fast", 0.33);
					$("input[name='lunghezza_ordinata']").val("");
					$("input[name='lunghezza_ordinata']").prop('disabled', true);
				}
				else{
					$("#misure_ritaglio_ordinato_field_box").fadeTo("fast", 0.33);
					$("input[name='misure_ritaglio_ordinato']").prop('disabled', true);
					$("#lunghezza_ordinata_field_box").fadeTo("fast", 1);
					$("input[name='misure_ritaglio_ordinato']").val("");
					$("input[name='lunghezza_ordinata']").prop('disabled', false);
				}
			});
	}
);
