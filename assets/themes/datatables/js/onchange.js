$(document).ready(
	function () {
		$.post('/assets/themes/datatables/js/retriever.php', {id_cliente:$("select[name='cliente']").val(), prima_operazione:2},
		function(data) {
		if(data!=""){
		    $("#cliente_note_box").html('<input style="font-size:15pt;" type="button" value="NOTA CLIENTE" class="note_produttore_button"><div class="nota_nascosta" style="width:400px;"><textarea style="color:red;">'+data+'</textarea></div>');
			$("#cliente_note_box").addClass("filled");}
		},"text"
		);


			$.post('/assets/themes/datatables/js/retriever.php', {id_produttore:$("select[name='id_produttore']").val(), prima_operazione:0},
			function(note) {
				$.each(note, function( index, value ) {
					$("#"+index+"_note_box").html('<input style="font-size:15pt;" type="button" value="NOTE" class="note_produttore_button"><div class="nota_nascosta" style="width:400px;"><textarea style="color:red;">'+value+'</textarea></div>');
					$("#"+index+"_note_box").addClass("filled");
				});
			},"json"
			);


     	$("select[name='id_produttore']").change(function(){
						if($('div.filled').length){
							$("div.filled").html("");
							$("div.filled").removeClass("filled");
						}
					$.post('/assets/themes/datatables/js/retriever.php', {id_produttore:$(this).val(), prima_operazione:1},
					function(data) {
					    $("select.lista-stamp").html(data);
						$("select[name='cliente'] option:contains('"+ $("option:selected", "select[name='id_produttore']").text() +"')").attr("selected","selected");
						$("select[name='cliente']").trigger("chosen:updated");

						$("select[name='id_destinatario'] option:contains('"+ $("option:selected", "select[name='id_produttore']").text() +"')").attr("selected","selected");
						$("select[name='id_destinatario']").trigger("chosen:updated");
						$.post('/assets/themes/datatables/js/retriever.php', {id_cliente:$("select[name='cliente']").val(), prima_operazione:2},
							function(data) {
							if(data!=""){
							    $("#cliente_note_box").html('<input style="font-size:15pt;" type="button" value="NOTA CLIENTE" class="note_produttore_button"><div class="nota_nascosta" style="width:400px;"><textarea style="color:red;">'+data+'</textarea></div>');
								$("#cliente_note_box").addClass("filled");}
							},"text"
							);
					}
					);
					$.post('/assets/themes/datatables/js/retriever.php', {id_produttore:$(this).val(), prima_operazione:0},
					function(note) {
						$.each(note, function( index, value ) {
							$("#"+index+"_note_box").html('<input style="font-size:15pt;" type="button" value="NOTE" class="note_produttore_button"><div class="nota_nascosta" style="width:400px;"><textarea style="color:red;">'+value+'</textarea></div>');
							$("#"+index+"_note_box").addClass("filled");
						});
					},"json"
					);
			});

			$("select[name='id_fornitore']").change(function(){
					$.post('/assets/themes/datatables/js/retriever.php', {id_fornitore:$(this).val(), prima_operazione:3},
					function(data) {
						$("select[name='id_modalita_pagamento']").val(data);
						$("select[name='id_modalita_pagamento']").trigger("chosen:updated");
						if(data!=0)
							$("#data_scadenza_fattura_field_box").hide();
						else
							$("#data_scadenza_fattura_field_box").show();
					},"text"
					);
			});

			$("select[name='id_modalita_pagamento']").change(function(){
					if($(this).val()=="") $("#data_scadenza_fattura_field_box").show();
					else $("#data_scadenza_fattura_field_box").hide();
			});
			$("select[name='id_tipo_pagamento']").change(function(){
					if($(this).val()!=10) $("#data_effettuato_pagamento_field_box").show();
					else $("#data_effettuato_pagamento_field_box").hide();
			});


			$("select[name='cliente']").change(function(){

				$.post('/assets/themes/datatables/js/retriever.php', {id_cliente:$(this).val(), prima_operazione:2},
				function(data) {
				if(data!=""){
				    $("#cliente_note_box").html('<input style="font-size:15pt;" type="button" value="NOTA CLIENTE" class="note_produttore_button"><div class="nota_nascosta" style="width:400px;"><textarea style="color:red;">'+data+'</textarea></div>');
					$("#cliente_note_box").addClass("filled");}
				},"text"
				);
			});
	}
);

$(document).on("mouseover","input.note_produttore_button", function(event){
		$(this).next("div").removeClass("nota_nascosta");
		$(this).next("div").addClass("nota_produttore");
		$("div.nota_produttore textarea").height($("div.nota_produttore textarea")[0].scrollHeight);
});
$(document).on("mouseout","input.note_produttore_button", function(event){
		$(this).next("div").removeClass("nota_produttore");
		$(this).next("div").addClass("nota_nascosta");
});
