$(document).ready(
	function () {
     	var previous;

		    $("select.lista-stamp").focus(function () {
		        previous = this.value;
		    }).change(function() {
		       		if (confirm("SI STA MODIFICANDO LA STAMPATRICE, COSI' FACENDO VERRA' ELIMINATO IL PRE-SET ATTUALE. CONTINUARE?"))
						alert("SALVANDO LE MODIFICHE VERRA' CARICATO UN NUOVO PRE-SET, SE ESISTENTE.");
					else
		        		$("select.lista-stamp").val(previous);
		    });
		});
