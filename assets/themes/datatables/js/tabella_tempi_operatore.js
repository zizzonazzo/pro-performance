$(document).ready(function(){
	$("input.tasto_modifica").toggle(function() {
			if(!$('div.t_op_column.editing').length){
				$(this).val('Salva');
				var row=$(this).attr('row');
				var valore = $('div.t_op_column[row="'+row+'"]').text();
				var valore1 = $('div.t_macch_column[row="'+row+'"]').text();
				var valore3 = $('div.centro_costo_column[row="'+row+'"]').text();
				var valore4 = $('div.data_column[row="'+row+'"]').text();
				
				
				$('div.data_column[row="'+row+'"]').addClass("editing");
				$('div.data_column[row="'+row+'"]').html("<input style='height:30px;font-size:12pt;' class='data_lav' size='12' type='text' value="+valore4+" maxlength='10' />");
				$('div.t_op_column[row="'+row+'"]').addClass("editing");
				$('div.t_op_column[row="'+row+'"]').html("<input style='height:30px;font-size:12pt;' class='t_op' size='12' type='text' value="+valore+" maxlength='10' />");
				$('div.t_macch_column[row="'+row+'"]').addClass("editing");
				$('div.t_macch_column[row="'+row+'"]').html("<input style='height:30px;font-size:12pt;' class='t_macch' size='12' type='text' value="+valore1+" maxlength='10'/>");
				$.post('/assets/themes/datatables/js/centri_costo.php',{},function(data) {
						$('div.centro_costo_column[row="'+row+'"]').html(data);
						$("select.centri_costo option:contains('"+ valore3 +"')").attr("selected","selected");
						},"text"
						);
			}
	},
	function(){
		if($('div.t_op_column.editing').attr('row')==$(this).attr('row')){
	
			$(this).val('Modifica');
			var row=$(this).attr('row');
			var valore = $('input.t_op').val();
			var valore1 = $('input.t_macch').val();
			var valore2 = $('select.centri_costo option:selected').val();
			var valore3 = $('select.centri_costo option:selected').text();
			var valore4 = $('input.data_lav').val();
			
			
			$.post('/assets/themes/datatables/js/aggiorna_lavorazione.php',{data_lav:valore4, id_lavorazione:$(this).attr('id_lav'), t_op:valore, t_macch:valore1, id_centro_costo:valore2},
					function(data) {
		                location.reload();
						},"text"
						);
			
			$('div.data_column[row="'+row+'"]').html(valore4);
			$('div.t_op_column[row="'+row+'"]').html(valore);
			$('div.t_macch_column[row="'+row+'"]').html(valore1);
			$('div.centro_costo_column[row="'+row+'"]').html(valore3);
			$('div.t_op_column[row="'+row+'"]').removeClass("editing");
			$('div.t_macch_column[row="'+row+'"]').removeClass("editing");
			$('div.data_column[row="'+row+'"]').removeClass("editing");
			
		
		}
		
	});

}
);

$(document).ready(function(){
	$("input.tasto_elimina").click(function() {
		if(!$('div.t_op_column.editing').length){
	
			if (confirm("ELIMINARE LA LAVORAZIONE?")){
				$.post('/assets/themes/datatables/js/elimina_lavorazione.php',{id_lavorazione:$(this).attr('id_lav')},
						function(data) {
			                location.reload();
							},"text"
							);
						}
			
		}
		else{ alert("PRIMA SALVARE LA MODIFICA CORRENTE");}
	});
  	
});


