var id_bolle_prod =[];
var accessori = [];


function caricaMaterialeEforme(){
	$.post('/assets/themes/datatables/js/gestione_mobile.php',
						 {id_materiale:$("option:selected", $("select[name='cod_bolla']")).attr('mat'),
							operazione: 2},
								function(data) {
									$("#span_materiale").html(data);
								},"text"
								);
	$.post('/assets/themes/datatables/js/gestione_mobile.php',
						 {id_materiale:$("option:selected", $("select[name='cod_bolla']")).attr('mat'),
							operazione: 3},
								function(data) {
									$("#span_forma").html(data);
									caricaSezioni();
									$("#span_11").html("Lunghezza");
									$("#span_12").html('<input name="lunghezza" type="text" size="10" value="">');
									$("#span_21").html("Ritaglio");
									$("#span_22").html('<input name="misura_ritaglio" type="text" size="10" value="">');
								},"text"
								);
}

function caricaSezioni(){
	$.post('/assets/themes/datatables/js/gestione_mobile.php',
			 {	forma_materiale:$("select[name='forma']").val(),
			 	id_materiale:$("option:selected", $("select[name='cod_bolla']")).attr('mat'),
				operazione: 4},
					function(data) {
						$("#span_sezioni").html(data);
						if($("select[name='sezione']").val()!="RITAGLIO"){
								$("#span_11").html("Lunghezza");
								$("#span_12").html('<input name="lunghezza" type="text" size="10" value="">');
								$("#span_21").html("Ritaglio");
								$("#span_22").html('<input name="misura_ritaglio" type="text" size="10" value="">');
						}else{
								$("#span_11").html("Misure Ritaglio");
								$("#span_12").html('<input name="lunghezza" type="text" size="10" value="">');
								$("#span_21").html("Kg");
								$("#span_22").html('<input name="kg" type="text" size="10" value="">');
						}
					},"text"
					);
}

$(document).on("click",".dati-button" ,function(){//carico le sezioni e il materiale della prima riga selezionata
			// caricaMaterialeEforme();
			$.post('/assets/themes/datatables/js/gestione_mobile.php',
						 {cod_bolla:id_bolle_prod[0],
							operazione: 5},
								function(data) {
									$("#span_materiale").html(data);
								},"text"
								);
});
	
$(document).ready(function(){
	 $('tr td:first-child').bind('click', function(event){ 

			    if(event.altKey) {

				      var pathArray = window.location.pathname.split( '/' );
				      if((pathArray[4]==="componenti")||($(this).find(".codice_accessorio_interno_column").attr("scaricato")=="N")){
						      if($(this).hasClass("multiple-select"))
						      { 
						      	$(this).removeClass("multiple-select");
						      	if(pathArray[4]==="accessori"){//rimuovi da 
						      		accessori.splice(accessori.indexOf($(this).find(".codice_accessorio_interno_column").attr("codice")),1);
						      	}
						      	if(!$( "td.multiple-select" ).length) $("#addButton").html("");
						  		}
						      else{ 
								      $(this).addClass("multiple-select");
								      // $(this).attr('id_bolla_prod','11');
								      if(pathArray[4]==="componenti"){
								      	id_bolle_prod.push($(this).siblings("#actions").find(".bottone").siblings().find(">:first-child").children().attr('href').split("/")[9]);
								      }
								      if(pathArray[4]==="accessori"){
								      	accessori.push($(this).find(".codice_accessorio_interno_column").attr("codice"));
								      }
								 }
						      if(!($( "#multiple-button" ).length)&&($( "td.multiple-select" ).length)){ 
						   		if(pathArray[4]==="componenti"){
						   			$("#addButton").html("<button style='margin-bottom:5px;' class='btn btn-default dati-button' data-toggle='modal' data-target='#myModalDati'><span class='glyphicon glyphicon glyphicon-check'></span> DATI MULTIPLI</button> &nbsp;&nbsp;&nbsp;");
						   			$("#addButton").append("<button style='margin-bottom:5px;' class='btn btn-default' data-toggle='modal' data-target='#myModalTempi'><span class='glyphicon glyphicon glyphicon-time'></span> TEMPI MULTIPLI</button> &nbsp;&nbsp;&nbsp;");
						   		}
						   		if(pathArray[4]==="accessori"){
						   			$("#addButton").html("<button style='margin-bottom:5px;' class='scarica_button btn btn-default' ><span class='glyphicon glyphicon glyphicon-export'></span>  SCARICA MAGAZZINO</button>");
						   		}
						   	  }
					 }
			    }

	   	}); 
});

$(document).on("change","select[name='cod_bolla']" ,function(){
			caricaMaterialeEforme();
});

$(document).on("change","select[name='forma']" ,function(){
			caricaSezioni();
			
});

$(document).on("click",".scarica_button",function(){
	$(this).html("SCARICAMENTO IN CORSO..");
	$.post('http://'+window.location.host+"/index.php/omis/scarica_magazzino_accessori",
			 {	accessori_scaricare:accessori},
					function(data) {
						location.reload();
					},"text"
					);
});

$(document).on("change","select[name='sezione']" ,function(){
			if($(this).val()!="RITAGLIO"){
					$("#span_11").html("Lunghezza");
					$("#span_12").html('<input name="lunghezza" type="text" size="10" value="">');
					$("#span_21").html("Ritaglio");
					$("#span_22").html('<input name="misura_ritaglio" type="text" size="10" value="">');
			}else{
					$("#span_11").html("Misure Ritaglio");
					$("#span_12").html('<input name="lunghezza" type="text" size="10" value="">');
					$("#span_21").html("Kg");
					$("#span_22").html('<input name="kg" type="text" size="10" value="">');
			}
});
	