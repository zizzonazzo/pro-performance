$(function() {
  if($("#dialog").length){
    $( "#dialog" ).dialog({
	    autoOpen: false,
	    maxWidth:600,
	    maxHeight: 500,
	    width: 600,
	    height: 500,
	    modal: true,
	    show: {
		        effect: "blind",
		        duration: 600
		      },
		      hide: {
		        effect: "explode",
		        duration: 600
		      }
		    });
      }
  });


$(document).keypress(function(e) {
    if(e.which == 13 && ($("input.ric_globale.editing").length)) {
		if($("input.ric_globale.editing").val()!="" && $("input.ric_globale.editing").val()!=" "){
			$.post('/assets/themes/datatables/js/ricerca_globale.php', {filter:$("select.select_ric_globale").val(), value:$("input.ric_globale.editing").val(),url:base_url},
					function(data) {
					$("div.risultato_ric_glob").html(data);
					$(".tasto_chiudi_ricerca").html("<input class='chiudi_ricerca' type='button' value='X'>");
					},"text"
					);
			}
    }
});

$(document).ready(function(){
	$(".completa_button a[href='NO']").hide();

	$("input.ric_globale").click(function() {
			$(this).addClass("editing");
	});
	$("input.ric_globale").bind('blur', function(){
		$("input.ric_globale.editing").removeClass("editing");
	});
});


$(document).on("click","input.chiudi_ricerca", function(event){
		$(".tasto_chiudi_ricerca").html("");
		$("div.risultato_ric_glob").html("");
});

$(document).on("click",".totali-mensili-button", function(event){
	event.preventDefault();

	$.post('http://'+window.location.host+"/index.php/omis/calcola_totali_mensili",
			 {},
					function(data) {
						$("#dialog").html(data);
						$( "#dialog" ).dialog( "open" );
						$("span.ui-dialog-title").text('Totali Mensili');
					},"text"
					);

});

$(document).on("click",".fatturazione-button", function(event){
	event.preventDefault();

	$.post('http://'+window.location.host+"/index.php/fatturazione/calcola_fatturazione_ddt",
			 {},
					function(data) {
						$("#dialog").html(data+'<br><br><button class="lancia-fatturazione-button btn btn-default" role="button"><span class="ui-button-text glyphicon glyphicon-euro"></span> FATTURA TUTTI I DDT!</button>');
						$( "#dialog" ).dialog( "open" );
						$("span.ui-dialog-title").text('Fatturazione DDT');
					},"text"
					).fail(function() {
    					$("#dialog").html("Non esistono DDT da fatturare per il mese scorso.");
						$( "#dialog" ).dialog( "open" );
						$("span.ui-dialog-title").text('Fatturazione DDT');
 					 });

});

$(document).on("click",".lancia-fatturazione-button", function(event){
	event.preventDefault();
	$("#dialog").html('<div class="row"><div class="col-sm-4"></div><div class="col-sm-4">FATTURAZIONE IN CORSO...</div><div class="col-sm-4"></div></div><div class="row"><div class="col-sm-4"></div><div class="col-sm-4"><img src="/application/views/icons/loading3.gif"></div><div class="col-sm-4"></div></div>');
	$.post('http://'+window.location.host+"/index.php/fatturazione/esegui_fatturazione_ddt",
			 {},
					function(data) {
						$("#dialog").html("FINITO!!!!!!");
					},"text"
					).fail(function() {
    					$("#dialog").html("Si e' verificato un errore.");

 					 });

});


$(document).on("click",".pagato-button", function(event){
	event.preventDefault();
	$(this).parent().parent().parent().parent().parent().addClass("editingtr");
	$.post('http://'+window.location.host+"/index.php/fatturazione/pagata_singola_fattura",
			 {id_riga:$(this).children("a").attr('href').split('/')[4]},
					function(data) {
						$("tr.editingtr").children().css( "background-color", "" );
						$("tr.editingtr td:nth-child(9)").html(data);
						$("tr.editingtr").removeClass("editingtr");
					},"text"
					);
});


$(document).ready(function() {
	var pathArray = window.location.pathname.split( '/' );
	if(pathArray[3]=="tabella_fatture_fornitori"){
		$("#addButton").append('<button class="totali-mensili-button btn btn-default" role="button"><span class="ui-button-text glyphicon glyphicon-euro"></span> TOTALI MENSILI</button>');

	}
	if(pathArray[3]=="operazioni_cassa"){
		$("#addButton").append('<a href="http://'+window.location.host+'/index.php/contabilita/genera_file_excel_cassa"><button class="btn btn-default" role="button"><span class="ui-button-text glyphicon glyphicon-file"></span> ESPORTA MOVIMENTI</button></a>');

	}
	if(pathArray[3]=="tabella_fatture"){
		$("#addButton").append('<button class="fatturazione-button btn btn-default" role="button"><span class="ui-button-text glyphicon glyphicon-euro"></span> FATTURAZIONE DDT</button>');

	}
if ( $('#groceryCrudTableScadenzario').length ){
	oTable = $('#groceryCrudTableScadenzario').DataTable({

    // ajax:           "ajax_call",

         "order": [],
         "ordering": false,
        "autoWidth": false,
        "oLanguage":{
		    "sSearch":  "Cerca:"
		  },
		// "sPaginationType": "full_numbers",
		// "bStateSave": true,
		// "iDisplayLength": default_per_page,
		"pageLength": 50,
		"bPaginate": false,
		"bLengthChange": false


	});
}

} );
