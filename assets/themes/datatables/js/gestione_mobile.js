


function caricaComponenti(){
	$.post('/assets/themes/datatables/js/gestione_mobile.php',
			 {	commessa:$("select[name='commessa']").val(),
				operazione: 1},
					function(data) {
						$("#lista_componenti").html(data);
						caricaMaterialeEforme();
					},"text"
					);
}

function caricaMaterialeEforme(){
	$("input[name='id_materiale']").val($("option:selected", $("select[name='cod_bolla']")).attr('mat'));
	$.post('/assets/themes/datatables/js/gestione_mobile.php',
						 {id_materiale:$("option:selected", $("select[name='cod_bolla']")).attr('mat'),
							operazione: 2},
								function(data) {
									$("#span_materiale").html(data);
								},"text"
								);
	$.post('/assets/themes/datatables/js/gestione_mobile.php',
						 {id_materiale:$("option:selected", $("select[name='cod_bolla']")).attr('mat'),
							operazione: 3},
								function(data) {
									$("#span_forma").html(data);
									caricaSezioni();
									$("#span_11").html("Lunghezza");
									$("#span_12").html('<input name="lunghezza" type="text" size="10" value="">');
									$("#span_21").html("Ritaglio");
									$("#span_22").html('<input name="misura_ritaglio" type="text" size="10" value="">');

								},"text"
								);
}

function caricaSezioni(){
	$.post('/assets/themes/datatables/js/gestione_mobile.php',
			 {	forma_materiale:$("select[name='forma']").val(),
			 	id_materiale:$("option:selected", $("select[name='cod_bolla']")).attr('mat'),
				operazione: 4},
					function(data) {
						$("#span_sezioni").html(data);
						if($("select[name='sezione']").val()!="RITAGLIO"){
								$("#span_11").html("Lunghezza");
								$("#span_12").html('<input name="lunghezza" type="text" size="10" value="">');
								$("#span_21").html("Ritaglio");
								$("#span_22").html('<input name="misura_ritaglio" type="text" size="10" value="">');
						}else{
								$("#span_11").html("Misure Ritaglio");
								$("#span_12").html('<input name="lunghezza" type="text" size="10" value="">');
								$("#span_21").html("Kg");
								$("#span_22").html('<input name="kg" type="text" size="10" value="">');
						}
					},"text"
					);
}


	
$(document).ready(function(){
				caricaComponenti();
				caricaMaterialeEforme();
});

$(document).on("change","select[name='commessa']" ,function(){//tipo trasporto
			caricaComponenti();
});//--------------------------------------------------------------------

$(document).on("change","select[name='cod_bolla']" ,function(){//tipo porto
			caricaMaterialeEforme();
});//---

$(document).on("change","select[name='forma']" ,function(){//tipo porto
			caricaSezioni();
			
});//--------------------------------------------------------------------

$(document).on("change","select[name='sezione']" ,function(){//tipo porto
			if($(this).val()!="RITAGLIO"){
					$("#span_11").html("Lunghezza");
					$("#span_12").html('<input name="lunghezza" type="text" size="10" value="">');
					$("#span_21").html("Ritaglio");
					$("#span_22").html('<input name="misura_ritaglio" type="text" size="10" value="">');
			}else{
					$("#span_11").html("Misure Ritaglio");
					$("#span_12").html('<input name="lunghezza" type="text" size="10" value="">');
					$("#span_21").html("Kg");
					$("#span_22").html('<input name="kg" type="text" size="10" value="">');
			}
});
	