
$(document).ready(function(){

	$("div.comm_div").dblclick(function() {
		if($("div.quick_view.editing").length){
			$("div.quick_view.editing").html("");
			$("div.quick_view.editing").removeClass("editing");
	}
			$(this).find("div.quick_view").addClass("editing");
			
			$("div.quick_view.editing").hide();
			$("div.quick_view.editing").html("    .");
			$("div.quick_view.editing").fadeIn(1000);	
			
				$.post('/assets/themes/datatables/js/quick-view-retr_op.php', {commessa:$(this).text()},
						function(data) {
							$("div.quick_view.editing").html(data);
						
						},"text"
						);
		
	});
	
$("*").click(function(){	$("div.quick_view.editing").html("");
	$("div.quick_view.editing").removeClass("editing");});

});
var default_per_page = typeof default_per_page !== 'undefined' ? default_per_page : 25;

$(document).ready(function() {

	$('tfoot').html("");

	oTable = $('#groceryCrudTableScadenzario').DataTable({
		  
    // ajax:           "ajax_call",
       
         "order": [],
         "ordering": false,
        "autoWidth": false,
        "oLanguage":{
		    "sSearch":  "Cerca:"
		  },
		// "sPaginationType": "full_numbers",
		// "bStateSave": true,
		// "iDisplayLength": default_per_page,
		"pageLength": 50,
		"bPaginate": false,
		"bLengthChange": false
		

	});

	
} ); 




function delete_row(delete_url , row_id)
{	
	if(confirm(message_alert_delete))
	{
		$.ajax({
			url: delete_url,
			dataType: 'json',
			success: function(data)
			{					
				if(data.success)
				{
					$('#ajax_refresh_and_loading').trigger('click');
					$('#report-success').html( data.success_message ).slideUp('fast').slideDown('slow');						
					$('#report-error').html('').slideUp('fast');
					$('tr#row-'+row_id).addClass('row_selected');
					var anSelected = fnGetSelected( oTable );
					oTable.fnDeleteRow( anSelected[0] );					
				}
				else
				{
					$('#report-error').html( data.error_message ).slideUp('fast').slideDown('slow');						
					$('#report-success').html('').slideUp('fast');						
					
				}
			}
		});
	}
	
	return false;
}

function fnGetSelected( oTableLocal )
{
	var aReturn = new Array();
	var aTrs = oTableLocal.fnGetNodes();
	
	for ( var i=0 ; i<aTrs.length ; i++ )
	{
		if ( $(aTrs[i]).hasClass('row_selected') )
		{
			aReturn.push( aTrs[i] );
		}
	}
	return aReturn;
}

