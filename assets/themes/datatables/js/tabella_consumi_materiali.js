$(document).ready(function(){
	$("input.tasto_modifica").toggle(function() {
			if(!$('div.forma_column.editing').length){
				$(this).val('Salva');

				var row=$(this).attr('row');
				var valore = $('div.forma_column[row="'+row+'"]').text();
				var valore1 = $('div.sezione_column[row="'+row+'"]').text();
				var valore3 = $('div.lunghezza_barra_column[row="'+row+'"]').text();
				var valore4 = $('div.misure_ritaglio_column[row="'+row+'"]').text();
				var valore5 = $('div.kg_consumati_column[row="'+row+'"]').text();
				var valore6 = $('div.kg_ritaglio_column[row="'+row+'"]').text();
				$('div.forma_column[row="'+row+'"]').addClass("editing");
				$(this).addClass("editing");

				$('div.forma_column[row="'+row+'"]').html("<select class='forma'><option value='PIATTO'>PIATTO</option><option value='TONDO'>TONDO</option><option value='TUBO'>TUBO</option><option value='ESAGONO'>ESAGONO</option></select>");
				$("select.forma option:contains('"+ valore +"')").attr("selected","selected");

				//$('div.lunghezza_barra_column[row="'+row+'"]').addClass("editing");
				$('div.lunghezza_barra_column[row="'+row+'"]').html("<input style='height:30px;font-size:12pt;' class='lunghezza_barra' size='12' type='text' value='"+valore3+"' maxlength='10'/>");
				$.post('/assets/themes/datatables/js/carica_sezioni.php',{forma_materiale:valore, id_materiale:$(this).attr('id_materiale')},function(data) {
						$('div.sezione_column[row="'+row+'"]').html('<select class="sezione">'+data+'</select>');
						$("select.sezione option:contains('"+ valore1 +"')").attr("selected","selected");
						},"text"
						);
				$('div.misure_ritaglio_column[row="'+row+'"]').html("<input style='height:30px;font-size:12pt;' class='misure_ritaglio' size='12' type='text' value='"+valore4+"' maxlength='10' />");
				$('div.kg_consumati_column[row="'+row+'"]').html("<input style='height:30px;font-size:12pt;' class='kg_consumati' size='12' type='text' value='"+valore5+"' maxlength='10' />");
				$('div.kg_ritaglio_column[row="'+row+'"]').html("<input style='height:30px;font-size:12pt;' class='kg_ritaglio' size='12' type='text' value='"+valore6+"' maxlength='10' />");
				if(valore1!="RITAGLIO")
				{
					$("input.kg_consumati").attr('disabled','disabled');
					$("input.kg_ritaglio").attr('disabled','disabled');

				}
				if(valore1=="RITAGLIO")
				{
					$("input.lunghezza_barra").attr('disabled','disabled');
					$("input.kg_consumati").attr('disabled','disabled');

				}

			}
	},
	function(){
		if($('div.forma_column.editing').attr('row')==$(this).attr('row')){
	
			$(this).val('Modifica');
			var row=$(this).attr('row');
			var valore = $('select.forma option:selected').val();
			var valore1 = $('select.sezione option:selected').val();
			var valore3 = $('input.lunghezza_barra').val();
			var valore4 = $('input.misure_ritaglio').val();
			var valore5 = $('input.kg_consumati').val();
			var valore6 = $('input.kg_ritaglio').val();
			
			
			
			$.post('/assets/themes/datatables/js/aggiorna_consumo.php',{ id_consumo_mat:$(this).attr('id_consumo'), id_materiale:$(this).attr('id_materiale'), forma:valore, sezione:valore1, lunghezza_barra:valore3,
						misure_ritaglio:valore4, kg_consumati:valore5, kg_ritaglio:valore6},
					function(data) {
		                location.reload();
		                //alert(data);
						},"text"
						);
			
			/*$('div.forma_column[row="'+row+'"]').html(valore4);
			$('div.t_op_column[row="'+row+'"]').html(valore);
			$('div.t_macch_column[row="'+row+'"]').html(valore1);
			$('div.centro_costo_column[row="'+row+'"]').html(valore3);
			$('div.forma_column[row="'+row+'"]').removeClass("editing");
			$('div.t_macch_column[row="'+row+'"]').removeClass("editing");
			$(this).removeClass("editing");*/

		
		}
		
	});





$("select.forma").on("change", function(event){
			$.post('/assets/themes/datatables/js/carica_sezioni.php',{forma_materiale:$(this).val(), id_materiale:$("input.tasto_modifica.editing").attr('id_materiale')},function(data) {
						$('div.sezione_column[row="'+$("input.tasto_modifica.editing").attr('row')+'"]').html('<select class="sezione">'+data+'</select>');
						},"text"
						);

});

$("select.sezione").on("change", function(event){
			if($(this).val()=="RITAGLIO")
			{
				$("input.lunghezza_barra").attr('disabled','disabled');
				$("input.lunghezza_barra").val("");
				$("input.kg_consumati").attr('disabled','disabled');
				$("input.kg_ritaglio").removeAttr('disabled');
			}
			if($(this).val()!="RITAGLIO")
			{
				$("input.kg_consumati").attr('disabled','disabled');
				$("input.kg_ritaglio").attr('disabled','disabled');
				$("input.lunghezza_barra").removeAttr('disabled');
			}

});

}
);




$(document).ready(function(){

	$("input.tasto_elimina").click(function() {
		if(!$('div.forma_column.editing').length){
	
			if (confirm("ELIMINARE LA TRANSAZIONE DI CONSUMO MATERIALE?")){
				$.post('/assets/themes/datatables/js/elimina_consumo.php',{id_consumo_mat:$(this).attr('id_consumo')},
						function(data) {
			                location.reload();
							},"text"
							);
						}
			
		}
		else{ alert("PRIMA SALVARE LA MODIFICA CORRENTE");}
	});
  	
});

$(document).ready(function(){
	$("input.tasto_elimina_scarico").click(function() {
	
			if (confirm("ELIMINARE LA TRANSAZIONE DI SCARICO ACCESSORIO?")){
				$.post("http://"+window.location.host+"/index.php/omis/ricarica_magazzino_accessori",
					{id_accessorio_commessa:$(this).attr('id_scarico')},
						function(data) {
			                location.reload();
							},"text"
							);
						}
			
		
	});
  	
});

$(document).ready(function() {


	oTable = $('#groceryCrudTableConsumi').DataTable({
		  
    // ajax:           "ajax_call",
       
         "order": [],
         "ordering": false,
        "autoWidth": false,
        "oLanguage":{
		    "sSearch":  "Cerca:"
		  },
		// "sPaginationType": "full_numbers",
		// "bStateSave": true,
		// "iDisplayLength": default_per_page,
		"pageLength": 50,
		"bPaginate": false,
		"bLengthChange": false
		

	});

	
} ); 


