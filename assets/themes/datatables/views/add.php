<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');

	$this->set_css($this->default_theme_path.'/datatables/css/datatables_views.css');

	$this->set_js($this->default_theme_path.'/datatables/js/jquery.form.js');
	$this->set_js($this->default_theme_path.'/datatables/js/datatables-add.js');
	$this->set_css($this->default_css_path.'/libraries/jquery-ui.min.css');
	$this->set_css($this->default_css_path.'/libraries/chosen.min.css');
	$this->set_css($this->default_css_path.'/menutendina.css');



	$this->set_js($this->default_javascript_path.'/libraries/jquery-ui.min.js');
	$this->set_js('assets/themes/datatables/js/onchange.js');
	$this->set_js('assets/themes/datatables/js/onchange_materiali.js');
	$this->set_js('assets/js/libraries/jquery_plugins/config/jquery.chosen.config.js');
	$this->set_js('assets/js/libraries/jquery_plugins/ajax-chosen.js');
	$this->set_js('assets/js/libraries/jquery_plugins/jquery.chosen.min.js');
	$this->set_js('assets/themes/datatables/js/ricerca_globale.js');

	$this->set_js($this->default_javascript_path.'/libraries/jquery-1.11.3.min.js');


?>

<script type='text/javascript'>
	var base_url = '<?php echo base_url();?>';

	var upload_a_file_string = '<?php echo $this->l('form_upload_a_file');?>';
</script>
<div class='ui-widget-content ui-corner-all datatables'>
	<h3 class="ui-accordion-header ui-helper-reset ui-state-default form-title">
		<div class='floatL form-title-left'>
			<a href="#"><?php echo $this->l('form_add'); ?> <?php echo $subject?></a>

		</div>
		<?php 	if(!$this->unset_back_to_list) { ?>
					<div class='form-button-box'>
						<input type='button' value='<?php echo $this->l('form_save_and_go_back'); ?>' class='ui-input-button' id="save-and-go-back-button1"/>
					</div>
						<?php 	if($add_something==1) { ?>
									<div class='form-button-box'>
										<input type='button' value='Salva e Aggiungi Nuovo' class='ui-input-button' id="save-and-add-new1"/>
									</div>
						<?php   } ?>

						<?php 	if($add_something==4) { ?>
									<div class='form-button-box'>
										<input type='button' value='Salva e Dettaglio' class='ui-input-button' id="save-and-detail"/>
									</div>
						<?php   } ?>
						<?php 	if($add_something==5) { ?>
									<div class='form-button-box'>
										<input type='button' value='Salva e Dettaglio' class='ui-input-button' id="save-and-detail-ordine"/>
									</div>
						<?php   } ?>
					<div class='form-button-box'>
						<input type='button' value='<?php echo $this->l('form_cancel'); ?>' onclick="javascript:  goToList()" class='ui-input-button' />
					</div>
		<?php   } ?>
<?php 	if(!$this->unset_back_to_list) { ?>
		<div class='floatR'>
			<a href='<?php echo $list_url?>' onclick='javascript: return goToList()' class='gotoListButton' >
				<?php echo $this->l('form_back_to_list'); ?>
			</a>
		</div>
<?php  } ?>
		<div class='clear'></div>
	</h3>
<div class='form-content form-div'>
	<form action='<?php echo $insert_url?>' method='post' id='crudForm' autocomplete='off' enctype="multipart/form-data">
		<div>
			<?php
			if($subject != 'Lavoro'){
			$counter = 0;
				foreach($fields as $field)
				{
					$even_odd = $counter % 2 == 0 ? 'odd' : 'even';
					$counter++;
			?>
			<div class='form-field-box <?php echo $even_odd?>' id="<?php echo $field->field_name; ?>_field_box">
				<div class='form-display-as-box' id="<?php echo $field->field_name; ?>_display_as_box">
					<?php echo $input_fields[$field->field_name]->display_as?><?php echo ($input_fields[$field->field_name]->required)? "<span class='required'>*</span> " : ""?> :
				</div>
				<div class='form-input-box' id="<?php echo $field->field_name; ?>_input_box">
					<?php echo $input_fields[$field->field_name]->input?>
				</div>
				<div class='clear'></div>
			</div>
			<?php }
			}
			else{ foreach($fields as $field);//INSERIRE FORMAT PER INPUTAZIONE NUOVO LAVORO:?>
				<div style="border-style:solid; padding:7px; font-size:large;">
					RIFERIMENTI OMIS
				</div>
				<div class='form-field-box even' id="n_stampo_field_box">
					<div class='form-display-as-box' id="n_stampo_display_as_box">
						Numero Stampo:
					</div>
					<div class='form-input-box' id="n_stampo_input_box">
						<?php echo $input_fields['n_stampo']->input?>
					</div>
					<div id="n_stampo_note_box" style='float:right; height:10px;'><textarea style='height:13px;width:300px;visibility:hidden;' readonly style='color:blue;'></textarea></div>

					<div class='clear'></div>

				</div>
				<div class='form-field-box odd' id="dis_sapone_field_box">
					<div class='form-display-as-box' id="dis_sapone_display_as_box">
						Disegno Sapone:
					</div>
					<div class='form-input-box' id="dis_sapone_input_box">
						<input name="dis_sapone" type="text" value="" maxlength="25">
					</div>
					<div id="dis_sapone_note_box" style='float:right; height:10px;'><textarea style='height:13px;width:300px;visibility:hidden;' readonly style='color:blue;'></textarea></div>
					<div class='clear'></div>
				</div>
				<div class='form-field-box even' id="tipo_lavoro_field_box">
					<div class='form-display-as-box' id="tipo_lavoro_display_as_box">
							Tipo Lavoro:
					</div>
					<div class='form-input-box' id="tipo_lavoro_input_box">
						<?php echo $input_fields['tipo_lavoro']->input?>
						</div>
						<div id="tipo_lavoro_note_box" style='float:right; height:10px;'><textarea style='height:13px;width:300px;visibility:hidden;' readonly style='color:blue;'></textarea></div>
						<div class='clear'></div>
				</div>
				<div class='form-field-box odd' id="c_lavorazione_field_box">
					<div class='form-display-as-box' id="c_lavorazione_display_as_box">
							C/ Lavorazione:
					</div>
					<div class='form-input-box' id="c_lavorazione_input_box">
						<?php echo $input_fields['c_lavorazione']->input?>
						</div>
						<div id="c_lavorazione_note_box" style='float:right; height:10px;'><textarea style='height:13px;width:300px;visibility:hidden;' readonly style='color:blue;'></textarea></div>

						<div class='clear'></div>
				</div>
				<div class='form-field-box even' id="id_produttore_field_box">
					<div class='form-display-as-box' id="id_produttore_display_as_box">
							Produttore*:
					</div>
					<div class='form-input-box' id="id_produttore_input_box">
						<?php echo $input_fields['id_produttore']->input?>
						</div>
						<div id="id_produttore_note_box" style='float:right; height:10px;'><textarea style='height:13px;width:300px;visibility:hidden;' readonly style='color:blue;'></textarea></div>

						<div class='clear'></div>
				</div>
				<div class='form-field-box odd' id="cliente_field_box">
					<div class='form-display-as-box' id="cliente_display_as_box">
							Cliente:
					</div>
					<div class='form-input-box' id="cliente_input_box">
						<?php echo $input_fields['cliente']->input?>
						</div>
						<div id="cliente_note_box" style='float:right; height:10px;'><textarea style='height:13px;width:300px;visibility:hidden;' readonly style='color:blue;'></textarea></div>

						<div class='clear'></div>
				</div>
				<div class='form-field-box even' id="id_destinatario_field_box">
					<div class='form-display-as-box' id="id_destinatario_display_as_box">
							Destinatario:
					</div>
					<div class='form-input-box' id="id_destinatario_input_box">
						<?php echo $input_fields['id_destinatario']->input?>
						</div>
						<div id="id_destinatario_note_box" style='float:right; height:10px;'><textarea style='height:13px;width:300px;visibility:hidden;' readonly style='color:blue;'></textarea></div>

						<div class='clear'></div>
				</div>
				<div style="border-style:solid; padding:7px; font-size:large;">
					RIFERIMENTI CLIENTE
				</div>
				<div class='form-field-box even' id="ordine_cliente_field_box">
					<div class='form-display-as-box' id="ordine_cliente_display_as_box">
						Ordine Cliente:
					</div>
					<div class='form-input-box' id="ordine_cliente_input_box">
						<?php echo $input_fields['ordine_cliente']->input?>
					</div>
					<div id="ordine_cliente_note_box" style='float:right; height:10px;'><textarea style='height:13px;width:300px;visibility:hidden;' readonly style='color:blue;'></textarea></div>

					<div class='clear'></div>
				</div>
				<div class='form-field-box odd' id="comm_cliente_field_box">
					<div class='form-display-as-box' id="comm_cliente_display_as_box">
						Commessa Cliente:
					</div>
					<div class='form-input-box' id="comm_cliente_input_box">
						<?php echo $input_fields['comm_cliente']->input?>
					</div>
					<div id="comm_cliente_note_box" style='float:right; height:10px;'><textarea style='height:13px;width:300px;visibility:hidden;' readonly style='color:blue;'></textarea></div>

					<div class='clear'></div>
				</div>
				<div class='form-field-box even' id="rif_cliente_field_box">
					<div class='form-display-as-box' id="rif_cliente_display_as_box">
							Riferimento Cliente:
					</div>
					<div class='form-input-box' id="rif_cliente_input_box">
						<?php echo $input_fields['rif_cliente']->input?>
						</div>
						<div id="rif_cliente_note_box" style='float:right; height:10px;'><textarea style='height:13px;width:300px;visibility:hidden;' readonly style='color:blue;'></textarea></div>

						<div class='clear'></div>
				</div>
				<div class='form-field-box odd' id="nome_sapone_field_box">
					<div class='form-display-as-box' id="nome_sapone_display_as_box">
							Nome Sapone:
					</div>
					<div class='form-input-box' id="nome_sapone_input_box">
						<?php echo $input_fields['nome_sapone']->input?>
						</div>
						<div id="nome_sapone_note_box" style='float:right; height:10px;'><textarea style='height:13px;width:300px;visibility:hidden;' readonly style='color:blue;'></textarea></div>

						<div class='clear'></div>
				</div>
				<div class='form-field-box even' id="peso_field_box">
					<div class='form-display-as-box' id="peso_display_as_box">
							Peso Sapone:
					</div>
					<div class='form-input-box' id="peso_input_box">
						<?php echo $input_fields['peso']->input?>
						</div>
						<div id="peso_note_box" style='float:right; height:10px;'><textarea style='height:13px;width:300px;visibility:hidden;' readonly style='color:blue;'></textarea></div>

						<div class='clear'></div>
				</div>
				<div class='form-field-box odd' id="peso_specifico_field_box">
					<div class='form-display-as-box' id="peso_specifico_display_as_box">
							Peso Specifico Sapone:
					</div>
					<div class='form-input-box' id="peso_specifico_input_box">
						<?php echo $input_fields['peso_specifico']->input?>
						</div>
						<div id="peso_specifico_note_box" style='float:right; height:10px;'><textarea style='height:13px;width:300px;visibility:hidden;' readonly style='color:blue;'></textarea></div>

						<div class='clear'></div>
				</div>
				<div style="border-style:solid; padding:7px; font-size:large;">
					RIFERIMENTI STAMPO
				</div>
				<div class='form-field-box even' id="stampatrice_field_box">
					<div class='form-display-as-box' id="stampatrice_display_as_box">
						Stampatrice:
					</div>
					<div class='form-input-box' id="stampatrice_input_box">
						<?php echo $input_fields['stampatrice']->input?>
					</div>
					<div id="stampatrice_note_box" style='float:right; height:10px;'><textarea style='height:13px;width:300px;visibility:hidden;' readonly style='color:blue;'></textarea></div>

					<div class='clear'></div>
				</div>
				<div class='form-field-box odd' id="compatibile_field_box">
					<div class='form-display-as-box' id="compatibile_display_as_box">
						Compatibile:
					</div>
					<div class='form-input-box' id="compatibile_input_box">
						<?php echo $input_fields['compatibile']->input?>
					</div>
					<div id="compatibile_note_box" style='float:right; height:10px;'><textarea style='height:13px;width:300px;visibility:hidden;' readonly style='color:blue;'></textarea></div>

					<div class='clear'></div>
				</div>
				<div class='form-field-box even' id="linea_produzione_field_box">
					<div class='form-display-as-box' id="linea_produzione_display_as_box">
							Linea di Produzione:
					</div>
					<div class='form-input-box' id="linea_produzione_input_box">
						<?php echo $input_fields['linea_produzione']->input?>
						</div>
						<div id="linea_produzione_note_box" style='float:right; height:10px;'><textarea style='height:13px;width:300px;visibility:hidden;' readonly style='color:blue;'></textarea></div>

						<div class='clear'></div>
				</div>
				<div class='form-field-box odd' id="n_cavita_field_box">
					<div class='form-display-as-box' id="n_cavita_display_as_box">
							Numero Cavita':
					</div>
					<div class='form-input-box' id="n_cavita_input_box">
						<?php echo $input_fields['n_cavita']->input?>
						</div>
						<div id="n_cavita_note_box" style='float:right; height:10px;'><textarea style='height:13px;width:300px;visibility:hidden;' readonly style='color:blue;'></textarea></div>

						<div class='clear'></div>
				</div>
				<div class='form-field-box even' id="tipo_stampo_field_box">
					<div class='form-display-as-box' id="tipo_stampo_display_as_box">
							Tipo Stampo:
					</div>
					<div class='form-input-box' id="tipo_stampo_input_box">
						<?php echo $input_fields['tipo_stampo']->input?>
						</div>
						<div id="tipo_stampo_note_box" style='float:right; height:10px;'><textarea style='height:13px;width:300px;visibility:hidden;' readonly style='color:blue;'></textarea></div>

						<div class='clear'></div>
				</div>
				<div class='form-field-box odd' id="orientam_scarico_field_box">
					<div class='form-display-as-box' id="orientam_scarico_display_as_box">
							Orientamento Scarico:
					</div>
					<div class='form-input-box' id="orientam_scarico_input_box">
						<?php echo $input_fields['orientam_scarico']->input?>
						</div>
						<div id="orientam_scarico_note_box" style='float:right; height:10px;'><textarea style='height:13px;width:300px;visibility:hidden;' readonly style='color:blue;'></textarea></div>

						<div class='clear'></div>
				</div>
					<div class='form-field-box even' id="id_materiale_matrice_field_box">
						<div class='form-display-as-box' id="id_materiale_matrice_display_as_box">
							Materiale Stampo:
						</div>
						<div class='form-input-box' id="id_materiale_matrice_input_box">
							<?php echo $input_fields['id_materiale_matrice']->input?>
						</div>
						<div id="id_materiale_matrice_note_box" style='float:right; height:10px;'><textarea style='height:13px;width:300px;visibility:hidden;' readonly style='color:blue;'></textarea></div>

						<div class='clear'></div>
					</div>
					<div class='form-field-box odd' id="id_materiale_piastre_field_box">
						<div class='form-display-as-box' id="id_materiale_piastre_display_as_box">
							Materiale Piastre:
						</div>
						<div class='form-input-box' id="id_materiale_piastre_input_box">
							<?php echo $input_fields['id_materiale_piastre']->input?>
						</div>
						<div id="id_materiale_piastre_note_box" style='float:right; height:10px;'><textarea style='height:13px;width:300px;visibility:hidden;' readonly style='color:blue;'></textarea></div>

						<div class='clear'></div>
					</div>
					<div class='form-field-box even' id="id_trattam_matrice_field_box">
						<div class='form-display-as-box' id="id_trattam_matrice_display_as_box">
								Trattamento Stampo:
						</div>
						<div class='form-input-box' id="id_trattam_matrice_input_box">
							<?php echo $input_fields['id_trattam_matrice']->input?>
							</div>
							<div id="id_trattam_matrice_note_box" style='float:right; height:10px;'><textarea style='height:13px;width:300px;visibility:hidden;' readonly style='color:blue;'></textarea></div>

							<div class='clear'></div>
					</div>
					<div class='form-field-box odd' id="id_trattam_piastre_field_box">
						<div class='form-display-as-box' id="id_trattam_piastre_display_as_box">
								Trattamento Piastre:
						</div>
						<div class='form-input-box' id="id_trattam_piastre_input_box">
							<?php echo $input_fields['id_trattam_piastre']->input?>
							</div>
							<div id="id_trattam_piastre_note_box" style='float:right; height:10px;'><textarea style='height:13px;width:300px;visibility:hidden;' readonly style='color:blue;'></textarea></div>

							<div class='clear'></div>
					</div>
					<div style="border-style:solid; padding:7px; font-size:large;">
						RIFERIMENTI DATE
					</div>
					<div class='form-field-box odd' id="data_consegna_cliente_field_box">
						<div class='form-display-as-box' id="data_consegna_cliente_display_as_box">
							Data Consegna al Cliente:
						</div>
						<div class='form-input-box' id="data_consegna_cliente_input_box">
							<?php echo $input_fields['data_consegna_cliente']->input?>
						</div>
						<div class='clear'></div>
					</div>
					<div class='form-field-box even' id="data_cons_dis_prev_field_box">
						<div class='form-display-as-box' id="data_cons_dis_prev_display_as_box">
							Data Consegna Dis Prevista:
						</div>
						<div class='form-input-box' id="data_cons_dis_prev_input_box">
							<?php echo $input_fields['data_cons_dis_prev']->input?>
						</div>
						<div class='clear'></div>
					</div>
					<div class='form-field-box odd' id="consegna_tassativa_field_box">
						<div class='form-display-as-box' id="consegna_tassativa_display_as_box">
							Consegna Tassativa:
						</div>
						<div class='form-input-box' id="consegna_tassativa_input_box">
							<?php echo $input_fields['consegna_tassativa']->input?>
						</div>
						<div class='clear'></div>
					</div>
					<div style="border-style:solid; padding:7px; font-size:large;">
						INFORMAZIONI PROGETTO
					</div>
					<table><tr><td>
					<div  width="50%">

					<div class='form-field-box even' id="descrizione_field_box">
						<div class='form-display-as-box' id="descrizione_display_as_box">
							Descrizione Progetto:
						</div>
						<div class='form-input-box' id="descrizione_input_box">
							<?php echo $input_fields['descrizione']->input?>
						</div>
						<div class='clear'></div>
					</div>
					<div class='form-field-box odd' id="note_field_box">
						<div class='form-display-as-box' id="note_display_as_box">
							Note di Produzione:
						</div>
						<div class='form-input-box' id="note_input_box">
							<?php echo $input_fields['note']->input?>
						</div>
						<div class='clear'></div>
					</div>
					</div>
					</td>
					<td>
					<div width="50%" >
						<div class='form-field-box even' id="lunghezza_sapone_field_box">
							<div class='form-display-as-box' id="lunghezza_sapone_display_as_box">
								Lunghezza Sapone:
							</div>
							<div class='form-input-box' id="lunghezza_sapone_input_box">
								<?php echo $input_fields['lunghezza_sapone']->input?>
							</div>
							<div class='clear'></div>
						</div>
						<div class='form-field-box odd' id="larghezza_sapone_field_box">
							<div class='form-display-as-box' id="larghezza_sapone_display_as_box">
								Larghezza Sapone:
							</div>
							<div class='form-input-box' id="larghezza_sapone_input_box">
								<?php echo $input_fields['larghezza_sapone']->input?>
							</div>
							<div class='clear'></div>
						</div>
						<div class='form-field-box even' id="altezza_sapone_field_box">
							<div class='form-display-as-box' id="altezza_sapone_display_as_box">
								Altezza Sapone:
							</div>
							<div class='form-input-box' id="altezza_sapone_input_box">
								<?php echo $input_fields['altezza_sapone']->input?>
							</div>
							<div class='clear'></div>
						</div>
						<div class='form-field-box odd' id="dati_filiera_field_box">
							<div class='form-display-as-box' id="dati_filiera_display_as_box">
								Diametro/Spessore Filiera:
							</div>
							<div class='form-input-box' id="dati_filiera_input_box">
								<?php echo $input_fields['dati_filiera']->input?>
							</div>
							<div class='clear'></div>
						</div>
					</div>
					</td>
					</tr>
					</table>
					<div style="border-style:solid; padding:7px; font-size:large;">
						AMMINISTRAZIONE
					</div>
					<div class='form-field-box even' id="offerta_n_field_box">
						<div class='form-display-as-box' id="offerta_n_display_as_box">
							Numero Offerta:
						</div>
						<div class='form-input-box' id="offerta_n_input_box">
							<?php echo $input_fields['offerta_n']->input?>
						</div>

						<div class='clear'></div>
					</div>
					<div class='form-field-box odd' id="offerta_data_field_box">
						<div class='form-display-as-box' id="offerta_data_display_as_box">
							Data Offerta:
						</div>
						<div class='form-input-box' id="offerta_data_input_box">
							<?php echo $input_fields['offerta_data']->input?>
						</div>
						<div class='clear'></div>
					</div>


			<?php }?>
			<!-- Start of hidden inputs -->
				<?php
					foreach($hidden_fields as $hidden_field){
						echo $hidden_field->input;
					}
				?>
			<!-- End of hidden inputs -->
			<div class='line-1px'></div>
			<div id='report-error' class='report-div error'></div>
			<div id='report-success' class='report-div success'></div>
		</div>
		<div class='buttons-box'>

<?php 	if(!$this->unset_back_to_list) { ?>
			<div class='form-button-box'>
				<input type='button' value='<?php echo $this->l('form_save_and_go_back'); ?>' class='ui-input-button' id="save-and-go-back-button"/>
			</div>
			<?php 	if($add_something==4) { ?>
									<div class='form-button-box'>
										<input type='button' value='Salva e Dettaglio' class='ui-input-button' id="save-and-detail1"/>
									</div>
						<?php   } ?>
			<?php 	if($add_something==5) { ?>
									<div class='form-button-box'>
										<input type='button' value='Salva e Dettaglio' class='ui-input-button' id="save-and-detail-ordine1"/>
									</div>
						<?php   } ?>
			<?php 	if($add_something==1) { ?>
									<div class='form-button-box'>
										<input type='button' value='Salva e Aggiungi Nuovo' class='ui-input-button' id="save-and-add-new"/>
									</div>
						<?php   } ?>
			<div class='form-button-box'>
				<input type='button' value='<?php echo $this->l('form_cancel'); ?>' onclick="javascript: internalLink=true; goToList()" class='ui-input-button' />
			</div>
<?php   } ?>
			<div class='form-button-box loading-box'>
				<div class='small-loading' id='FormLoading'><?php echo $this->l('form_insert_loading'); ?></div>
			</div>
			<div class='clear'></div>
		</div>
	</form>
</div>
</div>
<script>
	var validation_url = '<?php echo $validation_url?>';
	var list_url = '<?php echo $list_url?>';
	var add_new_url = '<?php echo $add_new_url?>';

	var message_alert_add_form = "<?php echo $this->l('alert_add_form')?>";
	var message_insert_error = "<?php echo $this->l('insert_error')?>";
</script>
