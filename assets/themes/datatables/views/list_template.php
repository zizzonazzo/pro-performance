<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');

	$this->set_css($this->default_theme_path.'/datatables/css/demo_table_jui.css');
	$this->set_css($this->default_css_path.'/libraries/jquery-ui.min.css');
	$this->set_css($this->default_theme_path.'/datatables/css/datatables.css');

	if($tipo_op == 'CLI'){
		$this->set_css('assets/themes/datatables/css/menuclienti.css');
	}else{
		$this->set_css($this->default_css_path.'/menutendina.css');
	}

	$this->set_js('assets/themes/datatables/js/dataTables.scroller.js');
	$this->set_js('assets/themes/datatables/js/quick_view.js');
	if($tipo_op == 'OPE'){
		$this->set_js('assets/themes/datatables/js/ricerca_globale_op.js');
	}else{
		if($nome_op=='DAVID'){
			$this->set_js('assets/themes/datatables/js/ricerca_globale_david.js');
		}else{
			$this->set_js('assets/themes/datatables/js/ricerca_globale.js');
		}
	}

	$this->set_js('assets/js/azioni.js');
	if(substr($uri_segment,0,9)=='dettaglio')
			$this->set_js($this->default_theme_path.'/datatables/js/datatables_dettaglio.js');
	else
			$this->set_js($this->default_theme_path.'/datatables/js/datatables.js');
	$this->set_js($this->default_theme_path.'/datatables/js/jquery.dataTables.min.js');
	$this->set_js($this->default_javascript_path.'/libraries/jquery-ui.min.js');

	$this->set_js($this->default_javascript_path.'/libraries/jquery-1.11.3.min.js');


?>
<script type='text/javascript'>
	var base_url = '<?php echo base_url();?>';
	var subject = '<?php echo $subject?>';

	var displaying_paging_string = "<?php echo str_replace( array('{start}','{end}','{results}'),
		array('_START_', '_END_', '_TOTAL_'),
		$this->l('list_displaying')
	   ); ?>";
	var filtered_from_string 	= "<?php echo str_replace('{total_results}','_MAX_',$this->l('list_filtered_from') ); ?>";
	var show_entries_string 	= "<?php echo str_replace('{paging}','_MENU_',$this->l('list_show_entries') ); ?>";
	var search_string 			= "<?php echo $this->l('list_search'); ?>";
	var list_no_items 			= "<?php echo $this->l('list_no_items'); ?>";
	var list_zero_entries 			= "<?php echo $this->l('list_zero_entries'); ?>";

	var list_loading 			= "<?php echo $this->l('list_loading'); ?>";

	var paging_first 	= "<?php echo $this->l('list_paging_first'); ?>";
	var paging_previous = "<?php echo $this->l('list_paging_previous'); ?>";
	var paging_next 	= "<?php echo $this->l('list_paging_next'); ?>";
	var paging_last 	= "<?php echo $this->l('list_paging_last'); ?>";

	var message_alert_delete = "<?php echo $this->l('alert_delete'); ?>";

	var default_per_page = '<?php echo $default_per_page;?>';

</script>


<?php
	if(!empty($actions)){
?>
	<style type="text/css">
		<?php foreach($actions as $action_unique_id => $action){?>
			<?php if(!empty($action->image_url)){ ?>
				.<?php echo $action_unique_id; ?>{
					background: url('<?php echo $action->image_url; ?>') !important;
				}
			<?php }?>
		<?php }?>
	</style>
<?php
	}
?>
<div id='report-error' class='report-div error report-list'></div>
<div id='report-success' class='report-div success report-list' <?php if($success_message !== null){?>style="display:block"<?php }?>>
<?php if($success_message !== null){?>
	<p><?php echo $success_message; ?></p>
<?php }?>
</div>
<div class="row" id='title-row'>
<div class='col-sm-4' id="addButton">
<?php if(!$unset_add){?>
<a href="<?php echo $add_url?>">
	<button class="btn btn-default" role='button'>
	<span class="ui-button-text glyphicon glyphicon-plus-sign" ></span>
</button>
</a>
<?php }?>
</div>

<!-- <table>
<tr><td width="20%"></td><td width="60%" ><div  align="center" class="risultato_ric_glob"></div></td><td class="tasto_chiudi_ricerca" width="20%" style="vertical-align:top;"></td>
</table> -->

<?php echo $list_view?>
