<?php  
	if (!defined('BASEPATH')) exit('No direct script access allowed');

									$DB = new mysqli("localhost","root","omis3008","omis");

?></div><table cellpadding="0" cellspacing="0" border="0" class="display" id="groceryCrudTable">
	<thead>
		<tr>
			
			<?php $commessa='';
				 foreach($columns as $column){?>
				<th><small><?php echo $column->display_as; ?></small></th>
			<?php }?>
			<?php if(!$unset_delete || !$unset_edit || !empty($actions)){?>
			<th class='actions'><?php echo $this->l('list_actions'); ?></th>
			<?php }?>
		</tr>
	</thead>
	<tfoot>
		<tr>
			
			<?php foreach($columns as $column){?>
				<th><input type="text" placeholder="<?php  echo $column->display_as; ?>" /></th>
			<?php }?>
			<?php if(!$unset_delete || !$unset_edit || !empty($actions)){?>
			<th class='actions'></th>
			<?php }?>
		</tr>
	</tfoot>
	<tbody>
		<?php foreach($list as $num_row => $row){ ?>  
		<tr id='row-<?php echo $num_row?>'>
				<?php foreach($columns as $column){?>
					<?php
					switch($column->display_as)
					{
					    
					    case 'Comm.':
							$commessa=$row->{$column->field_name}; ?>
					        <td class="column_comm"><?php echo $row->{$column->field_name}?><div  align="center" class="quick_view"></div>
							</td>
						<?php
					    break;
						case 'Data consegna cliente':
								//$omisDB = mysql_select_db('omis',$DB);

								$query = "SELECT consegna_tassativa FROM lavori WHERE commessa='".$commessa."'"; 
								$result = mysql_query($query);
								$riga = mysql_fetch_assoc($result);
								if($riga['consegna_tassativa']=='N') 
									$classe='';
								else $classe='tassativa';?>
						        <td id='<?php echo $classe?>' ><?php echo $row->{$column->field_name}?></td>
						
						<?php
					    break;
					    default:?>
					        <td><?php echo $row->{$column->field_name}?></td>
						<?php
					    break;
					}
					?>
					

				<?php }?>
			<?php if(!$unset_delete || !$unset_edit || !empty($actions)){?>
			<td id='actions'>
			<ul id="nav">
			<li id="azione">
				<?php if($tipo_op == 'CLI'){?>
				<a href='#' class="bottone">DISEGNO</a>
				<?php } else{?>
					<a href='#' class="bottone">Azioni</a>
					<?php }?>
				
					
			<ul>
					<?php if(!$unset_edit){?>
						<li class="tasto"><a href="<?php echo $row->edit_url?>" role="button" class="tasto">
							<?php echo $this->l('list_edit'); ?>
						</a></li>
					<?php }?>
				<?php 
				if(!empty($row->action_urls)){
					foreach($row->action_urls as $action_unique_id => $action_url){ 
						$action = $actions[$action_unique_id];
				?>
						<li class="<?php echo $action->css_class; ?>"><a href="<?php echo $action_url; ?>"  role="button" >
							<?php echo $action->label?>
						</a></li>	
				<?php }
				}
				?>			
			
				<?php if(!$unset_delete){?>
					<li><a onclick = "javascript: return delete_row('<?php echo $row->delete_url?>', '<?php echo $num_row?>')" 
						href="javascript:void(0)" role="button" class="delete_button">
						<?php echo $this->l('list_delete'); ?>
					</a></li>
				<?php }?>
			</ul>
			</li>
			
			</ul>	
			
			</td>
			
			<?php }?>
			
		</tr>
		<?php }?>
	</tbody>

</table>