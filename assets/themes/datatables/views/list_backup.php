<?php  
	if (!defined('BASEPATH')) exit('No direct script access allowed');
?><table cellpadding="0" cellspacing="0" border="0" class="display" id="groceryCrudTable">
	<thead>
		<tr>
			
			<?php $commessa='';
				 foreach($columns as $column){?>
				<th><?php echo $column->display_as; ?></th>
			<?php }?>
			<?php if(!$unset_delete || !$unset_edit || !empty($actions)){?>
			<th class='actions'><?php echo $this->l('list_actions'); ?></th>
			<?php }?>
		</tr>
	</thead>
	<tbody>
		<?php foreach($list as $num_row => $row){ ?>  
		<tr id='row-<?php echo $num_row?>'>
				<?php foreach($columns as $column){?>
					<?php if($column->display_as=="Comm.")
							$commessa=$row->{$column->field_name};
						?>
					<?php if($column->display_as!="Stato lavoro"){ 		if($column->display_as=="Comm."){?>
									<td class="column_comm"><?php echo $row->{$column->field_name}?><div  align="center" class="quick_view"></div>
									</td>
							<?php } else {?>
							
					<td><?php echo $row->{$column->field_name}?></td>
					<?php } } else { 
						if($commessa!=''){
						$DB = mysql_connect("localhost","root","omis3008");
						$omisDB = mysql_select_db('omis',$DB);

						$query = "SELECT stato_lavoro FROM lavori WHERE commessa='".$commessa."'"; 
						$result = mysql_query($query);
						$riga = mysql_fetch_assoc($result);
						@list($year,$month,$day) = explode("-",$riga['stato_lavoro']);
						$value = date ("d-m-Y",mktime (0,0,0,(int)$month , (int)$day , (int)$year));
						} 
						else $value='';?>
						<td class='stato_lav_column' data='<?php echo $value ?>' commessa='<?php echo $commessa ?>'><?php echo $row->{$column->field_name}?></td>
					    	<?php } ?>

				<?php }?>
			<?php if(!$unset_delete || !$unset_edit || !empty($actions)){?>
			<td class='actions'>
			<ul id="nav">
			<li id="azione"><a href='#' class="bottone">Azioni</a>
			<ul>
					<?php if(!$unset_edit){?>
						<li class="tasto"><a href="<?php echo $row->edit_url?>" onclick="internalLink=true;" role="button" class="tasto">
							<?php echo $this->l('list_edit'); ?>
						</a></li>
					<?php }?>
				<?php 
				if(!empty($row->action_urls)){
					foreach($row->action_urls as $action_unique_id => $action_url){ 
						$action = $actions[$action_unique_id];
				?>
						<li class="tasto"><a href="<?php echo $action_url; ?>"  onclick="internalLink=true;" role="button" >
							<?php echo $action->label?>
						</a></li>	
				<?php }
				}
				?>			
			
				<?php if(!$unset_delete){?>
					<li><a onclick = "javascript: internalLink=true; return delete_row('<?php echo $row->delete_url?>', '<?php echo $num_row?>')" 
						href="javascript:void(0)" role="button">
						<?php echo $this->l('list_delete'); ?>
					</a></li>
				<?php }?>
			</ul>
			</li>
			
			</ul>	
			
			</td>
			
			<?php }?>
			
		</tr>
		<?php }?>
	</tbody>
</table>