<?php  
	if (!defined('BASEPATH')) exit('No direct script access allowed');
?>

</div><table cellpadding="0" cellspacing="0" border="0" class="display" id="groceryCrudTable">
	<thead>
		<tr>
			
			<?php $commessa='';
				 foreach($columns as $column){?>
				<th><small><?php echo $column->display_as; ?></small></th>
			<?php }?>
			<?php if(!$unset_delete || !$unset_edit || !empty($actions)){?>
			<th class='actions'><?php echo $this->l('list_actions'); ?></th>
			<?php }?>
		</tr>
	</thead>
	<tfoot>
		<tr>
			
			<?php foreach($columns as $column){?>
				<th><input type="text" placeholder="<?php  echo $column->display_as; ?>" /></th>
			<?php }?>
			<?php if(!$unset_delete || !$unset_edit || !empty($actions)){?>
			<th class='actions'></th>
			<?php }?>
		</tr>
	</tfoot>
	<tbody>
	
	</tbody>

</table>