
function gestisciPrezzo() {
			var imponibile = 0; //calcolo la somma dei prezzi se tutte le commesse sono in AND
			var imposte=0;
			var totale_a_pagare =0;
			var totalepag = 0;
			var costo_trasp;
			var costo_trasp1;
			$('.prezzo_riga').each(function(i) {
				totale_riga=(parseFloat($(this).attr("prezzo"))*(1-(parseFloat($(this).parent().find(".sconto_riga").attr("value"))/100)))*($(this).parent().find(".qta_riga").attr("value"));
				totale_a_pagare+=(parseFloat($(this).attr("prezzo"))*(1-(parseFloat($(this).parent().find(".sconto_riga").attr("value"))/100)))*(1+parseFloat($(this).attr('valore_iva')/100))*($(this).parent().find(".qta_riga").attr("value"));
				imponibile += totale_riga;
				imposte +=(parseFloat($(this).attr("prezzo"))*(1-(parseFloat($(this).parent().find(".sconto_riga").attr("value"))/100)))*(parseFloat($(this).attr('valore_iva')/100))*($(this).parent().find(".qta_riga").attr("value"));
		    	$(this).parent().find(".totale_riga").html(totale_riga.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+" &#8364; &nbsp;");
		    });	

			$("#imponibile_tot").html(" "+imponibile.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+" ");
			imposte+=parseFloat($("#costo_trasp").attr("value"))*(parseFloat($("#costo_trasp").attr('classeiva')/100));
			costo_trasp=parseFloat($("#costo_trasp").attr("value"))*(1+parseFloat($("#costo_trasp").attr('classeiva')/100));
			imposte+=parseFloat($("#costo_trasp1").attr("value"))*(parseFloat($("#costo_trasp1").attr('classeiva')/100));
			costo_trasp1=parseFloat($("#costo_trasp1").attr("value"))*(1+parseFloat($("#costo_trasp1").attr('classeiva')/100));
			totale_a_pagare+=(costo_trasp+costo_trasp1);
			$("#imposta").html(" "+imposte.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
			$("#a_pagare").html(" "+totale_a_pagare.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+" ");
    }


$(document).ready(function(){

	gestisciPrezzo();
	
	$("select.revisioni").change(function(){//salvo cambio classe IVA
			window.location.replace("/index.php/preventivi/ricarica_prev/"+$("#id_preventivo").val()+"/"+$(this).val());
		});//--------------------------------------------------------------------

});


$(document).on("click",".aprimodal", function(event){
	$.post('/assets/themes/datatables/js/controllapdf.php', 
		{operazione:1, num_preventivo:$("#num_preventivo").val(),num_revisione:$("#num_revisione").val(),data_offerta_file:$("#data_offerta_file").val()},
				function(exists) {	
					if(exists!="YES"){
						    $('#myModal').modal('hide');
						    alert("Impossibile trovare file PDF da allegare.");
					}
					},"text"
				);
	
});

$(document).on("click",".stampa", function(event){
		$.post('/assets/themes/datatables/js/controllapdf.php', 
			{operazione:1, num_preventivo:$("#num_preventivo").val(),num_revisione:$("#num_revisione").val(),data_offerta_file:$("#data_offerta_file").val()},
				function(exists) {	
					if(exists!="YES")
					{
						$(".stampa").children( ".glyphicon" ).removeClass("glyphicon-print");
					 		$(".stampa").children( ".glyphicon" ).addClass("glyphicon-refresh");
					 		$(".stampa").children( ".glyphicon" ).addClass("glyphicon-refresh-animate");
						$.post('/application/libraries/tcpdf/examples/example_001.php', 
									{id_preventivo:$("#id_preventivo").val(),num_revisione:$("#num_revisione").val(), num_preventivo:$("#num_preventivo").val(),data_offerta_file:$("#data_offerta_file").val()},
										function(data) {	
											$(".stampa").children( ".glyphicon" ).removeClass("glyphicon-refresh-animate");
											$(".stampa").children( ".glyphicon" ).removeClass("glyphicon-refresh");
							 				$(".stampa").children( ".glyphicon" ).addClass("glyphicon-print");
											window.open('ftp:192.168.2.231:2222/ufficio/'+$("#num_preventivo").val()+"-"+$("#num_revisione").val()+"_"+$("#data_offerta_file").val()+".pdf");
										},"text"
										);
					}else{
							window.open('ftp:192.168.2.231:2222/ufficio/'+$("#num_preventivo").val()+"-"+$("#num_revisione").val()+"_"+$("#data_offerta_file").val()+".pdf");
					}
					},"text"
				);
});


$(document).on("click",".inviaofferta", function(event){
			var attachs=new Array();
			$('.allegato').each(function() {
				attachs.push($(this).attr("href"));
		    });
		  	var sendTo=$("#inputTo").val().split(";");
			sendTo.forEach(function(part, index, theArray) {theArray[index] = part.trim();});
			var sendToCc=$("#inputCc").val().split(";");
			sendToCc.forEach(function(part, index, theArray) {theArray[index] = part.trim();});

			var ccns=$("#inputCcn").val().split(";");
			ccns.forEach(function(part, index, theArray) {theArray[index] = part.trim();});
			$('#myModal').modal('hide');
			$('#myModalLoading').modal('show');
			$.post('/application/libraries/phpmailer/inviaemail.php', {ccns:ccns, allegati:attachs, inputTo:sendTo, inputSubject:$("#inputSubject").val(),inputBody:$("#inputBody").val().replace(/(?:\r\n|\r|\n)/g, '<br />'),inputCc:sendToCc,num_preventivo:$("#num_preventivo").val(),num_revisione:$("#num_revisione").val(),data_offerta_file:$("#data_offerta_file").val(),nome_operatore:$("#nome_operatore").val()},
						function(data) {
						$('#myModalLoading').modal('hide');	
							alert(data);
				},"text"
			);	
});

$(document).on("click",".creapdf", function(event){
		$(this).children( ".glyphicon" ).removeClass("glyphicon-floppy-save");
		$(this).children( ".glyphicon" ).addClass("glyphicon-refresh");
		$(this).children( ".glyphicon" ).addClass("glyphicon-refresh-animate");
		$.post('/application/libraries/tcpdf/examples/example_001.php', {id_preventivo:$("#id_preventivo").val(),num_revisione:$("#num_revisione").val(), num_preventivo:$("#num_preventivo").val(),data_offerta_file:$("#data_offerta_file").val()},
				function(data) {
					$(".creapdf").children( ".glyphicon" ).removeClass("glyphicon-refresh-animate");
					$(".creapdf").children( ".glyphicon" ).removeClass("glyphicon-refresh");
					$(".creapdf").children( ".glyphicon" ).addClass("glyphicon-floppy-save");	
					alert("PDF creato!");
				},"text"
				);
});
$(document).on("click","a.eliminaallegato", function(event){
		$(this).parent().html("");
});

