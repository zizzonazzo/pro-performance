
function gestisciPrezzo() {
			var imponibile = 0;
			var imposte=0;
			var totale_a_pagare =0;
			var totalepag = 0;
			var costo_trasp;
			var costo_trasp1;
			$('.prezzo_riga').each(function() {
				totale_riga=(parseFloat($(this).val())*(1-(parseFloat($(this).parent().parent().find(".sconto_riga").val())/100)))*($(this).parent().parent().find(".qta_riga").val());
				totale_a_pagare+=(parseFloat($(this).val())*(1-(parseFloat($(this).parent().parent().find(".sconto_riga").val())/100)))*(1+parseFloat($("option:selected", $(this).parent().parent().find("select[name='classe_iva_riga']")).attr('valore')/100))*($(this).parent().parent().find(".qta_riga").val());
				$(this).parent().parent().find(".totale_riga").attr("value",totale_riga.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
				imponibile += totale_riga;
				imposte +=(parseFloat($(this).val())*(1-(parseFloat($(this).parent().parent().find(".sconto_riga").val())/100)))*(parseFloat($("option:selected", $(this).parent().parent().find("select[name='classe_iva_riga']")).attr('valore')/100))*($(this).parent().parent().find(".qta_riga").val());
		    });	

			$("#imponibile_tot").html(" "+imponibile.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+" ");
			imposte+=parseFloat($("#costo_trasp").val())*(parseFloat($("option:selected", $("#costo_trasp").parent().parent().find("select[name='classe_iva']")).attr('valore')/100));
			costo_trasp=parseFloat($("#costo_trasp").val())*(1+parseFloat($("option:selected", $("#costo_trasp").parent().parent().find("select[name='classe_iva']")).attr('valore')/100));
			imposte+=parseFloat($("#costo_trasp1").val())*(parseFloat($("option:selected", $("#costo_trasp1").parent().parent().find("select[name='classe_iva1']")).attr('valore')/100));
			costo_trasp1=parseFloat($("#costo_trasp1").val())*(1+parseFloat($("option:selected", $("#costo_trasp1").parent().parent().find("select[name='classe_iva1']")).attr('valore')/100));
			totale_a_pagare+=(costo_trasp+costo_trasp1);
			$("#imposta").html(" "+imposte.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
			$("#a_pagare").html(" "+totale_a_pagare.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+" ");
    }

$(document).ready(function(){
		$(".chosen-select").chosen();

	$(".agg_comm").click(function() {
			
			$.post('/assets/themes/datatables/js/commesse_prev.php', {id_cliente:$("#id_cliente").val()},//apro tendina per aggiunta commessa
					function(data) {
						$("input.agg_comm").hide();
						$("div.elenco_comm").html(data);
						$("div.elenco_comm").append("<br><div align='center'><input  class='chiudi_ricerca' type='button' value='chiudi ^'></div>");
						$("div.elenco_comm").slideDown("slow");
					},"text"   //-------------------------------------------------------------------------------------------------------------------------
					);
			
	});
	if($("#secondo_costo_trasp").val()==0){

		$(".optionaltd").fadeTo("fast",0.3);
	}
	
});
$(document).ready(function(){

	
	$("div.elenco_comm").hide();
	gestisciPrezzo();
	
	
	$("textarea.riferimenti_prev").bind('blur', function(){//salvo i riferimenti cliente
		$.post('/assets/themes/datatables/js/salva_riferimentocliente.php', {id_preventivo:$("#id_preventivo").val(), riferimenti_prev:$("textarea.riferimenti_prev").val()},
				function(data) {
				},"text"//-------------------------------------------------------------
				);
	});
	$(document).on("blur","input.prezzo_riga", function(){//salvo il singolo prezzo di una riga
		$.post('/assets/themes/datatables/js/salva_prezzoprev.php', {id_preventivo:$("#id_preventivo").val(), id_riga:$(this).attr('id_riga'), prezzo:$(this).val()},
				function(data) {
					
				},"text"
				);//----------------------------------
				gestisciPrezzo();
		
	});
	
	$(document).on("blur","input.qta_riga", function(){//salvo il singolo prezzo di una riga
		$.post('/assets/themes/datatables/js/salva_qtaprev.php', {id_preventivo:$("#id_preventivo").val(), id_riga:$(this).attr('id_riga'), quantita:$(this).val()},
				function(data) {
				},"text"
				);//----------------------------------
						gestisciPrezzo();

	});
	$(document).on("blur","textarea.descrizione", function(){//salvo il singolo prezzo di una riga
		$.post('/assets/themes/datatables/js/salva_descrizioneprev.php', {id_preventivo:$("#id_preventivo").val(), id_riga:$(this).attr('id_riga'), descrizione:$(this).val().replace(/\r?\n/g, '<br>')},
				function(data) {

				},"text"
				);//----------------------------------
		
	});
	$(document).on("blur","textarea.stampo_nome_peso", function(){//salvo il singolo prezzo di una riga
		$.post('/assets/themes/datatables/js/salva_stampo_nome_pesoprev.php', {id_preventivo:$("#id_preventivo").val(),num_revisione:$("#num_revisione").val(), commessa:$(this).attr('commessa'), descrizione:$(this).val().replace(/\r?\n/g, '<br>')},
				function(data) {

				},"text"
				);//----------------------------------
		
	});
	$("#sconto").bind('blur', function(){//salvo lo sconto totale del preventivo
		var sconto=parseInt($(this).val());
		$.post('/assets/themes/datatables/js/salva_scontoprev.php', {id_preventivo:$("#id_preventivo").val(), sconto_prev:sconto},
				function(data) {
					
				},"text"
				);//----------------------------------
				gestisciPrezzo();
		
	});
	$(document).on("blur",".sconto_riga", function(){//salvo lo sconto totale del preventivo
		var sconto=parseInt($(this).val());
		$.post('/assets/themes/datatables/js/salva_scontoprev_riga.php', {id_riga:$(this).attr('id_riga'), sconto_prev:sconto},
				function(data) {
					
				},"text"
				);//----------------------------------
				gestisciPrezzo();
		
	});
	$("#costo_trasp").bind('blur', function(){//salvo il costo di trasporto
		var sconto=parseFloat($(this).val())/100;
		$.post('/assets/themes/datatables/js/salva_costotrasp.php', {id_preventivo:$("#id_preventivo").val(), costo_trasp:parseFloat($(this).val())},
				function(data) {
					
				},"text"
				);//----------------------------------
				gestisciPrezzo();
		
	});
	$("#costo_trasp1").bind('blur', function(){//salvo il costo di trasporto
		var sconto=parseFloat($(this).val())/100;
		$.post('/assets/themes/datatables/js/salva_costotrasp1.php', {id_preventivo:$("#id_preventivo").val(), costo_trasp:parseFloat($(this).val())},
				function(data) {
					
				},"text"
				);//----------------------------------
				gestisciPrezzo();
		
	});
	
	$("select[name='tipo_trasporto']").change(function(){//salvo AND o OR come stato di una commessa all'interno del preventivo
			$.post('/assets/themes/datatables/js/cambia_tipo_trasp_prev.php',
			 {id_preventivo:$("#id_preventivo").val(), id_tipo_trasp:$(this).val()},
					function(data) {
					},"text"
					);
		});//--------------------------------------------------------------------
	$("select[name='tipo_porto']").change(function(){//salvo AND o OR come stato di una commessa all'interno del preventivo
			$.post('/assets/themes/datatables/js/cambia_tipo_porto_prev.php',
			 {id_preventivo:$("#id_preventivo").val(), id_tipo_porto:$(this).val()},
					function(data) {
					},"text"
					);
		});//--------------------------------------------------------------------
	$("select[name='classe_iva']").change(function(){//salvo cambio classe IVA
			$.post('/assets/themes/datatables/js/salva_classeiva.php',
			 {id_preventivo:$("#id_preventivo").val(), id_classe_iva:$(this).val()},
					function(data) {
						
					},"text"
					);
					
			gestisciPrezzo();
		});//--------------------------------------------------------------------
	$("select[name='classe_iva1']").change(function(){//salvo cambio classe IVA
			$.post('/assets/themes/datatables/js/salva_classeiva1.php',
			 {id_preventivo:$("#id_preventivo").val(), id_classe_iva:$(this).val()},
					function(data) {
						
					},"text"
					);
					
			gestisciPrezzo();
		});//--------------------------------------------------------------------
	$(document).on("change","select[name='descrizione_componente']" ,function(){
		console.log("id riga "+$(this).attr('id_riga')+" commessa: "+$(this).attr('commessa')+" con quantita "+$(this).find("option:selected").attr('quantita'));
			$(this).addClass('changing');
			$.post('/assets/themes/datatables/js/carica_listino_componenti.php',
			 {id_riga:$(this).attr('id_riga'), commessa:$(this).attr('commessa'), 
			 id_categoria:$(this).val(),descrizione:$(this).find("option:selected").text(), id_trattamento:$(this).find("option:selected").attr('id_trattamento'),
			 id_materiale:$(this).find("option:selected").attr('id_materiale'),quantita:$(this).find("option:selected").attr('quantita')},
					function(data) {
						asd=JSON.parse(data);
						console.log("ecco il prezzo "+asd['prezzo']);
						$("select.changing").parent().next().html("<input type='text' style='text-align:right; width:50%;' value='"+asd['quantita']+"' id_riga='"+$("select.changing").attr('id_riga')+"' class='qta_riga'>");
						$("select.changing").parent().next().next().html("<input class='prezzo_riga' id_riga='"+$("select.changing").attr('id_riga')+"' style='text-align:right;' value='"+asd['prezzo']+"' type='text' size='8'> &#8364; ");
						$("select.changing").parent().html("<textarea class='descrizione' style='width:100%;' id_riga='"+$("select.changing").attr('id_riga')+"'>"+asd['descrizione']+"</textarea>");
						gestisciPrezzo();
					},"text"
					);
		});//--------------------------------------------------------------------
		$(document).on("change","select[name='classe_iva_riga']" ,function(){//salvo cambio classe IVA
			$.post('/assets/themes/datatables/js/salva_classeiva_riga.php',
			 {id_riga:$(this).attr('id_riga'), id_classe_iva:$(this).val()},
					function(data) {

					},"text"
					);
					
			gestisciPrezzo();
		});//--------------------------------------------------------------------
	$("select.revisioni").change(function(){//salvo cambio classe IVA
			window.location.replace("/index.php/preventivi/ricarica_prev/"+$("#id_preventivo").val()+"/"+$(this).val());
		});//--------------------------------------------------------------------
	
	
	
});

$(document).on("dblclick",".secondo_costo_tr.disabled", function(event){
	$(this).removeClass("disabled");
		$(this).addClass("enabled");

		$(".optionaltd").fadeTo("fast",1);
		$.post('/assets/themes/datatables/js/aggiorna_uso_secondo_costo.php', {id_preventivo:$("#id_preventivo").val(),enabled:'S'},
				function(data) {	
				},"text"
				);
});
$(document).on("dblclick",".secondo_costo_tr.enabled", function(event){
	$(this).removeClass("enabled");
		$(this).addClass("disabled");
		$("#costo_trasp1").attr("value",'0.00');
		$(".optionaltd").fadeTo("fast",0.3);
		$.post('/assets/themes/datatables/js/aggiorna_uso_secondo_costo.php', {id_preventivo:$("#id_preventivo").val(),enabled:'N'},
				function(data) {	
				},"text"
				);
});

$(document).on("click","input.chiudi_ricerca", function(event){
		$("div.elenco_comm").slideUp("slow");
		$("input.agg_comm").show();
});
//<input type='button' class='elimina_riga' id_riga='"+data+"' value='X'>

$(document).on("click","input.nuovariga", function(event){
	$(this).addClass("clicked");
	$.post('/assets/themes/datatables/js/aggiungi_rigaprev.php', {id_preventivo:$("#id_preventivo").val(), num_revisione:$("#num_revisione").val(),commessa:$(this).attr('commessa'),id_cliente:$("#id_cliente").val()},
				function(data) {
				
					$.post('/assets/themes/datatables/js/retrieve_classiiva.php', {id_riga:data, id_cliente:$("#id_cliente").val()},
					function(dataz) {
						$("input.nuovariga.clicked").parent().next().html("<input type='text' style='text-align:right; width:50%;' value='1' id_riga='"+data+"' class='qta_riga'>");
						$("input.nuovariga.clicked").parent().next().next().html("<input class='prezzo_riga' id_riga='"+data+"' style='text-align:right; width:80%;' value='0.00' type='text'> &#8364; ");
						$("input.nuovariga.clicked").parent().next().next().next().html("<input class='sconto_riga' id_riga='"+data+"' style='text-align:right; width:50%;' value='0' type='text'>% ");
						$("input.nuovariga.clicked").parent().next().next().next().next().html("<input class='totale_riga' id_riga='"+data+"' type='text' style='width:80%;' disabled value='0.00'> &#8364;");
						$("input.nuovariga.clicked").parent().next().next().next().next().next().html("<small>"+dataz+"</small><small><input type='button' class='elimina_riga' id_riga='"+data+"' value='X'></small>");
						if($("input.nuovariga.clicked").attr('tipo_lavoro')=='STA')
						{
							$("input.nuovariga.clicked").parent().parent().after("<div class='row' style='margin-top:10px;'><div class='col-sm-6'><input tipo_lavoro='STA' commessa='"+$("input.nuovariga.clicked").attr('commessa')+"' type='button' value='DESCRIZIONE' style='width:50%; font-size:20px; background-color:#33FFFF; ' class='nuovariga'><input commessa='"+$("input.nuovariga.clicked").attr('commessa')+"' type='button' value='COMPONENTE' style='width:50%; font-size:20px; background-color:#33FFFF; ' class='nuovarigacomponente'></div><div class='col-sm-1'></div><div class='col-sm-1' style='padding-right:5px;'></div><div class='col-sm-1' style='padding-right:5px;'></div><div class='col-sm-1'></div><div class='col-sm-2' style='padding-left:0px;'></div></div>");
						}else{
							$("input.nuovariga.clicked").parent().parent().after("<div class='row' style='margin-top:10px;'><div class='col-sm-6'><input tipo_lavoro='NS' commessa='"+$("input.nuovariga.clicked").attr('commessa')+"' type='button' value='DESCRIZIONE' style='width:100%; font-size:20px; background-color:#33FFFF; ' class='nuovariga'></div><div class='col-sm-1'></div><div class='col-sm-1' style='padding-right:5px;'></div><div class='col-sm-1' style='padding-right:5px;'></div><div class='col-sm-1'></div><div class='col-sm-2' style='padding-left:0px;'></div></div>");
						}

						$("input.nuovariga.clicked").parent().html("<textarea class='descrizione' style='width:100%;' id_riga='"+data+"'></textarea>");
					});
					
					
				},"text"
				);
});
$(document).on("click","input.nuovarigacomponente", function(event){
	$(this).addClass("clicked");
	$.post('/assets/themes/datatables/js/aggiungi_rigaprev.php', {id_preventivo:$("#id_preventivo").val(), num_revisione:$("#num_revisione").val(),commessa:$(this).attr('commessa'),id_cliente:$("#id_cliente").val()},
				function(data) {
					$.post('/assets/themes/datatables/js/retrieve_componenti_commessa.php', {commessa:$("input.nuovarigacomponente.clicked").attr('commessa'),id_riga:data,tipo_ddt:'C'},
					function(dataz) {
						$.post('/assets/themes/datatables/js/retrieve_classiiva.php', {id_riga:data, id_cliente:$("#id_cliente").val()},
						function(datio) {
							$("input.nuovarigacomponente.clicked").parent().next().html("<input type='text' style='text-align:right; width:50%;' value='1' id_riga='"+data+"' class='qta_riga'>");
							$("input.nuovarigacomponente.clicked").parent().next().next().html("<input class='prezzo_riga' id_riga='"+data+"' style='text-align:right; width:80%;' value='0.00' type='text'> &#8364; ");
							$("input.nuovarigacomponente.clicked").parent().next().next().next().html("<input class='sconto_riga' id_riga='"+data+"' style='text-align:right; width:50%;' value='0' type='text'>%");
							$("input.nuovarigacomponente.clicked").parent().next().next().next().next().html("<input class='totale_riga' id_riga='"+data+"' type='text' style='width:80%;' disabled value='0.00'> &#8364;");
							$("input.nuovarigacomponente.clicked").parent().next().next().next().next().next().html("<small>"+datio+"</small><small><input type='button' class='elimina_riga' id_riga='"+data+"' value='X'></small>");
							$("input.nuovarigacomponente.clicked").parent().parent().after("<div class='row' style='margin-top:10px;'><div class='col-sm-6'><input tipo_lavoro='STA' commessa='"+$("input.nuovarigacomponente.clicked").attr('commessa')+"' type='button' value='DESCRIZIONE' style='width:50%; font-size:20px; background-color:#33FFFF; ' class='nuovariga'><input commessa='"+$("input.nuovarigacomponente.clicked").attr('commessa')+"' type='button' value='COMPONENTE' style='width:50%; font-size:20px; background-color:#33FFFF; ' class='nuovarigacomponente'></div><div class='col-sm-1'></div><div class='col-sm-1' style='padding-right:5px;'></div><div class='col-sm-1' style='padding-right:5px;'></div><div class='col-sm-1'></div><div class='col-sm-2' style='padding-left:0px;'></div></div>");
							$("input.nuovarigacomponente.clicked").parent().html(dataz);
							
						});
						
					});
					
					
				},"text"
				);
});

$(document).on("click","input.elimina_riga", function(event){
	if (confirm("Eliminare riga selezionata?")){
		$(this).parent().parent().parent().css("margin-top","0");
		$(this).parent().parent().parent().html("");

		$.post('/assets/themes/datatables/js/elimina_rigaprev.php', {id_riga:$(this).attr('id_riga')},
					function(data) {	
					},"text"
					);
	}
	
});

$(document).on("click",".aprimodal", function(event){
	$.post('/assets/themes/datatables/js/controllapdf.php', 
		{operazione:1, num_preventivo:$("#num_preventivo").val(),num_revisione:$("#num_revisione").val(),data_offerta_file:$("#data_offerta_file").val()},
				function(exists) {	
					if(exists!="YES"){
						    $('#myModal').modal('hide');
						    alert("Impossibile trovare file PDF da allegare.");
					}
					},"text"
				);
	
});

$(document).on("click",".inviaofferta", function(event){
			var attachs=new Array();
			$('.allegato').each(function() {
				attachs.push($(this).attr("href"));
		    });
		  	var sendTo=$("#inputTo").val().split(";");
			sendTo.forEach(function(part, index, theArray) {theArray[index] = part.trim();});
			var sendToCc=$("#inputCc").val().split(";");
			sendToCc.forEach(function(part, index, theArray) {theArray[index] = part.trim();});

			var ccns=$("#inputCcn").val().split(";");
			ccns.forEach(function(part, index, theArray) {theArray[index] = part.trim();});
			$('#myModal').modal('hide');
			$('#myModalLoading').modal('show');
			$.post('/application/libraries/phpmailer/inviaemail.php', {ccns:ccns, allegati:attachs, inputTo:sendTo, inputSubject:$("#inputSubject").val(),inputBody:$("#inputBody").val().replace(/(?:\r\n|\r|\n)/g, '<br />'),inputCc:sendToCc,num_preventivo:$("#num_preventivo").val(),num_revisione:$("#num_revisione").val(),data_offerta_file:$("#data_offerta_file").val(),nome_operatore:$("#nome_operatore").val()},
						function(data) {	
							$('#myModalLoading').modal('hide');
							alert(data);
				},"text"
			);	
});

$(document).on("click",".stampa", function(event){
		$.post('/assets/themes/datatables/js/controllapdf.php', 
			{operazione:1, num_preventivo:$("#num_preventivo").val(),num_revisione:$("#num_revisione").val(),data_offerta_file:$("#data_offerta_file").val()},
				function(exists) {	
					if(exists!="YES")
					{
						$(".stampa").children( ".glyphicon" ).removeClass("glyphicon-print");
					 		$(".stampa").children( ".glyphicon" ).addClass("glyphicon-refresh");
					 		$(".stampa").children( ".glyphicon" ).addClass("glyphicon-refresh-animate");
						$.post('/application/libraries/tcpdf/examples/example_001.php', 
									{id_preventivo:$("#id_preventivo").val(),num_revisione:$("#num_revisione").val(), num_preventivo:$("#num_preventivo").val(),data_offerta_file:$("#data_offerta_file").val()},
										function(data) {	
											$(".stampa").children( ".glyphicon" ).removeClass("glyphicon-refresh-animate");
											$(".stampa").children( ".glyphicon" ).removeClass("glyphicon-refresh");
							 				$(".stampa").children( ".glyphicon" ).addClass("glyphicon-print");
											window.open('ftp:192.168.2.231:2222/ufficio/'+$("#num_preventivo").val()+"-"+$("#num_revisione").val()+"_"+$("#data_offerta_file").val()+".pdf");
										},"text"
										);
					}else{
							window.open('ftp:192.168.2.231:2222/ufficio/'+$("#num_preventivo").val()+"-"+$("#num_revisione").val()+"_"+$("#data_offerta_file").val()+".pdf");
					}
					},"text"
				);
});

$(document).on("click",".creapdf", function(event){
		$(this).children( ".glyphicon" ).removeClass("glyphicon-floppy-save");
		$(this).children( ".glyphicon" ).addClass("glyphicon-refresh");
		$(this).children( ".glyphicon" ).addClass("glyphicon-refresh-animate");
		$.post('/application/libraries/tcpdf/examples/example_001.php', 
			{id_preventivo:$("#id_preventivo").val(),num_revisione:$("#num_revisione").val(), num_preventivo:$("#num_preventivo").val(),data_offerta_file:$("#data_offerta_file").val()},
				function(data) {	
					$(".creapdf").children( ".glyphicon" ).removeClass("glyphicon-refresh-animate");
					$(".creapdf").children( ".glyphicon" ).removeClass("glyphicon-refresh");
					$(".creapdf").children( ".glyphicon" ).addClass("glyphicon-floppy-save");
					alert("PDF Creato!");
				},"text"
				);
});
$(document).on("click","a.eliminaallegato", function(event){
		$(this).parent().html("");
});

$(document).on("click",".elimina_prev", function(event){
	if (confirm("ELIMINARE OFFERTA CORRENTE ?"))
				window.location.replace("/index.php/preventivi/elimina_preventivo/"+$("#id_preventivo").val());
});

$(document).on("click","input.agg_singola_comm", function(event){
		if (confirm("AGGIUNGERE COMMESSA NUMERO "+ $(this).attr('commessa')+" ALL'ATTUALE OFFERTA?"))
				window.location.replace("/index.php/preventivi/aggiungi_comm/"+$("#id_preventivo").val()+"/"+$('#num_revisione').val()+"/"+$(this).attr('commessa'));
});

$(document).on("click","input.elimina_comm", function(event){
		if (confirm("ELIMINARE LA COMMESSA NUMERO "+ $(this).attr('commessa')+" DALL'ATTUALE OFFERTA?"))
				window.location.replace("/index.php/preventivi/elimina_comm/"+$("#id_preventivo").val()+"/"+$('#num_revisione').val()+"/"+$(this).attr('commessa'));
});


$(document).ready(function () {
	
     	$("select[name='id_destinatario']").change(function(){
				$.post('/assets/themes/datatables/js/trova_indirizzo_dest.php', {id_destinatario:$(this).val(), id_preventivo:$("#id_preventivo").val()},
						function(data) {
							$("#indirizzo_dest").html(data);						
						},"text"
						);
		


			});			
});


$(document).ready(function () {
	
     	$("select[name='data_cons_lav_prevista']").change(function(){
				$.post('/assets/themes/datatables/js/cambia_data_cons_lav_prevista.php',
				 {id_preventivo:$("#id_preventivo").val(), data_cons_lav_prevista:$(this).val()},
						function(data) {
						},"text"
						);						

			});	
			
});

$(document).ready(function () {
				$.post('/assets/themes/datatables/js/trova_indirizzo_dest.php', {id_preventivo:$("#id_preventivo").val(),id_destinatario:$("select[name='id_destinatario']").val()},
						function(data) {
							$("#indirizzo_dest").html(data);						
						},"text"
						).fail(function(err) {
    alert( "error"+JSON.stringify(err) );
  });
});

