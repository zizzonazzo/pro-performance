﻿$(function() {
    $( "#dialog" ).dialog({
	    autoOpen: false,
	    maxWidth:600,
	    maxHeight: 500,
	    width: 600,
	    height: 500,
	    modal: true,
	    show: {
		        effect: "blind",
		        duration: 600
		      },
		      hide: {
		        effect: "explode",
		        duration: 600
		      }
		    });

  });

$(document).on("click","li.image-button", function(event){
	event.preventDefault();
	$("#dialog").children('img').attr('src',$(this).children('a').attr("href"));
	$( "#dialog" ).dialog( "open" );
});
