$(document).ready(function(){
	$("*").click(function(){$('#report-success').slideUp(400);});
	$("div.tr_red").parent().css("background-color", "#00CC00");
});

$(document).on("click","a.preventivo_button", function(event){
	if (confirm("Non esiste alcun preventivo per la commessa corrente! \n\nCREARE UN NUOVO PREVENTIVO? "))
				window.location.replace("/index.php/preventivi/preventivo_inizio/"+$(this).attr('commessa'));
});

$(document).on("click","a.ddt_cliente_button", function(event){
	if (confirm("Non esiste alcun DDT CLIENTE! \n\nCREARNE UNO NUOVO? "))
				window.location.replace("/index.php/ddt/ddt_inizio/"+$(this).attr('commessa'));
});

$(document).on("click","a.ddt_fornitore_button", function(event){
	if (confirm("CREARE UN NUOVO DDT FORNITORE??????? "))
				window.location.replace("/index.php/ddt/ddt_fornitore_inizio/"+$(this).attr('commessa'));
});

$(document).on("click",".mostraTendina", function(){
		$(".tendina").slideDown(800);
		$(".glyphicon.glyphicon-hand-down.mostraTendina").removeClass("glyphicon-hand-down");
		$(".glyphicon.mostraTendina").addClass("glyphicon-hand-up");
		$(".glyphicon.mostraTendina.glyphicon-hand-up").removeClass("mostraTendina");
		$(".glyphicon.glyphicon-hand-up").addClass("nascondiTendina");
	});

$(document).on("click",".nascondiTendina", function(event){
		$(".tendina").slideUp(800);
		$(".glyphicon.glyphicon-hand-up.nascondiTendina").removeClass("glyphicon-hand-up");
		$(".glyphicon.nascondiTendina").addClass("glyphicon-hand-down");
		$(".glyphicon.nascondiTendina.glyphicon-hand-down").removeClass("nascondiTendina");
		$(".glyphicon.glyphicon-hand-down").addClass("mostraTendina");
});


//mi servono ancora questi due hover???? dovrebbero essere usati in onchange.js
$(document).on("hover","input.note_produttore_button", function(event){
		$(this).next("div").removeClass("nota_nascosta");
		$(this).next("div").addClass("nota_prod");
		$("div.nota_prod textarea").height($("div.nota_prod textarea")[0].scrollHeight);
});
$(document).on("mouseout","input.note_produttore_button", function(event){
		$(this).next("div").removeClass("nota_prod");
		$(this).next("div").addClass("nota_nascosta");
});
