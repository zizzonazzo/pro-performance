$(document).on("click",".stampa_ordine", function(event){
		$.post('/assets/themes/datatables/js/controllapdf.php',
			{operazione:4, num_ordine:$("#num_ordine").val(),anno_ordine:$("#anno_ordine").val()},
				function(exists) {
					if(exists!="YES"){
						    $(".stampa_ordine").children( ".glyphicon" ).removeClass("glyphicon-print");
							$(".stampa_ordine").children( ".glyphicon" ).addClass("glyphicon-refresh");
							$(".stampa_ordine").children( ".glyphicon" ).addClass("glyphicon-refresh-animate");
							$.post('/application/libraries/tcpdf/examples/example_003.php',
							{id_ordine:$("#id_ordine").val(),num_ordine:$("#num_ordine").val(),anno_ordine:$("#anno_ordine").val()},
								function(data) {
									$(".stampa_ordine").children( ".glyphicon" ).removeClass("glyphicon-refresh-animate");
									$(".stampa_ordine").children( ".glyphicon" ).removeClass("glyphicon-refresh");
									$(".stampa_ordine").children( ".glyphicon" ).addClass("glyphicon-print");
									w = window.open('ftp:192.168.2.231:2222/ordinifornitori/Ordine_'+$("#num_ordine").val()+"_"+$("#anno_ordine").val()+".pdf");
								},"text"
								);
					}else{
						w = window.open('ftp:192.168.2.231:2222/ordinifornitori/Ordine_'+$("#num_ordine").val()+"_"+$("#anno_ordine").val()+".pdf");
					}
					},"text"
				);
});

$(document).on("click",".crea_pdf_ordine", function(event){
			    $(".crea_pdf_ordine").children( ".glyphicon" ).removeClass("glyphicon-floppy-save");
				$(".crea_pdf_ordine").children( ".glyphicon" ).addClass("glyphicon-refresh");
				$(".crea_pdf_ordine").children( ".glyphicon" ).addClass("glyphicon-refresh-animate");
				$.post('/application/libraries/tcpdf/examples/example_003.php',
				{id_ordine:$("#id_ordine").val(),num_ordine:$("#num_ordine").val(),anno_ordine:$("#anno_ordine").val()},
					function(data) {
						$(".crea_pdf_ordine").children( ".glyphicon" ).removeClass("glyphicon-refresh-animate");
						$(".crea_pdf_ordine").children( ".glyphicon" ).removeClass("glyphicon-refresh");
						$(".crea_pdf_ordine").children( ".glyphicon" ).addClass("glyphicon-floppy-save");
					},"text"
					);

});

$(document).on("click",".invia_ordine", function(event){
		  	var sendTo=$("#inputTo").val().split(";");
			sendTo.forEach(function(part, index, theArray) {theArray[index] = part.trim();});
			var sendToCc=$("#inputCc").val().split(";");
			sendToCc.forEach(function(part, index, theArray) {theArray[index] = part.trim();});

			var ccns=$("#inputCcn").val().split(";");
			ccns.forEach(function(part, index, theArray) {theArray[index] = part.trim();});
			$('#myModal').modal('hide');
			$('#myModalLoading').modal('show');
			$.post('/application/libraries/phpmailer/inviaemail_ordine.php', {ccns:ccns, inputTo:sendTo, inputSubject:$("#inputSubject").val(),inputBody:$("#inputBody").val().replace(/(?:\r\n|\r|\n)/g, '<br />'),inputCc:sendToCc,num_ordine:$("#num_ordine").val(), anno_ordine:$("#anno_ordine").val(),nome_operatore:$("#nome_operatore").val()},
						function(data) {
							$('#myModalLoading').modal('hide');
							alert(data);
				},"text"
			);
});
