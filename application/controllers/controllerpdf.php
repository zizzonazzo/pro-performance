<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/////////////////////////////////////////////////////////////////count($ci->uri->segments)

class Controllerpdf extends CI_Controller {

	function __construct()
	{
		parent::__construct();

		/* Standard Libraries */
		$this->load->database();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('date');
		$this->load->helper('security');
		$this->load->library('session');


		/* ------------------ */

		$this->load->library('grocery_CRUD');
		// if(!$this->session->userdata('logged_in'))
		// 	redirect('/login/show', 'refresh');


	}

	function header_pdf_preventivo(){

		date_default_timezone_set('Europe/Rome');
		$output= new stdClass();


		$query = $this->db->get_where('preventivi',array('id_preventivo'=>$this->uri->segment(3)));
		foreach ($query->result() as $preventivo);
		$query = $this->db->get_where('preventivi_commesse',array('id_preventivo'=>$this->uri->segment(3),'num_revisione'=>$this->uri->segment(4)));
		foreach ($query->result() as $preventivo_commessa);
		$query = $this->db->get_where('clienti',array('id_cliente'=>$preventivo->id_cliente));
		foreach ($query->result() as $cliente);


		//HEADER
		$output->riferimenti_cliente=$preventivo->riferimenti_prev;//da leggere da preventivi
		$output->num_revisione=$preventivo_commessa->num_revisione;//da leggere da preventivi
		$output->id_cliente_prev=$preventivo->id_cliente;
		$output->nome_cliente=$cliente->nome_cliente_documenti;
		$output->indirizzo_cliente=$cliente->indirizzo;
		$output->citta_cliente=$cliente->citta;
		$output->nazione_cliente=$cliente->nazione;
		$query = $this->db->get_where('clienti',array('id_cliente'=>$preventivo->id_dest));
		foreach ($query->result() as $destinatario);
		$output->nome_destinatario=$destinatario->nome_cliente_documenti;
		$output->indirizzo_dest=$destinatario->indirizzo;
		$output->citta_dest=$destinatario->citta;
		$output->nazione_dest=$destinatario->nazione;
		$output->num_preventivo=$preventivo->num_preventivo;//da leggere da preventivo
		@list($year,$month,$day) = explode("-",$preventivo->data_creazione_prev);
		$output->data_offerta=date ("d-m-Y",mktime (0,0,0,(int)$month , (int)$day , (int)$year));

		$output->id_preventivo=$preventivo->id_preventivo;

		if($cliente->lingua=='ITA'){
		$this->load->view('/views_pdf/header_stampa_pdf_preventivi.php',$output);
		}
		if($cliente->lingua=='ENG'){
			$this->load->view('/views_pdf/header_stampa_pdf_eng_preventivi.php',$output);
		}
		if($cliente->lingua=='FRA'){
			$this->load->view('/views_pdf/header_stampa_pdf_fra_preventivi.php',$output);
		}

	}



	function corpo_pdf_preventivo(){

		date_default_timezone_set('Europe/Rome');
		$output= new stdClass();


		$query = $this->db->get_where('preventivi',array('id_preventivo'=>$this->uri->segment(3)));
		foreach ($query->result() as $preventivo);
		$query = $this->db->get_where('preventivi_commesse',array('id_preventivo'=>$this->uri->segment(3),'num_revisione'=>$this->uri->segment(4)));
		foreach ($query->result() as $preventivo_commessa);
		$query = $this->db->get_where('clienti',array('id_cliente'=>$preventivo->id_cliente));
		foreach ($query->result() as $cliente);


		//HEADER
		$output->riferimenti_cliente=$preventivo->riferimenti_prev;//da leggere da preventivi
		$output->num_revisione=$preventivo_commessa->num_revisione;//da leggere da preventivi
		$output->id_cliente_prev=$preventivo->id_cliente;
		$output->nome_cliente=$cliente->nome_cliente_documenti;
		$output->indirizzo_cliente=$cliente->indirizzo;
		$output->citta_cliente=$cliente->citta;
		$output->nazione_cliente=$cliente->nazione;
		$query = $this->db->get_where('clienti',array('id_cliente'=>$preventivo->id_dest));
		foreach ($query->result() as $destinatario);
		$output->nome_destinatario=$destinatario->nome_cliente_documenti;
		$output->indirizzo_dest=$destinatario->indirizzo;
		$output->citta_dest=$destinatario->citta;
		$output->nazione_dest=$destinatario->nazione;
		$output->num_preventivo=$preventivo->num_preventivo;//da leggere da preventivo
		@list($year,$month,$day) = explode("-",$preventivo->data_creazione_prev);
		$output->data_offerta=date ("d-m-Y",mktime (0,0,0,(int)$month , (int)$day , (int)$year));

		$output->id_preventivo=$preventivo->id_preventivo;


		if($cliente->lingua=='ITA'){
			$nsriferimento="Ns. Riferimento";
			}
			if($cliente->lingua=='ENG'){
			$nsriferimento="Our Reference n";
			}
			if($cliente->lingua=='FRA'){
			$nsriferimento="Notre Réf. n";
			}

		$query = $this->db->get_where('preventivi_commesse',array('id_preventivo'=>$preventivo->id_preventivo,'num_revisione'=>$this->uri->segment(4)));
		$output->tabella_descrizione="";

		foreach ($query->result() as $preventivocomm){


			$query = $this->db->get_where('lavori',array('commessa'=>$preventivocomm->commessa));
			foreach ($query->result() as $commessa);
			$prendi_righe = $this->db->get_where('righe_preventivi',array('commessa'=>$preventivocomm->commessa,
																	 'id_preventivo'=>$preventivo->id_preventivo,
																	 'num_revisione'=>$this->uri->segment(4)));

				if($prendi_righe->num_rows()>0){
					$output->tabella_descrizione.='<tr><td> &nbsp; </td>
					<td >&nbsp;</td><td >&nbsp;</td><td>&nbsp;</td></tr>';
					if($commessa->comm_cliente!=NULL){$comm_cliente=' - Vs. Commessa: '.$commessa->comm_cliente;}
					else{$comm_cliente='';}
					if($commessa->tipo_lavoro!='STA'){
						if($commessa->n_stampo!=NULL){
							if($cliente->lingua=='ITA'){
								$n_stampo=' - N° Disegno: <b>'.$commessa->n_stampo.'</b>';
								}
							if($cliente->lingua=='ENG'){
								$n_stampo=' - Drawing n° <b>'.$commessa->n_stampo.'</b>';
								}
							if($cliente->lingua=='FRA'){
								$n_stampo=' - Dessin n° <b>'.$commessa->n_stampo.'</b>';
								}
						}
						else{$n_stampo='';}
					}else{
						$n_stampo='';
					}

					$output->tabella_descrizione.='<tr><td width="64%">'.$nsriferimento.': <b>'.$commessa->commessa.'</b>'.$comm_cliente.'</td>
							<td width="4%"></td><td width="4%"></td>
							<td width="12%"></td><td width="6%" align="right"></td>
						<td width="10%" align="right"></td></tr>';
					if($commessa->tipo_lavoro!='STA'){
						$output->tabella_descrizione.='<tr><td width="80%">'.$preventivocomm->stampo_nome_peso.'</td>
							<td  width="10%"></td><td  width="10%"></td>
							<td></td></tr>';
					}
					foreach ($prendi_righe->result() as $riga)
					{
						$query = $this->db->get_where('classi_iva',array('id_classe_iva'=>$riga->id_classe_iva));
						foreach ($query->result() as $classe);
						if($riga->sconto=='0') $sconto='';
						else $sconto=$riga->sconto."%";
						$output->tabella_descrizione.='<tr>
						<td width="64%">'.$riga->descrizione.'</td>
						<td width="4%">Nr</td>
						<td width="4%">'.$riga->quantita.'</td>
						<td width="12%" align="right">'.number_format($riga->prezzo,2).' &#8364; &nbsp; </td>
						<td width="6%" align="right">'.$sconto.'&nbsp;</td>
						<td width="10%" align="right">'.number_format(($riga->prezzo*(1-$riga->sconto/100))*$riga->quantita, 2).' &#8364; &nbsp; </td>
					 	</tr>';
					}
				}
				$output->tabella_descrizione.='<tr>
						<td style="border-bottom:1px solid black;"></td>
						<td style="border-bottom:1px solid black;"></td>
						<td style="border-bottom:1px solid black;"></td>
						<td style="border-bottom:1px solid black;">  &nbsp; </td>
						<td style="border-bottom:1px solid black;"></td>
						<td style="border-bottom:1px solid black;"></td>
					 	</tr>';
		}


		if($cliente->lingua=='ITA'){
		$this->load->view('/views_pdf/corpo_stampa_pdf_preventivi.php',$output);
		}
		if($cliente->lingua=='ENG'){
			$this->load->view('/views_pdf/corpo_stampa_pdf_eng_preventivi.php',$output);
		}
		if($cliente->lingua=='FRA'){
			$this->load->view('/views_pdf/corpo_stampa_pdf_fra_preventivi.php',$output);
		}


	}

function corpo_pdf_ordine(){

		date_default_timezone_set('Europe/Rome');
		$output= new stdClass();
		$output->stampa_tabella_accessori=false;
		$output->stampa_tabella_materiali=false;
		$output->stampa_tabella_semilavorati=false;

		$query = $this->db->get_where('ordini_fornitori',array('id_ordine_fornitore'=>$this->uri->segment(3)));
		foreach ($query->result() as $ordine);
		$query = $this->db->get_where('fornitori', array('id_fornitore'=>$ordine->id_fornitore));
		foreach ($query->result() as $fornitore);



		//insert nuovo ordine
		$output->id_ordine_fornitore=$ordine->id_ordine_fornitore;


		$output->id_fornitore=$fornitore->id_fornitore;
		$output->nome_fornitore=$fornitore->nome_fornitore;
		$output->citta_fornitore=$fornitore->citta_fornitore;
		$output->indirizzo_fornitore=$fornitore->indirizzo_fornitore;
		$output->tabella_accessori="";
		$query = $this->db->get_where('ordini_accessori',array('id_ordine_fornitore'=>$ordine->id_ordine_fornitore));
		foreach ($query->result() as $accessori_ordine){
			$output->stampa_tabella_accessori=true;
			$query = $this->db->get_where('anagrafica_accessori',array('id_accessorio'=>$accessori_ordine->id_accessorio));
			foreach ($query->result() as $accessorio_amico);
			@list($year,$month,$day) = explode("-",$accessori_ordine->data_consegna_richiesta);
			$accessori_ordine->data_consegna_richiesta=date("d-m-Y", mktime(0,0,0,$month,$day,$year));
			$output->tabella_accessori.="<tr><td align='center'>".$accessorio_amico->codice_accessorio_fornitore."</td><td align='center'>".$accessori_ordine->ns_riferimento."</td>
				<td align='center'>".$accessorio_amico->nome_accessorio."</td>
				<td align='center'>NR</td><td align='center'>".$accessori_ordine->quantita_ordinata."</td>
				<td align='center'>".$accessori_ordine->data_consegna_richiesta."</td></tr>";
		}
		//tabella materiali----------------------------------------------------------------------------------------------------------------------------
		$output->tabella_materiali="";
		$query = $this->db->get_where('ordini_materiali',array('id_ordine_fornitore'=>$ordine->id_ordine_fornitore));
		foreach ($query->result() as $materiali_ordine){
			$output->stampa_tabella_materiali=true;
			$query = $this->db->get_where('anagrafica_materiali',array('codice_materiale_interno'=>$materiali_ordine->codice_materiale_interno));
			foreach ($query->result() as $materiale);
			$query = $this->db->get_where('magazzino_materiali',array('codice_materiale_interno'=>$materiali_ordine->codice_materiale_interno));
			foreach ($query->result() as $materiale_magazzino);
			$query = $this->db->get_where('materiali',array('id_materiale'=>$materiale->id_materiale));
			foreach ($query->result() as $materiale_original);
			if($materiale_magazzino->sezione_materiale!="RITAGLIO"){
				$misure=$materiale_magazzino->sezione_materiale."x".$materiali_ordine->lunghezza_ordinata;
			}else{
				$misure=$materiali_ordine->misure_ritaglio_ordinato;
			}
			@list($year,$month,$day) = explode("-",$materiali_ordine->data_consegna_richiesta);
			$materiali_ordine->data_consegna_richiesta=date("d-m-Y", mktime(0,0,0,$month,$day,$year));

			$output->tabella_materiali.="<tr><td align='center'>".$materiale->codice_materiale_fornitore."</td><td align='center'>".$materiali_ordine->ns_riferimento."</td><td align='center'>".$materiale_original->nome_materiale.
			"</td><td align='center'>".$materiale_magazzino->forma_materiale.
			"</td><td align='center'>".$misure." mm</td><td>NR</td><td align='center'>".$materiali_ordine->quantita_ordinata."</td><td>".$materiali_ordine->data_consegna_richiesta."</td></tr>";
		}
		//tabella semilavorati------------------------------------------------------------------------------------------------------------------
		$output->tabella_semilavorati="";
		$query = $this->db->get_where('ordini_semilavorati',array('id_ordine_fornitore'=>$ordine->id_ordine_fornitore));
		foreach ($query->result() as $semilavorato_ordine){
			$output->stampa_tabella_semilavorati=true;
			$query = $this->db->get_where('anagrafica_semilavorati',array('id_semilav_ana'=>$semilavorato_ordine->id_semilav_ana));
			foreach ($query->result() as $semilavorato_amico);
			@list($year,$month,$day) = explode("-",$semilavorato_ordine->data_consegna_richiesta);
			$semilavorato_ordine->data_consegna_richiesta=date("d-m-Y", mktime(0,0,0,$month,$day,$year));
			$output->tabella_semilavorati.="<tr><td align='center'>".$semilavorato_ordine->commessa.
			"</td><td align='center'>".$semilavorato_amico->nome_semilavorato."</td>
				<td align='center'>".$semilavorato_ordine->disegno."</td>
				<td align='center'>NR</td><td align='center'>".$semilavorato_ordine->quantita_semilav."</td>
				<td align='center'>".$semilavorato_ordine->data_consegna_richiesta."</td></tr>";
		}

		$output->riferimenti_ordine=$ordine->riferimenti_ordine;


		$output->num_ordine_fornitore=$ordine->n_ordine;
		@list($year,$month,$day) = explode("-",$ordine->data_creazione_ordine);
		$output->data_odierna=date("d/m/Y", mktime(0,0,0,$month,$day,$year));
		$this->load->view('/views_pdf/corpo_stampa_pdf_ordini.php',$output);
	}

function header_pdf_ordine(){

		date_default_timezone_set('Europe/Rome');
		$output= new stdClass();

		$query = $this->db->get_where('ordini_fornitori',array('id_ordine_fornitore'=>$this->uri->segment(3)));
		foreach ($query->result() as $ordine);
		$query = $this->db->get_where('fornitori', array('id_fornitore'=>$ordine->id_fornitore));
		foreach ($query->result() as $fornitore);



		$output->id_ordine_fornitore=$ordine->id_ordine_fornitore;


		$output->id_fornitore=$fornitore->id_fornitore;
		$output->nome_fornitore=$fornitore->nome_fornitore;
		$output->citta_fornitore=$fornitore->citta_fornitore;
		$output->indirizzo_fornitore=$fornitore->indirizzo_fornitore;

		$output->riferimenti_ordine=$ordine->riferimenti_ordine;


		$output->num_ordine_fornitore=$ordine->n_ordine;
		@list($year,$month,$day) = explode("-",$ordine->data_creazione_ordine);
		$output->data_odierna=date("d/m/Y", mktime(0,0,0,$month,$day,$year));
		$this->load->view('/views_pdf/header_stampa_pdf_ordini.php',$output);
	}

function footer_pdf_ordine(){
	$output= new stdClass();

	$query = $this->db->get_where('ordini_fornitori',array('id_ordine_fornitore'=>$this->uri->segment(3)));
	foreach ($query->result() as $ordine);
	$output->note_ordine=$ordine->note_ordine;

	$this->load->view('/views_pdf/footer_stampa_pdf_ordini.php',$output);

}


function corpo_pdf_ddt(){

		date_default_timezone_set('Europe/Rome');


		$query = $this->db->get_where('tabella_ddt',array('id_ddt'=>$this->uri->segment(3)));
		foreach ($query->result() as $mainDdt);
		$output = new stdClass();
		@list($year,$month,$day) = explode("-",$mainDdt->data_creazione_ddt);

		$output->anno_ddt=$year;


		if($mainDdt->tipo_ddt=='C'){
				$query = $this->db->get_where('clienti',array('id_cliente'=>$mainDdt->id_cliente));
				foreach ($query->result() as $cliente);

				//HEADER

				if($cliente->partita_iva_cliente!=NULL)
					$p_iva=$cliente->partita_iva_cliente;
				else
					$p_iva="";
				$output->partita_iva_cliente=$p_iva;

				$output->luogo_destinazione=$mainDdt->luogo_destinazione;//da leggere da preventivi
				$output->id_cliente=$cliente->id_cliente;
				$output->nome_cliente=$cliente->nome_cliente_documenti;
				$output->indirizzo_cliente=$cliente->indirizzo;
				$output->citta_cliente=$cliente->citta;
				$output->nazione_cliente=$cliente->nazione;
		}else{
			$query = $this->db->get_where('fornitori',array('id_fornitore'=>$mainDdt->id_fornitore));
			foreach ($query->result() as $fornitore);
			$output->nome_fornitore=$fornitore->nome_fornitore;
			$output->indirizzo_fornitore=$fornitore->indirizzo_fornitore;
			$output->citta_fornitore=$fornitore->citta_fornitore;
			if($fornitore->partita_iva_fornitore!=NULL)
					$p_iva=$fornitore->partita_iva_fornitore;
			else
					$p_iva="";
			$output->partita_iva_fornitore=$p_iva;
		}
		$output->num_ddt=$mainDdt->num_ddt;//da leggere da preventivo
		@list($year,$month,$day) = explode("-",$mainDdt->data_creazione_ddt);
		$output->data_ddt=date ("d-m-Y",mktime (0,0,0,(int)$month , (int)$day , (int)$year));

		$output->id_ddt=$mainDdt->id_ddt;


		$output->tabella_descrizione="";
		$this->db->order_by('commessa');
		$query = $this->db->get_where('ddt_commesse',array('id_ddt'=>$mainDdt->id_ddt));

		foreach ($query->result() as $commessa_ddt) {
				$query = $this->db->get_where('lavori',array('commessa'=>$commessa_ddt->commessa));
					foreach ($query->result() as $commessa);

					if(($commessa->comm_cliente!=NULL)&&($mainDdt->tipo_ddt=='C')){
						if($cliente->lingua=='ITA'){
							$vsriferimento="Vs. Riferimento";
						}
						if($cliente->lingua=='ENG'){
							$vsriferimento="Your Reference";
						}
						if($cliente->lingua=='FRA'){
							$vsriferimento="Votre Réf";
						}
						$comm_cliente=" - ".$vsriferimento.": <strong>".$commessa->comm_cliente."</strong>";}
					else{$comm_cliente='';}

					if($commessa->n_stampo!=NULL){
						if($mainDdt->tipo_ddt=='C'){
							if($cliente->lingua=='ITA'){
								$nome_n_stampo='N° Disegno';
								}
							if($cliente->lingua=='ENG'){
								$nome_n_stampo='Drawing n°';
								}
							if($cliente->lingua=='FRA'){
								$nome_n_stampo='Dessin n°';
								}
						}else{$nome_n_stampo='N° Disegno';}

						$n_stampo=" - ".$nome_n_stampo.": <strong>".$commessa->n_stampo."</strong>";}
					else{$n_stampo='';}


				if(($commessa->ordine_cliente!=NULL)&&($mainDdt->tipo_ddt=='C')){
					if($cliente->lingua=='ITA'){
							$ordine="Vs. Ordine";
							$del="del ";
						}
						if($cliente->lingua=='ENG'){
							$ordine="Your PO";
							$del="of";
						}
						if($cliente->lingua=='FRA'){
							$ordine="Votre Commande";
							$del="du ";
						}
						if($commessa->data_ordine_cliente!=NULL){
							@list($year,$month,$day) = explode("-",$commessa->data_ordine_cliente);
							$data_ordine_cliente=date ("d-m-Y",mktime (0,0,0,(int)$month , (int)$day , (int)$year));
						}else{$data_ordine_cliente="";}
						if(substr($commessa->ordine_cliente, 0, 4)=="mail") $ordine_nome="mail";
								else $ordine_nome=$commessa->ordine_cliente;

					$vs_ordine=$ordine.": <strong>".$ordine_nome."</strong> ".$del.$data_ordine_cliente;
				}else{ $vs_ordine="";}
				$output->tabella_descrizione.="
						<tr>
						<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
						</tr>
						<tr>
						<td width='80%'>Ns. Riferimento: <strong>".$commessa->commessa."</strong>
						</td>
						<td width='10%'></td><td width='10%'></td>
						</tr>";
				if($mainDdt->tipo_ddt=='C'){
						$output->tabella_descrizione.='
						<tr>
						<td width="80%">'.$vs_ordine.$comm_cliente.'</td>
						<td width="10%"></td><td width="10%"></td>
						</tr>';
						$output->tabella_descrizione.='
						<tr>
						<td  width="80%"><strong>'.$commessa_ddt->stampo_nome_peso.'</strong></td>
						<td  width="10%"></td><td  width="10%"></td>
						</tr>';
				}
				$query = $this->db->get_where('righe_ddt',array('id_ddt'=>$mainDdt->id_ddt,'commessa'=>$commessa_ddt->commessa));

				foreach ($query->result() as $riga_ddt){
							if($mainDdt->tipo_ddt=='C'){
									$output->tabella_descrizione.='
										<tr>
											<td  style="padding:5px;" width="80%">'.$riga_ddt->descrizione.'</td>
											<td style="text-align:center;" width="10%">Nr</td>
											<td width="10%">'.$riga_ddt->quantita.'</td>
										</tr>
									';
								}else{
									$query = $this->db->get_where('tipi_lav_esterne',array('id_tipo_lav'=>$riga_ddt->id_tipo_lav));
									foreach ($query->result() as $lavorazione);
									$output->tabella_descrizione.='
										<tr>
											<td  style="padding:5px;"  width="60%">'.$riga_ddt->descrizione.'</td>
											<td style="text-align:left;" width="25%">'.$lavorazione->nome_lav_esterna.'</td>
											<td style="text-align:center;" width="5%">Nr</td>
											<td width="10%">'.$riga_ddt->quantita.'</td>
										</tr>
									';
								}


				}

		}
		if($mainDdt->tipo_ddt=='C'){
			if($cliente->lingua=='ITA'){
			$this->load->view('/views_pdf/corpo_stampa_pdf_ddt.php',$output);
			}
			if($cliente->lingua=='ENG'){
				$this->load->view('/views_pdf/corpo_stampa_pdf_ddt.php',$output);
			}
			if($cliente->lingua=='FRA'){
				$this->load->view('/views_pdf/corpo_stampa_pdf_ddt.php',$output);
			}
		}else{
			$this->load->view('/views_pdf/corpo_stampa_pdf_ddt_fornitore.php',$output);
		}

	}

	function header_pdf_ddt(){

		date_default_timezone_set('Europe/Rome');


		$query = $this->db->get_where('tabella_ddt',array('id_ddt'=>$this->uri->segment(3)));
		foreach ($query->result() as $mainDdt);
		$output = new stdClass();
		@list($year,$month,$day) = explode("-",$mainDdt->data_creazione_ddt);

		$output->anno_ddt=$year;


		if($mainDdt->tipo_ddt=='C'){
				$query = $this->db->get_where('clienti',array('id_cliente'=>$mainDdt->id_cliente));
				foreach ($query->result() as $cliente);

				//HEADER

				if($cliente->partita_iva_cliente!=NULL)
					$p_iva=$cliente->partita_iva_cliente;
				else
					$p_iva="";
				$output->partita_iva_cliente=$p_iva;

				$output->luogo_destinazione=$mainDdt->luogo_destinazione;//da leggere da preventivi
				$output->id_cliente=$cliente->id_cliente;
				$output->nome_cliente=$cliente->nome_cliente_documenti;
				$output->indirizzo_cliente=$cliente->indirizzo;
				$output->citta_cliente=$cliente->citta;
				$output->nazione_cliente=$cliente->nazione;
		}else{
			$query = $this->db->get_where('fornitori',array('id_fornitore'=>$mainDdt->id_fornitore));
			foreach ($query->result() as $fornitore);
			$output->nome_fornitore=$fornitore->nome_fornitore;
			$output->indirizzo_fornitore=$fornitore->indirizzo_fornitore;
			$output->citta_fornitore=$fornitore->citta_fornitore;
			if($fornitore->partita_iva_fornitore!=NULL)
					$p_iva=$fornitore->partita_iva_fornitore;
			else
					$p_iva="";
			$output->partita_iva_fornitore=$p_iva;
		}
		$output->num_ddt=$mainDdt->num_ddt;//da leggere da preventivo
		@list($year,$month,$day) = explode("-",$mainDdt->data_creazione_ddt);
		$output->data_ddt=date ("d-m-Y",mktime (0,0,0,(int)$month , (int)$day , (int)$year));

		$output->id_ddt=$mainDdt->id_ddt;

		if($mainDdt->tipo_ddt=='C'){
			if($cliente->lingua=='ITA'){
			$this->load->view('/views_pdf/header_stampa_pdf_ddt.php',$output);
			}
			if($cliente->lingua=='ENG'){
				$this->load->view('/views_pdf/header_stampa_pdf_ddt.php',$output);
			}
			if($cliente->lingua=='FRA'){
				$this->load->view('/views_pdf/header_stampa_pdf_ddt.php',$output);
			}
		}else{
			$this->load->view('/views_pdf/header_stampa_pdf_ddt_fornitori.php',$output);
		}
	}

	function footer_pdf_ddt(){
		$output= new stdClass();
		$query = $this->db->get_where('tabella_ddt',array('id_ddt'=>$this->uri->segment(3)));
		foreach ($query->result() as $mainDdt);

		$query = $this->db->get_where('tipo_trasporto',array("id_tipo_trasp"=>$mainDdt->id_tipo_trasp));
		foreach ($query->result() as $tipo_trasp);
		$output->tipo_trasporto=$tipo_trasp->nome_tipo_trasp;

		$query = $this->db->get_where('vettori',array("id_vettore"=>$mainDdt->id_vettore));
		foreach ($query->result() as $vect);
		$output->vettori=$vect->nome_vettore."<br><br>".$vect->dati_vettore;

		$output->aspetto_beni=$mainDdt->id_aspetto_beni;

		$query = $this->db->get_where('causali_trasporto',array("id_causale_trasp"=>$mainDdt->id_causale_trasp));
		foreach ($query->result() as $causale_trasp);
		$output->causale_trasporto=$causale_trasp->nome_causale;

		$query = $this->db->get_where('tipo_porto',array("id_tipo_porto"=>$mainDdt->id_tipo_porto));
		foreach ($query->result() as $tipo_port);
		$output->tipo_porto=$tipo_port->nome_tipo_porto;

		$output->n_colli=$mainDdt->n_colli;
		$output->peso_netto=$mainDdt->peso_netto." kg";
		$output->peso_lordo=$mainDdt->peso_lordo." kg";
		if($mainDdt->dimensioni!="")
			$output->dimensioni=$mainDdt->dimensioni." cm";
		else
			$output->dimensioni=$mainDdt->dimensioni;

		$this->load->view('/views_pdf/footer_stampa_pdf_ddt.php',$output);

	}


	function footer_pdf_preventivo(){
				//FOOTER
		$output= new stdClass();

		$query = $this->db->get_where('preventivi',array('id_preventivo'=>$this->uri->segment(3)));
		foreach ($query->result() as $preventivo);
		$query = $this->db->get_where('preventivi_commesse',array('id_preventivo'=>$this->uri->segment(3),'num_revisione'=>$this->uri->segment(4)));
		foreach ($query->result() as $preventivo_commessa);
		$query = $this->db->get_where('clienti',array('id_cliente'=>$preventivo->id_cliente));
		foreach ($query->result() as $cliente);
		$imponibile_totale=0;
		$imposte_tot=0;
		$costo_trasporto=0;
		$prezzo_a_pagare=0;
		$query = $this->db->get_where('preventivi_commesse',array('id_preventivo'=>$preventivo->id_preventivo,'num_revisione'=>$this->uri->segment(4)));
		foreach ($query->result() as $preventivocomm){


			$query = $this->db->get_where('lavori',array('commessa'=>$preventivocomm->commessa));
			foreach ($query->result() as $commessa);
			$prendi_righe = $this->db->get_where('righe_preventivi',array('commessa'=>$preventivocomm->commessa,
																	 'id_preventivo'=>$preventivo->id_preventivo,
																	 'num_revisione'=>$this->uri->segment(4)));

				if($prendi_righe->num_rows()>0){
					foreach ($prendi_righe->result() as $riga)
					{
						$query = $this->db->get_where('classi_iva',array('id_classe_iva'=>$riga->id_classe_iva));
						foreach ($query->result() as $classe);
						$prezzo_a_pagare+=($riga->prezzo*(1-$riga->sconto/100))*(1+($classe->valore_classe/100))*$riga->quantita;
						$imponibile_totale+=($riga->prezzo*(1-$riga->sconto/100))*$riga->quantita;
						$imposte_tot+=($riga->prezzo*(1-$riga->sconto/100))*($classe->valore_classe/100)*$riga->quantita;

					}
				}
		}

		$query = $this->db->get_where('banca_appoggio_cliente',array('id_banca_cliente'=>$cliente->id_banca_cliente));
		if($query->num_rows() > 0){
			foreach ($query->result() as $banca);
			$output->banca_appoggio=$banca->nome_banca_cliente;

		}else{
			$output->banca_appoggio="";

		}
		$query = $this->db->get_where('condizioni_di_pagamento',array('id_condiz_pagam'=>$cliente->id_condiz_pagam));
		if($query->num_rows() > 0){
			foreach ($query->result() as $cond);
			$output->condizione_pagamento=$cond->nome_condiz_pagam;

		}else{
			$output->condizione_pagamento="";
		}
		if($preventivo->id_tipo_trasp!=NULL){
			$query = $this->db->get_where('tipo_trasporto',array('id_tipo_trasp'=>$preventivo->id_tipo_trasp));
			foreach ($query->result() as $tipo);
			$output->tipo_trasporto=$tipo->nome_tipo_trasp;

		}else{
			$output->tipo_trasporto="";
		}
		if($preventivo->id_tipo_porto!=NULL){
			$query = $this->db->get_where('tipo_porto',array('id_tipo_porto'=>$preventivo->id_tipo_porto));
			foreach ($query->result() as $tipo);
			$output->tipo_porto=$tipo->nome_tipo_porto;

		}else{
			$output->tipo_porto="";
		}
		if($preventivo->data_cons_lav_prevista>1){
			if($cliente->lingua=='ITA'){
			$dataconsegna=$preventivo->data_cons_lav_prevista." settimane";
			}
			if($cliente->lingua=='ENG'){
			$dataconsegna=$preventivo->data_cons_lav_prevista." weeks";
			}
			if($cliente->lingua=='FRA'){
			$dataconsegna=$preventivo->data_cons_lav_prevista." semaines";
			}
		}
		else{
			if($cliente->lingua=='ITA'){
			$dataconsegna=$preventivo->data_cons_lav_prevista." settimana";
			}
			if($cliente->lingua=='ENG'){
			$dataconsegna=$preventivo->data_cons_lav_prevista." week";
			}
			if($cliente->lingua=='FRA'){
			$dataconsegna=$preventivo->data_cons_lav_prevista." semaine";
			}
		}


		$output->data_cons_lav_prevista=$dataconsegna;
		$output->sconto="0";
		$output->costo_trasp=$preventivo->costo_trasp;
		$output->costo_trasp1=$preventivo->costo_trasp1;

		if($preventivo->costo_trasp1_attivo=='N'){
			$output->attiva_secondo_costo_trasp=0;
			$output->secondo_costo_tr_class='disabled';
		}else{
			$output->attiva_secondo_costo_trasp=1;
			$output->secondo_costo_tr_class='enabled';

		}

		$query = $this->db->get_where('classi_iva',array('id_classe_iva'=>$preventivo->id_classe_iva_cost_trasp));
		foreach ($query->result() as $classe_costo_trasp);
		$output->imposte=$classe_costo_trasp->nome_classe_iva;
		$output->costo_trasp_classe_iva=$classe_costo_trasp->valore_classe;

		$costo_trasp_totale=$preventivo->costo_trasp*(1+($classe_costo_trasp->valore_classe/100));
		if($preventivo->costo_trasp1_attivo=='S'){
			$query = $this->db->get_where('classi_iva',array('id_classe_iva'=>$preventivo->id_classe_iva_cost_trasp1));
			foreach ($query->result() as $classe_costo_trasp1);
			$imposte1=$classe_costo_trasp1->nome_classe_iva;
			$output->costo_trasp_classe_iva1=$classe_costo_trasp1->valore_classe;
			$costo_trasp_totale+=$preventivo->costo_trasp1*(1+($classe_costo_trasp1->valore_classe/100));
			$imposte_tot+=($preventivo->costo_trasp*($classe_costo_trasp->valore_classe/100))+($preventivo->costo_trasp1*($classe_costo1_trasp->valore_classe/100));
			$output->imposte1= "<strong>Costo trasporto:</strong> ".$preventivo->costo_trasp1." &#8364; - <small> ".$imposte1."</small>";
		}else{
			$output->imposte1="&nbsp;";
			$output->costo_trasp_classe_iva1='0';
			$imposte_tot+=$preventivo->costo_trasp*($classe_costo_trasp->valore_classe/100);
		}

		$output->prezzo_a_pagare=number_format( ($prezzo_a_pagare+$costo_trasp_totale), 2 );
		$output->imponibile_totale=number_format( $imponibile_totale, 2 );

		$output->imposte_tot=number_format( ($imposte_tot), 2 );


		if($cliente->lingua=='ITA'){
			$this->load->view('/views_pdf/footer_stampa_pdf_preventivi.php',$output);
		}
		if($cliente->lingua=='ENG'){
			$this->load->view('/views_pdf/footer_stampa_pdf_eng_preventivi.php',$output);
		}
		if($cliente->lingua=='FRA'){
			$this->load->view('/views_pdf/footer_stampa_pdf_fra_preventivi.php',$output);
		}
	}






}
