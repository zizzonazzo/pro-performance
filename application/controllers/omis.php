<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/////////////////////////////////////////////////////////////////count($ci->uri->segments)

class Omis extends CI_Controller {



	function __construct()
	{
		parent::__construct();

		/* Standard Libraries */
		$this->load->database();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('date');
		$this->load->helper('security');
		$this->load->library('session');
		$this->n_componente=0;

		/* ------------------ */

		$this->load->library('grocery_CRUD');
		if(!$this->session->userdata('logged_in'))
			redirect('/login/show', 'refresh');


	}


	function _example_output($output = null)
	{

		$output->menu=$this->build_menu();
		$this->load->view('omis_view.php',$output);
	}


	function intestazioneDettaglio()
	{
		date_default_timezone_set('Europe/Rome');

		$where = "commessa = ".$this->uri->segment(4);
		//$this->db->select('tipo_lavoro');
		$que = $this->db->get_where('lavori',$where);
		foreach ($que->result() as $row);
		switch($row->stato){
			case "D":
					$stato_url="progetti";
			break;
			case "E":
					$stato_url="eseguiti";
			break;
			case "C":
					$stato_url="incorso";
			break;
			case "S":
					$stato_url="sospesi";
			break;
			case "EV":
					$stato_url="vecchi";
			break;

		}

		$query = $this->db->get_where('clienti',array('id_cliente'=>$row->cliente));
		if($query->num_rows() > 0)
			foreach ($query->result() as $cliente);
	 	else{
	 		$cliente->ragione_soc="";
			$cliente->note_cliente="";
		}
		$query = $this->db->get_where('produttori',array('id_produttore'=>$row->id_produttore));
		foreach ($query->result() as $produttore);

		$tra = $this->db->get_where('trattamenti',array('id_trattamento'=>$row->id_trattam_matrice));
		$tratt_mat = new stdClass();
		if($tra->num_rows() > 0)
			foreach ($tra->result() as $tratt_mat);
		else $tratt_mat->nome_trattamento="&nbsp;";
		$tratt_piastre = new stdClass();
		$tra = $this->db->get_where('trattamenti',array('id_trattamento'=>$row->id_trattam_piastre));
		if($tra->num_rows() > 0)
			foreach ($tra->result() as $tratt_piastre);
		else $tratt_piastre->nome_trattamento="&nbsp;";
		$materiale_piastre = new stdClass();
		$que = $this->db->get_where('materiali',array('id_materiale'=>$row->id_materiale_piastre));
		if($que->num_rows() > 0)
			foreach ($que->result() as $materiale_piastre);
		else $materiale_piastre->nome_materiale="&nbsp;";
		$materiale_matrice = new stdClass();
		$que = $this->db->get_where('materiali',array('id_materiale'=>$row->id_materiale_matrice));
		if($que->num_rows() > 0)
			foreach ($que->result() as $materiale_matrice);
		else $materiale_matrice->nome_materiale="&nbsp;";
		//giro le date
		if($row->data_approv_dis!=NULL){
			@list($year,$month,$day) = explode("-",$row->data_approv_dis);
			$row->data_approv_dis = date ("d-m-Y",mktime (0,0,0,(int)$month , (int)$day , (int)$year));
		}
		if($row->data_inser_comm!=NULL){
			@list($year,$month,$day) = explode("-",$row->data_inser_comm);
			$row->data_inser_comm = date ("d-m-Y",mktime (0,0,0,(int)$month , (int)$day , (int)$year));
		}
		if($row->data_cons_dis_prev!=NULL){
			@list($year,$month,$day) = explode("-",$row->data_cons_dis_prev);
			$row->data_cons_dis_prev = date ("d-m-Y",mktime (0,0,0,(int)$month , (int)$day , (int)$year));
		}
		if($row->data_invio_dis!=NULL){
			@list($year,$month,$day) = explode("-",$row->data_invio_dis);
			$row->data_invio_dis = date ("d-m-Y",mktime (0,0,0,(int)$month , (int)$day , (int)$year));
		}
		if($row->data_consegna_cliente!=NULL){
			@list($year,$month,$day) = explode("-",$row->data_consegna_cliente);
			$row->data_consegna_cliente = date ("d-m-Y",mktime (0,0,0,(int)$month , (int)$day , (int)$year));
		}
		if($row->data_consegna_interna!=NULL){
				@list($year,$month,$day) = explode("-",$row->data_consegna_interna);
				$row->data_consegna_interna = date ("d-m-Y",mktime (0,0,0,(int)$month , (int)$day , (int)$year));
			}
		if($row->offerta_data!=NULL){
				@list($year,$month,$day) = explode("-",$row->offerta_data);
				$row->offerta_data= date ("d-m-Y",mktime (0,0,0,(int)$month , (int)$day , (int)$year));
		}
		if($row->ddt_data!=NULL){
				@list($year,$month,$day) = explode("-",$row->ddt_data);
				$row->ddt_data= date ("d-m-Y",mktime (0,0,0,(int)$month , (int)$day , (int)$year));
		}
		$query = $this->db->get_where('preventivi_commesse',array('commessa'=>$this->uri->segment(4)));
		foreach ($query->result() as $ellabella);
		if($query->num_rows()==0)
		{
			$preventivo_row='href="#" class="preventivo_button" commessa="'.$this->uri->segment(4).'"';
		}else{
			$preventivo_row='href="/index.php/preventivi/preventivo_inizio/'.$this->uri->segment(4).'"';

		}

		$query = $this->db->get_where('ddt_commesse',array('commessa'=>$this->uri->segment(4)));
		foreach ($query->result() as $ellabella);
		if($query->num_rows()==0)
		{
			$ddt_cliente_row='href="#" class="ddt_cliente_button" commessa="'.$this->uri->segment(4).'"';
		}else{
			$query = $this->db->get_where('tabella_ddt',array('id_ddt'=>$ellabella->id_ddt,'tipo_ddt'=>'C'));
			if($query->num_rows()==0)
			{
				$ddt_cliente_row='href="#" class="ddt_cliente_button" commessa="'.$this->uri->segment(4).'"';
			}else{
				$ddt_cliente_row='href="/index.php/ddt/ddt_inizio/'.$this->uri->segment(4).'"';
			}

		}
		if($row->dis_sapone!=NULL)
		{
			$nomefile=$row->dis_sapone;
		}else{
			$nomefile=$row->n_stampo;
		}
		$row->indirizzo_disegno="ftp://".$_SERVER["SERVER_NAME"].":2222/".$cliente->ragione_soc."/PDF/".$nomefile.".pdf";

		$vars=array();
		$vars['row']=$row;
		$vars['materiale_piastre']=$materiale_piastre;
		$vars['cliente']=$cliente;
		$vars['produttore']=$produttore;
		$vars['materiale_matrice']=$materiale_matrice;
		$vars['tratt_mat']=$tratt_mat;
		$vars['tratt_piastre']=$tratt_piastre;
		$vars['preventivo_row']=$preventivo_row;
		$vars['ddt_cliente_row']=$ddt_cliente_row;

		extract($vars);
			#region buffering...
			ob_start();

			include(getcwd().'/application/views/intestazione_view.php');

			$buffer = ob_get_contents();
		@ob_end_clean();
		return $buffer;
	}
	function intestazioneDettaglioOP()
		{
			$where = "commessa = ".$this->uri->segment(4);
			//$this->db->select('tipo_lavoro');
			$que = $this->db->get_where('lavori',$where);
			foreach ($que->result() as $row);
				switch($row->stato){
					case "D":
							$stato_url="progetti";
					break;
					case "E":
							$stato_url="eseguiti";
					break;
					case "C":
							$stato_url="incorso";
					break;
					case "S":
							$stato_url="sospesi";
					break;

				}
			$query = $this->db->get_where('clienti',array('id_cliente'=>$row->cliente));
			if($query->num_rows() > 0)
				foreach ($query->result() as $cliente);
		 	else{
		 		$cliente->ragione_soc="";
				$cliente->note_cliente="";
			}
			$query = $this->db->get_where('produttori',array('id_produttore'=>$row->id_produttore));
			foreach ($query->result() as $produttore);
			$tratt_mat = new stdClass();
			$tra = $this->db->get_where('trattamenti',array('id_trattamento'=>$row->id_trattam_matrice));
			if($tra->num_rows() > 0)
				foreach ($tra->result() as $tratt_mat);
			else $tratt_mat->nome_trattamento="";
			$tratt_piastre = new stdClass();
			$tra = $this->db->get_where('trattamenti',array('id_trattamento'=>$row->id_trattam_piastre));
			if($tra->num_rows() > 0)
				foreach ($tra->result() as $tratt_piastre);
			else $tratt_piastre->nome_trattamento="";
			$materiale_piastre = new stdClass();
			$que = $this->db->get_where('materiali',array('id_materiale'=>$row->id_materiale_piastre));
			if($que->num_rows() > 0)
				foreach ($que->result() as $materiale_piastre);
			else $materiale_piastre->nome_materiale="";
			$materiale_matrice = new stdClass();
			$que = $this->db->get_where('materiali',array('id_materiale'=>$row->id_materiale_matrice));
			if($que->num_rows() > 0)
				foreach ($que->result() as $materiale_matrice);
			else $materiale_matrice->nome_materiale="";

			$query = $this->db->get_where('note_produttori',array('id_produttore'=>$row->id_produttore, 'nome_campo'=>'id_produttore'));
			if($query->num_rows() > 0){
				foreach ($query->result() as $nota_produttore);
				$note_produtt=$nota_produttore->nota_campo;
			}
			else
			{
				$note_produtt="";
			}
			$query = $this->db->get_where('note_produttori',array('id_produttore'=>0, 'nome_campo'=>'id_produttore'));
			if($query->num_rows() > 0){
				foreach ($query->result() as $nota_produttore);
				$note_generic="\nNOTA GENERICA:-------------------------------------\n".$nota_produttore->nota_campo;
			}
			else
			{
				$note_generic="";
			}
			if($this->session->userdata('nome_operatore')=="DAVID"){
				$clona='<div class="row" align="right"><button class="btn btn-default" role="button"><a href="/index.php/omis/clona_commessa/'.$this->uri->segment(4).'">Clona &nbsp;</a></button></div>';
				$modifica='<div class="row" ><button class="btn btn-default" role="button"><a href="/index.php/omis/lavori/'.$stato_url.'/edit/'.$this->uri->segment(4).'">Modifica</a></button></div>';
				$progetto='<div class="row" align="right"><button class="btn btn-default" role="button"><a href="/index.php/omis/tornaprogS/'.$this->uri->segment(4).'">Progetto</a></button></div>';
				$incorso='';
				$query=$this->db->get_where('preventivi_commesse',array('commessa'=>$this->uri->segment(4)));
				if($query->num_rows()>0){foreach ($query->result() as $previo);
							$query = $this->db->get_where('preventivi',array('id_preventivo'=>$previo->id_preventivo));
							foreach ($query->result() as $prev);
							if($prev->num_revisione_confermata==NULL) $revisione=$prev->ultimo_num_revisione;
							else $revisione=$prev->num_revisione_confermata;
							$preventivo="<a  href='/index.php/preventivi/stampa_prev/".$previo->id_preventivo."/".$revisione."'>
																		<button class='btn btn-default'> Preventivo</button></a>";}
				else{
					$preventivo="";
				}
			}else
			{
				$incorso="";
				$progetto="";
				$modifica="";
				$clona="";
				$preventivo="";
			}


			if($row->dis_sapone!=NULL)
			{
				$nomefile=$row->dis_sapone;
			}else{
				$nomefile=$row->n_stampo;
			}
			$row->indirizzo_disegno="ftp://".$_SERVER["SERVER_NAME"].":2222/".$cliente->ragione_soc."/PDF/".$nomefile.".pdf";

			$vars=array();
			$vars['row']=$row;
			$vars['materiale_piastre']=$materiale_piastre;
			$vars['cliente']=$cliente;
			$vars['produttore']=$produttore;
			$vars['materiale_matrice']=$materiale_matrice;
			$vars['tratt_mat']=$tratt_mat;
			$vars['tratt_piastre']=$tratt_piastre;
			$vars['preventivo']=$preventivo;
			$vars['clona']=$clona;
			$vars['modifica']=$modifica;
			$vars['progetto']=$progetto;
			$vars['incorso']=$incorso;

			$vars['note_generic']=$note_generic;

			extract($vars);
			#region buffering...
			ob_start();

			include(getcwd().'/application/views/intestazione_view_op.php');

			$buffer = ob_get_contents();
			@ob_end_clean();
			return $buffer;

		}


	function commessaBolle($value, $primary_key)
	{

		$return="<input name='commessa' type='text' value=".((isset($value))? $value:$this->uri->segment(4))." readonly />";
		$this->setHeader=FALSE;
		return $return;
	}

	function PesoSpecifico()
	{
		$return="<input name='peso_specifico' type='text' value='1.035' maxlength='5' />";
		return $return;
	}


	function footerComponenteDati() // invocata da dettaglioop se devo modificare dati componente-----> invoca salvaComponente------------------//
	{


		$query = $this->db->get_where('bolle_prod',array('cod_bolla'=>$this->uri->segment(6)));
		foreach ($query->result() as $row);
		if($row->id_materiale!="")
		{$query = $this->db->get_where('materiali',array('id_materiale'=>$row->id_materiale));
				foreach ($query->result() as $nome_mat);
				$compon = $this->db->get_where('componenti',array('id_componente'=>$row->id_componente));
				foreach ($compon->result() as $componente);
				$lista_forme=array("PIATTO"=>"PIATTO",
									'TONDO'=>'TONDO',
									'TUBO'=>'TUBO',
									'ESAGONO'=>'ESAGONO');
				$this->db->select('sezione_materiale');
				$this->db->from('magazzino_materiali');
				$where = "id_materiale = ".$row->id_materiale." AND forma_materiale = 'PIATTO' ";
				$this->db->where($where);
				$this->db->order_by("sezione_materiale", "desc");
				$query = $this->db->get();

				if($query->num_rows()>0){
					foreach ($query->result() as $materiale){
						$lista_sezioni[$materiale->sezione_materiale]=$materiale->sezione_materiale;
					}
				}else{
					$lista_sezioni['RITAGLIO']="RITAGLIO";

				}
				if($row->codice_materiale_interno!=NULL){
					$query = $this->db->get_where('magazzino_materiali',array('codice_materiale_interno'=>$row->codice_materiale_interno));
					foreach ($query->result() as $materiale);
					$forma=$materiale->forma_materiale;
					$sezione=$materiale->sezione_materiale;
				}
				else{
					$forma="PIATTO";
					$sezione="";
				}

					$footer="
					<table align='center'>
					<form method='POST' action='/index.php/omis/salvaComponente'>
					<input type='hidden' name='cod_bolla' value='".$row->cod_bolla."'>
					<input type='hidden' name='id_materiale' value='".$row->id_materiale."'>
					<input type='hidden' name='redirect'  value='C'>


					<input type='hidden' name='commessa' value='".$this->uri->segment(4)."'>
					<tr><td width='500'><fieldset>
			    	<legend><font size='4' color='red'>$componente->nome_componente</font></legend>
			<font size='4'>
					<table>
					<tr><td width='180' align='left'>Quantita': </td><td><input style='height:30px;font-size:12pt;' type='text' size='10' value='".$row->quantita."' readonly/></td><td></td></tr>
					<tr><td width='180' align='left'>Materiale: </td><td>".$nome_mat->nome_materiale."</td><td></td></tr>

					<tr><td align='left'>Forma: </td><td>".form_dropdown('forma', $lista_forme, $forma )."</td><td></td></tr>
			    	<tr><td align='left'>Sezione: </td><td>".form_dropdown('sezione', $lista_sezioni,$sezione)."</td><td></td></tr>
					<tr class='lunghezza'><td width='200' align='left'><div class='lunghezza_sezione'>Lunghezza:</div> </td><td><input class='lunghezza' style='height:30px;font-size:12pt;' name='lunghezza' type='text' size='10' value='' /></td><td width='200'><div class='ritaglio'>Ritaglio: <input style='height:30px;font-size:12pt;' name='misura_ritaglio' type='text' size='10' value='".$row->misure_ritaglio."' /></div></td></tr>

			    	<tr class='kili'><td width='150' align='left'>Kg: </td><td><input style='height:30px;font-size:12pt;' class='kili' name='kg' type='text' size='10' value='' /></td></tr>
					<tr><td align='left'>Non Conformi: </td><td> <input style='height:30px;font-size:12pt;' name='non_conformi' type='text' size='10' value='".$row->non_conformi."' /></td></tr>
					<tr><td align='right'><input  style='font-size:15pt;' type='submit' value='SALVA'></td>
					<td><a href='/index.php/omis/dettaglioop/componenti/".$this->uri->segment(4)."' ><input style='font-size:15pt;' type='button' value='ANNULLA' /></a></td></tr>
					</table>
					</font>

			  		</fieldset></td>
					<td><fieldset>
					<legend><font size='4' color='red'>NOTE COMPONENTE</font></legend>".$row->note."</fieldset></td>
					</tr>
					</table>
					";}else{
					$footer="Non e' stato specificato nessun materiale per il componente selezionato.";
				}

		return $footer;
	}



	function footerSemilavoratiTempi() // invocata da dettaglioop se devo modificare tempi semilavorato-----> invoca salvaTempiSemilavorato------------------//
	{
		date_default_timezone_set('Europe/Rome');

		$query = $this->db->get('centri_di_costo');
		foreach ($query->result() as $row)
		{
		    $centricosto[$row->id_centro_costo]=$row->nome_centro_costo;
		}
		$query = $this->db->get_where('ordini_semilavorati',array('id_semilav_comm'=>$this->uri->segment(6)));
		foreach ($query->result() as $row);
		$compon = $this->db->get_where('anagrafica_semilavorati',array('id_semilav_ana'=>$row->id_semilav_ana));
		foreach ($compon->result() as $semilavorato_ana);
		$lavorazioni = $this->db->get_where('lavorazioni',array('id_semilav_comm'=>$this->uri->segment(6), 'id_operatore' => $this->session->userdata('id_operatore')));
		$this->load->library('table');

		$this->table->set_heading('Tempo Operatore ', 'Tempo Macchina', 'Data Lavorazione', 'Centro di Costo');
		foreach ($lavorazioni->result() as $lavorazione){
			@list($year,$month,$day) = explode("-",$lavorazione->data_lavorazione);
			$value = date ("d-m-Y",mktime (0,0,0,(int)$month , (int)$day , (int)$year));
			$centro = $this->db->get_where('centri_di_costo',array('id_centro_costo'=>$lavorazione->id_centro_costo));
			foreach ($centro->result() as $centro_costo);
			//$bottone_elimina="<form method='POST' action='/index.php/omis/elimina_lavorazione/".$lavorazione->id_lavorazione."'><input type='hidden' name='cod_bolla' value='".$this->uri->segment(6)."'>";
			$this->table->add_row($lavorazione->t_operatore, $lavorazione->t_macchina, $value, $centro_costo->nome_centro_costo);
		}
		$tabula_lavorazioni=$this->table->generate();

			$footer="
			<table align='center'>
			<tr>

			<td><fieldset>
			<legend><font size='4' color='red'>TEMPI INSERITI IN QUESTO SEMILAVORATO</font></legend>".$tabula_lavorazioni."</fieldset></td>
			<td width='370'><fieldset>
			<form method='POST' action='/index.php/omis/salvaTempiSemilavorato'>
			<input type='hidden' name='id_semilav_comm' value='".$row->id_semilav_comm."'>
			<input type='hidden' name='commessa' value='".$this->uri->segment(4)."'>
			<input type='hidden' name='id_operatore' value='".$this->session->userdata('id_operatore')."'>


	    	<legend><font size='4' color='red'>$semilavorato_ana->nome_semilavorato</font></legend>
	<font size='4'>
			<table>
			<tr><td align='left'>Centro di costo: </td><td>".form_dropdown('id_centro_costo', $centricosto, $this->session->userdata('id_centro_costo') )."</td></tr>
			<tr><td align='left'>Operatore: </td><td>".$this->session->userdata('nome_operatore')."</td></tr>
			<tr><td width='170' align='left'>Tempo Operatore: </td><td width='170'><input style='height:30px;font-size:12pt;' name='t_operatore' type='text' size='10' value='0.0'/> ore</td></tr>
	    	<tr><td width='170' align='left'>Tempo Macchina: </td><td><input style='height:30px;font-size:12pt;' name='t_macchina' type='text' size='10' value='0.0' />  ore</td></tr>

			<tr><td align='right'><input  style='font-size:15pt;' type='submit'  value='SALVA'></td>
			<td><a href='/index.php/omis/dettaglioop/semilavorati/".$this->uri->segment(4)."'  ><input style='font-size:15pt;' type='button' value='ANNULLA' /></a></td></tr>
			</table>
			</font>
	  		</fieldset></td>
			<td><fieldset>
			<legend><font size='4' color='red'>NOTE SEMILAVORATO</font></legend>".$row->note_semilav."</fieldset></td>
			</tr>
			</table>
			";

		return $footer;
	}

	function footerComponenteTempi() // invocata da dettaglioop se devo modificare tempi componente-----> invoca salvaTempiComponente------------------//
	{
		date_default_timezone_set('Europe/Rome');

		$query = $this->db->get('centri_di_costo');
		foreach ($query->result() as $row)
		{
		    $centricosto[$row->id_centro_costo]=$row->nome_centro_costo;
		}
			$query = $this->db->get_where('bolle_prod',array('cod_bolla'=>$this->uri->segment(6)));
			foreach ($query->result() as $row);
			$compon = $this->db->get_where('componenti',array('id_componente'=>$row->id_componente));
			foreach ($compon->result() as $componente);
			$lavorazioni = $this->db->get_where('lavorazioni',array('cod_bolla'=>$this->uri->segment(6)));



			$this->load->library('table');
			$tmpl = array (
                    'table_open'          => '<table border="0" cellpadding="10" cellspacing="5">',

                    'heading_row_start'   => '<tr>',
                    'heading_row_end'     => '</tr>',
                    'heading_cell_start'  => '<th style="padding:7px;">',
                    'heading_cell_end'    => '</th>',

                    'row_start'           => '<tr>',
                    'row_end'             => '</tr>',
                    'cell_start'          => '<td style="padding:7px; text-align:center;">',
                    'cell_end'            => '</td>',

                    'row_alt_start'       => '<tr>',
                    'row_alt_end'         => '</tr>',
                    'cell_alt_start'      => '<td>',
                    'cell_alt_end'        => '</td>',

                    'table_close'         => '</table>'
              );

		$this->table->set_template($tmpl);

			$this->table->set_heading('Nome Op','Tempo Op ', 'Tempo Macch', 'Data Lav', 'Centro di Costo');
			foreach ($lavorazioni->result() as $lavorazione){
				@list($year,$month,$day) = explode("-",$lavorazione->data_lavorazione);
				$value = date ("d-m-Y",mktime (0,0,0,(int)$month , (int)$day , (int)$year));
				$centro = $this->db->get_where('centri_di_costo',array('id_centro_costo'=>$lavorazione->id_centro_costo));
				foreach ($centro->result() as $centro_costo);
				$operatore = $this->db->get_where('operatori',array('id_operatore'=>$lavorazione->id_operatore));
				foreach ($operatore->result() as $nome_op);
				$this->table->add_row($nome_op->nome_op,$lavorazione->t_operatore, $lavorazione->t_macchina, $value, $centro_costo->nome_centro_costo);
			}
		$tabula_lavorazioni=$this->table->generate();

			$footer="


			<div class='col-sm-5'>
				<fieldset>
					<legend><font size='4' color='red'>TEMPI INSERITI IN QUESTO COMPONENTE</font></legend>".$tabula_lavorazioni."
				</fieldset>
			</div>
			<div class='col-sm-5'>
				<fieldset>
					<form method='POST' action='/index.php/omis/salvaTempiComponente'>
					<input type='hidden' name='cod_bolla' value='".$row->cod_bolla."'>
					<input type='hidden' name='commessa' value='".$this->uri->segment(4)."'>
					<input type='hidden' name='id_operatore' value='".$this->session->userdata('id_operatore')."'>


		    		<legend><font size='4' color='red'>$componente->nome_componente</font></legend>
					<font size='4'>
					<table>
					<tr><td align='left'>Centro di costo: </td><td>".form_dropdown('id_centro_costo', $centricosto, $this->session->userdata('id_centro_costo') )."</td></tr>
					<tr><td align='left'>Operatore: </td><td>".$this->session->userdata('nome_operatore')."</td></tr>
					<tr><td width='170' align='left'>Tempo Operatore: </td><td width='170'><input style='height:30px;font-size:12pt;' name='t_operatore' type='text' size='10' value='0.0'/> ore</td></tr>
			    	<tr><td width='170' align='left'>Tempo Macchina: </td><td><input style='height:30px;font-size:12pt;' name='t_macchina' type='text' size='10' value='0.0' />  ore</td></tr>

					<tr><td align='right'><input  style='font-size:15pt;' type='submit'  value='SALVA'></td>
					<td><a href='/index.php/omis/dettaglioop/componenti/".$this->uri->segment(4)."'  ><input style='font-size:15pt;' type='button' value='ANNULLA' /></a></td></tr>
					</table>
					</font>
	  			</fieldset>
	  		</div>
			<div class='col-sm-2'><fieldset>
			<legend><font size='4' color='red'>NOTE COMPONENTE</font></legend>".$row->note."</fieldset></div>

			";

		return $footer;
	}

		/*	<tr><td align='left'>Tempo Macchina Extra: </td><td> <input style='height:30px;font-size:12pt;' name='t_macch_extra' type='text' size='10' value='0.0' /> ore</td></tr> */

	function footerFaseTempi() // invocata da dettaglioop se devo modificare tempi fase-----> invoca salvaTempiFase------------------//
	{
		date_default_timezone_set('Europe/Rome');

		$query = $this->db->get('centri_di_costo');
		foreach ($query->result() as $row)
		{
		    $centricosto[$row->id_centro_costo]=$row->nome_centro_costo;
		}
			$lavorazioni = $this->db->get_where('lavorazioni',array('id_fase_commessa'=>$this->uri->segment(6), 'id_operatore' => $this->session->userdata('id_operatore')));
			$this->load->library('table');

			$this->table->set_heading('Tempo Operatore ', 'Tempo Macchina', 'Data Lavorazione');
			foreach ($lavorazioni->result() as $lavorazione){
				list($year,$month,$day) = explode("-",$lavorazione->data_lavorazione);
				$value = date ("d-m-Y",mktime (0,0,0,(int)$month , (int)$day , (int)$year));
				$this->table->add_row($lavorazione->t_operatore, $lavorazione->t_macchina, $value);
			}
			$tabula_lavorazioni=$this->table->generate();

		$query = $this->db->get_where('fasi_commessa',array('id_fase_commessa'=>$this->uri->segment(6)));
		foreach ($query->result() as $row);
			$footer="
			<table align='center'>
				<tr>
			<td><fieldset>
			<legend><font size='4' color='red'>TEMPI INSERITI</font></legend>".$tabula_lavorazioni."</fieldset></td>
			<td width='370'><fieldset>
			<form method='POST' action='/index.php/omis/salvaTempiFase'>
			<input type='hidden' name='id_fase_commessa' value='".$row->id_fase_commessa."'>
			<input type='hidden' name='commessa' value='".$this->uri->segment(4)."'>
			<input type='hidden' name='id_operatore' value='".$this->session->userdata('id_operatore')."'>

	    	<legend><font size='4' color='red'>$row->nome_fase</font></legend>
	<font size='4'>
			<table>
			<tr><td align='left'>Centro di costo: </td><td>".form_dropdown('id_centro_costo', $centricosto, $this->session->userdata('id_centro_costo') )."</td></tr>
			<tr><td align='left'>Operatore: </td><td>".$this->session->userdata('nome_operatore')."</td></tr>
			<tr><td width='170' align='left'>Tempo Operatore: </td><td width='170'><input style='height:30px;font-size:12pt;' name='t_operatore' type='text' size='10' value='0.0'/> ore</td></tr>
			<tr><td width='170' align='left'>Tempo Macchina: </td><td><input style='height:30px;font-size:12pt;' name='t_macchina' type='text' size='10' value='0.0' />  ore</td></tr>
			<tr><td align='right'><input  style='font-size:15pt;' type='submit'  value='SALVA'></td>
			<td><a href='/index.php/omis/dettaglioop/fasi/".$this->uri->segment(4)."'  ><input style='font-size:15pt;' type='button' value='ANNULLA' /></a></td></tr>
			</table>
			</font>
	  		</fieldset></td>
			</tr>
			</table>
			";

		return $footer;
	}

	function footerComponenteTempiNonModificabileComponenti() // invocata da dettaglio se devo controllare stato tempi componente----------------------//
	{
			date_default_timezone_set('Europe/Rome');

			$query = $this->db->get_where('bolle_prod',array('cod_bolla'=>$this->uri->segment(6)));
			foreach ($query->result() as $row);
			$compon = $this->db->get_where('componenti',array('id_componente'=>$row->id_componente));
			foreach ($compon->result() as $componente);
			$lavorazioni = $this->db->get_where('lavorazioni',array('cod_bolla'=>$this->uri->segment(6)));



			$this->load->library('table');
			$tmpl = array (
                    'table_open'          => '<table style="border-spacing: 10px; border-collapse: separate;">',

                    'heading_row_start'   => '<tr>',
                    'heading_row_end'     => '</tr>',
                    'heading_cell_start'  => '<th><small>',
                    'heading_cell_end'    => '</small></th>',

                    'row_start'           => '<tr>',
                    'row_end'             => '</tr>',
                    'cell_start'          => '<td>',
                    'cell_end'            => '</td>',

                    'row_alt_start'       => '<tr>',
                    'row_alt_end'         => '</tr>',
                    'cell_alt_start'      => '<td>',
                    'cell_alt_end'        => '</td>',

                    'table_close'         => '</table>'
              );

$this->table->set_template($tmpl);

			$this->table->set_heading('Operatore','Tempo Op. ', 'Tempo Macch.', 'Data Lavorazione', 'Centro di Costo');
			foreach ($lavorazioni->result() as $lavorazione){
				@list($year,$month,$day) = explode("-",$lavorazione->data_lavorazione);
				$value = date ("d-m-Y",mktime (0,0,0,(int)$month , (int)$day , (int)$year));
				$centro = $this->db->get_where('centri_di_costo',array('id_centro_costo'=>$lavorazione->id_centro_costo));
				foreach ($centro->result() as $centro_costo);
				$operatore = $this->db->get_where('operatori',array('id_operatore'=>$lavorazione->id_operatore));
				foreach ($operatore->result() as $nome_op);
				$this->table->add_row($nome_op->nome_op,$lavorazione->t_operatore, $lavorazione->t_macchina, $value, $centro_costo->nome_centro_costo);
			}
			$tabula_lavorazioni=$this->table->generate();

				$footer="


				<fieldset>
				<legend><font size='4' color='red'>TEMPI INSERITI IN ".$componente->nome_componente."</font></legend>".$tabula_lavorazioni."</fieldset>
				<br>
				";

			return $footer;
	}

	function footerComponenteTempiNonModificabileFasi() // invocata da dettaglio se devo controllare stato tempi componente----------------------//
	{
			date_default_timezone_set('Europe/Rome');

			$lavorazioni = $this->db->get_where('lavorazioni',array('id_fase_commessa'=>$this->uri->segment(6)));

				foreach ($lavorazioni->result() as $row);
				$compon = $this->db->get_where('fasi_commessa',array('id_fase_commessa'=>$this->uri->segment(6)));
				foreach ($compon->result() as $componente);



			$this->load->library('table');
			$tmpl = array (
                    'table_open'          => '<table style="border-spacing: 10px; border-collapse: separate;">',

                    'heading_row_start'   => '<tr>',
                    'heading_row_end'     => '</tr>',
                    'heading_cell_start'  => '<th><small>',
                    'heading_cell_end'    => '</small></th>',

                    'row_start'           => '<tr>',
                    'row_end'             => '</tr>',
                    'cell_start'          => '<td>',
                    'cell_end'            => '</td>',

                    'row_alt_start'       => '<tr>',
                    'row_alt_end'         => '</tr>',
                    'cell_alt_start'      => '<td>',
                    'cell_alt_end'        => '</td>',

                    'table_close'         => '</table>'
              );

$this->table->set_template($tmpl);

			$this->table->set_heading('Operatore','Tempo Op. ', 'Tempo Macch.', 'Data Lavorazione', 'Centro di Costo');
			if($lavorazioni->num_rows() > 0)
			{
				foreach ($lavorazioni->result() as $lavorazione){
					@list($year,$month,$day) = explode("-",$lavorazione->data_lavorazione);
					$value = date ("d-m-Y",mktime (0,0,0,(int)$month , (int)$day , (int)$year));
					$centro = $this->db->get_where('centri_di_costo',array('id_centro_costo'=>$lavorazione->id_centro_costo));
					foreach ($centro->result() as $centro_costo);
						$operatore = $this->db->get_where('operatori',array('id_operatore'=>$lavorazione->id_operatore));
					foreach ($operatore->result() as $nome_op);
						$this->table->add_row($nome_op->nome_op,$lavorazione->t_operatore, $lavorazione->t_macchina, $value, $centro_costo->nome_centro_costo);
				}
			}

			$tabula_lavorazioni=$this->table->generate();

				$footer="
						<fieldset>
						<legend><font size='4' color='red'>TEMPI INSERITI IN ".$componente->nome_fase."</font></legend>".$tabula_lavorazioni."
						</fieldset><br>

				";

			return $footer;
	}


	function salvaTempiComponente()			//----------------------GESTIONE SALVATAGGIO TEMPI DI SINGOLA BOLLA DI PRODUZIONE(COMPONENTE)-------------------------//
	{
		date_default_timezone_set('Europe/Rome');

		$que = $this->db->get_where('bolle_prod',array('cod_bolla' => $_POST['cod_bolla']));  //---quando inserisco dei tempi relativi a una bolla: se era gi� completata la lavorazione, questa rimane tale
		foreach($que->result() as $row);														//se era da iniziare metto stato:IN CORSO dato che sto inserendo dei tempi e quindi ci ho lavorato.
		if($this->session->userdata('tipo_op')!="DIS")
		{
			if($row->stato=='INIZIARE')
			 	$stato='IN CORSO';
			else $stato=$row->stato;
			}

		else
			$stato='INIZIARE';


		$data = array(
		               'stato'    => $stato
		            );


		$nuovaLav=array(
					'cod_bolla' => $_POST['cod_bolla'],
					't_operatore' => $_POST['t_operatore'],
					't_macchina' => $_POST['t_macchina'],
		           /* 't_macch_extra' => $_POST['t_macch_extra'],*/
					'id_operatore'=>$_POST['id_operatore'],
	               	'id_centro_costo' => $_POST['id_centro_costo'],
					'data_lavorazione' => date('Y-m-d'),
					'commessa' => $_POST['commessa']
					);

		$this->db->insert('lavorazioni',$nuovaLav);
		$this->db->where('cod_bolla', $_POST['cod_bolla']);
		$this->db->update('bolle_prod', $data);
		redirect('/omis/dettaglioop/componenti/'.$_POST['commessa'], 'refresh');
	}
	function salvaTempiFase()			//----------------------GESTIONE SALVATAGGIO TEMPI DI SINGOLA FASE-------------------------//
	{
		date_default_timezone_set('Europe/Rome');

		$nuovaLav=array(
					'id_fase_commessa' => $_POST['id_fase_commessa'],
					't_operatore' => $_POST['t_operatore'],
					't_macchina' => $_POST['t_macchina'],
					'id_operatore'=>$_POST['id_operatore'],
	               	'id_centro_costo' => $_POST['id_centro_costo'],
					'data_lavorazione' => date('Y-m-d'),
					'commessa' => $_POST['commessa']

					);
		($this->session->userdata('tipo_op')!="DIS") ? $stato='IN CORSO' : $stato='INIZIARE';
		$this->db->insert('lavorazioni',$nuovaLav);
		$this->db->where('id_fase_commessa', $_POST['id_fase_commessa']);
		$this->db->update('fasi_commessa', array('stato'=> $stato));
		redirect('/omis/dettaglioop/fasi/'.$_POST['commessa'], 'refresh');
	}
	function salvaTempiSemilavorato()			//----------------------GESTIONE SALVATAGGIO TEMPI DI SINGOLO SEMILAVORATO-------------------------//
	{
		date_default_timezone_set('Europe/Rome');

		$nuovaLav=array(
					'id_semilav_comm' => $_POST['id_semilav_comm'],
					't_operatore' => $_POST['t_operatore'],
					't_macchina' => $_POST['t_macchina'],
					'id_operatore'=>$_POST['id_operatore'],
	               	'id_centro_costo' => $_POST['id_centro_costo'],
					'data_lavorazione' => date('Y-m-d'),
					'commessa' => $_POST['commessa']

					);
		$this->db->insert('lavorazioni',$nuovaLav);
		$this->db->where('id_semilav_comm', $_POST['id_semilav_comm']);
		//$this->db->update('ordini_semilavorati', array('stato'=> 'IN CORSO'));
		redirect('/omis/dettaglioop/semilavorati/'.$_POST['commessa'], 'refresh');
	}

	function salvaComponente()				//----------------------GESTIONE SALVATAGGIO DATI DI SINGOLA BOLLA DI PRODUZIONE(COMPONENTE)-------------------------//
	{

		if(!isset($_POST['misura_ritaglio'])) $_POST['misura_ritaglio']="";
		$misure_ritaglio="";
		$lunghezza_barra="";
		$lav = $this->db->get_where('bolle_prod',array('cod_bolla'=>$_POST['cod_bolla']));
		foreach($lav->result() as $lavCorrente);

	/*	if($lavCorrente->quantita==$_POST['completati'])
		{							//	----------	Se COMPLETATI = QUANTITA' lo stato della bolla diventa COMPLETATO ---
		 	$stato='COMPLETATO';
			($_POST['kg'] != '0.00') ? $stato='IN CORSO' : $stato='INSERIRE KG';
		}
		else*/

		if(($_POST['forma']=="PIATTO")&&($_POST['sezione']!="RITAGLIO"))
		{
			$mater =$this->db->get_where('magazzino_materiali',array('id_materiale'=>$_POST['id_materiale'],'forma_materiale'=>$_POST['forma'],'sezione_materiale'=>$_POST['sezione']));
			foreach($mater->result() as $cod_mat_interno);
			$sezione=str_replace("*","x",$_POST['sezione']);
			$sezione=str_replace("X","x",$sezione);
			@list($valore1,$valore2,$valore3) = explode("x",$sezione);
			$_POST['sezione']=$sezione;
			$peso = $this->db->get_where('materiali',array('id_materiale'=>$_POST['id_materiale']));
			foreach($peso->result() as $peso_spec);
			$kg=($valore1*$valore2*$_POST['lunghezza']*$peso_spec->peso_specifico_materiale)/1000000;


			if($_POST['misura_ritaglio']!="")
			{
				$mater=$this->db->get_where('magazzino_materiali',array('id_materiale'=>$_POST['id_materiale'],'forma_materiale'=>$_POST['forma'],
					'sezione_materiale'=>'RITAGLIO'));
				foreach($mater->result() as $cod_mat);

				$nuovi_kg=($valore1*$valore2*$_POST['misura_ritaglio']*$peso_spec->peso_specifico_materiale)/1000000;
				$nuova_qta_disponibile=$cod_mat->qta_disponibile+$nuovi_kg;
				//qui sotto aggiorno i kg di sezione ritaglio nel magazzino e aggiorno le misure ritaglio
				$this->db->where('codice_materiale_interno', $cod_mat->codice_materiale_interno);
				$this->db->update('magazzino_materiali', array('qta_disponibile'=>$nuova_qta_disponibile));

				$kg_ritaglio=$nuovi_kg;
				$misure_ritaglio=$valore1."x".$valore2."x".$_POST['misura_ritaglio'];
			}else{
				$kg_ritaglio=0;
			}
			$lunghezza_barra=$_POST['lunghezza'];

		}

		if($_POST['forma']=="TUBO")
		{
			$mater =$this->db->get_where('magazzino_materiali',array('id_materiale'=>$_POST['id_materiale'],'forma_materiale'=>$_POST['forma'],'sezione_materiale'=>$_POST['sezione']));
			foreach($mater->result() as $cod_mat_interno);
			$sezione=str_replace("*","x",$_POST['sezione']);
			$sezione=str_replace("X","x",$sezione);
			@list($valore1,$valore2) = explode("x",$sezione);
			$peso = $this->db->get_where('materiali',array('id_materiale'=>$_POST['id_materiale']));
			foreach($peso->result() as $peso_spec);
			$area1=($valore1/2)*($valore1/2)*3.1415926535;
			$area2=(($valore1/2)-($valore2))*(($valore1/2)-($valore2))*3.1415926535;
			$kg=(($area1-$area2)*$_POST['lunghezza']*$peso_spec->peso_specifico_materiale)/1000000;


			$kg_ritaglio=0;

			$lunghezza_barra=$_POST['lunghezza'];

		}

		if(($_POST['forma']=="TONDO")&&($_POST['sezione']!="RITAGLIO"))
		{
			$mater=$this->db->get_where('magazzino_materiali',array('id_materiale'=>$_POST['id_materiale'],'forma_materiale'=>$_POST['forma'],'sezione_materiale'=>$_POST['sezione']));
			foreach($mater->result() as $cod_mat_interno);
			$peso = $this->db->get_where('materiali',array('id_materiale'=>$_POST['id_materiale']));
			foreach($peso->result() as $peso_spec);
			$kg=(($_POST['sezione']/2)*($_POST['sezione']/2)*3.1415926535*$_POST['lunghezza']*$peso_spec->peso_specifico_materiale)/1000000;


			if($_POST['misura_ritaglio']!="")
			{
				$mater=$this->db->get_where('magazzino_materiali',array('id_materiale'=>$_POST['id_materiale'],'forma_materiale'=>$_POST['forma'],
					'sezione_materiale'=>'RITAGLIO'));
				foreach($mater->result() as $cod_mat);

				$nuovi_kg=(($_POST['sezione']/2)*($_POST['sezione']/2)*3.1415926535*$_POST['misura_ritaglio']*$peso_spec->peso_specifico_materiale)/1000000;
				//$nuova_qta_disponibile=$cod_mat->qta_disponibile+
				//$nuovi_kg-((($_POST['sezione']/2)*($_POST['sezione']/2)*3.1415926535*($bolla->misure_ritaglio)*$peso_spec->peso_specifico_materiale)/1000000);
				$misure_ritaglio=$_POST['sezione']."x".$_POST['misura_ritaglio'];
				$this->db->where('codice_materiale_interno', $cod_mat->codice_materiale_interno);
				$this->db->update('magazzino_materiali', array('qta_disponibile'=>$nuova_qta_disponibile));

				$kg_ritaglio=$nuovi_kg;

			}	else{
					$kg_ritaglio=0;
				}
			$lunghezza_barra=$_POST['lunghezza'];
		}
		if(($_POST['forma']=="PIATTO")&&($_POST['sezione']=="RITAGLIO"))
		{
			$mater=$this->db->get_where('magazzino_materiali',array('id_materiale'=>$_POST['id_materiale'],'forma_materiale'=>$_POST['forma'],'sezione_materiale'=>$_POST['sezione']));
			foreach($mater->result() as $cod_mat_interno);
			$peso = $this->db->get_where('materiali',array('id_materiale'=>$_POST['id_materiale']));
			foreach($peso->result() as $peso_spec);
			$que=$this->db->get_where('bolle_prod',array('cod_bolla'=> $_POST['cod_bolla']));
			foreach($que->result() as $bolla);
			if(isset($_POST['lunghezza'])&&($_POST['lunghezza']!="")){
				$lunghezza=str_replace("*","x",$_POST['lunghezza']);
				$lunghezza=str_replace("X","x",$lunghezza);
				@list($valore1,$valore2,$valore3) = explode("x",$lunghezza);
				$kg=($valore1*$valore2*$valore3*$peso_spec->peso_specifico_materiale)/1000000;
				$misure_ritaglio=$lunghezza;
			}else{
				if($_POST['kg']!=""){
					$kg= $_POST['kg'];
					$misure_ritaglio="";
				}
			}
			$kg_ritaglio=0;

		}
		date_default_timezone_set('Europe/Rome');

		$this->db->where('codice_materiale_interno', $cod_mat_interno->codice_materiale_interno);
		$this->db->update('magazzino_materiali', array('qta_disponibile'=>($cod_mat_interno->qta_disponibile-$kg-$kg_ritaglio)));
			$nuovoConsumo=array(
					'cod_bolla' => $_POST['cod_bolla'],
					'commessa' => $_POST['commessa'],
					'data_consumo_mat' => date('Y-m-d'),
					'kg_consumati'=>$kg,
					'kg_ritaglio'=>$kg_ritaglio,
	               	'codice_materiale_interno' => $cod_mat_interno->codice_materiale_interno,
					'lunghezza_barra' => $lunghezza_barra,
					'misure_ritaglio' => $misure_ritaglio,
					'id_operatore' => $this->session->userdata('id_operatore')
					);

		$this->db->insert('consumi_materiali',$nuovoConsumo);



		$stato='IN CORSO';
		$data = array(																//------Aggiorno stato bolla produzione-------//
		               //'lunghezza_misure' => $_POST['lunghezza'],
						'codice_materiale_interno' => $cod_mat_interno->codice_materiale_interno,
		               //'completati' => $_POST['completati'],
		               'non_conformi' => $_POST['non_conformi'],
					   'stato' => $stato

		            );


		$this->db->where('cod_bolla', $_POST['cod_bolla']);
		$this->db->update('bolle_prod', $data);
		$this->commessa_completata($_POST['commessa']);
		if($_POST['redirect']!='M'){
			redirect('/omis/dettaglioop/componenti/'.$_POST['commessa'], 'refresh');
		}else{
			redirect('/operatori/pagina_mobile/'.$_POST['commessa'], 'refresh');
		}
	}
	function bolla_completata()
	{
		date_default_timezone_set('Europe/Rome');
		$bolla = $this->db->get_where('bolle_prod',array('cod_bolla'=>$this->uri->segment(4)));
		foreach($bolla->result() as $row);
		if(($row->semilavorato=="N")&&($row->id_componente!=32)&&($row->c_lavoro=='N')){
			$stato='COMPLETATO';
		}else{
			$stato='COMPLETATO';
		}

			$this->db->where('cod_bolla', $this->uri->segment(4));
			$this->db->update('bolle_prod', array('stato'=>$stato ,'completati'=> $row->quantita,'timestamp_completamento'=>date("Y-m-d H:i:s")));
			$this->commessa_completata($this->uri->segment(3));
			redirect('/omis/dettaglioop/componenti/'.$this->uri->segment(3), 'refresh');

	}
	function bolla_completata_uff()
	{
		date_default_timezone_set('Europe/Rome');
		$bolla = $this->db->get_where('bolle_prod',array('cod_bolla'=>$this->uri->segment(4)));
		foreach($bolla->result() as $row);
		if(($row->semilavorato=="N")&&($row->id_componente!=32)&&($row->c_lavoro=='N')){
			$stato='COMPLETATO';
		}else{
			$stato='COMPLETATO';
		}
			$this->db->where('cod_bolla', $this->uri->segment(4));
			$this->db->update('bolle_prod', array('stato'=>$stato ,'completati'=> $row->quantita,'timestamp_completamento'=>date("Y-m-d H:i:s")));
			$this->commessa_completata($this->uri->segment(3));
			redirect('/omis/dettaglio/componenti/'.$this->uri->segment(3), 'refresh');

	}


	function fase_completata()
	{
		date_default_timezone_set('Europe/Rome');
		$this->db->where('id_fase_commessa', $this->uri->segment(4));
		$this->db->update('fasi_commessa', array('stato'=>'COMPLETATO','timestamp_completamento'=>date("Y-m-d H:i:s")));
		$this->commessa_completata($this->uri->segment(3));
		redirect('/omis/dettaglioop/fasi/'.$this->uri->segment(3), 'refresh');
	}
	function fase_completata_uff()
	{
		date_default_timezone_set('Europe/Rome');
		$this->db->where('id_fase_commessa', $this->uri->segment(4));
		$this->db->update('fasi_commessa', array('stato'=>'COMPLETATO','timestamp_completamento'=>date("Y-m-d H:i:s")));
		$this->commessa_completata($this->uri->segment(3));
		redirect('/omis/dettaglio/fasi/'.$this->uri->segment(3), 'refresh');
	}

	function commessa_completata($commessa) //verifica che tutti componenti e 'assemblaggio, finitura' siano completati, se S�->commessa COMPLETATA
	{
			$tuttoCompletato=TRUE;
			$query = $this->db->get_where('bolle_prod',array('commessa'=>$commessa));
			foreach($query->result() as $row){													//Controllo se tutto i componenti di un lavoro sono completati
				if($row->stato!='COMPLETATO' && $row->stato!='COSTRUITO'){													//Se s� cambio stato commessa al lavoro in COMPLETATO
					$tuttoCompletato=FALSE;
				}
			}
			$fase = $this->db->get_where('fasi_commessa',array('commessa' => $commessa, 'nome_fase' => 'IMBALLAGGIO'));
			foreach($fase->result() as $faseCorrente);
			if($faseCorrente->stato != 'COMPLETATO'){	$tuttoCompletato=FALSE; }
		($tuttoCompletato==TRUE) ? $data=array('stato_comm' => 'COMPLETATO') : $data=array('stato_comm' => 'IN CORSO'); //---------Aggiorno lo stato della commessa nei lavori---------//
		$this->db->where('commessa', $commessa);
		$this->db->update('lavori', $data);
	}


	function footerComponenteLavorazioni() // invocata da dettaglioop se devo iniziare una lavorazione su un componente
	{
		$bolla = $this->db->get_where('bolle_prod',array('cod_bolla'=>$this->uri->segment(6)));
		foreach ($bolla->result() as $row_bolla);
		($row_bolla->lavorazione=='STOP') ? $goto='playLavorazione' :  $goto='stopLavorazione';
		$footer="<table align='center'>
				<form method='POST' action='/index.php/omis/".$goto."'>
				<input type='hidden' name='cod_bolla' value='".$this->uri->segment(6)."'>
				<input type='hidden' name='commessa' value='".$this->uri->segment(4)."'>";
		$query = $this->db->get_where('lavorazioni',array('cod_bolla'=> $this->uri->segment(6)));
		$footer.="<tr><td align='center' width='100'>-</td><td align='center' width='250'><font size='4' color='green'>".$row_bolla->nome_componente."</font> </td><td align='center' width='100'>-</td>";

		if($query->num_rows() > 0)
		{
			$abilitaTasto=TRUE;
			$footer.="<tr><td align='center' width='200'> TEMPO LAVORAZIONE: </td><td width ='200' align='center'>DATA LAVORAZIONE:</td>";

			foreach ($query->result() as $row)
				{

					($row_bolla->lavorazione=='STOP') ? $valueTasto='PLAY': $valueTasto='STOP';
					$footer.="<tr><td align='center'>".$row->tempo." </td><td align='center'>".$row->data_lavorazione."</td>";
				/*	if($row->data_lavorazione == date('d-m-Y'))
					{
						$footer.="<td align='right'><input style='font-size:15pt;' type='submit' value='".$valueTasto."'></td>";
						$abilitaTasto=FALSE;
					}*/

					$footer.="</tr>";
				}
				if($abilitaTasto==TRUE)
					$footer.="<tr><td align='center'><input  style='font-size:15pt;' type='submit' value='".$valueTasto."'></td></tr>";
		}
		else{

			$footer.="<tr><td>NON ESISTONO LAVORAZIONI PER QUESTO COMPONENTE</td></tr>
					<tr> <td align='center'><input  style='font-size:15pt;' type='submit' value='INZIA LAVORAZIONE'></td></tr>";
		}
		$footer.="</table>";
			return $footer;
	}

	function tempoOperatoriFase($value, $row)
	{
		$this->db->select_sum('t_operatore');
		$query = $this->db->get_where('lavorazioni',array('id_fase_commessa'=>$row->id_fase_commessa , 'id_operatore' => $this->session->userdata('id_operatore')));
		foreach ($query->result() as $sum_tempi);
		return $sum_tempi->t_operatore;

	}
	function tempoMacchinaFase($value, $row)
	{
		$this->db->select_sum('t_macchina');
		$query = $this->db->get_where('lavorazioni',array('id_fase_commessa'=>$row->id_fase_commessa , 'id_operatore' => $this->session->userdata('id_operatore')));
		foreach ($query->result() as $sum_tempi);
		return $sum_tempi->t_macchina;

	}
	function tempoOperatoriComponente($value, $row)
	{
		$this->db->select_sum('t_operatore');
		$query = $this->db->get_where('lavorazioni',array('cod_bolla'=>$row->cod_bolla , 'id_operatore' => $this->session->userdata('id_operatore')));
		foreach ($query->result() as $sum_tempi);
		return $sum_tempi->t_operatore;

	}
	function tempoMacchinaComponente($value, $row)
	{
		$this->db->select_sum('t_macchina');
		$query = $this->db->get_where('lavorazioni',array('cod_bolla'=>$row->cod_bolla , 'id_operatore' => $this->session->userdata('id_operatore')));
		foreach ($query->result() as $sum_tempi);
		return $sum_tempi->t_macchina;

	}
	function tempoMacchinaExtraComponente($value, $row)
	{
		$this->db->select_sum('t_macch_extra');
		$query = $this->db->get_where('lavorazioni',array('cod_bolla'=>$row->cod_bolla , 'id_operatore' => $this->session->userdata('id_operatore')));
		foreach ($query->result() as $sum_tempi);
		return $sum_tempi->t_macch_extra;

	}

	function dettaglioop()
	{
			$crud = new grocery_CRUD();
			$crud->set_theme('datatables');
			$where = "commessa = ".$this->uri->segment(4);
			$que = $this->db->get_where('lavori',$where);
			foreach ($que->result() as $row);
			$tipo=$row->tipo_lavoro;
			$stato=$row->stato;
			switch($this->uri->segment(3)){
				case "componenti":
					$crud->set_table('bolle_prod');
					$crud->add_action('DATI', '', 'omis/dettaglioop/componenti/'.$this->uri->segment(4).'/D','ui-icon-plus');
					$crud->add_action('TEMPI', '', 'omis/dettaglioop/componenti/'.$this->uri->segment(4).'/T','ui-icon-plus');

					$crud->add_action('COMPLETATO', '', '','completa_button',array($this,'tasto_completa'));



					$crud->set_relation('id_componente','componenti','nome_componente');
					$crud->display_as('id_componente',"Nome Componente");

					$crud->columns('N°','id_componente','quantita','id_trattamento','stato','id_materiale',
					'kg','t_operatori','t_macchina','note','semilavorato','c_lavoro','id_operatore','misure_finito');
					$crud->callback_column('t_operatori',array($this,'tempoOperatoriComponente'));//disattivata la colonna, da aggiungere columns in caso di necessità
					$crud->callback_column('t_macchina',array($this,'tempoMacchinaComponente'));
					$crud->callback_column('stato',array($this,'completato_verde'));
					$crud->callback_column('kg',array($this,'somma_kg_bolla'));
					$crud->callback_column('N°',array($this,'numera_componente'));


					$crud->set_subject('Componente');

					$crud->unset_texteditor('note');
					$crud->display_as('quantita',"Quantita'");
					$crud->display_as('lunghezza_misure',"Misure");
					$crud->display_as('c_lavoro',"C/Lav");
					$crud->display_as('semilavorato',"Semilav");

					$crud->add_fields('id_componente','quantita', 'commessa','id_trattamento','cod_disegno','id_materiale','semilavorato','c_lavoro','note');
					$crud->edit_fields('id_componente','quantita' ,'commessa','id_trattamento','cod_disegno','id_materiale','semilavorato','c_lavoro','note');
					$crud->set_relation('id_materiale','materiali','nome_materiale');
					$crud->required_fields('id_componente', 'quantita');
					$crud->callback_add_field('commessa',array($this,'commessaBolle'));
					$crud->callback_edit_field('commessa',array($this,'commessaBolle'));
					$crud->set_relation('id_trattamento','trattamenti','nome_trattamento');
					$crud->display_as('id_trattamento',"Trattamento");
					$crud->display_as('id_materiale',"Materiale");


					$crud->display_as('id_operatore',"Assegnato a");
					$crud->set_relation('id_operatore','operatori','nome_op');
					$crud->unset_delete();
					if($this->session->userdata('nome_operatore')!="DAVID")
						$crud->unset_add();
				//	$crud->callback_column('t_macch_extra',array($this,'tempoMacchinaExtraComponente'));

				break;
				case "fasi":
					$crud->set_table('fasi_commessa');
					$crud->columns('nome_fase','t_operatore','t_macchina','id_operatore','stato');
					$crud->add_action('TEMPI', '', 'omis/dettaglioop/fasi/'.$this->uri->segment(4).'/F','ui-icon-plus');
					$crud->add_action('COMPLETA', '', 'omis/fase_completata/'.$this->uri->segment(4),'ui-icon-plus');
					$crud->callback_column('t_operatore',array($this,'tempoOperatoriFase'));
					$crud->callback_column('t_macchina',array($this,'tempoMacchinaFase'));
					$crud->callback_column('stato',array($this,'completato_verde'));
					$crud->set_subject('Fase');
					$crud->set_relation('nome_fase','fasi_lavorazione','nome_fase');
					$crud->required_fields('nome_fase');
					$crud->callback_add_field('commessa',array($this,'commessaBolle'));
					$crud->callback_edit_field('commessa',array($this,'commessaBolle'));
					$crud->display_as('id_operatore',"Assegnato a");
					$crud->set_relation('id_operatore','operatori','nome_op');
					$crud->callback_edit_field('id_operatore',array($this,'elenco_operatori'));
					$crud->callback_add_field('id_operatore',array($this,'elenco_operatori'));
					$crud->unset_edit_fields('data_inizio','data_fine','stato','timestamp_completamento','id_operatore');
					$crud->unset_add_fields('data_inizio','data_fine','stato','timestamp_completamento','id_operatore');
					$crud->unset_delete();
				break;
				case "accessori":
					$crud->set_table('accessori_commessa');
					$crud->order_by("id_componente asc, codice_accessorio_interno", "asc");

					$crud->set_relation('id_componente','componenti','nome_componente');

					$crud->columns('nome_accessorio_interno','codice_accessorio_interno','ubicazione','id_componente','quantita','scaricato');
					$crud->add_action('Immagine', '', '','image-button',array($this,'apri_accessorio'));


					$crud->callback_column('ubicazione',array($this,'prendi_ubicazione'));
					$crud->callback_column('nome_accessorio_interno',array($this,'prendi_nome_accessorio_interno'));
					$crud->callback_column('scaricato',array($this,'scaricato_column'));

					$crud->display_as('quantita',"Quantita'");
					$crud->set_subject('Accessorio');
					$crud->callback_add_field('commessa',array($this,'commessaBolle'));
					$crud->callback_edit_field('commessa',array($this,'commessaBolle'));

					$crud->unset_add_fields('ubicazione','scaricato','timestamp_scaricamento');
					$crud->unset_edit_fields('ubicazione','scaricato','timestamp_scaricamento');

					$crud->display_as('id_componente',"Componente Accessorio");

					$crud->callback_column('nome_accessorio_interno',array($this,'prendi_nome_accessorio_interno'));

					$crud->callback_add_field('codice_accessorio_interno',array($this,'ListaAccessoriCodice'));
					$crud->callback_edit_field('codice_accessorio_interno',array($this,'ListaAccessoriCodice'));

					$crud->callback_add_field('id_componente',array($this,'ListaComponenteAccessorio'));
					$crud->callback_edit_field('id_componente',array($this,'ListaComponenteAccessorioEdit'));

					//if($this->session->userdata('nome_operatore')!="DAVID")
					//	$crud->unset_add();


					$crud->required_fields('codice_accessorio_interno');

					$crud->callback_delete(array($this,'callback_delete_accessorio'));
				break;
					case "lav_esterne":
						$crud->set_table('lav_esterne_commessa');
						$crud->set_subject('Lavorazione Esterna');
						$crud->set_relation('id_fornitore','fornitori','ragione_soc');
						$crud->set_relation('id_categoria','categorie_componenti','nome_categoria');
						$crud->display_as('id_categoria',"Componente");
						$crud->set_relation('id_tipo_lav','tipi_lav_esterne','nome_lav_esterna');
						$crud->display_as('id_fornitore',"Fornitore");
						$crud->display_as('id_tipo_lav',"Nome lavorazione esterna");
						$crud->unset_add();
						$crud->unset_columns('commessa','costo');

						$crud->unset_delete();
					break;
					case "semilavorati":
						$crud->set_table('semilavorati_commessa');
					//$crud->where('commessa',$this->uri->segment(4));
					$crud->set_subject('Semilavorato');
					$crud->set_relation('codice_semilavorato_interno','magazzino_semilavorati','nome_semilavorato');
					$crud->display_as('quantita',"Quantita'");
					$crud->unset_columns('commessa');
					$crud->unset_texteditor('note');
							if($this->session->userdata('nome_operatore')!="DAVID")
								$crud->unset_add();
						$crud->add_action('TEMPI', '', 'omis/dettaglioop/semilavorati/'.$this->uri->segment(4).'/S','ui-icon-plus');
						$crud->add_action('COMPLETA', '', 'omis/semilav_completato/'.$this->uri->segment(4),'ui-icon-plus');
						//$crud->callback_column('stato',array($this,'completato_verde'));

					$crud->unset_delete();


					break;
			}
			$crud->where('commessa',$this->uri->segment(4));
			if($this->session->userdata('nome_operatore')!="DAVID"){
				//$crud->unset_delete();
				$crud->unset_edit();
			}
			//$crud->add_action('LAVORAZIONI', '', 'omis/dettaglioop/'.$this->uri->segment(3).'/L','ui-icon-plus');

			$output = $crud->render();
			$output->intestazione=$this->intestazioneDettaglioOP();

			switch($this->uri->segment(5)){
					case "D":
						$output->footer=$this->footerComponenteDati();
					break;
					// case "L":
					// 	$output->footer=$this->footerComponenteLavorazioni();
					// break;
					case "T":
						$output->footer=$this->footerComponenteTempi();
					break;
					case "F":
						$output->footer=$this->footerFaseTempi();
					break;
					case "S":
						$output->footer=$this->footerSemilavoratiTempi();
					break;
					default:
						$output->footer="";
					break;
			}

			$output->commessa=$this->uri->segment(4);
			switch($stato){
				case 'C':
					$stato_titolo="IN CORSO";
				break;
				case 'D':
					$stato_titolo="IN PROGETTO";
				break;
				case 'S':
					$stato_titolo="IN SOSPESO";
				break;
				case 'E':
					$stato_titolo="ESEGUITO";
				break;
			}

			switch($tipo){
				case "MOD":
					$output->titolo_header="MODELLO";
				break;
				case "STA":
					$output->titolo_header='ATTREZZATURA COMPLETA';
				break;
				case "PRO":
					$output->titolo_header="PROGETTO";
				break;
				case "RO":
					$output->titolo_header="RULLI ORIZZONTALI";
				break;
				case "RV":
					$output->titolo_header="RULLI VERTICALI";
				break;
				case "VAR":
					$output->titolo_header="LAVORAZIONE VARIABILE";
				break;
				case "ROV":
					$output->titolo_header="RULLI ORIZZONTALI + VERTICALI";
				break;
				case "MDD":
					$output->titolo_header="MODIFICA SAPONE";
				break;
				case "MDS":
					$output->titolo_header="MODIFICA STAMPO";
				break;
				case "ESP":
						$output->titolo_header="TASSELLI";
				break;
				case "ACC":
						$output->titolo_header="ACCESSORI";
				break;
			}

				$output->titolo_header.=": ".$stato_titolo;



			$output->titolo=$this->uri->segment(4);
			$this->db->select_sum('t_operatore');
			$query = $this->db->get_where('lavorazioni',array('commessa'=>$this->uri->segment(4) , 'id_operatore' => $this->session->userdata('id_operatore')));
			foreach ($query->result() as $sum_tempi);
			($sum_tempi->t_operatore > 0) ? $output->ore_inserite=$sum_tempi->t_operatore : $output->ore_inserite="0";
			$this->n_componente=0;
			$output->nome_op=$this->session->userdata('nome_operatore');
			$output->menu=$this->build_menu();
			$output->footerDati="da inserire qui la chiamata al nuovo footerbuilder";
			$this->load->view('dettaglioop_view.php',$output);

	}

	function apri_accessorio($primary_key , $row)
	{

		return "ftp://".$_SERVER["SERVER_NAME"].":2222/accessori/".$row->codice_accessorio_interno.".jpg";
		//return "file:///C:/apri_server.bat";
	}

	function somma_kg_bolla($value, $row)
	{
		$this->db->select_sum('kg_consumati');
		$where = "consumi_materiali.cod_bolla = ".$row->cod_bolla." AND consumi_materiali.kg_consumati <> 0.00 ";
		$this->db->where($where);
		$query = $this->db->get('consumi_materiali');
		foreach ($query->result() as $sum_consumati);

		return $sum_consumati->kg_consumati;
	}


	function prendi_nome_accessorio_interno($value,$row){
		$que = $this->db->get_where('magazzino_accessori',array('codice_accessorio_interno'=>$row->codice_accessorio_interno));
		foreach ($que->result() as $accessorio);

		 return "<span class='codice_accessorio_interno_column' scaricato='".$row->scaricato."' codice='".$row->id_accessorio_commessa."'>".$accessorio->nome_accessorio_interno."</span>";
	}

	function gestionePresetComponenti($contarighe, $commessa_passata)// tutto da cambiare in id_materiale_matrice in caso verrà mai riutilizzato il preset
	{
			$where = "commessa = ".$commessa_passata;
			$que = $this->db->get_where('bolle_prod',$where);
			if($que->num_rows() == 0 || $contarighe==1)
			{
				$this->db->select('id_produttore, stampatrice, tipo_stampo, n_cavita, mat_matrice, mat_piastre');
				$where = "commessa = ".$commessa_passata;
				$query = $this->db->get_where('lavori',$where);
				foreach ($query->result() as $row);
				$cavita=$row->n_cavita;
				if($cavita=="")
				$cavita=1;
				$mat_matrice=$row->mat_matrice;
				$mat_piastre=$row->mat_piastre;
				$this->db->select('id_preset');
				$get = $this->db->get_where('preset',array('id_produttore'=> $row->id_produttore,
																	'stampatrice'=> $row->stampatrice,
					 												'tipo_stampo'=>$row->tipo_stampo));

				if($get->num_rows() > 0)																//Se esiste pre-set ma non ancora inseriti i componenti,fasi e accessori in bolle_prod,fasi_commessa
				{																						// e accessori_commessa li inserisco
					foreach ($get->result() as $row){
						$id_preset=$row->id_preset;
					}
					$query = $this->db->get_where('associa_componenti',array('id_preset'=>$id_preset));
					foreach ($query->result() as $row)
					{
						$materiale='';
						$compon = $this->db->get_where('componenti',array('id_componente'=>$row->id_componente));
						foreach ($compon->result() as $componente);
						(substr($componente->nome_componente, 0, 3) == 'Esp' || substr($componente->nome_componente, 0, 3) == 'Mas') ? $quantita=$cavita : $quantita = 1; //n� espulsori = n� cavita'
						if(substr($componente->nome_componente, 0, 3) == 'Esp' || substr($componente->nome_componente, 0, 3) == 'Mas' || substr($componente->nome_componente, 0, 8) == 'Cassetta' || substr($componente->nome_componente, 0, 7) == 'Matrice')
						$materiale=$mat_matrice;
						else
						{	if(substr($componente->nome_componente, 0, 7) == 'Piastra')
							$materiale=$mat_piastre;
						}
						$data = array(
				   				'id_componente' => $row->id_componente,
								'commessa'		=> $commessa_passata,
								'materiale'		=> $materiale,
								'quantita'     => $quantita
								);

							$this->db->insert('bolle_prod', $data);
					}

				}
			}
	}


/*	function visualizzaFasi()
	{
		$crudFasi = new grocery_CRUD();
		$crudFasi->set_theme('datatables');
		$crudFasi->set_table('fasi_commessa');
		$crudFasi->where('commessa',$this->uri->segment(3));
		$crudFasi->set_subject('Fase');
		$crudFasi->render();
	}*/

	function gestionePresetAccessori()
	{
			$where = "commessa = ".$this->uri->segment(4);
			$que = $this->db->get_where('accessori_commessa',$where);
			if($que->num_rows() == 0)
			{
				$this->db->select('id_produttore, stampatrice, tipo_stampo, n_cavita');
				$where = "commessa = ".$this->uri->segment(4);
				$query = $this->db->get_where('lavori',$where);
				foreach ($query->result() as $row);
				$this->db->select('id_preset');
				$get = $this->db->get_where('preset',array('id_produttore'=> $row->id_produttore,
																	'stampatrice'=> $row->stampatrice,
					 												'tipo_stampo'=>$row->tipo_stampo));

				if($get->num_rows() > 0)																//Se esiste pre-set ma non ancora inseriti i componenti,fasi e accessori in bolle_prod,fasi_commessa
				{																						// e accessori_commessa li inserisco
					foreach ($get->result() as $row){
						$id_preset=$row->id_preset;
					}
					$que = $this->db->get_where('associa_accessori',array('id_preset'=>$id_preset));
					foreach ($que->result() as $row)
					{
						$query = $this->db->get_where('accessori',array('id_accessorio'=>$row->id_accessorio));
						foreach ($query->result() as $access);
						($access->quantita_default != "") ? $quantita=$access->quantita_default : $quantita=1;

						$data = array(
				   			'id_accessorio' => $row->id_accessorio,
							'codice_accessorio' => $access->codice_accessorio,
							'quantita' => $quantita,
							'commessa'		=> $this->uri->segment(4)
							);

					$this->db->insert('accessori_commessa', $data);
					}


				}
			}
	}


	function dettaglio()
	{
		if($this->session->userdata('nome_operatore')=="DAVID")
		{
			redirect('/omis/dettaglioop/componenti/'.$this->uri->segment(4), 'refresh');
		}
		$ordini_semilavorati="";
		$this->setHeader=TRUE;
		$where = "commessa = ".$this->uri->segment(4);
		$que = $this->db->get_where('lavori',$where);
		foreach ($que->result() as $row){
		$tipo=$row->tipo_lavoro;
		$stato=$row->stato;
		}
		/*if($tipo=='STA'){												//				se	Attrezzatura COMPLETA verifico che esistano dei pre-set, altrimenti visualizzo tabella per
																		//				inserimento manuale componenti per Lavorazione rapida.
			$this->gestionePresetComponenti(0, $this->uri->segment(4));
			$this->gestionePresetAccessori();
		}*/					//preset abbandonati!!!!!!!!!!

			$crud = new grocery_CRUD();
			$crud->set_theme('datatables');
			switch($this->uri->segment(3)){
				case "componenti":
					$crud->set_table('bolle_prod');
					$crud->where('commessa',$this->uri->segment(4));
					$crud->set_subject('Componente');
					$crud->unset_texteditor('note');
					$crud->display_as('quantita',"Quantita'");
					$crud->display_as('id_componente',"Nome Componente");
					$crud->display_as('id_operatore',"Assegnato a");

					$crud->add_fields('id_componente','quantita', 'commessa','cod_disegno','id_trattamento','id_materiale','note','semilavorato','c_lavoro','misure_finito','id_operatore');
					$crud->edit_fields('id_componente','quantita' ,'commessa','cod_disegno','id_trattamento','id_materiale','note','semilavorato','c_lavoro',
					'escludi_ddt','misure_finito','id_operatore');
					//$crud->set_relation('id_componente','componenti','nome_componente');
					$crud->set_relation('id_operatore','operatori','nome_op');
					$crud->set_relation('id_materiale','materiali','nome_materiale');
					$crud->display_as('id_materiale',"Materiale");

					//$crud->set_relation('materiale','materiali','nome_materiale');
					$crud->add_action('Dett. Lav.', '', 'omis/dettaglio/componenti/'.$this->uri->segment(4).'/TS','ui-icon-plus');
					$crud->add_action('COMPLETATO', '', '','completa_button',array($this,'tasto_completa_uff'));
					$crud->columns('N°','id_componente','stato','id_materiale','quantita','id_trattamento','kg','note','c_lavoro','semilavorato','id_operatore','misure_finito');
					$crud->required_fields('id_componente', 'quantita');
					$crud->callback_column('stato',array($this,'completato_verde'));
					$crud->callback_column('id_componente',array($this,'riga_componente'));
					$crud->callback_column('N°',array($this,'numera_componente'));

					$crud->set_relation('id_trattamento','trattamenti','nome_trattamento');
					$crud->display_as('id_trattamento',"Trattamento");
					$crud->callback_edit_field('id_operatore',array($this,'elenco_operatori'));
					$crud->callback_add_field('id_operatore',array($this,'elenco_operatori'));
					$crud->callback_add_field('id_componente',array($this,'elenco_componenti'));
					$crud->callback_edit_field('id_componente',array($this,'elenco_componenti'));
					$crud->callback_after_update(array($this,'after_update_componente'));
					$crud->callback_after_insert(array($this,'after_insert_componente'));
					$crud->callback_column('kg',array($this,'somma_kg_bolla'));


				break;
				case "accessori":
					$crud->set_table('accessori_commessa');
					$crud->order_by("id_componente asc, codice_accessorio_interno", "asc");

					$crud->where('commessa',$this->uri->segment(4));
					$crud->unset_edit();
					$crud->set_subject('Accessorio');
					$crud->set_relation('codice_accessorio_interno','magazzino_accessori','codice_accessorio_interno');
					$crud->set_relation('id_componente','componenti','nome_componente');

					$crud->display_as('id_componente',"Componente Accessorio");

					$crud->columns('nome_accessorio_interno','codice_accessorio_interno','ubicazione','id_componente','quantita','scaricato');
					$crud->display_as('quantita',"Quantita'");

					$crud->callback_column('ubicazione',array($this,'prendi_ubicazione'));
					//$crud->add_action('Immagine', '', '','ui-icon-plus',array($this,'apri_accessorio'));
					$crud->add_action('Immagine', '', '','image-button',array($this,'apri_accessorio'));

					$crud->unset_add_fields('ubicazione','scaricato','timestamp_scaricamento');
					$crud->unset_edit_fields('ubicazione','scaricato','timestamp_scaricamento');

					$crud->callback_add_field('codice_accessorio_interno',array($this,'ListaAccessoriCodice'));
					$crud->callback_edit_field('codice_accessorio_interno',array($this,'ListaAccessoriCodice'));


					$crud->callback_add_field('id_componente',array($this,'ListaComponenteAccessorio'));
					$crud->callback_edit_field('id_componente',array($this,'ListaComponenteAccessorioEdit'));

					$crud->required_fields('codice_accessorio_interno');
					$crud->callback_column('nome_accessorio_interno',array($this,'prendi_nome_accessorio_interno'));

					$crud->callback_delete(array($this,'callback_delete_accessorio'));
					//$crud->callback_after_insert(array($this,'after_insert_accessorio'));
					//$crud->callback_before_delete(array($this,'before_delete_accessorio'));
					//$crud->callback_before_update(array($this,'before_update_accessorio'));

				break;
				case "fasi":
					$crud->set_table('fasi_commessa');
					$crud->where('commessa',$this->uri->segment(4));
					$crud->columns('nome_fase','id_operatore','stato');
					$crud->set_subject('Fase');
					$crud->set_relation('nome_fase','fasi_lavorazione','nome_fase');
					$crud->add_action('Dett. Lav.', '', 'omis/dettaglio/componenti/'.$this->uri->segment(4).'/TF','ui-icon-plus');
					$crud->add_action('COMPLETA', '', 'omis/fase_completata_uff/'.$this->uri->segment(4),'ui-icon-plus');
					$crud->display_as('id_operatore',"Assegnato a");
					$crud->set_relation('id_operatore','operatori','nome_op');
					$crud->callback_edit_field('id_operatore',array($this,'elenco_operatori'));
					$crud->callback_add_field('id_operatore',array($this,'elenco_operatori'));
					$crud->required_fields('nome_fase');
					$crud->unset_add_fields('stato','data_inizio','data_fine','timestamp_completamento');
					$crud->unset_edit_fields('stato','data_inizio','data_fine','timestamp_completamento');
					$crud->callback_after_update(array($this,'after_update_fase'));
					$crud->callback_after_insert(array($this,'after_insert_fase'));

					$crud->callback_column('stato',array($this,'completato_verde'));

				break;
				case "lav_esterne":
					$crud->set_table('lav_esterne_commessa');
					$crud->where('commessa',$this->uri->segment(4));

					$crud->set_relation('id_fornitore','fornitori','ragione_soc');
					$crud->set_relation('id_tipo_lav','tipi_lav_esterne','nome_lav_esterna');
					$crud->set_relation('id_categoria','categorie_componenti','nome_categoria');
					$crud->display_as('id_categoria',"Componente");
					$crud->display_as('id_fornitore',"Fornitore");
					$crud->display_as('id_tipo_lav',"Nome lavorazione esterna");
					$crud->unset_columns('commessa');
					$crud->unset_add();
					$crud->unset_delete();
					$crud->edit_fields('ddt_fornitore','data_ddt_fornitore','costo');
					$crud->unset_texteditor('note');

					$crud->callback_after_update(array($this,'cambiaStatoLavEst'));



				break;
				case "semilavorati":
					$crud->set_table('semilavorati_commessa');
					$crud->where('commessa',$this->uri->segment(4));
					$crud->set_subject('Semilavorato');
					$crud->set_relation('codice_semilavorato_interno','magazzino_semilavorati','nome_semilavorato');
					$crud->display_as('quantita',"Quantita'");
					$crud->unset_columns('commessa');
					$crud->unset_texteditor('note');
					$this->load->library('table');
					$tmpl = array (
					                    'table_open'          => '<table style="text-align: left; width: 411px;" border="1" cellpadding="2" cellspacing="1">',

					                    'heading_row_start'   => '<tr>',
					                    'heading_row_end'     => '</tr>',
					                    'heading_cell_start'  => '<th>',
					                    'heading_cell_end'    => '</th>',

					                    'row_start'           => '<tr>',
					                    'row_end'             => '</tr>',
					                    'cell_start'          => '<td>',
					                    'cell_end'            => '</td>',

					                    'row_alt_start'       => '<tr>',
					                    'row_alt_end'         => '</tr>',
					                    'cell_alt_start'      => '<td>',
					                    'cell_alt_end'        => '</td>',

					                    'table_close'         => '</table>'
					              );

					$this->table->set_template($tmpl);

					$this->table->set_heading(' NOME SEMILAVORATO', " Q.TA'", "STATO");
					$query=$this->db->get_where('ordini_semilavorati',array('commessa'=>$this->uri->segment(4)));
					if($query->num_rows()>0){
					foreach ($query->result() as $row){
							$query=$this->db->get_where('anagrafica_semilavorati',array('id_semilav_ana'=>$row->id_semilav_ana));
							foreach ($query->result() as $ana_sem);

							$this->table->add_row($ana_sem->nome_semilavorato, $row->quantita_semilav,$row->stato_ordine);
						}
					$ordini_semilavorati="<br>".$this->table->generate();

					}else{
						$ordini_semilavorati="";
					}
				$crud->callback_after_insert(array($this,'after_insert_semilavorato'));
				$crud->callback_before_delete(array($this,'before_delete_semilavorato'));
				$crud->callback_before_update(array($this,'before_update_semilavorato'));

				break;


			}

			$crud->callback_add_field('commessa',array($this,'commessaBolle'));
			$crud->callback_edit_field('commessa',array($this,'commessaBolle'));
			$output = $crud->render();
			$output->ordini_semilavorati=$ordini_semilavorati;
			$output->commessa=$this->uri->segment(4);
			($this->uri->segment(5)=='TS') ? $output->footer=$this->footerComponenteTempiNonModificabileComponenti() : $output->footer="";
			if($this->uri->segment(5)=='TF')
				$output->footer=$this->footerComponenteTempiNonModificabileFasi();

			$output->nome_op=$this->session->userdata('nome_operatore');

			if($this->setHeader==TRUE){
			 	$output->intestazione=$this->intestazioneDettaglio();
				}
			else{
			  	$output->intestazione='';  //se devo inserire un nuovo componente non devo visualizz intestazione.s
				$output->goback="";

			}
			switch($stato){
				case 'C':
					$stato_titolo="IN CORSO";
				break;
				case 'D':
					$stato_titolo="IN PROGETTO";
				break;
				case 'S':
					$stato_titolo="IN SOSPESO";
				break;
				case 'E':
					$stato_titolo="ESEGUITO";
				break;
			}

			switch($tipo){
				case "MOD":
					$output->titolo_header="MODELLO";
				break;
				case "STA":
					$output->titolo_header='ATTREZZATURA COMPLETA';
				break;
				case "PRO":
					$output->titolo_header="PROGETTO";
				break;
				case "RO":
					$output->titolo_header="RULLI ORIZZONTALI";
				break;
				case "RV":
					$output->titolo_header="RULLI VERTICALI";
				break;
				case "VAR":
					$output->titolo_header="LAVORAZIONE VARIABILE";
				break;
				case "ROV":
					$output->titolo_header="RULLI ORIZZONTALI + VERTICALI";
				break;
				case "MDD":
					$output->titolo_header="MODIFICA SAPONE";
				break;
				case "MDS":
					$output->titolo_header="MODIFICA STAMPO";
				break;
				case "ESP":
						$output->titolo_header="TASSELLI";
				break;
				case "ACC":
						$output->titolo_header="ACCESSORI";
				break;
			}

				$output->titolo_header.=": ".$stato_titolo;


			$output->titolo=$this->uri->segment(4);
			$this->n_componente=0;
			$output->menu=$this->build_menu();

			$this->load->view('dettaglio_commessa_view.php',$output);

	}

	function tasto_completa_uff($primary_key , $row){
		if(!strpos($row->stato,"COMPLETATO"))
			$indirizzo= site_url('omis/bolla_completata_uff/'.$row->commessa."/".$primary_key);
		else $indirizzo="NO";
		return $indirizzo;
	}

	function tasto_completa($primary_key , $row){
		if(!strpos($row->stato,"COMPLETATO"))
			$indirizzo= site_url('omis/bolla_completata/'.$row->commessa."/".$primary_key);
		else $indirizzo="NO";
		return $indirizzo;
	}

	function callback_delete_accessorio($primary_key)//se accessorio già scaricato da magazzino non posso eliminarlo
	{
		$acc = $this->db->get_where('accessori_commessa',array('id_accessorio_commessa'=>$primary_key));
		foreach($acc->result() as $accessorio_comm);
		if($accessorio_comm->scaricato=="S"){
			$acc = $this->db->get_where('magazzino_accessori',array('codice_accessorio_interno'=>$accessorio_comm->codice_accessorio_interno));
			foreach($acc->result() as $accessorio_magazz);
			$this->db->where('codice_accessorio_interno', $accessorio_comm->codice_accessorio_interno);
			$this->db->update('magazzino_accessori', array('qta_disponibile'=>$accessorio_magazz->qta_disponibile+$accessorio_comm->quantita));
		}
		$this->db->delete('accessori_commessa', array('id_accessorio_commessa'=>$primary_key));
		return true;
	}

	function scarica_magazzino_accessori(){
		date_default_timezone_set('Europe/Rome');
		foreach ($_POST['accessori_scaricare'] as $id_accessorio_commessa) {
			$acc = $this->db->get_where('accessori_commessa',array('id_accessorio_commessa'=>$id_accessorio_commessa));
			foreach($acc->result() as $accessorio_comm);
			$acc = $this->db->get_where('magazzino_accessori',array('codice_accessorio_interno'=>$accessorio_comm->codice_accessorio_interno));
			foreach($acc->result() as $accessorio_magazz);
			$this->db->where('codice_accessorio_interno', $accessorio_comm->codice_accessorio_interno);
			$this->db->update('magazzino_accessori', array('qta_disponibile'=>$accessorio_magazz->qta_disponibile-$accessorio_comm->quantita));

			$this->db->where('id_accessorio_commessa', $id_accessorio_commessa);
			$this->db->update('accessori_commessa', array('scaricato'=>'S','timestamp_scaricamento'=>date("Y-m-d H:i:s")));
		}

		echo "ciao";
	}

	function calcola_totali_mensili(){
		date_default_timezone_set('Europe/Rome');
		$year=date('Y');
		$stringa="";
		$mesi = array("asd","Gennaio","Febbraio","Marzo","Aprile","Maggio","Giugno","Luglio","Agosto","Settembre","Ottobre","Novembre","Dicembre");
		$pagamenti = array(10,12,3,6,13,5,1);
		$nomi_pagamenti = array("RBI","RID","BONIFICO","CASSA","SDD","CARTA DI CREDITO","ASSEGNO");
		for($i=1;$i<13;$i++){
			$stringa.="<b>".$mesi[$i]."</b>: <br><br>";
			for($j=0;$j<7;$j++){
				$sum=0.00;
				if($j==0){
					if($i<12)
						$where="id_tipo_pagamento = ".$pagamenti[$j]." AND tipo_fattura='F' AND (data_scadenza_fattura  BETWEEN '".date("Y-m-d", strtotime($year."-".$i."-20"))."' AND '".date("Y-m-d", strtotime($year."-".($i+1)."-15"))."')";
					else
						$where="id_tipo_pagamento = ".$pagamenti[$j]." AND tipo_fattura='F' AND (data_scadenza_fattura  BETWEEN '".$year."-12-20' AND '".($year+1)."-01-15')";
				}else{
						$where="id_tipo_pagamento = ".$pagamenti[$j]." AND tipo_fattura='F' AND (data_scadenza_fattura  BETWEEN '".date("Y-m-d", strtotime($year."-".$i."-01"))."' AND '".date("Y-m-t", strtotime($year."-".$i."-01"))."')";
				}

				$query=$this->db->get_where('fatture_fornitori',$where);
				foreach($query->result() as $fattura){
					$sum+=floatval($fattura->importo);
				}
				$stringa.=$nomi_pagamenti[$j].": ".number_format($sum,2)."€  <br>";
			}
			$stringa.="<br>";
		}
		echo $stringa;
	}

	function after_insert_semilavorato($post_array,$insert_id){

		$acc = $this->db->get_where('magazzino_semilavorati',array('codice_semilavorato_interno'=>$post_array['codice_semilavorato_interno']));
		foreach($acc->result() as $semilavorato_magazz);
		$this->db->where('codice_semilavorato_interno', $post_array['codice_semilavorato_interno']);
		$this->db->update('magazzino_semilavorati', array('qta_disponibile'=>$semilavorato_magazz->qta_disponibile-($post_array['quantita'])));

	}
	function ricarica_magazzino_accessori(){
		$id_accessorio_commessa=$_POST['id_accessorio_commessa'];
		$acc = $this->db->get_where('accessori_commessa',array('id_accessorio_commessa'=>$id_accessorio_commessa));
		foreach($acc->result() as $accessorio_comm);
		$acc = $this->db->get_where('magazzino_accessori',array('codice_accessorio_interno'=>$accessorio_comm->codice_accessorio_interno));
		foreach($acc->result() as $accessorio_magazz);
		$this->db->where('codice_accessorio_interno', $accessorio_comm->codice_accessorio_interno);
		$this->db->update('magazzino_accessori', array('qta_disponibile'=>$accessorio_magazz->qta_disponibile+$accessorio_comm->quantita));

		$this->db->where('id_accessorio_commessa', $id_accessorio_commessa);
		$this->db->update('accessori_commessa', array('scaricato'=>'N','timestamp_scaricamento'=>NULL));

		echo "ciao";
	}
	function before_update_accessorio($post_array,$primary_key){
		$this->db->where('id_accessorio_commessa',$primary_key);
    	$accessorio = $this->db->get('accessori_commessa')->row();
    	if($post_array['codice_accessorio_interno']==$accessorio->codice_accessorio_interno){
   //  		$acc = $this->db->get_where('magazzino_accessori',array('codice_accessorio_interno'=>$accessorio->codice_accessorio_interno));
			// foreach($acc->result() as $accessorio_magazz);
			// $this->db->where('codice_accessorio_interno', $accessorio->codice_accessorio_interno);
			// $this->db->update('magazzino_accessori', array('qta_disponibile'=>$accessorio_magazz->qta_disponibile+($accessorio->quantita)-$post_array['quantita']));
    	}else{
   //  		$acc = $this->db->get_where('magazzino_accessori',array('codice_accessorio_interno'=>$accessorio->codice_accessorio_interno));
			// foreach($acc->result() as $accessorio_magazz);
			// $this->db->where('codice_accessorio_interno', $accessorio->codice_accessorio_interno);
			// $this->db->update('magazzino_accessori', array('qta_disponibile'=>$accessorio_magazz->qta_disponibile+($accessorio->quantita)));
			//dopo aver riaggiunto la quantita del vecchio codice accessorio alla quantità disponibile in magazzino ora scarico da magazzino la quantità del nuovo accessorio:
			$acc = $this->db->get_where('magazzino_accessori',array('codice_accessorio_interno'=>$post_array['codice_accessorio_interno']));
			foreach($acc->result() as $accessorio_magazz);
			// $this->db->where('codice_accessorio_interno', $post_array['codice_accessorio_interno']);
			// $this->db->update('magazzino_accessori', array('qta_disponibile'=>$accessorio_magazz->qta_disponibile-$post_array['quantita']));
			//aggiorno ubicazione nuovo accessorio
			$post_array['ubicazione']=$accessorio_magazz->ubicazione;
    	}

	}
	function before_delete_semilavorato($primary_key){
		$this->db->where('id_semilavorato_commessa',$primary_key);
		$semilavorato = $this->db->get('semilavorati_commessa')->row();
		$acc = $this->db->get_where('magazzino_semilavorati',array('codice_semilavorato_interno'=>$semilavorato->codice_semilavorato_interno));
		foreach($acc->result() as $semilavorato_magazz);
		$this->db->where('codice_semilavorato_interno', $semilavorato->codice_semilavorato_interno);
		$this->db->update('magazzino_semilavorati', array('qta_disponibile'=>$semilavorato_magazz->qta_disponibile+($semilavorato->quantita)));
	}
	function before_update_semilavorato($post_array,$primary_key){
		$this->db->where('id_semilavorato_commessa',$primary_key);
		$semilavorato = $this->db->get('semilavorati_commessa')->row();
		$acc = $this->db->get_where('magazzino_semilavorati',array('codice_semilavorato_interno'=>$semilavorato->codice_semilavorato_interno));
		foreach($acc->result() as $semilavorato_magazz);
		$this->db->where('codice_semilavorato_interno', $semilavorato->codice_semilavorato_interno);
		$this->db->update('magazzino_semilavorati', array('qta_disponibile'=>$semilavorato_magazz->qta_disponibile+($semilavorato->quantita)-$post_array['quantita']));
	}
	function ListaAccessoriCodice($value, $primary_key)
	{
		if(!(isset($value)&&isset($primary_key)))
		{
			$value="";
			$primary_key="";
		}

		$class="id='' class='chosen-select'";
		$query = $this->db->get('magazzino_accessori');
		$asd['']='';

		foreach ($query->result() as $row)
		{
			$asd[$row->codice_accessorio_interno]=$row->codice_accessorio_interno." - ".$row->nome_accessorio_interno;
		}

		return form_dropdown('codice_accessorio_interno', $asd, $value,$class);
	}
	function ListaSemilavoratiCodice($value, $primary_key)
	{
		if(!(isset($value)&&isset($primary_key)))
		{
			$value="";
			$primary_key="";
		}

		$class="id='' class='chosen-select'";
		$query = $this->db->get('magazzino_semilavorati');
		$asd['']='';

		foreach ($query->result() as $row)
		{
			$asd[$row->codice_semilavorato_interno]=$row->codice_semilavorato_interno." - ".$row->nome_semilavorato;
		}

		return form_dropdown('codice_semilavorato_interno', $asd, $value,$class);
	}



	function after_update_componente($post_array,$primary_key){

		$query=$this->db->get_where('bolle_prod',array('cod_bolla'=>$primary_key));
		foreach ($query->result() as $bolla);
		$query=$this->db->get_where('lavori',array('commessa'=>$post_array['commessa']));
		unset($post_array);
		foreach ($query->result_array() as $post_array);
		@list($year,$month,$day) = explode("-",$post_array['data_consegna_interna']);
		$post_array['data_consegna_interna']=$day.'/'.$month.'/'.$year;
		$this->calcola_data_inizio($bolla,$post_array,1);
		$this->matrice_espulsori($post_array['commessa']);

	}
	function after_update_fase($post_array,$primary_key){

		$query=$this->db->get_where('fasi_commessa',array('id_fase_commessa'=>$primary_key));
		foreach ($query->result() as $fase);
		$query=$this->db->get_where('lavori',array('commessa'=>$post_array['commessa']));
		unset($post_array);
		foreach ($query->result_array() as $post_array);

			@list($year,$month,$day) = explode("-",$post_array['data_consegna_interna']);
			$post_array['data_consegna_interna']=$day.'/'.$month.'/'.$year;

			@list($year,$month,$day) = explode("-",$post_array['data_cons_dis_prev']);
			$post_array['data_cons_dis_prev']=$day.'/'.$month.'/'.$year;

			@list($year,$month,$day) = explode("-",$post_array['data_approv_dis']);
			$post_array['data_approv_dis']=$day.'/'.$month.'/'.$year;

		$this->calcola_data_inizio($fase,$post_array,2);
	}
	function after_insert_fase($post_array,$insert_id){

		$query=$this->db->get_where('fasi_commessa',array('id_fase_commessa'=>$insert_id));
		foreach ($query->result() as $fase);
		$query=$this->db->get_where('lavori',array('commessa'=>$post_array['commessa']));
		//unset($post_array);
		foreach ($query->result_array() as $post_array);
		@list($year,$month,$day) = explode("-",$post_array['data_consegna_interna']);
		$post_array['data_consegna_interna']=$day.'/'.$month.'/'.$year;
		$this->calcola_data_inizio($fase,$post_array,2);
	}



	function after_insert_componente($post_array,$insert_id){
		$query=$this->db->get_where('bolle_prod',array('cod_bolla'=>$insert_id));
		foreach ($query->result() as $bolla);
		$query=$this->db->get_where('lavori',array('commessa'=>$post_array['commessa']));
		unset($post_array);
		foreach ($query->result_array() as $post_array);
		@list($year,$month,$day) = explode("-",$post_array['data_consegna_interna']);
		$post_array['data_consegna_interna']=$day.'/'.$month.'/'.$year;
		$this->calcola_data_inizio($bolla,$post_array,1);
		$this->matrice_espulsori($post_array['commessa']);
	}

	function matrice_espulsori($commessa)
	{
		$this->db->select('*');
		$this->db->from('bolle_prod');
		$this->db->join('componenti', 'bolle_prod.id_componente = componenti.id_componente');
		$where = "bolle_prod.commessa = ".$commessa." AND (componenti.id_categoria = 1 OR componenti.id_categoria BETWEEN 23 AND 32 OR componenti.id_categoria = 41)";
		$this->db->where($where);
		$this->db->order_by("bolle_prod.data_inizio", "asc");
		$this->db->limit(1);
		$que = $this->db->get();
		if($que->num_rows()>0){
			foreach ($que->result() as $matrice);
			$this->db->select('*');
			$this->db->from('bolle_prod');
			$this->db->join('componenti', 'bolle_prod.id_componente = componenti.id_componente');
			$where = "bolle_prod.commessa = ".$commessa." AND (componenti.id_categoria = 2 OR componenti.id_categoria = 4)";
			$this->db->where($where);
			$que = $this->db->get();
			foreach ($que->result() as $espulsore)
			{
				$this->db->where('cod_bolla', $espulsore->cod_bolla);
				$this->db->update('bolle_prod', array("data_inizio" => $matrice->data_inizio));
			}

		}
	}



	function elenco_operatori($value, $primary_key)
	{
		$where="tipo_op = 'OPE' OR tipo_op = 'DIS' OR tipo_op = 'UFF'";
		$query = $this->db->get_where('operatori', $where);
		$asd[0]='';

		foreach ($query->result() as $row)
		{
			$asd[$row->id_operatore]=$row->nome_op;
		}

		return form_dropdown('id_operatore', $asd, $value);

	}





	function elenco_componenti($value, $primary_key)
	{
		if(!(isset($value)&&isset($primary_key)))
		{
			$value="";
			$primary_key="";
		}

		$class="class='chosen-select'";
		$query = $this->db->get('componenti');
		$asd['']='';

		foreach ($query->result() as $row)
		{
			$asd[$row->id_componente]=$row->nome_componente;
		}

		return form_dropdown('id_componente', $asd, $value,$class);

	}

	function numera_componente(){
		$this->n_componente++;
		return $this->n_componente;
	}

	function riga_componente($value,$row){


		$query = $this->db->get_where('bolle_prod', array("cod_bolla"=>$row->cod_bolla));
		foreach ($query->result() as $bolla);
		$query = $this->db->get_where('componenti', array("id_componente"=>$bolla->id_componente));
		foreach ($query->result() as $compo);
		($bolla->escludi_ddt=="S")? $attr="class='escludi_riga' cod_bolla='".$bolla->cod_bolla."'":$attr=" class='includi_riga'   cod_bolla='".$bolla->cod_bolla."'";

		return "<div ".$attr." >".$compo->nome_componente."</div>";

	}

	function ListaComponenteAccessorio()
	{
		$where="commessa = ".$this->uri->segment(4);

		$this->db->select('id_componente');
		$query = $this->db->get_where('bolle_prod', $where);
		$asd['']='';

		foreach ($query->result() as $row)
		{
			$this->db->select('nome_componente');
			$query1 = $this->db->get_where('componenti', array('id_componente'=>$row->id_componente));
			foreach ($query1->result() as $componente);
			$asd[$row->id_componente]=$componente->nome_componente;
		}

		return form_dropdown('id_componente', $asd);
	}

	function ListaComponenteAccessorioEdit()
	{
		$where="commessa = ".$this->uri->segment(4);

		$this->db->select('id_componente');
		$query = $this->db->get_where('bolle_prod', $where);
		$asd['']='';
		foreach ($query->result() as $row)
		{
			$this->db->select('nome_componente');
			$query1 = $this->db->get_where('componenti', array('id_componente'=>$row->id_componente));
			foreach ($query1->result() as $componente);
			$asd[$row->id_componente]=$componente->nome_componente;
		}
		$this->db->select('id_componente');
		$query2 = $this->db->get_where('accessori_commessa', array('id_accessorio_commessa'=>$this->uri->segment(6)));
		foreach ($query2->result() as $componente1);
		$compo_sel=$componente1->id_componente;

		return form_dropdown('id_componente', $asd,$compo_sel);
	}








	function cambiaStatoLavEst($post_array,$primary_key)
	{
		if($post_array['ddt_fornitore']!=""){
			$this->db->where('id_lav_esterna', $primary_key);
			$this->db->update('lav_esterne_commessa', array("stato_lav_est" => 'DENTRO'));
		}else{
			$this->db->where('id_lav_esterna', $primary_key);
			$this->db->update('lav_esterne_commessa', array("stato_lav_est" => 'FUORI'));
		}
	}

	function mandaincorso()
	{
		$data = array('stato' => 'C');
		$where = "commessa = ".$this->uri->segment(3);
		$str = $this->db->update_string('lavori', $data, $where);
		$this->db->query($str);

		$lavoro = $this->db->get_where('lavori', $where);
		foreach ($lavoro->result_array() as $post_array);
		foreach ($lavoro->result() as $lavoro);
		@list($year,$month,$day) = explode("-",$lavoro->data_consegna_interna);

		//COMMENTATO PER EVITARE RIDONDANZE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		// if($post_array['data_consegna_interna']!=NULL)
		// {
		// 	$post_array['data_consegna_interna']=$day.'/'.$month.'/'.$year;

		// 	$this->db->select('*');
		// 	$this->db->from('bolle_prod');
		// 	$this->db->join('lavori', 'bolle_prod.commessa = lavori.commessa');
		// 	$where = "lavori.stato = 'C' AND lavori.commessa=".$this->uri->segment(3)." AND (bolle_prod.stato='INIZIARE' OR bolle_prod.stato='IN CORSO')";
		// 	$this->db->where($where);
		// 	$que = $this->db->get();
		// 	foreach ($que->result() as $bolla){
		// 		$this->calcola_data_inizio($bolla,$post_array,1);
		// 	}

		// 	$this->db->select('*');
		// 	$this->db->from('fasi_commessa');
		// 	$this->db->join('lavori', 'fasi_commessa.commessa = lavori.commessa');
		// 	$where = "lavori.stato = 'C' AND lavori.commessa=".$this->uri->segment(3)." AND (fasi_commessa.stato='INIZIARE' OR fasi_commessa.stato='IN CORSO')";
		// 	$this->db->where($where);
		// 	$que = $this->db->get();

		// 	foreach ($que->result() as $fase){
		// 		$this->calcola_data_inizio($fase,$post_array,2);
		// 	}
		// 	$this->matrice_espulsori($post_array['commessa']);


		// }
		redirect('/omis/lavori/progetti', 'refresh');
	}
	function tornaprogetto()
	{
		$data = array('stato' => 'D');
		$where = "commessa = ".$this->uri->segment(3);
		$str = $this->db->update_string('lavori', $data, $where);
		$this->db->query($str);
		redirect('/omis/lavori/incorso', 'refresh');
	}
	function tornaprogS()
	{
		$this->db->where('commessa', $this->uri->segment(3));
		$this->db->update('lavori', array("stato" => 'D'));
		redirect('/omis/lavori/sospesi', 'refresh');
	}
	function tornacorsoS()
	{
		$this->db->where('commessa', $this->uri->segment(3));
		$this->db->update('lavori', array("stato" => 'C'));
		redirect('/omis/lavori/sospesi', 'refresh');
	}
	function sospendi()
	{
		$data = array('stato' => 'S');
		$where = "commessa = ".$this->uri->segment(4);
		$str = $this->db->update_string('lavori', $data, $where);
		$this->db->query($str);
 		$backto =$this->uri->segment(3);
		redirect('/omis/lavori/'.$backto, 'refresh');
	}


	function prendi_ubicazione($value,$row)
	{

		$query = $this->db->get_where('magazzino_accessori', array('codice_accessorio_interno' => $row->codice_accessorio_interno));
		foreach ($query->result() as $rowRes);

		return $rowRes->ubicazione;

	}
	function scaricato_column($value,$row)
	{

		if($value=='S') $field="<div class='scaricato_column' style='background-color:green;'>".$value."</div>";
		else $field=$value;

		return $field;

	}

	function aggiungiAClienti()
	{
		$query = $this->db->get_where('produttori', array('id_produttore' => $this->uri->segment(3)));
		foreach ($query->result() as $row);
		$data = array(
		   'ragione_soc' => $row->ragione_soc
		);
		$this->db->insert('clienti', $data);
		redirect('/omis/produttori', 'refresh');
	}
	function ListaStampatriciEdit()
	{
		$js='class="lista-stamp"' ;
		if($this->uri->segment(2)=='lavori')
		$where="commessa = ".$this->uri->segment(5);
		else
		$where="commessa = ".$this->uri->segment(4);

		$this->db->select('id_produttore, stampatrice');
		$query = $this->db->get_where('lavori', $where);
		foreach ($query->result() as $row)
		{
	   		$id_produttore=$row->id_produttore;
			$stamp_sel=$row->stampatrice;
		}
		$where="id_produttore = '".$id_produttore."'";
		$this->db->select('nome_stamp');
		$query = $this->db->get_where('client_stamp', $where);
		$asd['']="";
		foreach ($query->result() as $row)
		{
	  		$asd[$row->nome_stamp]=$row->nome_stamp;
		}
		$this->setHeader=FALSE;
		return form_dropdown('stampatrice', $asd ,$stamp_sel, $js);
	}

	function ListaStampatriciEditAuto()
	{
		$js='class="lista-stamp"' ;

		$where="id_preset = ".$this->uri->segment(4);
		$this->db->select('id_produttore, stampatrice');
		$query = $this->db->get_where('preset', $where);
		foreach ($query->result() as $row);
	   		$id_produttore=$row->id_produttore;
			$stamp_sel=$row->stampatrice;

		$where="id_produttore = ".$id_produttore;
		$this->db->select('nome_stamp');
		$query = $this->db->get_where('client_stamp', $where);
		$asd['']="";
		if($query->num_rows()>0)						//in caso un produttore non abbia nessuna stampatrice definita nei preset(ma non dovrebbe accadere eccetto per COMMESSE INTERNE )
		{
			foreach ($query->result() as $row)
	  			$asd[$row->nome_stamp]=$row->nome_stamp;
		}
		$this->setHeader=FALSE;
		return form_dropdown('stampatrice', $asd ,$stamp_sel,$js);
	}






	function ListaStampatrici()
	{
		$js='class="lista-stamp"' ;
			$asd['']="";

		$this->setHeader=FALSE;
		return form_dropdown('stampatrice', $asd,'',$js);
	}
	function TipoLavoroEdit()
	{
		$js='' ;

			$this->db->select('tipo_lavoro');
				if($this->uri->segment(2)=='lavori')
				$where="commessa = ".$this->uri->segment(5);
				else
				$where="commessa = ".$this->uri->segment(4);
			$que = $this->db->get_where('lavori',$where);
			if($que->num_rows>0)
			{	foreach ($que->result() as $row)
				{
			   		$selected=$row->tipo_lavoro;
				}
			}else{
				$selected=0;
			}

		$lista=array('PRO' => "PROGETTO", 'MOD' => "MODELLO", 'STA' =>"ATTREZZATURA COMPLETA", 'VAR' => 'VARIABILE','ESP' => 'ESPULSORI', 'RO' => 'RULLI ORIZZONTALI', 'RV'=>'RULLI VERTICALI', 'ROV' => 'RULLI ORIZZONTALI + VERTICALI', 'ACC' => 'ACCESSORI', 'MDD' => 'MODIFICA SAPONE','MDS' => 'MODIFICA STAMPO');
		return form_dropdown('tipo_lavoro', $lista, $selected,$js );

	}
	function TipoLavoro()
	{
		$js='' ;

		$lista=array('PRO' => "PROGETTO", 'MOD' => "MODELLO", 'STA' =>"ATTREZZATURA COMPLETA", 'VAR' => 'VARIABILE','ESP' => 'ESPULSORI', 'RO' => 'RULLI ORIZZONTALI', 'RV'=>'RULLI VERTICALI', 'ROV' => 'RULLI ORIZZONTALI + VERTICALI', 'ACC' => 'ACCESSORI', 'MDD' => 'MODIFICA SAPONE','MDS' => 'MODIFICA STAMPO');
		return form_dropdown('tipo_lavoro', $lista,'',$js);

	}

	function ListaProduttoriEdit()
	{
		$this->db->select('ragione_soc');
		$js=' id="ListaProduttori" class="chosen-select" name="lista-prod"' ;
		$where = "commessa = ".$this->uri->segment(5);
		$que = $this->db->get_where('lavori',$where);
		foreach ($que->result() as $row)
		{
	   		$cliente=$row->ragione_soc;
		}
		$query=$this->db->get('produttori');
		$asd['']="";
		foreach ($query->result() as $row)
		{
		   $asd[$row->ragione_soc]=$row->ragione_soc;
		}
		return form_dropdown('ragione_soc', $asd ,$cliente,$js);
	}


	function ListaProduttoriEditAuto()
	{
		$js=' id="ListaProduttori" class=""' ;
		$where = "id_preset = ".$this->uri->segment(4);
		$this->db->select('ragione_soc');
		$query = $this->db->get_where('preset', $where);
			foreach ($query->result() as $row)
			{
		   		$cliente=$row->ragione_soc;
			}
		$query=$this->db->get('produttori');
		$asd['']="";
		foreach ($query->result() as $row)
		{
		   $asd[$row->ragione_soc]=$row->ragione_soc;
		}
				return form_dropdown('ragione_soc', $asd ,$cliente,$js);
	}
	function ListaProduttori()
	{
		$this->db->select('ragione_soc');
		$query=$this->db->get('produttori');
		$js=' id="ListaProduttori" class=""' ;
		$asd['']="";
		foreach ($query->result() as $row)
		{
		   $asd[$row->ragione_soc]=$row->ragione_soc;
		}
		$selected='';
		return form_dropdown('ragione_soc', $asd, $selected,$js);
	}

	/*function ListaClienti()
	{
		$query=$this->db->get('clienti');
		$js=' id="" class=""' ;
		$asd[1]="";
		foreach ($query->result() as $row)
		{
		   $asd[$row->id_cliente]=$row->ragione_soc;
		}
		$selected='';
		return form_dropdown('cliente', $asd, $selected,$js);
	}*/


	function listini(){
			$crud = new grocery_CRUD();
			$crud->set_theme('datatables');
			switch($this->uri->segment(3)){
				case "stampi":

					$crud->set_table('listini_stampi');
					$crud->set_subject('Listino');
				/*	$crud->callback_add_field('ragione_soc',array($this,'ListaProduttori'));
					$crud->callback_edit_field('ragione_soc',array($this,'ListaProduttoriEditAuto'));
					$crud->callback_add_field('stampatrice',array($this,'ListaStampatrici'));
					$crud->callback_edit_field('stampatrice',array($this,'ListaStampatriciEditAuto'));*/
					$crud->set_relation('tipo_stampo','tipo_stampo','tipo');
					$crud->set_relation('stampatrice','stampatrici','nome_stamp');
					$crud->set_relation('id_materiale','materiali','nome_materiale');
					$crud->set_relation('id_trattamento','trattamenti','nome_trattamento');
					$crud->required_fields('id_cliente','stampatrice','tipo_stampo');
					$crud->set_relation('id_cliente','clienti','ragione_soc');
					$crud->display_as('id_materiale','Mat Matrice');
					$crud->display_as('id_trattamento','Trattamento');

					$crud->display_as('id_cliente','Cliente');
				break;
				case "componenti":

						$crud->set_table('listino_componenti');
						$crud->set_subject('Listino');
					/*	$crud->callback_add_field('ragione_soc',array($this,'ListaProduttori'));
						$crud->callback_edit_field('ragione_soc',array($this,'ListaProduttoriEditAuto'));
						$crud->callback_add_field('stampatrice',array($this,'ListaStampatrici'));
						$crud->callback_edit_field('stampatrice',array($this,'ListaStampatriciEditAuto'));*/
						$crud->set_relation('id_categoria','categorie_componenti','nome_categoria');
						$crud->set_relation('id_cliente','clienti','ragione_soc');
						$crud->set_relation('nome_stamp','stampatrici','nome_stamp');
						$crud->set_relation('id_materiale','materiali','nome_materiale');
						$crud->set_relation('id_trattamento','trattamenti','nome_trattamento');
						$crud->display_as('id_categoria','Categoria');
						$crud->display_as('nome_stamp','Stampatrice');
						$crud->display_as('id_materiale','Materiale');
						$crud->display_as('id_cliente','Cliente');
						$crud->display_as('id_trattamento','Trattamento');
				break;

			}

			if($this->session->userdata('nome_operatore')!='ANDREA')
				$crud->unset_add();


			$output = $crud->render();
			switch($this->uri->segment(3)){
				case "stampi":
				$output->header="LISTINI STAMPI";

				break;
				case "componenti":
				$output->header="LISTINI COMPONENTI";

				break;
			}


			$output->titolo="LISTINI";
			$this->_example_output($output);

		}



	function lavori()
	{
		$ajax_rendering=false;
		// if($this->session->userdata('nome_operatore')=="DAVID")
		// {
		// 	if((!strpos($this->uri->uri_string(), 'edit'))&&(!strpos($this->uri->uri_string(), 'add')))
		// 		redirect('/omis/lavorazioni/progetti', 'refresh');
		// }
		$this->setHeader=TRUE;
		$crud = new grocery_CRUD();
		$crud->set_theme('datatables');
		$crud->set_table('lavori');
			if($this->session->userdata('nome_operatore')!="DAVID"){
					$crud->add_action('Dettaglio', '', 'omis/dettaglio/componenti','ui-icon-plus');
			}
			else
			{
				$crud->add_action('Dettaglio', '', 'omis/dettaglioop/componenti','ui-icon-plus');

			}
		switch($this->uri->segment(3)){
			case "eseguiti":
				$this->db->order_by("ddt_data", "desc");

				$this->db->where('stato','E');
				//$this->db->limit(244);

				$crud->add_action('Stampa', '', 'omis/stampa','ui-icon-plus');

				//$crud->add_action('Rimetti in lavorazione', '', 'omis/tornacorsoE','ui-icon-plus');
			 	$crud->columns('commessa','comm_cliente','cliente','n_stampo','dis_sapone','nome_sapone',
				'stampatrice','peso','tipo_lavoro','ddt_n','ddt_data','stato_comm');

				//$crud->callback_column('stato_comm',array($this,'completato_verde'));
				$crud->add_action('DISEGNO', '', '','ui-icon-plus',array($this,'apri_link'));
				$crud->callback_after_update(array($this,'after_update_lavoro'));
				$ajax_rendering=true;

				$crud->order_by('ddt_data','desc');

				$header_index=1;
				$crud->unset_delete();
				$crud->unset_add();
				$titolo="ESEGUITI";
				$crud->set_js('assets/themes/datatables/js/dynamic_menu.js');
			break;
			case "sospesi":
				$crud->where('stato','S');
				//$crud->add_action('Rimetti LAV', '', 'omis/tornacorsoS','ui-icon-plus');
				//$crud->add_action('Rimetti PROG', '', 'omis/tornaprogS','ui-icon-plus');
				$crud->columns('commessa','cliente','n_stampo','dis_sapone','nome_sapone','stampatrice',
				'peso','tipo_lavoro','data_inser_comm','data_consegna_cliente','data_invio_dis','comm_cliente','stato_comm');
				$crud->add_action('DISEGNO', '', '','ui-icon-plus',array($this,'apri_link'));
								$ajax_rendering=true;

				$header_index=2;
				$crud->unset_add();
				$titolo="SOSPESI";
			break;
			case "progetti":
				$crud->where('stato','D');
				$crud->unset_delete();
				$crud->add_action('Stampa', '', 'omis/stampa','ui-icon-plus');

				if(($this->session->userdata('nome_operatore')!="DAVID")&&($this->session->userdata('nome_operatore')!="CLAUDIO")){
					$crud->columns('commessa','cliente','n_stampo','dis_sapone','nome_sapone','stampatrice','peso','tipo_lavoro','id_trattam_matrice',
					'data_inser_comm','data_approv_dis','data_invio_dis','comm_cliente');
				}else{
					$crud->columns('commessa','comm_cliente','cliente','n_stampo','nome_sapone','stampatrice','peso','tipo_lavoro',
					'data_inser_comm','data_cons_dis_prev','data_invio_dis');

				}

				$crud->callback_column('tipo_lavoro',array($this,'tipo_lavoro_column'));
				$crud->callback_column('id_trattam_matrice',array($this,'sigla_trattam'));
				$crud->add_action('DISEGNO', '', '','ui-icon-plus',array($this,'apri_link'));

				$titolo="PROGETTI";
				$header_index=3;
			break;
			case "incorso":
				$crud->where('stato','C');
				$crud->unset_delete();
				//$crud->add_action('Modifica', '', 'omis/lavori/incorso/edit','ui-icon-plus');
				//$crud->add_action('Ese', '', 'omis/eseguito','ui-icon-plus');
				//$crud->add_action('SOSPENDI', '', 'omis/sospendi/incorso','ui-icon-plus');
				//$crud->add_action('PROGETTO', '', 'omis/tornaprogetto/','ui-icon-plus');

				$crud->add_action('Stampa', '', 'omis/stampa','ui-icon-plus');
				$crud->columns('commessa','cliente','n_stampo','dis_sapone','nome_sapone','stampatrice','peso','tipo_lavoro','stato_lavoro','id_trattam_matrice',
				'data_consegna_interna','data_consegna_cliente','stato_comm','comm_cliente');
				$crud->callback_column('id_trattam_matrice',array($this,'sigla_trattam'));
				$crud->callback_column('stato_comm',array($this,'completato_verde'));
				//$crud->callback_column('data_consegna_cliente',array($this,'sfondo_consegna_tassativa'));
				$crud->callback_column('tipo_lavoro',array($this,'tipo_lavoro_column'));
				$crud->callback_column('stato_lavoro',array($this,'stato_lavoro_column'));

				$crud->order_by('data_consegna_interna asc, cliente','asc');


				$crud->callback_after_update(array($this,'after_update_lavoro'));
				$crud->add_action('DISEGNO', '', '','ui-icon-plus',array($this,'apri_link'));
				$titolo="IN CORSO";
				$header_index=0;
			break;
		}

		$headers=array(0 => "LAVORI IN CORSO", 1 =>"LAVORI ESEGUITI",2 =>"LAVORI IN SOSPESO", 3 => "LAVORI IN PROGETTO", 4 => "LAVORI ESEGUITI VECCHI");
		$crud->set_relation('id_produttore','produttori','ragione_soc');
		$crud->set_relation('tipo_stampo','tipo_stampo','tipo');
		$crud->set_relation('cliente','clienti','ragione_soc');
		$crud->set_relation('id_destinatario','clienti','ragione_soc');
		$crud->set_relation('id_materiale_piastre','materiali','nome_materiale');
		$crud->set_relation('id_materiale_matrice','materiali','nome_materiale');
		$crud->set_relation('compatibile','stampatrici','nome_stamp');
		$crud->set_relation('orientam_scarico','orient_scarico','orientamento');


		$crud->unset_add_fields('stato','stato_comm','D','C');


		$crud->edit_fields(	'commessa',
											'id_produttore',
											'cliente', 'comm_cliente', 'id_destinatario',
											'n_stampo','nome_sapone','peso','peso_specifico','stampatrice','compatibile','linea_produzione','tipo_lavoro',
											'n_cavita','tipo_stampo','orientam_scarico',
											'id_materiale_matrice','data_ordine_cliente',
											'id_materiale_piastre','id_trattam_matrice','id_trattam_piastre','data_cons_dis_prev','data_invio_dis','data_approv_dis',
											'data_consegna_cliente','data_consegna_interna','consegna_tassativa',
											'ordine_cliente','rif_cliente','dis_sapone','c_lavorazione','offerta_n','offerta_data',
											'ddt_n','ddt_data','descrizione','note','note_2','fattura_n','fattura_data','lunghezza_sapone','larghezza_sapone',
											'altezza_sapone','dati_filiera');

		if($this->session->userdata('nome_operatore')=='DAVID'){
					$crud->add_action('Clona', '', 'omis/clona_commessa','ui-icon-plus');
					$crud->add_action('Rimetti PROG', '', 'omis/tornaprogS','ui-icon-plus');

		}
		date_default_timezone_set('Europe/Rome');

		$crud->required_fields('id_produttore');
		$crud->unset_texteditor('descrizione');
		$crud->unset_texteditor('note');
		$crud->unset_texteditor('note_2');

		//$crud->callback_add_field('ragione_soc',array($this,'ListaProduttori'));
		//$crud->callback_edit_field('ragione_soc',array($this,'ListaProduttoriEdit'));
		$crud->callback_before_insert(array($this,'before_insert_lavoro')); //DA RISOLVERE
		$crud->callback_add_field('peso_specifico',array($this,'PesoSpecifico'));
		$crud->callback_before_update(array($this,'before_update_lavori'));

		//$crud->callback_add_field('data_inser_comm',array($this,'DataInserComm'));

		//$crud->callback_add_field('cliente',array($this,'ListaClienti'));
		if($this->session->userdata('nome_operatore')=="DAVID")
		$crud->callback_add_field('n_stampo',array($this,'n_stampo_suggerito'));
		$crud->callback_edit_field('n_stampo',array($this,'n_stampo_suggerito'));
		$crud->callback_edit_field('commessa',array($this,'commessaBolle'));

	/*	$lavoro = $this->db->get_where('lavori', array('commessa'=> ));
		foreach ($lavoro->result() as $row)
		if()*/// DA INSERIRE IN CALLBACK_BEFORE_EDIT

		$crud->callback_add_field('id_trattam_matrice',array($this,'ListaTrattamMatrice'));
		$crud->callback_edit_field('id_trattam_matrice',array($this,'ListaTrattamMatriceEdit'));
		$crud->callback_add_field('id_trattam_piastre',array($this,'ListaTrattamPiastre'));
		$crud->callback_edit_field('id_trattam_piastre',array($this,'ListaTrattamPiastreEdit'));
		$crud->callback_add_field('stampatrice',array($this,'ListaStampatrici'));
		$crud->callback_edit_field('stampatrice',array($this,'ListaStampatriciEdit'));
		$crud->callback_add_field('tipo_lavoro',array($this,'TipoLavoro'));
		$crud->callback_edit_field('tipo_lavoro',array($this,'TipoLavoroEdit'));

		$crud->callback_after_insert(array($this, 'inserisci_fasi_after_insert_commessa'));



		$crud->display_as('descrizione','Descrizione Progetto');
		$crud->display_as('id_trattam_matrice','Tratt. matrice');
		$crud->display_as('id_trattam_piastre','Tratt. piastre');
		$crud->display_as('peso','Peso (gr)');
		$crud->display_as('comm_cliente','Comm. Cliente');
		$crud->display_as('ddt_n','Numero DDT');
		$crud->display_as('offerta_n','Numero OFFERTA');
		$crud->display_as('offerta_data','Data OFFERTA');
		$crud->display_as('ddt_data','Data DDT');
		$crud->display_as('stato_comm','STATO');
		$crud->display_as('commessa','Comm.');
		$crud->display_as('id_materiale_piastre','Materiale Piastre');
		$crud->display_as('id_materiale_matrice','Materiale Matrice');
		$crud->display_as('id_produttore','Produttore');
		$crud->set_subject('Lavoro');
       	if($ajax_rendering){
	       	//$output= new stdClass;
	       	if($this->uri->segment($this->uri->total_segments())!='ajax_call' ){
				$output = $crud->render_ajax();


				// <script type='text/javascript' language='javascript'> alert(".$_POST['prova'].") </script>
				($this->setHeader==TRUE) ? $output->header=$headers[$header_index] : 	$output->header='';


				//.var_dump($que);*/
				$output->titolo=$titolo;
				$this->_example_output($output);
			}else{

				$output = $crud->render_ajax();
				echo $output->output;

			}
       	}else{
				$output = $crud->render();
				($this->setHeader==TRUE) ? $output->header=$headers[$header_index] : 	$output->header='';
				$output->titolo=$titolo;
				$this->_example_output($output);

       	}

	}


	function n_stampo_suggerito($value, $primary_key)
	{
		if(!(isset($value)))
		{
				$value="";

		}
		$que = $this->db->query("SELECT ultimo_n_stampo
		FROM importazione_tempi
		");
		foreach ($que->result() as $lavoro);

		return '<input type="text" maxlength="30" value="'.$value.'" name="n_stampo" style="width:400px"> Ultimo numero stampo IMPEGNATO: <b>'.$lavoro->ultimo_n_stampo.'</b>';
	}


	function stato_lavoro_column($value, $row){
		$datrattare=false;
		$totale=0;
		$completato=0;
		$this->db->select('bolle_prod.stato, bolle_prod.id_trattamento,componenti.id_categoria');
		$this->db->from('bolle_prod');
		$this->db->join('componenti', 'bolle_prod.id_componente = componenti.id_componente');
		$where = "bolle_prod.commessa =".$row->commessa;
		$this->db->where($where);
		$que = $this->db->get();
		foreach($que->result() as $bolla)
		{
			switch($bolla->id_categoria){
				case 1: //5
				case 9:
				case 10:
				case 11:
				case 13:
					$totale+=5;
					if(($bolla->stato=='DA COMPLETARE')||($bolla->stato=='COSTRUITO')||($bolla->stato=='COMPLETATO')){
						$completato+=5;

					}
				break;
				case 2: //4
				case 3:
				case 4:
					$totale+=4;
					if(($bolla->stato=='DA COMPLETARE')||($bolla->stato=='COSTRUITO')||($bolla->stato=='COMPLETATO'))
					$completato+=4;
				break;
				case 14://3
					$totale+=3;
					if(($bolla->stato=='DA COMPLETARE')||($bolla->stato=='COSTRUITO')||($bolla->stato=='COMPLETATO'))
					$completato+=3;
				break;
				case NULL:
				break;
				default:
					$totale+=1;
					if(($bolla->stato=='DA COMPLETARE')||($bolla->stato=='COSTRUITO')||($bolla->stato=='COMPLETATO'))
					$completato+=1;
				break;

			}
			if(($bolla->id_trattamento!=NULL)&&($bolla->id_trattamento!=1)&&($bolla->id_trattamento!=10)) //se esiste un trattamento e non è nè sabbiatura nè lucidatura
			{

				$datrattare=true;
			}
		}
		if($datrattare)
		{
			$totale+=1;

			$lavoro = $this->db->get_where('lav_esterne_commessa', array('commessa'=> $row->commessa));
			if($lavoro->num_rows()>0){
				$dentro=true;
				foreach ($lavoro->result() as $lavorazione){
						if($lavorazione->stato_lav_est=="FUORI")
						$dentro=false;
				}
				if($dentro)
				{
					$completato+=1;
				}
			}

		}
		if($totale>0)
			return (int)((100*$completato)/($totale))."%";
		else
			return "no componenti";
	}


	function before_insert_lavoro($post_array)
	{
		date_default_timezone_set('Europe/Rome');

		if($post_array['data_consegna_cliente']!=NULL)
		{
			@list($day,$month,$year) = explode("/",$post_array['data_consegna_cliente']);
			if(date('l',mktime(0,0,0,$month,($day-1),$year))=="Saturday"){
				$post_array['data_consegna_interna']=date("d/m/Y", mktime(0,0,0,$month,($day-2),$year));
			}else{
				if(date('l',mktime(0,0,0,$month,($day-1),$year))=="Sunday")
					$post_array['data_consegna_interna']=date("d/m/Y", mktime(0,0,0,$month,($day-3),$year));
				else
					$post_array['data_consegna_interna']=date("d/m/Y",mktime (0,0,0,(int)$month , (int)($day-1) , (int)$year));
			}

		}
		$post_array['data_inser_comm']=date('d/m/Y');

		$que = $this->db->query("SELECT ultimo_n_stampo
		FROM importazione_tempi
		");
		foreach ($que->result() as $lavoro);

		if(is_numeric($post_array['n_stampo'])){

			if(intval($post_array['n_stampo'])==((int)($lavoro->ultimo_n_stampo)+1)){

					$data = array('ultimo_n_stampo' => intval($post_array['n_stampo']));
					$this->db->update('importazione_tempi', $data);
			}else{
				if(intval($post_array['n_stampo'])>((int)($lavoro->ultimo_n_stampo)+1)){
					$post_array['n_stampo']="";
				}

			}
		}



		return $post_array;
	}


	function before_update_lavori($post_array, $primary_key)
	{

		$lavoro = $this->db->get_where('lavori', array('commessa'=> $primary_key));
		foreach ($lavoro->result() as $lavoro);
		if($lavoro->stato!='E'){
					date_default_timezone_set('Europe/Rome');
					if(($post_array['data_consegna_cliente']!=NULL)&&($post_array['data_consegna_interna']==NULL))
					{
						@list($day,$month,$year) = explode("/",$post_array['data_consegna_cliente']);
						if(date('l',mktime(0,0,0,$month,($day-1),$year))=="Saturday"){
							$post_array['data_consegna_interna']=date("d/m/Y", mktime(0,0,0,$month,($day-2),$year));
						}else{
							if(date('l',mktime(0,0,0,$month,($day-1),$year))=="Sunday")
								$post_array['data_consegna_interna']=date("d/m/Y", mktime(0,0,0,$month,($day-3),$year));
							else
								$post_array['data_consegna_interna']=date("d/m/Y",mktime (0,0,0,(int)$month , (int)($day-1) , (int)$year));
						}
						$lavoro = $this->db->get_where('lavori', array('commessa'=> $primary_key));
						foreach ($lavoro->result() as $lavoro);
						@list($year,$month,$day) = explode("-",$lavoro->data_consegna_interna);
								$this->db->select('*');
								$this->db->from('bolle_prod');
								$this->db->join('lavori', 'bolle_prod.commessa = lavori.commessa');
								$where = "(lavori.stato = 'C' OR lavori.stato='D') AND lavori.commessa=".$primary_key." AND (bolle_prod.stato='INIZIARE' OR bolle_prod.stato='IN CORSO')";
								$this->db->where($where);
								$que = $this->db->get();
								foreach ($que->result() as $bolla){

									$this->calcola_data_inizio($bolla,$post_array,1);
								}
								$this->db->select('*');
								$this->db->from('fasi_commessa');
								$this->db->join('lavori', 'fasi_commessa.commessa = lavori.commessa');
								$where = "(lavori.stato = 'C' OR lavori.stato='D') AND lavori.commessa=".$primary_key." AND (fasi_commessa.stato='INIZIARE' OR fasi_commessa.stato='IN CORSO')";
								$this->db->where($where);
								$que = $this->db->get();
								foreach ($que->result() as $fase){
									$this->calcola_data_inizio($fase,$post_array,3);
								}
								$this->matrice_espulsori($post_array['commessa']);

					}
					else{

									$this->db->select('*');
									$this->db->from('bolle_prod');
									$this->db->join('lavori', 'bolle_prod.commessa = lavori.commessa');
									$where = "(lavori.stato = 'C' OR lavori.stato='D') AND lavori.commessa=".$primary_key." AND (bolle_prod.stato='INIZIARE' OR bolle_prod.stato='IN CORSO')";
									$this->db->where($where);
									$que = $this->db->get();
									foreach ($que->result() as $bolla){
										$this->calcola_data_inizio($bolla,$post_array,1);
									}
									$this->db->select('*');
									$this->db->from('fasi_commessa');
									$this->db->join('lavori', 'fasi_commessa.commessa = lavori.commessa');
									$where = "(lavori.stato = 'C' OR lavori.stato='D') AND lavori.commessa=".$primary_key." AND (fasi_commessa.stato='INIZIARE' OR fasi_commessa.stato='IN CORSO')";
									$this->db->where($where);
									$que = $this->db->get();
									foreach ($que->result() as $fase){

										$this->calcola_data_inizio($fase,$post_array,3);
									}

									$this->matrice_espulsori($post_array['commessa']);



					}

					$que = $this->db->query("SELECT ultimo_n_stampo
					FROM importazione_tempi
					");
					foreach ($que->result() as $lavoro);

					if(is_numeric($post_array['n_stampo'])){

						if(intval($post_array['n_stampo'])==((int)($lavoro->ultimo_n_stampo)+1)){

								$data = array('ultimo_n_stampo' => intval($post_array['n_stampo']));
								$this->db->update('importazione_tempi', $data);
						}else{
							if(intval($post_array['n_stampo'])>((int)($lavoro->ultimo_n_stampo)+1)){
								$post_array['n_stampo']="";
							}

						}
					}
		}


		return $post_array;
	}


	function apri_link($primary_key , $row)
	{
	   // return site_url('smb://srv02/officina2/Immagini tech omis/albatros/pdf/ST455-S-REV-2.pdf');
		$lavoro = $this->db->get_where('clienti', array('id_cliente'=> $row->cliente));
		foreach ($lavoro->result() as $cliente);
		if($row->dis_sapone!=NULL)
		{
			$nomefile=$row->dis_sapone;
		}else{
			$nomefile=$row->n_stampo;
		}
		return "ftp://".$_SERVER["SERVER_NAME"].":2222/".$cliente->ragione_soc."/PDF/".$nomefile.".pdf";


		//return "file:///I:/disegni_saponi/".$cliente->ragione_soc."/PDF/";
		//return "file:///C:/apri_server.bat";
	}

	function verifica_stampatrice($post_array, $primary_key) //se durante la modifica di una commessa modifico la stampatrice verifico eventuali nuovi preset e svuoto le tabelle associate con la
																//vecchia stampatrice

{

		$lavoro = $this->db->get_where('lavori', array('commessa'=> $primary_key));
		foreach ($lavoro->result() as $row);
		if($post_array['stampatrice'] != $row->stampatrice)
		{
			$this->db->delete('accessori_commessa', array('commessa'=> $primary_key)); //elimino tutti gli accessori del vecchio preset associati alla commessa
			$this->db->select('cod_bolla'); //invece di eliminare tutti componenti da bolle_prod elimino solo i componenti su cui non sono ancora state effettuate lavorazioni(modello in resina lo lascer�)
			$codici = $this->db->get_where('bolle_prod', array('commessa'=> $primary_key));

			foreach ($codici->result() as $row)
			{
				$query = $this->db->get_where('lavorazioni',array('cod_bolla'=> $row->cod_bolla));

				if($query->num_rows()==0){//se il codice bolla non corrisponde a nessun tempo nella tabella lavorazioni lo elimino
					$this->db->delete('bolle_prod', array('cod_bolla'=> $row->cod_bolla));
				}
			}
			//$this->gestionePresetComponenti(1, $primary_key);

		}
	}



	function inserisci_fasi_after_insert_commessa( $post_array , $insert_id)
	{
		$nuovaFase=array(
					'commessa' => $insert_id,
					'nome_fase' => 'ASSEMBLAGGIO, FINITURA',
					'stato' => 'INIZIARE'
					);

		$this->db->insert('fasi_commessa',$nuovaFase);
		$nuovaFase=array(
					'commessa' => $insert_id,
					'nome_fase' => 'DESIGN',
					'stato' => 'INIZIARE'
					);

		$this->db->insert('fasi_commessa',$nuovaFase);
		$nuovaFase=array(
					'commessa' => $insert_id,
					'nome_fase' => 'TEMPO MACCHINA COMMESSA',
					'stato' => 'INIZIARE'
					);

		$this->db->insert('fasi_commessa',$nuovaFase);

	}





	function DataInserComm()
	{
		date_default_timezone_set('Europe/Rome');
		return "<input name='data_inser_comm' type='text' value='".date('d/m/Y')."' maxlength='10' class='datepicker-input' />
				<button class='datepicker-input-clear'>Azzera</button> (dd/mm/yyyy)";
	}

	function ListaTrattamPiastre()
	{
		$trattamenti['']='';
		$query = $this->db->get('trattamenti');
		foreach ($query->result() as $row)
		{
			$trattamenti[$row->id_trattamento]=$row->nome_trattamento;
		}
		return form_dropdown('id_trattam_piastre', $trattamenti, '');
	}

	function ListaTrattamPiastreEdit()
	{
		$lavoro = $this->db->get_where('lavori', array('commessa'=> $this->uri->segment(5)));
		foreach ($lavoro->result() as $row);
		$trattam=$row->id_trattam_piastre;

		$query = $this->db->get('trattamenti');
		$trattamenti['']='';
		foreach ($query->result() as $row)
		{
			$trattamenti[$row->id_trattamento]=$row->nome_trattamento;
		}
		return form_dropdown('id_trattam_piastre', $trattamenti, $trattam);
	}
	function ListaTrattamMatrice()
	{
		$trattamenti['']='';
		$query = $this->db->get('trattamenti');
		foreach ($query->result() as $row)
		{
			$trattamenti[$row->id_trattamento]=$row->nome_trattamento;
		}
		return form_dropdown('id_trattam_matrice', $trattamenti);
	}

	function ListaTrattamMatriceEdit()
	{
		$lavoro = $this->db->get_where('lavori', array('commessa'=> $this->uri->segment(5)));
		foreach ($lavoro->result() as $row);
		$trattam=$row->id_trattam_matrice;

		$query = $this->db->get('trattamenti');
		$trattamenti['']='';
		foreach ($query->result() as $row)
		{
			$trattamenti[$row->id_trattamento]=$row->nome_trattamento;
		}
		return form_dropdown('id_trattam_matrice', $trattamenti, $trattam);
	}

	function sigla_trattam($value,$row)
	{
		$query = $this->db->get_where('trattamenti', array('id_trattamento' => $value));
		if($query->num_rows()>0){
			foreach ($query->result() as $row);
			$sigla=$row->sigla_trattamento;
		}
		else $sigla='';
		if($this->session->userdata('nome_operatore')=="AGOSTINO"){
			switch($sigla){
				case "ARG":
					$colore='yellow';
				break;
				case "NKT":
					$colore='orange';
				break;
				default:
					$colore='';
				break;

			}

			$sigla="<label style='background-color:".$colore.";'>".$sigla."</label>";
		}
		return $sigla;

	}


	function completato_verde($value,$row)
	{
		if( $value=='DA COMPLETARE')
			$value="<label style='background-color:yellow;'>".$value."</label>";

		if($value=='COSTRUITO' || $value=='COMPLETATO')
			$value="<label style='background-color:green;'>".$value."</label>";
		if($value=='INSERIRE KG')
			$value="<label style='background-color:red;'>".$value."</label>";
		return $value;
	}

	function tipo_lavoro_column($value,$row)
	{
		$colore='transparent';
		if(isset($row->id_materiale_matrice)){
			$que = $this->db->get_where('materiali',array('id_materiale'=> $row->id_materiale_matrice));
			foreach ($que->result() as $commessa);
		}
		else{
			$commessa = new stdClass;
			$commessa->nome_materiale="";
		}

		if($value=='STA'){
			switch($commessa->nome_materiale){
				case "CCZ":
					$colore='pink';
				break;
				case "CNCS":
					$colore='pink';
				break;
				case "AMPCO 940":
					$colore='pink';
				break;
				case "AMPCO 972":
					$colore='pink';
				break;
				case "AZQI":
					$colore='gray';
				case "B14":
					$colore='yellow';
				case "OT58":
					$colore='yellow';
				case "OTK":
					$colore='yellow';
				break;
			}
		}
		return "<label style='background-color:".$colore.";'>".$value."</label>";
	}


	function completato()
	{

		switch($this->uri->segment(3)){
			case "niente":
				$completato="NO";
			break;
			case "disegno":
				$completato="DIS";
			break;

		}
		($this->session->userdata('nome_operatore')=="DAVID") ? $disegnatore = "D" : $disegnatore = "C";

		$this->db->where('commessa', $this->uri->segment(4));
		$this->db->update('lavori', array($disegnatore => $completato));
		if($this->session->userdata('nome_operatore')=="DAVID")
			redirect('/omis/lavorazioni/progetti', 'refresh');
		else
			redirect('/omis/progetti_op', 'refresh');



	}

	function after_update_lavoro( $post_array , $insert_id) //se inserisco data DDT lo stato della commessa passa da incorso a eseguito(invocata da lavori in corso)
	{

		($post_array['ddt_data']==NULL) ? $stato='C' : $stato='E' ;

			$this->db->where('commessa', $insert_id);
			$this->db->update('lavori', array("stato" => $stato));

	}



	function calcola_data_inizio($oggetto,$post_array,$tipologia)
	{ //in questa funzione si usa post_array[data_consegna_interna] al posto di lavoro->data_consegna_interna perchè questa funzione viene invocata
		//anche da callback_before_update, quindi in post_array ho la nuova data consegna commessa prevista e invece in lavoro->data_consegna_interna ho quella nel db
		//che potrebbe non essere quella che sto aggiornando nel momento in cui chiamo la callback_before_update
		date_default_timezone_set('Europe/Rome');
		if($tipologia==1){		//se è un componente
					$que = $this->db->get_where('componenti',array('id_componente'=> $oggetto->id_componente));
					foreach ($que->result() as $compo);
					$que = $this->db->get_where('lavori',array('commessa'=> $oggetto->commessa));
					foreach ($que->result() as $lavo);
					$id_categoria=(int)$compo->id_categoria;
					$id_trattamento=(int)$oggetto->id_trattamento;
					@list($day,$month,$year) = explode("/",$post_array['data_consegna_interna']);
					$numero_data_comm=date('w',mktime(0,0,0,$month,($day-1),$year));/*prendo il numero del giorno della settimana di data consegna, però con -1 perchè date() attribuisce 0 alla domenica*/
					$que = $this->db->get_where('categorie_componenti',array('id_categoria'=> $compo->id_categoria));
					foreach ($que->result() as $categoria);
					$que = $this->db->get_where('trattamenti',array('id_trattamento'=> $oggetto->id_trattamento));
					foreach ($que->result() as $trattamento);
					$data_inizio=date('y-m-d', mktime(0,0,0,$month,($day-($categoria->giorni_arretramento_produzione+$trattamento->giorni_arretramento_produzione)),$year));

					@list($year,$month,$day) = explode("-",$data_inizio);
					if(date('l',mktime(0,0,0,$month,$day,$year))=="Saturday"){
						$data_inizio=date('y-m-d', mktime(0,0,0,$month,($day-1),$year));
					}else{
						if(date('l',mktime(0,0,0,$month,$day,$year))=="Sunday")
							$data_inizio=date('y-m-d', mktime(0,0,0,$month,($day-2),$year));
					}
					$data = array('data_inizio' => $data_inizio);
					$this->db->where('cod_bolla', $oggetto->cod_bolla);
					$this->db->update('bolle_prod', $data);
			}
		if($tipologia>=2){ //se è una fase
				$casistica=false;
				@list($day,$month,$year) = explode("/",$post_array['data_consegna_interna']);
				$que = $this->db->get_where('lavori',array('commessa'=> $oggetto->commessa));
				foreach ($que->result() as $lavo);
				$que = $this->db->get_where('arretramento_fasi',array('nome_fase'=> $oggetto->nome_fase,"tipo_lavoro"=>$lavo->tipo_lavoro));
				foreach ($que->result() as $arretramento);

				$data_inizio=date('y-m-d', mktime(0,0,0,$month,($day-$arretramento->giorni_arretramento_produzione),$year));

				@list($year,$month,$day) = explode("-",$data_inizio);
				if(date('l',mktime(0,0,0,$month,$day,$year))=="Saturday"){

					$data_inizio=date('y-m-d', mktime(0,0,0,$month,($day-1),$year));
				}else{
					if(date('l',mktime(0,0,0,$month,$day,$year))=="Sunday"){

						$data_inizio=date('y-m-d', mktime(0,0,0,$month,($day-2),$year));
					}
				}

				$data = array('data_inizio' => $data_inizio);
				$this->db->where('id_fase_commessa', $oggetto->id_fase_commessa);
				$this->db->update('fasi_commessa', $data);

			}
	}

function build_menu(){
		$vars=array();
		$vars['tipo_op']=$this->session->userdata('tipo_op');
		$vars['nome_op']=$this->session->userdata('nome_operatore');


		extract($vars);
			#region buffering...
			ob_start();

			include(getcwd().'/application/views/menu_builder.php');

			$buffer = ob_get_contents();
		@ob_end_clean();
		return $buffer;
}

	function fasi_arretramento(){

		$crud = new grocery_CRUD();
		$crud->set_theme('datatables');
		$crud->set_table('arretramento_fasi');
		$crud->set_subject('Arretramento');
		$crud->callback_add_field('tipo_lavoro',array($this,'TipoLavoro'));
		$crud->callback_edit_field('tipo_lavoro',array($this,'TipoLavoroEdit'));
		$crud->set_relation('nome_fase','fasi_lavorazione','nome_fase');

		$output = $crud->render();
		$output->header="ARRETRAMENTO FASI";
		$output->footer="";

		$output->menu=$this->build_menu();



			$this->load->view('omis_view.php',$output);



	}

	function passwordCifrata($post_array , $insert_id)//invocata per inserire la password cifrata nel DB - non servirebbe a nulla! da rimuovere prima o poi
	{
			$this->db->where('id_operatore', $insert_id);
			$this->db->update('operatori', array("password_op" => do_hash($post_array['password_op'], 'md5')));
	}

	function lavorazioni()
	{
			$ajax_rendering=false;
			$this->setHeader=TRUE;
			$crud = new grocery_CRUD();
			$crud->set_theme('datatables');
			$crud->set_table('lavori');
			switch($this->uri->segment(3)){
				case "eseguiti":
					$where="stato = 'E'";
					$ajax_rendering=true;
					$crud->set_js('assets/js/dynamic_menu_op.js');

				break;
				case "incorso":
					$where="stato = 'C'";

				break;
				case "progetti":
					$where="stato = 'D'";

				break;
				default:
					$where="stato = 'C'";
					if($this->session->userdata('nome_operatore')=='AGOSTINO')
					 redirect('omis/lavorazioni/incorso', 'refresh');

				break;

			}
			$this->db->where($where);
			$crud->unset_edit();
			$crud->unset_add();
			$crud->unset_delete();
			$crud->set_relation('cliente','clienti','ragione_soc');
			$crud->display_as('id_produttore','Produttore');
			$crud->display_as('commessa','Comm.');
			$crud->display_as('id_trattam_matrice','Tratt. Matrice');

			$crud->add_action('DETTAGLIO', '', 'omis/dettaglioop/componenti','ui-icon-plus');

			$crud->add_action('DISEGNO', '', '','ui-icon-plus',array($this,'apri_link'));

			if($this->session->userdata('nome_operatore')=="DAVID")
				{
					$crud->add_action('Stampa', '', 'omis/stampa','ui-icon-plus');
				}
			$crud->columns('commessa','cliente','n_stampo','dis_sapone','nome_sapone','stampatrice','peso','tipo_lavoro','stato_lavoro','id_trattam_matrice','data_consegna_cliente','stato_comm','comm_cliente');
			$crud->callback_column('stato_comm',array($this,'completato_verde'));
			$crud->callback_column('id_trattam_matrice',array($this,'sigla_trattam'));
			$crud->callback_column('stato_lavoro',array($this,'stato_lavoro_column'));
			$crud->order_by('data_consegna_cliente');


			//$crud->callback_column('data_cons_comm_prev',array($this,'sfondo_consegna_tassativa'));
			$crud->callback_column('tipo_lavoro',array($this,'tipo_lavoro_column'));

			if($ajax_rendering){
	       	//$output= new stdClass;
	       	if($this->uri->segment($this->uri->total_segments())!='ajax_call' ){
				$output = $crud->render_ajax();

					$output->footer='';
				switch($this->uri->segment(3)){
					case "eseguiti":
						$output->header='LAVORI ESEGUITI';

					break;
					case "vecchi":
						$output->header='LAVORI VECCHI';

					break;
					case "incorso":
						$output->header='LAVORI IN CORSO';

					break;
					case "progetti":
						$output->header='LAVORI IN PROGETTO';

					break;
					default:
						$output->header='LAVORI';

					break;

				}
					// <script type='text/javascript' language='javascript'> alert(".$_POST['prova'].") </script>


					//.var_dump($que);*/
					$output->titolo="LAVORI";
					$output->menu=$this->build_menu();
				$this->load->view('omis_view.php',$output);
				}else{

					$output = $crud->render_ajax();
					echo $output->output;

				}
	       	}else{
					$output = $crud->render();
						$output->footer='';
				switch($this->uri->segment(3)){
					case "eseguiti":
						$output->header='LAVORI ESEGUITI';

					break;
					case "vecchi":
						$output->header='LAVORI VECCHI';

					break;
					case "incorso":
						$output->header='LAVORI IN CORSO';

					break;
					case "progetti":
						$output->header='LAVORI IN PROGETTO';

					break;
					default:
						$output->header='LAVORI';

					break;

				}
					$output->titolo="LAVORI";
					$output->menu=$this->build_menu();
					$this->load->view('omis_view.php',$output);

	       	}

	}
	//PER DAVID <a href=".site_url('omis/lavori/sospesi')." ><font size='3'>LAVORI SOSPESI</font></a> |



	function anagrafiche()
	{
		$crud = new grocery_CRUD();

		$crud->set_table('clienti');
		$output = $crud->render();

		$output->output="SCEGLI UN'ANAGRAFICA";
		$output->header="ANAGRAFICHE";
		$output->footer="";

		$output->menu=$this->build_menu();
		$this->load->view('omis.php',$output);

	}

	function stampa()
	{
		date_default_timezone_set('Europe/Rome');

		$where = "commessa = ".$this->uri->segment(3);
		$que = $this->db->get_where('lavori',$where);
		foreach ($que->result() as $dati);
		$que = $this->db->get_where('clienti',array('id_cliente'=>$dati->cliente));
		if($que->num_rows() > 0){
			foreach ($que->result() as $cliente);
			$dati->cliente=$cliente->ragione_soc;
		}
		else $dati->cliente='';
		$query = $this->db->get_where('trattamenti',array('id_trattamento'=>$dati->id_trattam_matrice));
		if($query->num_rows() > 0){
			foreach ($query->result() as $trattam);
			$dati->id_trattam_matrice=$trattam->nome_trattamento;
		}
		else $dati->id_trattam_matrice='';
		$query = $this->db->get_where('trattamenti',array('id_trattamento'=>$dati->id_trattam_piastre));
		if($query->num_rows() > 0){
			foreach ($query->result() as $trattam);
			$dati->id_trattam_piastre=$trattam->nome_trattamento;
		}
		else $dati->id_trattam_piastre='';

		$queryProd = $this->db->get_where('produttori',array('id_produttore'=>$dati->id_produttore));
		if($queryProd->num_rows() > 0){
		foreach ($queryProd->result() as $prod);
			$dati->ragione_soc=$prod->ragione_soc;
			}
		else $dati->ragione_soc='';
		$que = $this->db->get_where('materiali',array('id_materiale'=>$dati->id_materiale_matrice));
		if($que->num_rows() > 0) {
			foreach ($que->result() as $materiale);
			$dati->mat_matrice=$materiale->nome_materiale;
		}
		else
			$dati->mat_matrice="";
			$que = $this->db->get_where('materiali',array('id_materiale'=>$dati->id_materiale_piastre));
			if($que->num_rows() > 0) {
				foreach ($que->result() as $materiale);
				$dati->mat_piastre=$materiale->nome_materiale;
			}
			else
				$dati->mat_piastre="";

			$dati->tab_componenti="";

		if($dati->ddt_data!=''){
			@list($year,$month,$day) = explode("-",$dati->ddt_data);
			$dati->ddt_data = date ("d-m-Y",mktime (0,0,0,(int)$month , (int)$day , (int)$year));
		}
		if($dati->offerta_data!=''){
			@list($year,$month,$day) = explode("-",$dati->offerta_data);
			$dati->offerta_data = date ("d-m-Y",mktime (0,0,0,(int)$month , (int)$day , (int)$year));
		}
		$dati->nome_op = $this->session->userdata('nome_operatore');

		$dati->data_odierna = date('d-m-Y');

		$this->load->view('/views_stampa/pagina_stampa.php',$dati);
	}

	function stampa_componenti()
	{
		date_default_timezone_set('Europe/Rome');

		$where = "commessa = ".$this->uri->segment(3);
		$que = $this->db->get_where('lavori',$where);
		foreach ($que->result() as $dati);
		$que = $this->db->get_where('clienti',array('id_cliente'=>$dati->cliente));
		if($que->num_rows() > 0){
			foreach ($que->result() as $cliente);
			$dati->cliente=$cliente->ragione_soc;
		}
		else $dati->cliente='';
		$query = $this->db->get_where('trattamenti',array('id_trattamento'=>$dati->id_trattam_matrice));
		if($query->num_rows() > 0){
			foreach ($query->result() as $trattam);
			$dati->id_trattam_matrice=$trattam->nome_trattamento;
		}
		else $dati->id_trattam_matrice='';
		$query = $this->db->get_where('trattamenti',array('id_trattamento'=>$dati->id_trattam_piastre));
		if($query->num_rows() > 0){
			foreach ($query->result() as $trattam);
			$dati->id_trattam_piastre=$trattam->nome_trattamento;
		}
		else $dati->id_trattam_piastre='';

		$queryProd = $this->db->get_where('produttori',array('id_produttore'=>$dati->id_produttore));
		if($queryProd->num_rows() > 0){
		foreach ($queryProd->result() as $prod);
			$dati->ragione_soc=$prod->ragione_soc;
			}
		else $dati->ragione_soc='';
		$que = $this->db->get_where('materiali',array('id_materiale'=>$dati->id_materiale_matrice));
		if($que->num_rows() > 0) {
			foreach ($que->result() as $materiale);
			$dati->mat_matrice=$materiale->nome_materiale;
		}
		else
			$dati->mat_matrice="";
			$que = $this->db->get_where('materiali',array('id_materiale'=>$dati->id_materiale_piastre));
			if($que->num_rows() > 0) {
				foreach ($que->result() as $materiale);
				$dati->mat_piastre=$materiale->nome_materiale;
			}
			else
				$dati->mat_piastre="";

			$this->load->library('table');
			$tmpl = array (
			                    'table_open'          => '<table style="text-align: left; width: 1029px;" border="1" cellpadding="1" cellspacing="1">',

			                    'heading_row_start'   => '<tr>',
			                    'heading_row_end'     => '</tr>',
			                    'heading_cell_start'  => '<th style="padding:5px;">',
			                    'heading_cell_end'    => '</th>',

			                    'row_start'           => '<tr>',
			                    'row_end'             => '</tr>',
			                    'cell_start'          => '<td style="padding:5px;">',
			                    'cell_end'            => '</td>',

			                    'row_alt_start'       => '<tr>',
			                    'row_alt_end'         => '</tr>',
			                    'cell_alt_start'      => '<td style="padding:5px;">',
			                    'cell_alt_end'        => '</td>',

			                    'table_close'         => '</table>'
			              );

			$this->table->set_template($tmpl);

			$this->table->set_heading(' NOME COMPONENTE ', ' MATERIALE', " Q.TA'", "TRATT.","C/L","SMLV","ASSEGNATO","MISURE");


			$this->db->select('*');
			$this->db->from('bolle_prod');
			$this->db->join('componenti', 'bolle_prod.id_componente = componenti.id_componente');
			$this->db->where('commessa', $this->uri->segment(3));
			$this->db->order_by("nome_componente", "asc");
			$prendi_compo = $this->db->get();
			if($prendi_compo->num_rows() > 0){

				foreach ($prendi_compo->result() as $componente)
				{
					if($componente->id_trattamento != ''){
						$where = "id_trattamento = ".$componente->id_trattamento;
						$prendi_n_tratt = $this->db->get_where('trattamenti',$where);
						foreach ($prendi_n_tratt->result() as $nome_tratt);
					}
					else{
						$nome_tratt= new stdClass();

						$nome_tratt->sigla_trattamento='';
					}

					$que = $this->db->get_where('materiali',array('id_materiale'=>$componente->id_materiale));
					if($que->num_rows() > 0)
						foreach ($que->result() as $materiale);
					else{
						$materiale = new stdClass();
						$materiale->nome_materiale="";
					}
					$que = $this->db->get_where('operatori',array('id_operatore'=>$componente->id_operatore));
					if($que->num_rows() > 0)
						foreach ($que->result() as $operatore);
					else{
						$operatore = new stdClass();
						$operatore->nome_op="";
					}
					$this->table->add_row($componente->nome_componente, $materiale->nome_materiale,
						$componente->quantita,$nome_tratt->sigla_trattamento,$componente->c_lavoro,$componente->semilavorato,$operatore->nome_op,$componente->misure_finito);
				}
			}
			$dati->tab_componenti=$this->table->generate();

		if($dati->ddt_data!=''){
			@list($year,$month,$day) = explode("-",$dati->ddt_data);
			$dati->ddt_data = date ("d-m-Y",mktime (0,0,0,(int)$month , (int)$day , (int)$year));
		}
		if($dati->offerta_data!=''){
			@list($year,$month,$day) = explode("-",$dati->offerta_data);
			$dati->offerta_data = date ("d-m-Y",mktime (0,0,0,(int)$month , (int)$day , (int)$year));
		}
		$dati->nome_op = $this->session->userdata('nome_operatore');

		$dati->data_odierna = date('d-m-Y');

		$this->load->view('/views_stampa/pagina_stampa_componenti.php',$dati);
	}

	function stampa_accessori(){

			$componente_corrente=0;
			$where = "commessa = ".$this->uri->segment(3);
			$que = $this->db->get_where('lavori',$where);

			foreach ($que->result() as $dati);
			$que = $this->db->get_where('clienti',array('id_cliente'=>$dati->cliente));
			if($que->num_rows() > 0){
				foreach ($que->result() as $cliente);
				$dati->cliente=$cliente->ragione_soc;
			}

			$queryProd = $this->db->get_where('produttori',array('id_produttore'=>$dati->id_produttore));
			if($queryProd->num_rows() > 0){
				foreach ($queryProd->result() as $prod);
				$dati->ragione_soc=$prod->ragione_soc;

				}

			else $dati->ragione_soc='';

			$this->load->library('table');
			$this->table->set_heading(' NOME ACCESSORIO ', ' CODICE ACCESSORIO',"UBICATO" ,"COMPONENTE" ," Q.TA'");


			$this->db->select('*');
			$this->db->from('accessori_commessa');
			$this->db->where('commessa', $this->uri->segment(3));
			$this->db->order_by("id_componente asc, codice_accessorio_interno", "asc");

			$prendi_acc = $this->db->get();

			if($prendi_acc->num_rows() > 0){
				foreach ($prendi_acc->result() as $accessorio)
				{
					$this->db->select('nome_accessorio_interno, ubicazione');
					$que = $this->db->get_where('magazzino_accessori',array('codice_accessorio_interno'=>$accessorio->codice_accessorio_interno));
					if($que->num_rows() > 0)
						foreach ($que->result() as $magazzino);
					else
						$magazzino->nome_accessorio_interno='';


					$this->db->select('nome_componente');
					$que = $this->db->get_where('componenti',array('id_componente'=>$accessorio->id_componente));
					if($que->num_rows() > 0)
						foreach ($que->result() as $componente);
					else{
						$componente = new stdClass();
						$componente->nome_componente='';
					}

					if($componente_corrente!=$accessorio->id_componente){
						$componente_corrente=$accessorio->id_componente;
						$this->table->add_row("&nbsp;", "&nbsp;","&nbsp;","&nbsp;", "&nbsp;");
					}
					$this->table->add_row($magazzino->nome_accessorio_interno, $accessorio->codice_accessorio_interno ,$magazzino->ubicazione,$componente->nome_componente, $accessorio->quantita);
				}
			}

			$tmpl = array (
			                    'table_open'          => '<table style="text-align: left; width: 1029px;" border="1" cellpadding="1" cellspacing="1">',
			                    'heading_row_start'   => '<tr>',
			                    'heading_row_end'     => '</tr>',
			                    'heading_cell_start'  => '<th>',
			                    'heading_cell_end'    => '</th>',
			                    'row_start'           => '<tr>',
			                    'row_end'             => '</tr>',
			                    'cell_start'          => '<td>',
			                    'cell_end'            => '</td>',
			                    'row_alt_start'       => '<tr>',
			                    'row_alt_end'         => '</tr>',
			                    'cell_alt_start'      => '<td>',
			                    'cell_alt_end'        => '</td>',
			                    'table_close'         => '</table>'

			              );

			$this->table->set_template($tmpl);
			$dati->tab_accessori=$this->table->generate();
			$this->load->view('/views_stampa/pagina_stampa_accessori.php',$dati);
	}


	function clona_commessa()
	{
		$where = "commessa = ".$this->uri->segment(3);
		$que = $this->db->get_where('lavori',$where);
		foreach ($que->result_array() as $dati);
		unset($dati['commessa']);
		unset($dati['ddt_n']);
		unset($dati['ddt_data']);
		unset($dati['fattura_n']);
		unset($dati['fattura_data']);
		unset($dati['stato_comm']);
		$this->db->insert('lavori',$dati);
		$nuova_commessa=$this->db->insert_id();

		$que = $this->db->get_where('bolle_prod',$where);
		foreach ($que->result_array() as $dati){
			$dati['commessa']=$nuova_commessa;
			unset($dati['cod_bolla']);
			unset($dati['id_operatore']);
			unset($dati['stato']);
			$this->db->insert('bolle_prod',$dati);
		}
		$que = $this->db->get_where('fasi_commessa',$where);
		foreach ($que->result_array() as $dati){
			$dati['commessa']=$nuova_commessa;
			unset($dati['id_fase_commessa']);
			unset($dati['id_operatore']);
			unset($dati['stato']);
			$this->db->insert('fasi_commessa',$dati);
		}

		redirect('/omis/lavori/incorso', 'refresh');

	}


	function trattamenti()
	{
		$crud = new grocery_CRUD();
		$crud->set_theme('datatables');
		$crud->set_table('trattamenti');
		$crud->set_subject('Trattamento');

		$output = $crud->render();
		$output->header="ANAGRAFICA TRATTAMENTI";
		$output->footer="";

		$output->menu=$this->build_menu();

			$this->load->view('omis_view.php',$output);



	}
	function piano_dei_conti()
	{
		$crud = new grocery_CRUD();
		$crud->set_theme('datatables');
		$crud->set_table('piano_dei_conti');
		$crud->set_subject('Piano');

		$output = $crud->render();
		$output->header="PIANO DEI CONTI";
		$output->footer="";

		$output->menu=$this->build_menu();

			$this->load->view('omis_view.php',$output);



	}
	function fornitori()
	{
		$crud = new grocery_CRUD();
		$crud->set_theme('datatables');
		$crud->set_table('fornitori');
		$crud->set_subject('Fornitore');
		$crud->display_as('id_modalita_pagamento','Mod. Pagam');
		$crud->unset_texteditor('note_fornitore_ordine');
		$crud->set_relation_n_n('piani_dei_conti','conti_fornitori','piano_dei_conti','id_fornitore','id_piano_conti','num_piano_conto');
		$crud->set_relation('id_modalita_pagamento','modalita_pagamento_a_fornitore','nome_modalita_pagamento');
		$output = $crud->render();
		$output->header="ANAGRAFICA FORNITORI";
		$output->footer="";

		$output->menu=$this->build_menu();

		$this->load->view('omis_view.php',$output);

	}
	function lavesterne()
	{
		$crud = new grocery_CRUD();
		$crud->set_theme('datatables');
		$crud->set_table('tipi_lav_esterne');
		$crud->set_subject('Tipo Lavorazione');

		$output = $crud->render();
		$output->header="ANAGRAFICA LAVORAZIONI ESTERNE";
		$output->footer="";

		$output->menu=$this->build_menu();


			$this->load->view('omis_view.php',$output);


	}
	function operatori()
	{
		$crud = new grocery_CRUD();
		$crud->set_theme('datatables');
		$crud->set_table('operatori');
		$crud->set_subject('Operatore');

		$crud->change_field_type('password_op', 'password');

		$crud->set_relation('id_centro_costo','centri_di_costo','nome_centro_costo');
		$crud->set_relation('id_cliente','clienti','ragione_soc');
		$crud->set_relation('id_produttore','produttori','ragione_soc');
		$crud->display_as('id_centro_costo','Centro di costo associato');
		//$crud->unset_edit_fields('password_op');

		$output = $crud->render();
		$output->header="ANAGRAFICA OPERATORI";
		$output->footer="";

		$output->menu=$this->build_menu();

		$this->load->view('omis_view.php',$output);

	}
	function fasi()
	{
		$crud = new grocery_CRUD();
		$crud->set_theme('datatables');
		$crud->set_table('fasi_lavorazione');
		$crud->set_subject('Fase');

		$output = $crud->render();
		$output->header="ANAGRAFICA FASI";
		$output->footer="";

		$output->menu=$this->build_menu();

			$this->load->view('omis_view.php',$output);


	}
	function semilavorati()
	{
		$crud = new grocery_CRUD();
		$crud->set_theme('datatables');
		$crud->set_table('anagrafica_semilavorati');
		$crud->set_subject('Semilavorato');
		$crud->set_relation('id_fornitore','fornitori','nome_fornitore');
		$crud->unset_texteditor('note_semilavorato');
		$crud->callback_add_field('codice_semilavorato_interno',array($this,'ListaSemilavoratiCodice'));
		$crud->callback_edit_field('codice_semilavorato_interno',array($this,'ListaSemilavoratiCodice'));

		$crud->display_as('id_fornitore','Fornitore');

		$output = $crud->render();
		$output->header="ANAGRAFICA SEMILAVORATI";
		$output->footer="";

		$output->menu=$this->build_menu();

		$this->load->view('omis_view.php',$output);

	}
	function componenti()
	{
		$crud = new grocery_CRUD();
		$crud->set_theme('datatables');
		$crud->set_table('componenti');
		$crud->set_subject('Componente');
		$crud->columns('nome_componente','id_categoria');
		$crud->set_relation('id_categoria','categorie_componenti','nome_categoria');
		$crud->display_as('id_categoria','Categoria');


		$output = $crud->render();
		$output->header="ANAGRAFICA COMPONENTI";
		$output->footer="";

		$output->menu=$this->build_menu();


			$this->load->view('omis_view.php',$output);


	}
	function banche()
	{
		$crud = new grocery_CRUD();
		$crud->set_theme('datatables');
		$crud->set_table('banca_appoggio_cliente');
		$crud->set_subject('Banca');


		$output = $crud->render();
		$output->header="BANCHE CLIENTI";
		$output->footer="";

		$output->menu=$this->build_menu();


			$this->load->view('omis_view.php',$output);


	}
	function tipi_porto()
	{
		$crud = new grocery_CRUD();
		$crud->set_theme('datatables');
		$crud->set_table('tipo_porto');
		$crud->set_subject('Tipo Porto');


		$output = $crud->render();
		$output->header="TIPI PORTO";
		$output->footer="";

		$output->menu=$this->build_menu();


			$this->load->view('omis_view.php',$output);


	}
	function condizioni_pagamento()
	{
		$crud = new grocery_CRUD();
		$crud->set_theme('datatables');
		$crud->set_table('condizioni_di_pagamento');
		$crud->set_subject('Condizione pagamento');


		$output = $crud->render();
		$output->header="CONDIZIONI PAGAMENTO";
		$output->footer="";

		$output->menu=$this->build_menu();


		$this->load->view('omis_view.php',$output);


	}
	function costi_cdc_annui()
	{
		$crud = new grocery_CRUD();
		$crud->set_theme('datatables');
		$crud->set_table('costi_cdc_annui');
		$crud->set_subject('Costo Annuo');
		$crud->set_relation('id_centro_costo','centri_di_costo','nome_centro_costo');
		$crud->display_as('id_centro_costo','Centro di Costo');


		$output = $crud->render();
		$output->header="COSTI CDC ANNUI";
		$output->footer="";

		$output->menu=$this->build_menu();

		$this->load->view('omis_view.php',$output);


	}

	function tipi_trasporto()
	{
		$crud = new grocery_CRUD();
		$crud->set_theme('datatables');
		$crud->set_table('tipo_trasporto');
		$crud->set_subject('Tipo Trasporto');


		$output = $crud->render();
		$output->header="TIPI TRASPORTO";
		$output->footer="";

		$output->menu=$this->build_menu();


		$this->load->view('omis_view.php',$output);


	}
	function clienti()
	{
		$crud = new grocery_CRUD();
		$crud->set_theme('datatables');
		$crud->set_table('clienti');
		$crud->set_subject('Cliente');
		$crud->display_as('ragione_soc','Ragione Sociale');
		$crud->display_as('part_iva','Partita IVA');
		$crud->display_as('cod_fiscale','Codice Fiscale');
		$crud->display_as('id_condiz_pagam','Condizione di pagamento');
		$crud->display_as('id_banca_cliente','Banca di appoggio');
		$crud->display_as('id_tipo_trasp','Tipo trasporto');
		$crud->display_as('id_tipo_porto','Tipo porto');
		$crud->display_as('id_classe_iva','Classe IVA');
		$crud->display_as('partita_iva_cliente','Partita IVA');
		$crud->display_as('cf_cliente','Codice Fiscale');

		$crud->columns('ragione_soc','indirizzo','email_cliente','citta','riferimenti_cliente');
		$crud->add_fields('nome_cliente_documenti','indirizzo','citta','indirizzo_destinazione','citta_destinazione','nazione','partita_iva_cliente','cf_cliente','email_cliente','lingua','note_cliente','note_fatturazione','riferimenti_cliente','ragione_soc','id_condiz_pagam','id_banca_cliente','id_tipo_trasp','id_tipo_porto','id_classe_iva');
		$crud->edit_fields('nome_cliente_documenti','indirizzo','citta','indirizzo_destinazione','citta_destinazione','nazione','partita_iva_cliente','cf_cliente','email_cliente','lingua','note_cliente','note_fatturazione','riferimenti_cliente','ragione_soc','id_condiz_pagam','id_banca_cliente','id_tipo_trasp','id_tipo_porto','id_classe_iva');

		$crud->set_relation('id_condiz_pagam','condizioni_di_pagamento','nome_condiz_pagam');
		$crud->set_relation('id_banca_cliente','banca_appoggio_cliente','nome_banca_cliente');
		$crud->set_relation('id_tipo_trasp','tipo_trasporto','nome_tipo_trasp');
		$crud->set_relation('id_tipo_porto','tipo_porto','nome_tipo_porto');

		$crud->set_relation('id_classe_iva','classi_iva','nome_classe_iva');

		$crud->unset_texteditor('note_cliente');
		$crud->unset_texteditor('riferimenti_cliente');
		$crud->unset_texteditor('email_cliente');
		$crud->unset_texteditor('note_fatturazione');





		$output = $crud->render();
		$output->header="ANAGRAFICA CLIENTI";
		$output->footer="";

		$output->menu=$this->build_menu();

			$this->load->view('omis_view.php',$output);

	}

	function accessori()
	{
			$crud = new grocery_CRUD();
			$crud->set_theme('datatables');
			$crud->set_table('anagrafica_accessori');
			$crud->set_subject('Accessorio');
			$crud->set_relation('id_fornitore','fornitori','nome_fornitore');
			$crud->callback_add_field('codice_accessorio_interno',array($this,'ListaAccessoriCodice'));
			$crud->callback_edit_field('codice_accessorio_interno',array($this,'ListaAccessoriCodice'));


			$crud->required_fields('nome_accessorio');
			$crud->add_action('Immagine', '', '','ui-icon-plus',array($this,'apri_accessorio'));
			$crud->display_as('id_fornitore','Fornitore');

			if($this->uri->segment($this->uri->total_segments())!='ajax_call' ){
				$output = $crud->render_ajax();
				$output->header="ANAGRAFICA ACCESSORI";
				$output->footer="";

				$output->menu=$this->build_menu();

				$this->load->view('omis_view.php',$output);

			}else{
				$output = $crud->render_ajax();
				echo $output->output;
			}

	}
	function materiali()
	{
			$crud = new grocery_CRUD();
			$crud->set_theme('datatables');
			$crud->set_table('materiali');
			$crud->set_subject('Materiale');

			$output = $crud->render();
			$output->header="ANAGRAFICA MATERIALI";
			$output->footer="";

			$output->menu=$this->build_menu();

			$this->load->view('omis_view.php',$output);

	}
	function materiali_fornitori()
	{
			$crud = new grocery_CRUD();
			$crud->set_theme('datatables');
			$crud->set_table('anagrafica_materiali');
			$crud->set_subject('Materiale Fornitore');
			$crud->set_relation('id_fornitore','fornitori','nome_fornitore');
			$crud->set_relation('id_materiale','materiali','nome_materiale');
			$crud->display_as('id_fornitore','Fornitore');
			$crud->display_as('id_materiale','Materiale');
			$crud->callback_add_field('codice_materiale_interno',array($this,'ListaMagazzinoMateriali'));
			$crud->callback_edit_field('codice_materiale_interno',array($this,'ListaMagazzinoMateriali'));
			$crud->callback_column('codice_materiale_interno',array($this,'ListaMagazzinoMaterialiColumn'));
			$output = $crud->render();
			$output->header="ANAGRAFICA MATERIALI FORNITORE";
			$output->footer="";

			$output->menu=$this->build_menu();

			$this->load->view('omis_view.php',$output);

	}
	function ListaMagazzinoMaterialiColumn($value,$row)
	{
		if($value!=""){
			$query = $this->db->get_where('magazzino_materiali',array('codice_materiale_interno'=>$value));
			foreach ($query->result() as $mat);
			$query = $this->db->get_where('materiali',array('id_materiale'=>$mat->id_materiale));
			foreach ($query->result() as $nomemat);
			$asd=$nomemat->nome_materiale."  ".$mat->forma_materiale." ".$mat->sezione_materiale;
		}else
		{$asd="";
		}
		return $asd;
	}

	function ListaMagazzinoMateriali($value, $primary_key)
	{
		$class="id='' class='chosen-select'";
		$asd['']='';
		if(!(isset($value)))
		{
			$value="";
		}
		$query = $this->db->get('magazzino_materiali');
		foreach ($query->result() as $row)
		{
			if($row->codice_materiale_interno!=""){
				$query = $this->db->get_where('magazzino_materiali',array('codice_materiale_interno'=>$row->codice_materiale_interno));
				foreach ($query->result() as $mat);
				$query = $this->db->get_where('materiali',array('id_materiale'=>$mat->id_materiale));
				foreach ($query->result() as $nomemat);
				$asd[$row->codice_materiale_interno]=$nomemat->nome_materiale."  ".$mat->forma_materiale." ".$mat->sezione_materiale;
			}
		}

		return form_dropdown('codice_materiale_interno', $asd, $value,$class);
	}

	function categorie_comp()
	{
			$crud = new grocery_CRUD();
			$crud->set_theme('datatables');
			$crud->set_table('categorie_componenti');
			$crud->set_subject('Categoria');

			$output = $crud->render();
			$output->header="ANAGRAFICA CATEGORIE COMPONENTI";
			$output->footer="";

			$output->menu=$this->build_menu();

			$this->load->view('omis_view.php',$output);

	}
	function centricosto()
	{
			$crud = new grocery_CRUD();
			$crud->set_theme('datatables');
			$crud->set_table('centri_di_costo');
			$crud->set_subject('Centro di Costo');

			$output = $crud->render();
			$output->header="ANAGRAFICA CENTRI DI COSTO";
			$output->footer="";

			$output->menu=$this->build_menu();


				$this->load->view('omis_view.php',$output);

	}

	function produttori()
	{
		$crud = new grocery_CRUD();
		$crud->set_theme('datatables');
		$crud->set_table('produttori');
		$crud->set_subject('Produttore');
		$crud->display_as('ragione_soc','Ragione Sociale');
		$crud->display_as('part_iva','Partita IVA');
		$crud->display_as('cod_fiscale','Codice Fiscale');
		$crud->add_action('Copia in clienti', '', 'omis/aggiungiAClienti','ui-icon-plus');
		$crud->unset_texteditor('note_produttore');





		$crud->set_relation_n_n('stampatrici','client_stamp','stampatrici','id_produttore','nome_stamp','nome_stamp');
		$output = $crud->render();
		$output->header="ANAGRAFICA PRODUTTORI";
		$output->footer="";

		$output->menu=$this->build_menu();


			$this->load->view('omis_view.php',$output);

	}
	function stampatrici()
	{
		$crud = new grocery_CRUD();
		$crud->set_theme('datatables');
		$crud->set_table('stampatrici');
		$crud->set_subject('Stampatrice');
		$output = $crud->render();
		$output->header="STAMPATRICI";
		$output->footer="";

		$output->menu=$this->build_menu();


			$this->load->view('omis_view.php',$output);

	}
	function note_produttori()
	{
		$crud = new grocery_CRUD();
		$crud->set_theme('datatables');
		$crud->set_table('note_produttori');
		$crud->set_subject('Nota');

		$crud->set_relation('id_produttore','produttori','ragione_soc');
		$crud->display_as('id_produttore','Produttore');
		$crud->unset_texteditor('nota_campo');
		$crud->callback_add_field('nome_campo',array($this,'nomi_campi_lavori'));
		$crud->callback_edit_field('nome_campo',array($this,'nomi_campi_lavori_edit'));



		$output = $crud->render();
		$output->header="NOTE PRODUTTORI";
		$output->footer="";

		$output->menu=$this->build_menu();


			$this->load->view('omis_view.php',$output);

	}
	function nomi_campi_lavori(){
		$asd['n_stampo']="Numero Stampo";
		$asd['dis_sapone']="Disegno Sapone";
		$asd['tipo_lavoro']="Tipo Lavoro";
		$asd['c_lavorazione']="Conto Lavorazione";
		$asd['id_produttore']="Produttore";
		$asd['id_destinatario']="Destinatario";
		$asd['ordine_cliente']="Ordine Cliente";
		$asd['comm_cliente']="Commessa Cliente";
		$asd['rif_cliente']="Riferimento Cliente";
		$asd['nome_sapone']="Nome Sapone";
		$asd['peso']="Peso Sapone";
		$asd['peso_specifico']="Peso Specifico";
		$asd['stampatrice']="Stampatrice";
		$asd['compatibile']="Compatibile";
		$asd['linea_produzione']="Linea di Produzione";
		$asd['n_cavita']="Numero Cavita'";
		$asd['tipo_stampo']="Tipo Stampo";
		$asd['data_cons_dis_prev']="Data Consegna Disegno Prev";
		$asd['data_consegna_cliente']="Data Consegna al Cliente";
		$asd['data_consegna_interna']="Data Consegna Interna";
		$asd['orientam_scarico']="Orientamento Scarico";
		$asd['id_materiale_matrice']="Materiale Stampo";
		$asd['id_materiale_piastre']="Materiale Piastre";
		$asd['id_trattam_matrice']="Trattamento Stampo";
		$asd['id_trattam_piastre']="Trattamento Piastre";
		return form_dropdown('nome_campo', $asd);
	}
	function nomi_campi_lavori_edit(){
		$query = $this->db->get_where('note_produttori', array('id_nota_produttore' => $this->uri->segment(4)));
		foreach ($query->result() as $row);

		$asd['n_stampo']="Numero Stampo";
		$asd['dis_sapone']="Disegno Sapone";
		$asd['tipo_lavoro']="Tipo Lavoro";
		$asd['c_lavorazione']="Conto Lavorazione";
		$asd['id_produttore']="Produttore";
		$asd['id_destinatario']="Destinatario";
		$asd['ordine_cliente']="Ordine Cliente";
		$asd['comm_cliente']="Commessa Cliente";
		$asd['rif_cliente']="Riferimento Cliente";
		$asd['nome_sapone']="Nome Sapone";
		$asd['peso']="Peso Sapone";
		$asd['peso_specifico']="Peso Specifico";
		$asd['stampatrice']="Stampatrice";
		$asd['compatibile']="Compatibile";
		$asd['linea_produzione']="Linea di Produzione";
		$asd['n_cavita']="Numero Cavita'";
		$asd['tipo_stampo']="Tipo Stampo";
		$asd['data_cons_dis_prev']="Data Consegna Disegno Prev";
		$asd['data_consegna_cliente']="Data Consegna al Cliente";
		$asd['data_consegna_interna']="Data Consegna Interna";
		$asd['orientam_scarico']="Orientamento Scarico";
		$asd['id_materiale_matrice']="Materiale Stampo";
		$asd['id_materiale_piastre']="Materiale Piastre";
		$asd['id_trattam_matrice']="Trattamento Stampo";
		$asd['id_trattam_piastre']="Trattamento Piastre";
		return form_dropdown('nome_campo', $asd,$row->nome_campo);
	}
	function logout()
	{
		$this->session->sess_destroy();
		redirect('/login/show', 'refresh');
	}


}
