<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Ddt extends CI_Controller {

	function __construct()
	{
		parent::__construct();

		/* Standard Libraries */
		$this->load->database();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('date');
		$this->load->helper('security');
		$this->load->library('session');

		$this->load->library('grocery_CRUD');
		if(!$this->session->userdata('logged_in'))
			redirect('/login/show', 'refresh');


	}

	function build_menu(){
		$vars=array();
		$vars['tipo_op']=$this->session->userdata('tipo_op');
		$vars['nome_op']=$this->session->userdata('nome_operatore');

		extract($vars);
			#region buffering...
			ob_start();

			include(getcwd().'/application/views/menu_builder.php');

			$buffer = ob_get_contents();
		@ob_end_clean();
		return $buffer;
	}

	function aggiungi_comm_ddt(){
		date_default_timezone_set('Europe/Rome');

		$this->popola_ddt($this->uri->segment(3), $this->uri->segment(4));
		$query=$this->db->get_where('tabella_ddt',array('id_ddt'=>$this->uri->segment(3)));
		foreach ($query->result() as $ddt);
		$this->db->where('commessa', $this->uri->segment(4));
		$this->db->update('lavori', array('ddt_n'=> $ddt->num_ddt,'ddt_data'=>$ddt->data_creazione_ddt,'stato'=>'E'));

		redirect('/ddt/ricarica_ddt/'.$this->uri->segment(3), 'refresh');


	}

	function aggiungi_comm_ddt_fornitore(){
		date_default_timezone_set('Europe/Rome');
		$ddt_commessa=array(
							'id_ddt' => $this->uri->segment(3),
							'commessa' => $this->uri->segment(4)
						);
		$this->db->insert('ddt_commesse',$ddt_commessa);;

		redirect('/ddt/ricarica_ddt/'.$this->uri->segment(3), 'refresh');

	}

	function elimina_comm_ddt()
	{
    $this->db->delete('ddt_commesse', array('id_ddt'=> $this->uri->segment(3),'commessa'=>$this->uri->segment(4)));
		$this->db->delete('righe_ddt', array('id_ddt'=> $this->uri->segment(3),'commessa'=>$this->uri->segment(4)));
		$this->db->where('commessa', $this->uri->segment(4));
		$this->db->update('lavori', array('ddt_n'=> NULL,'ddt_data'=>NULL,'stato'=>'C'));

		$query=$this->db->get_where('tabella_ddt',array('id_ddt'=>$this->uri->segment(3)));
		foreach ($query->result() as $ddt);
		$this->db->delete('lav_esterne_commessa', array('ddt_omis'=> $ddt->num_ddt, 'commessa'=>$this->uri->segment(4)));

		redirect('/ddt/ricarica_ddt/'.$this->uri->segment(3), 'refresh');
	}

function ddt_inizio(){

		date_default_timezone_set('Europe/Rome');

		$query = $this->db->get_where('lavori',array('commessa'=>$this->uri->segment(3)));
		foreach ($query->result() as $commessa);
		$query = $this->db->get_where('clienti',array('id_cliente'=>$commessa->cliente));
		foreach ($query->result() as $cliente);


		//sto creando ddt cliente quindi commessa è eseguita!

		$this->db->where('commessa', $commessa->commessa);
		$this->db->update('lavori', array("stato" => 'E'));

		$query = $this->db->get_where('ddt_commesse',array('commessa'=>$commessa->commessa));
		foreach ($query->result() as $ellabella);
		if($query->num_rows()>0){
			$query = $this->db->get_where('tabella_ddt',array('id_ddt'=>$ellabella->id_ddt,'tipo_ddt'=>'C'));
		}
		if($query->num_rows()==0){ //controllo che effettivamente non esista già un DDT relativo a questa commessa
				$this->db->select_max('num_ddt');
				$this->db->where("data_creazione_ddt BETWEEN '".date('Y')."-01-01' AND '".date('Y')."-12-31'");
				$query = $this->db->get('tabella_ddt');

				foreach ($query->result() as $ddtmax);

				$this->db->where('commessa', $commessa->commessa);
				$this->db->update('lavori', array('ddt_n'=> (($ddtmax->num_ddt)+1),'ddt_data'=>date('Y-m-d')));
				$luogo_destinazione=$cliente->nome_cliente_documenti."<br>".$cliente->indirizzo_destinazione." <br>".$cliente->citta_destinazione." - ".$cliente->nazione;
				$nuovoDdt=array(
					'num_ddt'=> ($ddtmax->num_ddt)+1,
					'luogo_destinazione' => $luogo_destinazione,
					'data_creazione_ddt' => date('Y-m-d'),
					'id_cliente' => $commessa->cliente,
					'id_tipo_trasp'=>$cliente->id_tipo_trasp,
					'id_tipo_porto' => $cliente->id_tipo_porto,
					'id_causale_trasp'=>1,
					'id_vettore'=>1,
					'id_aspetto_beni'=>'A VISTA',
					'n_colli'=>1,
					'peso_lordo'=>"0.00",
					'peso_netto'=>"0.00",
					'dimensioni' => "",
					'tipo_ddt' => 'C'
				);
				$this->db->insert('tabella_ddt',$nuovoDdt);
				$id_ddt=$this->db->insert_id();

				$this->popola_ddt($id_ddt,$commessa->commessa);
		}else{
			$id_ddt=$ellabella->id_ddt;
		}
		redirect('/ddt/ricarica_ddt/'.$id_ddt, 'refresh');

	}

	function ddt_fornitore_inizio(){
		date_default_timezone_set('Europe/Rome');

		$query = $this->db->get_where('lavori',array('commessa'=>$this->uri->segment(3)));
		foreach ($query->result() as $commessa);
		$query = $this->db->get_where('clienti',array('id_cliente'=>$commessa->cliente));
		foreach ($query->result() as $cliente);


		$this->db->select_max('num_ddt');
		$this->db->where("data_creazione_ddt BETWEEN '".date('Y')."-01-01' AND '".date('Y')."-12-31'");
		$query = $this->db->get('tabella_ddt');

		foreach ($query->result() as $ddtmax);

		$luogo_destinazione="";
		$nuovoDdt=array(
			'num_ddt'=> ($ddtmax->num_ddt)+1,
			'luogo_destinazione' => $luogo_destinazione,
			'data_creazione_ddt' => date('Y-m-d'),
			'id_tipo_trasp'=>2,
			'id_tipo_porto' => 3,
			'id_causale_trasp'=>3,
			'id_vettore'=>1,
			'id_aspetto_beni'=>'A VISTA',
			'n_colli'=>1,
			'peso_lordo'=>"0.00",
			'peso_netto'=>"0.00",
			'dimensioni' => "",
			'tipo_ddt' => 'F'
		);

		$this->db->insert('tabella_ddt',$nuovoDdt);
		$id_ddt=$this->db->insert_id();

		$ddt_commessa=array(
			'id_ddt' => $id_ddt,
			'commessa' => $commessa->commessa
		);
		$this->db->insert('ddt_commesse',$ddt_commessa);
		redirect('/ddt/ricarica_ddt/'.$id_ddt, 'refresh');
	}


function popola_ddt($id_ddt, $commessaArg){
		date_default_timezone_set('Europe/Rome');

		$trovatoDDT=false;
		$query = $this->db->get_where('preventivi_commesse',array('commessa'=>$commessaArg));
		if($query->num_rows()>0){
			foreach ($query->result() as $preventivo){
				$queryinterna = $this->db->get_where('preventivi',array('id_preventivo'=>$preventivo->id_preventivo,'num_revisione_confermata'=>$preventivo->num_revisione));
				if($queryinterna->num_rows()>0){$trovatoDDT=true; break;}
			}
			if($trovatoDDT){

				$ddt_commessa=array(
							'id_ddt' => $id_ddt,
							'commessa' => $commessaArg,
							'stampo_nome_peso' => $preventivo->stampo_nome_peso
						);
				$this->db->insert('ddt_commesse',$ddt_commessa);

				$query = $this->db->get_where('righe_preventivi',array('id_preventivo'=>$preventivo->id_preventivo,'commessa'=>$commessaArg,'num_revisione'=>$preventivo->num_revisione));
				foreach ($query->result() as $riga_prev){
						$nuovaRigaDdt=array(
							'id_ddt' => $id_ddt,
							'commessa' => $commessaArg,
							'descrizione' => $riga_prev->descrizione,
							'quantita' => $riga_prev->quantita,
							'prezzo' => $riga_prev->prezzo,
							'id_classe_iva' => $riga_prev->id_classe_iva,
							'sconto' => $riga_prev->sconto
						);
						$this->db->insert('righe_ddt',$nuovaRigaDdt);

				}

			}

		}
		if(!$trovatoDDT){ //creo ddt dal nulla se non lo trovo nei preventivi

			$query = $this->db->get_where('lavori',array('commessa'=>$commessaArg));
			foreach ($query->result() as $commessa);

			$ddt_commessa=array(
							'id_ddt' => $id_ddt,
							'commessa' => $commessaArg,
							'stampo_nome_peso' => "N° Disegno: ".$commessa->n_stampo."  '".$commessa->nome_sapone."'  ".$commessa->peso." gr."
						);
			$this->db->insert('ddt_commesse',$ddt_commessa);
			$query = $this->db->get_where('clienti',array('id_cliente'=>$commessa->cliente));
			foreach ($query->result() as $cliente);
			if($commessa->tipo_lavoro=='STA'){
				if($commessa->id_materiale_matrice!=NULL){
					$query = $this->db->get_where('materiali',array('id_materiale'=>$commessa->id_materiale_matrice));
					foreach ($query->result() as $mat_matrice);
				}else{
					$mat_matrice = new stdClass();
					$mat_matrice->nome_materiale="";
				}
				if($commessa->id_trattam_matrice!=NULL){
						$query = $this->db->get_where('trattamenti',array('id_trattamento'=>$commessa->id_trattam_matrice));
						foreach ($query->result() as $trattam);
						if($cliente->lingua=='ITA'){
							$trattamento=" con trattamento ".$trattam->nome_trattamento;
						}
						if($cliente->lingua=='ENG'){
							$trattamento=" with ".$trattam->nome_trattamento." coating";
						}
				}else{
					$trattamento="";
				}

				if($cliente->lingua=='ITA'){
				$descrizione="Stampo numero ".$commessa->n_stampo." '".$commessa->nome_sapone."' da ".$commessa->peso."gr per ".$commessa->stampatrice." a ".$commessa->n_cavita." cavita' in ".$mat_matrice->nome_materiale.$trattamento." compreso di incisioni e accessori standard";
				}
				if($cliente->lingua=='ENG'){
				$descrizione="Mould number ".$commessa->n_stampo." '".$commessa->nome_sapone."', ".$commessa->peso."gr for  ".$commessa->stampatrice." with ".$commessa->n_cavita." cavities in ".$mat_matrice->nome_materiale.$trattamento." including engravings and standard accessories";
				}
				if($cliente->lingua=='FRA'){
				$descrizione="Moule num ".$commessa->n_stampo." '".$commessa->nome_sapone."', ".$commessa->peso." gr pour  ".$commessa->stampatrice." avec ".$commessa->n_cavita." cavités en ".$mat_matrice->nome_materiale.$trattamento." complet des gravures et accessoires standard ";
				}
				$nuovaRigaPrev=array(
					'id_ddt' => $id_ddt,
					'commessa' => $commessaArg,
					'descrizione' => $descrizione,
					'quantita' => 1,
					'sconto' => 0,
					'id_classe_iva' => $cliente->id_classe_iva,
					'prezzo' =>"0.00"

				);
				$this->db->insert('righe_ddt',$nuovaRigaPrev);
			}else{
					$this->db->select('*');
					$this->db->from('bolle_prod');
					$this->db->join('componenti', 'bolle_prod.id_componente = componenti.id_componente');
					$this->db->where('commessa', $commessaArg);
					$this->db->order_by("nome_componente", "asc");
					$prendi_compo = $this->db->get();
					if($prendi_compo->num_rows()>0){
						foreach ($prendi_compo->result() as $componente)
						{

							if($componente->id_materiale!=NULL){
								$query = $this->db->get_where('materiali',array('id_materiale'=>$componente->id_materiale));
								foreach ($query->result() as $materialo);
							}else{
								$materialo = new stdClass();
								$materialo->nome_materiale="";
							}
							$query = $this->db->get_where('categorie_componenti',array('id_categoria'=>$componente->id_categoria));
							foreach ($query->result() as $categoria);

							if($componente->escludi_ddt=='N'){

								if($componente->id_trattamento!=NULL){
									$query = $this->db->get_where('trattamenti',array('id_trattamento'=>$componente->id_trattamento));
									foreach ($query->result() as $trattam);
									$trattamento=" con trattamento ".$trattam->nome_trattamento;
								}else{
									$trattamento="";
								}
								$descrizione=$categoria->nome_categoria." in ".$materialo->nome_materiale.$trattamento;
							 	$nuovaRigaPrev=array(
									'id_ddt' => $id_ddt,
									'commessa' => $commessa->commessa,
									'descrizione' => $descrizione,
									'quantita' => $componente->quantita,
									'sconto' => 0,
									'id_classe_iva' => $cliente->id_classe_iva,
									'prezzo' =>0

								);
								$this->db->insert('righe_ddt',$nuovaRigaPrev);

							}

						}
					}
				}
		}

	}

	function ricarica_ddt(){

		date_default_timezone_set('Europe/Rome');


		$query = $this->db->get_where('tabella_ddt',array('id_ddt'=>$this->uri->segment(3)));
		foreach ($query->result() as $mainDdt);
		$query = $this->db->get_where('clienti',array('id_cliente'=>$mainDdt->id_cliente));
		foreach ($query->result() as $cliente);




		//HEADER
		$output = new stdClass();
		@list($year,$month,$day) = explode("-",$mainDdt->data_creazione_ddt);
		$output->anno_ddt=$year;
		if($mainDdt->tipo_ddt=='C'){//preparare header con informazioni sul cliente

			$output->luogo_destinazione=str_replace('<br>', "\xA", $mainDdt->luogo_destinazione);
			$output->id_cliente=$cliente->id_cliente;
			$output->nome_cliente=$cliente->nome_cliente_documenti;
			$output->indirizzo_cliente=$cliente->indirizzo;
			$output->citta_cliente=$cliente->citta;
			$output->nazione_cliente=$cliente->nazione;
		}else{
			//preparare header con informazioni sul fornitore
			$output->luogo_destinazione=$mainDdt->luogo_destinazione;
			$this->db->order_by("nome_fornitore", "asc");
			$query = $this->db->get('fornitori');

			foreach ($query->result() as $row){
						$fornitori[$row->id_fornitore]=$row->nome_fornitore;
			}
			$output->fornitori=form_dropdown('id_fornitore', $fornitori, $mainDdt->id_fornitore,'class="chosen-select"');
			$output->data_ddt_db=$mainDdt->data_creazione_ddt;
		}


		$output->num_ddt=$mainDdt->num_ddt;
		@list($year,$month,$day) = explode("-",$mainDdt->data_creazione_ddt);
		$output->data_ddt=date ("d-m-Y",mktime (0,0,0,(int)$month , (int)$day , (int)$year));

		$output->id_ddt=$mainDdt->id_ddt;


		$output->tabella_descrizione="";
		$this->db->order_by('commessa');
		$query = $this->db->get_where('ddt_commesse',array('id_ddt'=>$mainDdt->id_ddt));


		foreach ($query->result() as $commessa_ddt) {
				$query = $this->db->get_where('lavori',array('commessa'=>$commessa_ddt->commessa));
				foreach ($query->result() as $commessa);

				if(($commessa->comm_cliente!=NULL)&&($mainDdt->tipo_ddt=='C')){$comm_cliente=" - Vs. Riferimento: ".$commessa->comm_cliente;}
				else{$comm_cliente='';}
								if($commessa->tipo_lavoro!='STA'){
									if($commessa->n_stampo!=NULL){$n_stampo="N° Disegno: ".$commessa->n_stampo;}
									else{$n_stampo='';}
								}else{
									$n_stampo='';
								}

							if(($commessa->ordine_cliente!=NULL)&&($mainDdt->tipo_ddt=='C')){
								if($commessa->data_ordine_cliente!=NULL){
									@list($year,$month,$day) = explode("-",$commessa->data_ordine_cliente);
									$data_ordine_cliente=date ("d-m-Y",mktime (0,0,0,(int)$month , (int)$day , (int)$year));
								}else{$data_ordine_cliente="";}

								if(substr($commessa->ordine_cliente, 0, 4)=="mail") $ordine_nome="mail";
								else $ordine_nome=$commessa->ordine_cliente;
								$vs_ordine="Vs. Ordine: ".$ordine_nome." del ".$data_ordine_cliente;
							}else{ $vs_ordine="";}



							$output->tabella_descrizione.="<div class='row' style='margin-top:20px;'>
							<div class='col-sm-6'> Ns. Riferimento: <strong>".$commessa->commessa."</strong>
													<input type='button' class='elimina_comm' value='ELIMINA COMM' commessa='".$commessa->commessa."' />
													<a href='".site_url("omis/dettaglio/componenti/".$commessa->commessa)."''><input type='button' value='DETTAGLIO'/></a>
							</div>
							<div class='col-sm-1'></div><div class='col-sm-1'></div><div class='col-sm-1'></div><div class='col-sm-1'></div><div class='col-sm-2'></div>
							</div>";
							$output->tabella_descrizione.="<div class='row' style='margin-top:10px;'>
							<div class='col-sm-6'>".$vs_ordine.$comm_cliente." </div>
							<div class='col-sm-1'></div><div class='col-sm-1'></div><div class='col-sm-1'></div><div class='col-sm-1'></div><div class='col-sm-2'></div>
							</div>";
							if($mainDdt->tipo_ddt=='C')
								$output->tabella_descrizione.="<div class='row' style='margin-top:10px;'>
							<div class='col-sm-6'><textarea commessa='".$commessa->commessa."' class='stampo_nome_peso' style='width:100%; font-weight: bold;' rows='2'>".str_replace('<br>', "\xA", $commessa_ddt->stampo_nome_peso)."</textarea> </div>
							<div class='col-sm-1'></div>
							<div class='col-sm-1'></div>
							<div class='col-sm-1'></div>
							<div class='col-sm-1'></div>
							<div class='col-sm-2'></div>
							</div>";
				$query = $this->db->get_where('righe_ddt',array('id_ddt'=>$mainDdt->id_ddt,'commessa'=>$commessa_ddt->commessa));

				foreach ($query->result() as $riga_ddt){

					if($mainDdt->tipo_ddt=='C'){
						$lista_imposte='<small><select name="classe_iva_riga" id_riga="'.$riga_ddt->id_riga_ddt.'">';
									$queryoz = $this->db->get('classi_iva');
									foreach ($queryoz->result() as $classe){
											if($classe->id_classe_iva==$riga_ddt->id_classe_iva){//da leggere da preventivo
												$selected='selected="selected"';

											}else{$selected="";}

											$lista_imposte.='<option value="'.$classe->id_classe_iva.'" valore="'.$classe->valore_classe.'"'.$selected.' >'.$classe->nome_classe_iva.'</option>';
									}
									$lista_imposte.='</select></small>';
									$elimina_riga="<small><input type='button' class='elimina_riga' id_riga='".$riga_ddt->id_riga_ddt."' value='X'></small>";
									$quantita_campo="<input type='text' style='text-align:right; width:50%;' value='".$riga_ddt->quantita."' id_riga='".$riga_ddt->id_riga_ddt."' class='qta_riga'>";


									$output->tabella_descrizione.='
										<div class="row" style="margin-top:10px;">
											<div class="col-sm-6"><textarea style="width:100%;" id_riga="'.$riga_ddt->id_riga_ddt.'" class="descrizione">'.str_replace('<br>', "\xA", $riga_ddt->descrizione).'</textarea></div>
											<div class="col-sm-1">'.$quantita_campo.'</div>
											<div class="col-sm-1" style="padding-right:5px;"><input class="prezzo_riga" id_riga="'.$riga_ddt->id_riga_ddt.'"" style="text-align:right; width:80%;" value="'.$riga_ddt->prezzo.'"" type="text"> &#8364; </div>
											<div class="col-sm-1" style="padding-right:5px;"><input class="sconto_riga" id_riga="'.$riga_ddt->id_riga_ddt.'"" style="text-align:right; width:50%;" value="'.$riga_ddt->sconto.'"" type="text"> % </div>
											<div class="col-sm-1"><input class="totale_riga" id_riga="'.$riga_ddt->id_riga_ddt.'"" type="text" value="0.00" style="width:80%;" disabled> &#8364;</div>
											<div class="col-sm-2" style="padding-left:0px;">'.$lista_imposte.$elimina_riga.'</div>
										</div>
									';
					}else{ //se è un ddt fornitore
						$queryoz = $this->db->get('tipi_lav_esterne');
						$lista_lavorazioni='<select name="lavorazione_riga" id_riga="'.$riga_ddt->id_riga_ddt.'">';
						foreach ($queryoz->result() as $lav){
								if($lav->id_tipo_lav==$riga_ddt->id_tipo_lav){//da leggere da preventivo
										$selected='selected="selected"';

										}else{$selected="";}

									$lista_lavorazioni.='<option value="'.$lav->id_tipo_lav.'" '.$selected.' >'.$lav->nome_lav_esterna.'</option>';
								}
						$lista_lavorazioni.='</select>';
						$elimina_riga="<small><input type='button' class='elimina_riga' id_riga='".$riga_ddt->id_riga_ddt."' value='X'></small>";
						$quantita_campo="<input type='text' style='text-align:right; width:50%;' value='".$riga_ddt->quantita."' id_riga='".$riga_ddt->id_riga_ddt."' class='qta_riga'>";
						$output->tabella_descrizione.='
										<div class="row" style="margin-top:10px;">
											<div class="col-sm-6"><textarea style="width:100%;" id_riga="'.$riga_ddt->id_riga_ddt.'" class="descrizione">'.str_replace('<br>', "\xA", $riga_ddt->descrizione).'</textarea></div>
											<div class="col-sm-3">'.$lista_lavorazioni.'</div>
											<div class="col-sm-1">'.$quantita_campo.'</div>
											<div class="col-sm-2" style="padding-left:0px;">'.$elimina_riga.'</div>
										</div>
									';
					}
				}
			if($mainDdt->tipo_ddt=='C'){

						if($commessa->tipo_lavoro=='STA')
							$output->tabella_descrizione.='<div class="row" style="margin-top:10px;">
											<div class="col-sm-6"><input tipo_lavoro="STA" commessa="'.$commessa->commessa.'" type="button" value="DESCRIZIONE" style="width:50%; font-size:20px; background-color:#33FFFF;" class="nuovariga"><input commessa="'.$commessa->commessa.'" type="button" value="COMPONENTE" style="width:50%; font-size:20px; background-color:#33FFFF;" class="nuovarigacomponente"></div>
											<div class="col-sm-1"></div>
											<div class="col-sm-1" ></div>
											<div class="col-sm-1" ></div>
											<div class="col-sm-1"></div>
											<div class="col-sm-2" style="padding-left:0px;"></div>
										</div>';
						else
							$output->tabella_descrizione.='<div class="row" style="margin-top:10px;">
											<div class="col-sm-6"><input tipo_lavoro="NS" commessa="'.$commessa->commessa.'" type="button" value="DESCRIZIONE" style="width:100%; font-size:20px; background-color:#33FFFF;" class="nuovariga"></div>
											<div class="col-sm-1"></div>
											<div class="col-sm-1" ></div>
											<div class="col-sm-1" ></div>
											<div class="col-sm-1"></div>
											<div class="col-sm-2" style="padding-left:0px;"></div>
										</div>';
			}else{
				$output->tabella_descrizione.='<div class="row" style="margin-top:10px;">
											<div class="col-sm-6"><input tipo_lavoro="STA" commessa="'.$commessa->commessa.'" type="button" value="DESCRIZIONE" style="width:50%; font-size:20px; background-color:#33FFFF;" class="nuovariga_fornitore"><input commessa="'.$commessa->commessa.'" type="button" value="COMPONENTE" style="width:50%; font-size:20px; background-color:#33FFFF;" class="nuovarigacomponente_fornitore"></div>
											<div class="col-sm-3"></div>
											<div class="col-sm-1"></div>
											<div class="col-sm-2" style="padding-left:0px;"></div>
										</div>';
			}
		}

		//---fine tabella descrizione-------------------------------------------------FINEFINEINFINEINFIDNINEIENEINE

		$query = $this->db->get('tipo_trasporto');
		foreach ($query->result() as $row){
					$tipi_trasporto[$row->id_tipo_trasp]=$row->nome_tipo_trasp;
		}
		$output->tipo_trasporto=form_dropdown('tipo_trasporto', $tipi_trasporto, $mainDdt->id_tipo_trasp);
		$query = $this->db->get('vettori');
		foreach ($query->result() as $row){
					$vettori[$row->id_vettore]=$row->nome_vettore;
		}
		$output->vettori=form_dropdown('vettore', $vettori, $mainDdt->id_vettore);
		$aspettobeni=array(
			'A VISTA'=>'A VISTA',
			'BUSTA'=>'BUSTA',
			'CARTONE'=>'CARTONE',
			'CASSA IN LEGNO'=>'CASSA IN LEGNO'
			);
		$output->aspetto_beni=form_dropdown('aspetto_beni', $aspettobeni, $mainDdt->id_aspetto_beni);
		$query = $this->db->get('causali_trasporto');
		foreach ($query->result() as $row){
		 			$causali[$row->id_causale_trasp]=$row->nome_causale;
		 }
		$output->causali_trasporto=form_dropdown('id_causale_trasp', $causali, $mainDdt->id_causale_trasp);
		$query = $this->db->get('tipo_porto');
		foreach ($query->result() as $row){
					$tipi_porto[$row->id_tipo_porto]=$row->nome_tipo_porto;
		}
		$output->tipo_porto=form_dropdown('tipo_porto', $tipi_porto, $mainDdt->id_tipo_porto);
		$output->n_colli=$mainDdt->n_colli;
		$output->peso_netto=$mainDdt->peso_netto;
		$output->peso_lordo=$mainDdt->peso_lordo;
		$output->dimensioni=$mainDdt->dimensioni;
		// //$output->sconto="0";


		$output->base_url="http://".$_SERVER["SERVER_NAME"].":8000/";

		$output->nome_op=$this->session->userdata('nome_operatore');
		switch($this->session->userdata('nome_operatore')){
			case "ANDREA":
				$user='Andrea Piatto';
				$mail='andrea.piatto@omis.it';

			break;
			case "FRANZ":
				$user='Franz Piatto';
				$mail='franz.piatto@omis.it';

			break;
			case "GIANNI":
				$user='Gianni Piatto';
				$mail='gianni.piatto@omis.it';

			break;
			case "LORI":
				$user='Lori';
				$mail='ufficio@omis.it';

			break;
			default:
				$user="";
			break;
		}


		$output->menu=$this->build_menu();

		if($mainDdt->tipo_ddt=='C'){
			$output->signature="Buongiorno, \xA\xA"."Avviso di merce pronta. \xA\xAIn allegato nostro DDT come in oggetto."."\xA\xA".'Grazie e saluti. '."\xA\xA".$user."\xA".'www.omis.it '."\xA".'Tel. +39 0331 776097 '."\xA".'Fax  +39 0331 772290';
			$output->oggetto_email="Documento di Trasporto ".$mainDdt->num_ddt." del ".$output->data_ddt;
			$output->email_cliente=$cliente->email_cliente;
			$output->ccns_email=$mail;
			$this->load->view('ddt_view.php',$output);
		}
		else{
			$output->signature="Buongiorno, \xA\xAIn allegato nostro DDT come in oggetto."."\xA\xA".'Grazie e saluti. '."\xA\xA".$user."\xA".'www.omis.it '."\xA".'Tel. +39 0331 776097 '."\xA".'Fax  +39 0331 772290';
			$output->oggetto_email="Documento di Trasporto ".$mainDdt->num_ddt." del ".$output->data_ddt;
			$output->ccns_email=$mail;
			$this->load->view('ddt_fornitore_view.php',$output);
		}


	}


	function elimina_ddt(){
		$this->before_delete_ddt($this->uri->segment(3));

		$this->db->delete('tabella_ddt', array('id_ddt'=> $this->uri->segment(3)));

		redirect('/ddt/tabella_ddt', 'refresh');

	}


	function tabella_ddt()
	{
			$crud = new grocery_CRUD();
			$crud->set_theme('datatables');
			$crud->set_table('tabella_ddt');

			$crud->unset_texteditor('luogo_destinazione');
			$crud->unset_texteditor('annotazioni');

			$crud->unset_add();
			$crud->unset_edit();
			$crud->unset_delete();

			$crud->add_action('Visualizza', '', 'ddt/ricarica_ddt','ui-icon-plus');
			$crud->display_as('id_cliente','Cliente');
			$crud->display_as('id_fornitore','Fornitore');

			$crud->set_relation('id_cliente','clienti','nome_cliente_documenti');
			$crud->set_relation('id_fornitore','fornitori','nome_fornitore');

			$crud->columns('num_ddt','data_creazione_ddt','id_cliente','id_fornitore','tipo_ddt');


			$crud->order_by('data_creazione_ddt desc, num_ddt','desc');


			$output = $crud->render();
			$output->header="DDT";
			$output->footer="";

			$output->menu=$this->build_menu();

			$this->load->view('omis_view.php',$output);

	}


	function before_delete_ddt($primary_key)
	{
		$query = $this->db->get_where('tabella_ddt',array('id_ddt'=>$primary_key));
		foreach ($query->result() as $mainDdt);
		if($mainDdt->tipo_ddt=='C'){
			$query = $this->db->get_where('ddt_commesse',array('id_ddt'=>$primary_key));
			foreach ($query->result() as $prev){
				$this->db->where('commessa', $prev->commessa);
				$this->db->update('lavori', array('ddt_n'=> NULL,'ddt_data'=>NULL,'stato'=>'C'));
			}
		}else{
			$query = $this->db->get_where('righe_ddt',array('id_ddt'=>$primary_key));
			foreach ($query->result() as $riga){
				$this->db->delete('lav_esterne_commessa', array('id_lav_esterna'=> $riga->id_lav_esterna));
			}


		}
		$this->db->delete('ddt_commesse', array('id_ddt'=> $primary_key));
		$this->db->delete('righe_ddt', array('id_ddt'=> $primary_key));

	}


}
