<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Contabilita extends CI_Controller {

	function __construct()
	{
		parent::__construct();

		/* Standard Libraries */
		$this->load->database();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('date');
		$this->load->helper('security');
		$this->load->library('session');
		$this->load->library('grocery_CRUD');
		if(!$this->session->userdata('logged_in'))
			redirect('/login/show', 'refresh');
	}

	function build_menu(){
		$vars=array();
		$vars['tipo_op']=$this->session->userdata('tipo_op');
		$vars['nome_op']=$this->session->userdata('nome_operatore');

		extract($vars);
			#region buffering...
			ob_start();
			include(getcwd().'/application/views/menu_builder.php');
			$buffer = ob_get_contents();
		@ob_end_clean();
		return $buffer;
	}

	function operazioni_cassa()
	{
			$crud = new grocery_CRUD();
			$crud->set_theme('datatables');
			$crud->set_table('operazioni_cassa');
			$crud->order_by('id_operazione_cassa','desc');

			$crud->change_field_type('saldo', 'hidden');
			$crud->edit_fields('data_operazione','descrizione_operazione','da_pagare');
			$crud->unset_texteditor('descrizione_operazione');

			$crud->callback_before_insert(array($this,'before_insert_operazione_cassa'));

			$output = $crud->render();
			$output->header="OPERAZIONI DI CASSA";
			$output->footer="";
			$output->menu=$this->build_menu();
			$this->load->view('omis_view.php',$output);

	}

	function genera_file_excel_cassa(){
					/** Error reporting */
			error_reporting(E_ALL);
			ini_set('display_errors', TRUE);
			ini_set('display_startup_errors', TRUE);
			date_default_timezone_set('Europe/London');

			if (PHP_SAPI == 'cli')
				die('This example should only be run from a Web Browser');

			/** Include PHPExcel */
			require_once dirname(__FILE__) . '/../../assets/themes/datatables/extensions/PHPExcel/Classes/PHPExcel.php';


			// Create new PHPExcel object
			$objPHPExcel = new PHPExcel();

			// Set document properties
			$objPHPExcel->getProperties()->setTitle("Office 2007 XLSX Test Document");


			// Add some data
			$objPHPExcel->setActiveSheetIndex(0)
			            ->setCellValue('A1', 'DATA')
			            ->setCellValue('B1', 'DESCRIZIONE')
			            ->setCellValue('C1', 'ENTRATA')
			            ->setCellValue('D1', 'USCITA')
			            ->setCellValue('E1', 'SALDO');

			$query = $this->db->get('operazioni_cassa');	 //aggiungere where per filtrare sull'anno
			$i=1;
			foreach($query->result() as $operazione){
				$i++;
				@list($year,$month,$day) = explode("-",$operazione->data_operazione);

				$objPHPExcel->setActiveSheetIndex(0)
			            ->setCellValue('A'.$i, date ("d/m/Y",mktime (0,0,0,(int)$month , (int)$day , (int)$year)))
			            ->setCellValue('B'.$i, $operazione->descrizione_operazione)
			            ->setCellValue('C'.$i, $operazione->entrata)
			            ->setCellValue('D'.$i, $operazione->uscita)
			            ->setCellValue('E'.$i, $operazione->saldo);
			}

			// // Miscellaneous glyphs, UTF-8
			// $objPHPExcel->setActiveSheetIndex(0)
			//             ->setCellValue('A4', 'Miscellaneous glyphs')
			//             ->setCellValue('A5', 'éàèùâêîôûëïüÿäöüç');

			// Rename worksheet
			$objPHPExcel->getActiveSheet()->setTitle('Simple');


			// Set active sheet index to the first sheet, so Excel opens this as the first sheet
			$objPHPExcel->setActiveSheetIndex(0);


			// Redirect output to a client’s web browser (Excel5)
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="Operazioni_Cassa.xls"');
			header('Cache-Control: max-age=0');
			// If you're serving to IE 9, then the following may be needed
			header('Cache-Control: max-age=1');

			// If you're serving to IE over SSL, then the following may be needed
			header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
			header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
			header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
			header ('Pragma: public'); // HTTP/1.0

			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter->save('php://output');
			exit;
	}

	function before_insert_operazione_cassa($post_array){
		$this->db->order_by("id_operazione_cassa", "desc");
		$this->db->limit(1);
		$que = $this->db->get('operazioni_cassa');
		if($que->num_rows()>0)
			foreach ($que->result() as $operazione);
		else{
			$operazione=new stdClass();
			$operazione->saldo=0.00;
		}

		if($post_array['entrata']!=NULL){
			$post_array['saldo']=$operazione->saldo+$post_array['entrata'];
		}
		if($post_array['uscita']!=NULL){
			$post_array['saldo']=$operazione->saldo-$post_array['uscita'];
		}

		return $post_array;
	}


}
