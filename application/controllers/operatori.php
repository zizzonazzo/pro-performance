<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Operatori extends CI_Controller {

	function __construct()
	{
		parent::__construct();

		/* Standard Libraries */
		$this->load->database();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('date');
		$this->load->helper('security');
		$this->load->library('session');

		/* ------------------ */

		$this->load->library('grocery_CRUD');
		if(!$this->session->userdata('logged_in'))
			redirect('/login/show', 'refresh');
	}

	function build_menu(){
		$vars=array();
		$vars['tipo_op']=$this->session->userdata('tipo_op');
		$vars['nome_op']=$this->session->userdata('nome_operatore');


		extract($vars);
			#region buffering...
			ob_start();

			include(getcwd().'/application/views/menu_builder.php');

			$buffer = ob_get_contents();
		@ob_end_clean();
		return $buffer;
}

	function controllo_produzione_bolle()
	{

		date_default_timezone_set('Europe/Rome');


		$this->load->library('table');
		$tmpl = array (
			                'table_open'          => '<table class="display groceryCrudTable"  id="groceryCrudTableScadenzario"  style="text-align:center">',

		                    'heading_row_start'   => '<tr>',
		                    'heading_row_end'     => '</tr>',
		                    'heading_cell_start'  => '<th style="padding:5px; text-align:center;"><font color="blue">',
		                    'heading_cell_end'    => '</font></th>',

		                    'row_start'           => '<tr>',
		                    'row_end'             => '</tr>',
		                    'cell_start'          => '<td style="padding:5px 2px 5px 2px;">',
		                    'cell_end'            => '</td>',

		                    'row_alt_start'       => '<tr>',
		                    'row_alt_end'         => '</tr>',
		                    'cell_alt_start'      => '<td style="padding:5px 2px 5px 2px;">',
		                    'cell_alt_end'        => '</td>',

		                    'table_close'         => '</table>'
		              );

		$this->table->set_template($tmpl);
		//if($this->session->userdata('nome_operatore')!="ILARIO")
		$this->table->set_heading('COMMESSA','STAMPO', 'TIPO LAV.','CLIENTE',  'NOME SAPONE' ,'COMPONENTE', "Q.TA'",'MATERIALE','TRATT.','INIZIO','STATO','AZIONI' );

			$que=$this->db->query("SELECT cod_bolla AS primary_key, id_componente AS oggetto, quantita, b.commessa as commessa, id_trattamento, b.stato AS stato_oggetto, id_operatore, l.stato AS stato_lav, id_materiale, data_inizio
			FROM lavori AS l
			JOIN bolle_prod AS b ON b.commessa = l.commessa
			WHERE b.id_operatore =  '".$this->uri->segment(3)."'
			AND (
			b.stato =  'INIZIARE'
			OR b.stato =  'IN CORSO'
			)
			UNION
			SELECT id_fase_commessa AS primary_key, nome_fase AS oggetto, null as quantita, f.commessa as commessa, null as id_trattamento, f.stato AS stato_oggetto, id_operatore, ll.stato AS stato_lav, null as id_materiale, data_inizio
			FROM lavori AS ll
			JOIN fasi_commessa AS f ON f.commessa = ll.commessa
			WHERE f.id_operatore =  '".$this->uri->segment(3)."'
			AND (
			f.stato =  'INIZIARE'
			OR f.stato =  'IN CORSO'
			)
			ORDER BY `data_inizio` asc,  `commessa` asc LIMIT 30");

		$data_attuale=0;
		if($que->num_rows() > 0){
			foreach ($que->result() as $oggetto){
				if(($data_attuale!=$oggetto->data_inizio)){

					if($data_attuale!=0)
						$this->table->add_row('<div style="background-color:orange;">&nbsp;</div>',
						 '<div style="background-color:orange;">&nbsp;</div>',  '<div style="background-color:orange;">&nbsp;</div>' ,'<div style="background-color:orange;">&nbsp;</div>', '<div style="background-color:orange;">&nbsp;</div>','<div style="background-color:orange;">&nbsp;</div>','<div style="background-color:orange;">&nbsp;</div>','<div style="background-color:orange;">&nbsp;</div>','<div style="background-color:orange;">&nbsp;</div>','<div>'.(($saturazione/8)*100).'%</div>','<div style="background-color:orange;">&nbsp;</div>','<div style="background-color:orange;">&nbsp;</div>' );
					$data_attuale=$oggetto->data_inizio;
					$saturazione=0;
				}

				if(isset($oggetto->id_materiale)){//se è un componente
												$azioni='<div align="right"><ul id="nav">
													<li id="azione" ><a href="#" class="bottone">Azioni</a>
														<ul>
															<li class="tasto"><a href="'.site_url('omis/dettaglio/componenti').'/'.$oggetto->commessa.'"   role="button" >
															DETTAGLIO						</a></li>
															<li class="tasto"><a  href="'.site_url('operatori/assegna_bolla').'/'.$oggetto->primary_key.'/3"  role="button" >
															AGOSTINO						</a></li>
																<li class="tasto"><a  href="'.site_url('operatori/assegna_bolla').'/'.$oggetto->primary_key.'/8"  role="button" >
																NICOLA						</a>
																</li><li class="tasto"><a  href="'.site_url('operatori/assegna_bolla').'/'.$oggetto->primary_key.'/5"  role="button" >
																FABIO						</a></li>
																<li class="tasto"><a  href="'.site_url('operatori/assegna_bolla').'/'.$oggetto->primary_key.'/7"  role="button" >
																SALVATORE						</a></li>
														</ul>
													</li>
													</ul></div>';


											@list($year,$month,$day) = explode("-",$oggetto->data_inizio);
											$data_inizio=mktime (0,0,0,(int)$month , (int)$day , (int)$year);
											$oggetto->data_inizio=date ("d-m-Y",mktime (0,0,0,(int)$month , (int)$day , (int)$year));

											$query = $this->db->get_where('lavori',array('commessa'=> $oggetto->commessa));
											foreach ($query->result() as $commessa);
											$query = $this->db->get_where('componenti',array('id_componente'=> $oggetto->oggetto));
											foreach ($query->result() as $componente);
											if(isset($oggetto->id_trattamento)){
												$query = $this->db->get_where('trattamenti',array('id_trattamento'=> $oggetto->id_trattamento));
												foreach ($query->result() as $trattamento);
											}else{
												$trattamento = new stdClass();
												$trattamento->sigla_trattamento="";
											}
											$query = $this->db->get_where('clienti',array('id_cliente'=> $commessa->cliente));
											foreach ($query->result() as $cliente);
												$query = $this->db->get_where('materiali',array('id_materiale'=> $oggetto->id_materiale));
												if($query->num_rows() > 0){
													foreach ($query->result() as $materiale);
												}else{
													$materiale->nome_materiale="";
												}


											$this->table->add_row($oggetto->commessa,$commessa->n_stampo,$commessa->tipo_lavoro,$cliente->ragione_soc,$commessa->nome_sapone,$componente->nome_componente,
																$oggetto->quantita,$materiale->nome_materiale,$trattamento->sigla_trattamento,$oggetto->data_inizio,$oggetto->stato_oggetto,$azioni);

											$query = $this->db->get_where('categorie_componenti',array('id_categoria'=> $componente->id_categoria));
											foreach ($query->result() as $categoria);
											if($query->num_rows()>0)
												$saturazione+=($categoria->ore_standard_categoria)*$oggetto->quantita;
					}else{//se e' una fase
						$azioni_fasi='<div align="right"><ul id="nav">
										<li id="azione" ><a href="#" class="bottone">Azioni</a>
											<ul>
												<li class="tasto"><a href="'.site_url('omis/dettaglio/componenti').'/'.$oggetto->commessa.'/10"  role="button" >
												DETTAGLIO						</a></li>
												<li class="tasto"><a href="'.site_url('operatori/assegna_fase').'/'.$oggetto->primary_key.'/10"   role="button" >
												ILARIO						</a></li>
												<li class="tasto"><a href="'.site_url('operatori/assegna_fase').'/'.$oggetto->primary_key.'/21"   role="button" >
													MISTER X						</a></li>
											</ul>
										</li>
										</ul></div>';


						@list($year,$month,$day) = explode("-",$oggetto->data_inizio);
						$data_inizio=mktime (0,0,0,(int)$month , (int)$day , (int)$year);
						$oggetto->data_inizio=date ("d-m-Y",mktime (0,0,0,(int)$month , (int)$day , (int)$year));


						$query = $this->db->get_where('lavori',array('commessa'=> $oggetto->commessa));
						foreach ($query->result() as $commessa);
						$query = $this->db->get_where('clienti',array('id_cliente'=> $commessa->cliente));
						foreach ($query->result() as $cliente);

						$query = $this->db->get_where('trattamenti', array('id_trattamento' => $commessa->id_trattam_matrice));
						if($query->num_rows()>0){
							foreach ($query->result() as $row);
							$sigla=$row->sigla_trattamento;
						}
						else $sigla='';

						$this->table->add_row($oggetto->commessa,$commessa->n_stampo,$commessa->tipo_lavoro,$cliente->ragione_soc,$commessa->nome_sapone,$oggetto->oggetto,
											"","","",$oggetto->data_inizio,$oggetto->stato_oggetto,$azioni_fasi);


					}

			}
			$this->table->add_row('<div style="background-color:orange;">&nbsp;</div>',
						 '<div style="background-color:orange;">&nbsp;</div>',  '<div style="background-color:orange;">&nbsp;</div>' ,'<div style="background-color:orange;">&nbsp;</div>', '<div style="background-color:orange;">&nbsp;</div>','<div style="background-color:orange;">&nbsp;</div>','<div style="background-color:orange;">&nbsp;</div>','<div style="background-color:orange;">&nbsp;</div>','<div style="background-color:orange;">&nbsp;</div>','<div>'.(($saturazione/8)*100).'%</div>','<div style="background-color:orange;">&nbsp;</div>','<div style="background-color:orange;">&nbsp;</div>' );

			$output = new stdClass();

			$output->output=$this->table->generate();

		}else{$output->output="NON CI SONO LAVORI PER TEEEEE!";}
			$output->menu=$this->build_menu();
			$nomi=array();
			$nomi[3]="AGOSTINO";
			$nomi[21]="FRANCESCO";
			$nomi[5]="FABIO";
			$nomi[10]="ILARIO";
			$nomi[8]="NICOLA";
			$nomi[7]="SALVATORE";
			$nomi[11]="MISTER X";
			$output->header="CONTROLLO SCADENZARIO di ".$nomi[$this->uri->segment(3)];
						$output->base_url="http://".$_SERVER["SERVER_NAME"].":8000/";

			$this->load->view('controllo_bolle_view.php',$output);

	}

	function controllo_produzione_fasi()
	{

			date_default_timezone_set('Europe/Rome');
			@list($month,$day,$year) = explode("-",date('m-d-Y'));
			$oggi = mktime (0,0,0,(int)$month , (int)$day , (int)$year);
			$this->load->library('table');
			$tmpl = array (
			                    'table_open'          => '<table class="display groceryCrudTable">',

			                    'heading_row_start'   => '<tr>',
			                    'heading_row_end'     => '</tr>',
			                    'heading_cell_start'  => '<th><font color="blue">',
			                    'heading_cell_end'    => '</font></th>',

			                    'row_start'           => '<tr>',
			                    'row_end'             => '</tr>',
			                    'cell_start'          => '<td>',
			                    'cell_end'            => '</td>',

			                    'row_alt_start'       => '<tr>',
			                    'row_alt_end'         => '</tr>',
			                    'cell_alt_start'      => '<td>',
			                    'cell_alt_end'        => '</td>',

			                    'table_close'         => '</table>'
			              );

		$operatori=array( 0=>6, //claudio
						1=>4, //david
						2=>22 //Hippelaar
			);

		for($i=0;$i<sizeof($operatori);$i++)
		{
			$colonne=false;
			$this->table->set_template($tmpl);

			$this->table->set_heading('COMMESSA','STAMPO', 'CLIENTE',  'NOME SAPONE' ,'COMPONENTE', "Q.TA'",'MATERIALE','TRATT.','INIZIO','STATO','AZIONI');

			$que=$this->db->query("SELECT cod_bolla, id_componente, quantita, b.commessa, id_trattamento, b.stato AS stato_bolla, id_operatore, l.stato, id_materiale, data_inizio, data_fine
			FROM lavori AS l
			JOIN bolle_prod AS b ON b.commessa = l.commessa
			WHERE b.id_operatore =  '".$operatori[$i]."'
			AND (
			b.stato =  'INIZIARE'
			OR b.stato =  'IN CORSO'
			) ORDER BY `data_inizio` asc, `id_materiale` asc, `commessa` asc LIMIT 30");
			if($que->num_rows() > 0){
				$colonne=true;
				foreach ($que->result() as $bolla){

					$azioni='<div align="right"><ul id="nav">
										<li id="azione" ><a href="#" class="bottone">Azioni</a>
											<ul>
												<li class="tasto"><a href="http://'.$_SERVER["SERVER_NAME"].':8000/index.php/omis/dettaglio/componenti/'.$bolla->commessa.'"  onclick="internalLink=true;" role="button" >
												DETTAGLIO						</a></li>
												<li class="tasto"><a href="http://'.$_SERVER["SERVER_NAME"].':8000/index.php/operatori/assegna_bolla/'.$bolla->cod_bolla.'/3"  onclick="internalLink=true;" role="button" >
												AGOSTINO						</a></li>
													<li class="tasto"><a href="http://'.$_SERVER["SERVER_NAME"].':8000/index.php/operatori/assegna_bolla/'.$bolla->cod_bolla.'/8"  onclick="internalLink=true;" role="button" >
													NICOLA						</a></li><li class="tasto"><a href="http://'.$_SERVER["SERVER_NAME"].':8000/index.php/operatori/assegna_bolla/'.$bolla->cod_bolla.'/5"  onclick="internalLink=true;" role="button" >
													FABIO						</a></li><li class="tasto"><a href="http:/'.$_SERVER["SERVER_NAME"].':8000/index.php/operatori/assegna_bolla/'.$bolla->cod_bolla.'/7"  onclick="internalLink=true;" role="button" >
													SALVATORE						</a></li>
											</ul>
										</li>
										</ul></div>';


					@list($year,$month,$day) = explode("-",$bolla->data_inizio);
					$data_inizio=mktime (0,0,0,(int)$month , (int)$day , (int)$year);
					$bolla->data_inizio=date ("d M Y",mktime (0,0,0,(int)$month , (int)$day , (int)$year));
					@list($year,$month,$day) = explode("-",$bolla->data_fine);
					$bolla->data_fine=date ("d M Y",mktime (0,0,0,(int)$month , (int)$day , (int)$year));
					$data_fine=mktime (0,0,0,(int)$month , (int)$day , (int)$year);

					$query = $this->db->get_where('lavori',array('commessa'=> $bolla->commessa));
					foreach ($query->result() as $commessa);
					$query = $this->db->get_where('componenti',array('id_componente'=> $bolla->id_componente));
					foreach ($query->result() as $componente);
					if($bolla->id_trattamento!=NULL){
						$query = $this->db->get_where('trattamenti',array('id_trattamento'=> $bolla->id_trattamento));
						foreach ($query->result() as $trattamento);
					}else{
						$trattamento->sigla_trattamento="";
					}
					$query = $this->db->get_where('clienti',array('id_cliente'=> $commessa->cliente));
					foreach ($query->result() as $cliente);
						$query = $this->db->get_where('materiali',array('id_materiale'=> $bolla->id_materiale));
						foreach ($query->result() as $materiale);
						if($query->num_rows() > 0){
							foreach ($query->result() as $materiale);
						}else{
							$materiale->nome_materiale="";
						}
					$this->table->add_row($bolla->commessa,$commessa->n_stampo,$cliente->ragione_soc,$commessa->nome_sapone,$componente->nome_componente,
										$bolla->quantita,$materiale->nome_materiale
										,$trattamento->sigla_trattamento,$bolla->data_inizio,$bolla->stato_bolla,$azioni);
				}
				//order_by();


			}else{
				$tabelle[$i]="<div align='center'>NON ESISTONO ELEMENTI PER QUESTA TABELLA</div>";

			}
			$que=$this->db->query("SELECT  nome_fase, b.commessa,  b.stato , id_operatore, l.stato,  data_inizio, data_fine
			FROM lavori AS l
			JOIN fasi_commessa AS b ON b.commessa = l.commessa
			WHERE b.id_operatore =  '".$operatori[$i]."'
			AND (
			b.stato =  'INIZIARE'
			OR b.stato =  'IN CORSO'
			) ORDER BY `data_inizio` asc, `commessa` asc LIMIT 30");
			if($que->num_rows() > 0){
				$colonne=true;
				foreach ($que->result() as $fase){

					$azioni='<div align="right"><ul id="nav">
										<li id="azione" ><a href="#" class="bottone">Azioni</a>
											<ul>
												<li class="tasto"><a href="http://192.168.2.231:8000/index.php/omis/dettaglio/componenti/'.$fase->commessa.'"  onclick="internalLink=true;" role="button" >
												DETTAGLIO						</a></li>
											</ul>
										</li>
										</ul></div>';


					@list($year,$month,$day) = explode("-",$fase->data_inizio);
					$data_inizio=mktime (0,0,0,(int)$month , (int)$day , (int)$year);
					$fase->data_inizio=date ("d M Y",mktime (0,0,0,(int)$month , (int)$day , (int)$year));
					@list($year,$month,$day) = explode("-",$fase->data_fine);
					$fase->data_fine=date ("d M Y",mktime (0,0,0,(int)$month , (int)$day , (int)$year));
					$data_fine=mktime (0,0,0,(int)$month , (int)$day , (int)$year);

					$query = $this->db->get_where('lavori',array('commessa'=> $fase->commessa));
					foreach ($query->result() as $commessa);

					$query = $this->db->get_where('clienti',array('id_cliente'=> $commessa->cliente));
					foreach ($query->result() as $cliente);

					$this->table->add_row($fase->commessa,$commessa->n_stampo,$cliente->ragione_soc,$commessa->nome_sapone,$fase->nome_fase,
										"",""
										,"",$fase->data_inizio,$fase->stato,$azioni);
				}
			}

			if($colonne){  $tabelle[$i]=$this->table->generate();   }

			$this->table->clear();


		}
			$output= new stdClass();
			$output->tab_claudio=$tabelle[0];
			$output->tab_david=$tabelle[1];
			$output->tab_francesco=$tabelle[2];

			$output->footer="";
			$output->header="<br> CONTROLLO SCADENZE";
			$output->nome_op=$this->session->userdata('nome_operatore');

			$output->base_url="http://".$_SERVER["SERVER_NAME"].":8000/";
			$output->menu=$this->build_menu();

			$this->load->view('controllo_bolle_view_fasi.php',$output);

	}


	function elenco_tempi_operatore()
	{
			date_default_timezone_set('Europe/Rome');


			$this->load->library('table');
			$tmpl = array (
		                    'table_open'          => '<table align="center" style="text-align: center; font-size: 16px;" border="1" cellpadding="8"  cellspacing="0">',

		                    'heading_row_start'   => '<tr>',
		                    'heading_row_end'     => '</tr>',
		                    'heading_cell_start'  => '<th style="padding:5px; text-align:center;"><font color="blue">',
		                    'heading_cell_end'    => '</font></th>',

		                    'row_start'           => '<tr>',
		                    'row_end'             => '</tr>',
		                    'cell_start'          => '<td style="padding:5px 2px 5px 2px;">',
		                    'cell_end'            => '</td>',

		                    'row_alt_start'       => '<tr>',
		                    'row_alt_end'         => '</tr>',
		                    'cell_alt_start'      => '<td style="padding:5px 2px 5px 2px;">',
		                    'cell_alt_end'        => '</td>',

		                    'table_close'         => '</table>'
		              );

			$this->table->set_template($tmpl);

			$this->table->set_heading('DATA LAVORAZIONE ','NUM STAMPO', 'COMMESSA',  'NOME COMPONENTE' ,'TEMPO OPERATORE', 'TEMPO MACCHINA','CENTRO DI COSTO','MODIFICA','ELIMINA');
			$this->db->order_by("data_lavorazione desc, id_lavorazione", "desc");
			$que = $this->db->get_where('consuntivaz_lavorazioni',array('nome_op'=> $this->session->userdata('nome_operatore')),60,0);
			$data_attuale="";
			$t_macchina_giornaliero=0;
			$t_op_giornaliero=0;
			$count=0;
			if($que->num_rows() > 0){

				foreach ($que->result() as $lavorazione)
				{	if($lavorazione->data_lavorazione != $data_attuale){
						if($data_attuale!="")
							$this->table->add_row('-','-','-','-','<font color="red" style="font-size: 18px; font-weight: bold;">'.$t_op_giornaliero.'</font>',
												'<font color="red" style="font-size: 18px; font-weight: bold;">'.$t_macchina_giornaliero.'</font>',
														'-','-','-');
					$data_attuale=$lavorazione->data_lavorazione;
					$t_macchina_giornaliero=0;
					$t_op_giornaliero=0;
					}
					@list($year,$month,$day) = explode("-",$lavorazione->data_lavorazione);
					$value = date ("d-m-Y",mktime (0,0,0,(int)$month , (int)$day , (int)$year));
					$quer = $this->db->get_where('lavori',array('commessa'=> $lavorazione->commessa));
					foreach ($quer->result() as $lavoro);

					$this->table->add_row('<div class="data_column" row="'.$count.'">'.$value.'</div>',$lavoro->n_stampo, $lavorazione->commessa,$lavorazione->nome,
						 				'<div class="t_op_column" row="'.$count.'">'.$lavorazione->t_operatore.'</div>',
										'<div class="t_macch_column" row="'.$count.'">'.$lavorazione->t_macchina.'</div>',
										'<div class="centro_costo_column" row="'.$count.'">'.$lavorazione->nome_centro_costo.'<div>',
										"<input class=tasto_modifica type='button' row='".$count."' id_lav='".$lavorazione->id_lavorazione."' value='Modifica'>",
										"<input class=tasto_elimina type='button' row='".$count."' id_lav='".$lavorazione->id_lavorazione."' value='Elimina'>");
					$t_macchina_giornaliero+=$lavorazione->t_macchina;
					$t_op_giornaliero+=$lavorazione->t_operatore;
					$count++;
				}
			}
			$output= new stdClass();
			$output->output=$this->table->generate();


			$output->header="<br> TEMPI INSERITI DA ".$this->session->userdata('nome_operatore');
			$output->footer="";

			$output->menu=$this->build_menu();

			$this->load->view('tempi_lavorazioni_op_view.php',$output);
	}


function elenco_consumi_materiali()
	{
			date_default_timezone_set('Europe/Rome');


			$this->load->library('table');
			$tmpl = array (
		                    'table_open'          => '<table align="center" class="display groceryCrudTable"  id="groceryCrudTableConsumi"  style="text-align: center; font-size: 16px;" border="1" cellpadding="8"  cellspacing="0">',

		                    'heading_row_start'   => '<tr>',
		                    'heading_row_end'     => '</tr>',
		                    'heading_cell_start'  => '<th style="padding:5px; text-align:center;"><font color="blue">',
		                    'heading_cell_end'    => '</font></th>',

		                    'row_start'           => '<tr>',
		                    'row_end'             => '</tr>',
		                    'cell_start'          => '<td style="padding:5px 2px 5px 2px;">',
		                    'cell_end'            => '</td>',

		                    'row_alt_start'       => '<tr>',
		                    'row_alt_end'         => '</tr>',
		                    'cell_alt_start'      => '<td style="padding:5px 2px 5px 2px;">',
		                    'cell_alt_end'        => '</td>',

		                    'table_close'         => '</table>'
		              );

			$this->table->set_template($tmpl);

			$this->table->set_heading('DATA CONSUMO ','NUM STAMPO', 'COMMESSA','COMPONENTE', 'MATERIALE','FORMA','SEZIONE','LUNGH. BARRA', 'MISURE RITAGLIO','KG CONSUMATI','KG RITAGLIO','ELIMINA');
			$this->db->order_by("data_consumo_mat", "desc");
			if($this->session->userdata('tipo_op')=="OPE"){
				$this->db->order_by("id_consumo_mat","desc");
				$que = $this->db->get_where('consumi_materiali',array('id_operatore' => $this->session->userdata('id_operatore')),120,0);
			}else
			{
				$que = $this->db->get('consumi_materiali',120,0);
			}
			$data_attuale="";
			$count=0;
			if($que->num_rows() > 0){

				foreach ($que->result() as $consumo)
				{	if($consumo->data_consumo_mat != $data_attuale){
						if($data_attuale!="")
							$this->table->add_row('-','-','-','-','-','-','-','-','-','-','-','-');
						$data_attuale=$consumo->data_consumo_mat;
					}
					@list($year,$month,$day) = explode("-",$consumo->data_consumo_mat);
					$value = date ("d-m-Y",mktime (0,0,0,(int)$month , (int)$day , (int)$year));
					$quer = $this->db->get_where('lavori',array('commessa'=> $consumo->commessa));
					foreach ($quer->result() as $lavoro);
					$query = $this->db->get_where('magazzino_materiali',array('codice_materiale_interno'=> $consumo->codice_materiale_interno));
					foreach ($query->result() as $materiale);
					$query = $this->db->get_where('materiali',array('id_materiale'=> $materiale->id_materiale));
					foreach ($query->result() as $nome_materiale);
					$query = $this->db->get_where('bolle_prod',array('cod_bolla'=> $consumo->cod_bolla));
					foreach ($query->result() as $bolla);
					$query = $this->db->get_where('componenti',array('id_componente'=> $bolla->id_componente));
					foreach ($query->result() as $componente);
					$this->table->add_row($value,
										$lavoro->n_stampo,
										$consumo->commessa,
										$componente->nome_componente,
						 				$nome_materiale->nome_materiale,
										'<div class="forma_column" row="'.$count.'">'.$materiale->forma_materiale.'<div>',
										'<div class="sezione_column" row="'.$count.'">'.$materiale->sezione_materiale.'<div>',
										'<div class="lunghezza_barra_column" row="'.$count.'">'.$consumo->lunghezza_barra.'<div>',
										'<div class="misure_ritaglio_column" row="'.$count.'">'.$consumo->misure_ritaglio.'<div>',
										'<div class="kg_consumati_column" row="'.$count.'">'.$consumo->kg_consumati.'<div>',
										'<div class="kg_ritaglio_column" row="'.$count.'">'.$consumo->kg_ritaglio.'<div>',
										"<input class=tasto_elimina type='button' row='".$count."' id_consumo='".$consumo->id_consumo_mat."' value='Elimina'>");
					$count++;
				}
			}
			$output = new stdClass;
			$output->output=$this->table->generate();


			$output->header="<br> CONSUMI MATERIALI ";
			$output->footer="";
			$output->nome_op=$this->session->userdata('nome_operatore');

			$output->menu=$this->build_menu();

					if($this->session->userdata('tipo_op')!="OPE")
						$this->load->view('consumi_materiali_view.php',$output);
					else
						$this->load->view('consumi_materiali_view_op.php',$output);
	}

function elenco_scarico_accessori()
{
			date_default_timezone_set('Europe/Rome');


			$this->load->library('table');
			$tmpl = array (
		                    'table_open'          => '<table class="display groceryCrudTable"  id="groceryCrudTableConsumi"  style="text-align:center">',

		                    'heading_row_start'   => '<tr>',
		                    'heading_row_end'     => '</tr>',
		                    'heading_cell_start'  => '<th style="padding:5px; text-align:center;"><font color="blue">',
		                    'heading_cell_end'    => '</font></th>',

		                    'row_start'           => '<tr>',
		                    'row_end'             => '</tr>',
		                    'cell_start'          => '<td style="padding:5px;">',
		                    'cell_end'            => '</td>',

		                    'row_alt_start'       => '<tr>',
		                    'row_alt_end'         => '</tr>',
		                    'cell_alt_start'      => '<td style="padding:5px;">',
		                    'cell_alt_end'        => '</td>',

		                    'table_close'         => '</table>'
		              );

			$this->table->set_template($tmpl);

			$this->table->set_heading('DATA SCARICAMENTO','NUM STAMPO', 'COMMESSA','ACCESSORIO', 'CODICE','COMPONENTE',"Q.TA'",'ELIMINA');
						$this->db->order_by("timestamp_scaricamento","desc");

			$que = $this->db->get_where('accessori_commessa',array('scaricato' => 'S'),500,0);




			$data_attuale="";
			if($que->num_rows() > 0){

				foreach ($que->result() as $scarico){

					@list($year,$month,$day) = explode("-",$scarico->timestamp_scaricamento);
					$value = date ("d-m-Y",mktime (0,0,0,(int)$month , (int)$day , (int)$year));

					if($value != $data_attuale){
						if($data_attuale!="")
							$this->table->add_row('-','-','-','-','-','-','-','-');
						$data_attuale=$value;
					}
					$quer = $this->db->get_where('lavori',array('commessa'=> $scarico->commessa));
					foreach ($quer->result() as $lavoro);
					$quer = $this->db->get_where('magazzino_accessori',array('codice_accessorio_interno'=> $scarico->codice_accessorio_interno));
					foreach ($quer->result() as $accessorio);
					if($scarico->id_componente!=NULL){
						$query = $this->db->get_where('componenti',array('id_componente'=> $scarico->id_componente));
						foreach ($query->result() as $componente);
					}else{
						$componente = new stdClass();
						$componente->nome_componente="";
					}

					$this->table->add_row(
										$value,
										$lavoro->n_stampo,
										$lavoro->commessa,
						 				$accessorio->nome_accessorio_interno,
										$scarico->codice_accessorio_interno,
										$componente->nome_componente,
										$scarico->quantita,
										"<input class='tasto_elimina_scarico' type='button' id_scarico='".$scarico->id_accessorio_commessa."' value='Elimina'>");
				}
			}
			$output = new stdClass();
			$output->output=$this->table->generate();


			$output->header="<br> ACCESSORI SCARICATI ";
			$output->footer="";
			$output->nome_op=$this->session->userdata('nome_operatore');

			$output->menu=$this->build_menu();

			$this->load->view('consumi_materiali_view_op.php',$output);

	}

function elenco_scarico_mancanti()
{
			date_default_timezone_set('Europe/Rome');


			$this->load->library('table');
			$tmpl = array (
		                    'table_open'          => '<table class="display groceryCrudTable"  id="groceryCrudTableConsumi"  style="text-align:center">',

		                    'heading_row_start'   => '<tr>',
		                    'heading_row_end'     => '</tr>',
		                    'heading_cell_start'  => '<th style="padding:5px; text-align:center;"><font color="blue">',
		                    'heading_cell_end'    => '</font></th>',

		                    'row_start'           => '<tr>',
		                    'row_end'             => '</tr>',
		                    'cell_start'          => '<td style="padding:5px;">',
		                    'cell_end'            => '</td>',

		                    'row_alt_start'       => '<tr>',
		                    'row_alt_end'         => '</tr>',
		                    'cell_alt_start'      => '<td style="padding:5px;">',
		                    'cell_alt_end'        => '</td>',

		                    'table_close'         => '</table>'
		              );

			$this->table->set_template($tmpl);

			$this->table->set_heading('NUM STAMPO', 'COMMESSA','ACCESSORIO', 'CODICE','COMPONENTE',"Q.TA'",'DETTAGLIO');
						$this->db->order_by("codice_accessorio_interno","desc");
			$where="scaricato = 'N' AND commessa IN (SELECT commessa FROM lavori WHERE stato='C')";
			$que = $this->db->get_where('accessori_commessa',$where);




			if($que->num_rows() > 0){

				foreach ($que->result() as $scarico){



					$quer = $this->db->get_where('lavori',array('commessa'=> $scarico->commessa));
					foreach ($quer->result() as $lavoro);
					$quer = $this->db->get_where('magazzino_accessori',array('codice_accessorio_interno'=> $scarico->codice_accessorio_interno));
					foreach ($quer->result() as $accessorio);
					if($scarico->id_componente!=NULL){
						$query = $this->db->get_where('componenti',array('id_componente'=> $scarico->id_componente));
						foreach ($query->result() as $componente);
					}else{
						$componente = new stdClass();
						$componente->nome_componente="";
					}

					$this->table->add_row(

										$lavoro->n_stampo,
										$lavoro->commessa,
						 				$accessorio->nome_accessorio_interno,
										$scarico->codice_accessorio_interno,
										$componente->nome_componente,
										$scarico->quantita,
										"<a href='/index.php/omis/dettaglioop/accessori/".$scarico->commessa."'><button class='btn btn-default' role='button'> DETTAGLIO </button></a>"
										);
				}
			}
			$output = new stdClass();
			$output->output=$this->table->generate();


			$output->header="<br> ACCESSORI NON SCARICATI ";
			$output->footer="";
			$output->nome_op=$this->session->userdata('nome_operatore');

			$output->menu=$this->build_menu();

			$this->load->view('consumi_materiali_view_op.php',$output);

	}

	function elenco_materiali_mancanti()
	{
			date_default_timezone_set('Europe/Rome');


			$this->load->library('table');
			$tmpl = array (
		                    'table_open'          => '<table class="display groceryCrudTable"  id="groceryCrudTableConsumi"  style="text-align:center">',

		                    'heading_row_start'   => '<tr>',
		                    'heading_row_end'     => '</tr>',
		                    'heading_cell_start'  => '<th style="padding:5px; text-align:center;"><font color="blue">',
		                    'heading_cell_end'    => '</font></th>',

		                    'row_start'           => '<tr>',
		                    'row_end'             => '</tr>',
		                    'cell_start'          => '<td style="padding:5px;">',
		                    'cell_end'            => '</td>',

		                    'row_alt_start'       => '<tr>',
		                    'row_alt_end'         => '</tr>',
		                    'cell_alt_start'      => '<td style="padding:5px;">',
		                    'cell_alt_end'        => '</td>',

		                    'table_close'         => '</table>'
		              );

			$this->table->set_template($tmpl);

			$this->table->set_heading('NUM STAMPO', 'COMMESSA','COMPONENTE','MATERIALE','MISURE', "Q.TA'",'DETTAGLIO');
						$this->db->order_by("commessa","desc");
			$where="cod_bolla NOT IN (SELECT DISTINCT(cod_bolla) FROM consumi_materiali) AND misure_finito<>' ' AND commessa IN (SELECT commessa FROM lavori WHERE stato='C')";
			$que = $this->db->get_where('bolle_prod',$where);




			if($que->num_rows() > 0){
				foreach ($que->result() as $scarico){
					$quer = $this->db->get_where('lavori',array('commessa'=> $scarico->commessa));
					foreach ($quer->result() as $lavoro);

					$query = $this->db->get_where('componenti',array('id_componente'=> $scarico->id_componente));
					foreach ($query->result() as $componente);
					$query = $this->db->get_where('materiali',array('id_materiale'=> $scarico->id_materiale));
					foreach ($query->result() as $materiale);


					$this->table->add_row(

										$lavoro->n_stampo,
										$lavoro->commessa,
						 				$componente->nome_componente,
						 				$materiale->nome_materiale,
						 				$scarico->misure_finito,
										$scarico->quantita,
										"<a href='/index.php/omis/dettaglioop/componenti/".$scarico->commessa."'><button class='btn btn-default' role='button'> DETTAGLIO </button></a>"
										);
				}
			}
			$output = new stdClass();
			$output->output=$this->table->generate();


			$output->header="<br> MATERIALI NON INSERITI ";
			$output->footer="";
			$output->nome_op=$this->session->userdata('nome_operatore');

			$output->menu=$this->build_menu();

			$this->load->view('consumi_materiali_view_op.php',$output);

	}

	function elenco_bolle_produzione()
	{

		switch($this->uri->segment(3))
		{
			case "tempi":
				$footer=$this->footerComponenteTempi();
			break;
			case "dati":
				$footer=$this->footerComponenteDati();
			break;
			case "fasetempi":
				$footer=$this->footerFaseTempi();
			break;
			default:
					$footer='';
			break;
		}


		date_default_timezone_set('Europe/Rome');
		@list($month,$day,$year) = explode("-",date('m-d-Y'));
		$oggi = mktime (0,0,0,(int)$month , (int)$day , (int)$year);

		$this->load->library('table');
		$tmpl = array (
		                    'table_open'          => '<table class="display groceryCrudTable" id="groceryCrudTableScadenzario" style="text-align:center">',

		                    'heading_row_start'   => '<tr>',
		                    'heading_row_end'     => '</tr>',
		                    'heading_cell_start'  => '<th style="padding:5px; text-align:center;"><font color="blue">',
		                    'heading_cell_end'    => '</font></th>',

		                    'row_start'           => '<tr>',
		                    'row_end'             => '</tr>',
		                    'cell_start'          => '<td style="padding:5px 2px 5px 2px;">',
		                    'cell_end'            => '</td>',

		                    'row_alt_start'       => '<tr>',
		                    'row_alt_end'         => '</tr>',
		                    'cell_alt_start'      => '<td style="padding:5px 2px 5px 2px;">',
		                    'cell_alt_end'        => '</td>',

		                    'table_close'         => '</table>'
		              );

		$this->table->set_template($tmpl);
		//if($this->session->userdata('nome_operatore')!="ILARIO")
		$this->table->set_heading('COMMESSA','STAMPO', 'TIPO LAV.','CLIENTE',  'NOME SAPONE' ,'COMPONENTE', "Q.TA'",'MATERIALE','TRATT.','INIZIO','STATO','AZIONI' );

			$que=$this->db->query("SELECT cod_bolla AS primary_key, id_componente AS oggetto, quantita, b.commessa as commessa, id_trattamento, b.stato AS stato_oggetto, id_operatore, l.stato AS stato_lav, id_materiale, data_inizio, data_fine
			FROM lavori AS l
			JOIN bolle_prod AS b ON b.commessa = l.commessa
			WHERE b.id_operatore =  '".$this->session->userdata('id_operatore')."'
			AND (
			b.stato =  'INIZIARE'
			OR b.stato =  'IN CORSO'
			)
			UNION
			SELECT id_fase_commessa AS primary_key, nome_fase AS oggetto, null as quantita, f.commessa as commessa, null as id_trattamento, f.stato AS stato_oggetto, id_operatore, ll.stato AS stato_lav, null as id_materiale, data_inizio, data_fine
			FROM lavori AS ll
			JOIN fasi_commessa AS f ON f.commessa = ll.commessa
			WHERE f.id_operatore =  '".$this->session->userdata('id_operatore')."'
			AND (
			f.stato =  'INIZIARE'
			OR f.stato =  'IN CORSO'
			)

			ORDER BY `data_inizio` asc,  `commessa` asc LIMIT 30");



		$commessa_attuale=0;
		if($que->num_rows() > 0){
			foreach ($que->result() as $oggetto){
				if(($commessa_attuale!=$oggetto->commessa)){

					if($commessa_attuale!=0)
						$this->table->add_row('<div style="background-color:orange;">&nbsp;</div>',
						 '<div style="background-color:orange;">&nbsp;</div>',
						 '<div style="background-color:orange;">&nbsp;</div>' ,
						 '<div style="background-color:orange;">&nbsp;</div>',
						 '<div style="background-color:orange;">&nbsp;</div>',
						 '<div style="background-color:orange;">&nbsp;</div>',
						 '<div style="background-color:orange;">&nbsp;</div>',
						 '<div style="background-color:orange;">&nbsp;</div>',
						 '<div style="background-color:orange;">&nbsp;</div>',
						 '<div style="background-color:orange;">&nbsp;</div>',
						 '<div style="background-color:orange;">&nbsp;</div>','<div style="background-color:orange;">&nbsp;</div>' );
					$commessa_attuale=$oggetto->commessa;
				}

				if(isset($oggetto->id_materiale)){//se è un componente
												$azioni='<ul id="nav">
																<li id="azione" ><a href="#" class="bottone">Azioni</a>
																	<ul>
																		<li class="tasto"><a href="http://192.168.2.231:8000/index.php/operatori/elenco_bolle_produzione/dati/'.$oggetto->primary_key.'"  role="button" >
																		DATI						</a></li>
																		<li class="tasto"><a href="http://192.168.2.231:8000/index.php/operatori/elenco_bolle_produzione/tempi/'.$oggetto->primary_key.'" role="button" >
																		TEMPI						</a></li>
																		<li class="tasto"><a href="http://192.168.2.231:8000/index.php/operatori/bolla_completata/'.$oggetto->primary_key.'"  role="button" >
																		COMPLETATO						</a></li>
																			<li class="tasto"><a href="http://192.168.2.231:8000/index.php/omis/dettaglioop/componenti/'.$oggetto->commessa.'"  role="button" >
																			DETTAGLIO						</a></li>
																	</ul>
																</li>
																</ul>';


											@list($year,$month,$day) = explode("-",$oggetto->data_inizio);
											$data_inizio=mktime (0,0,0,(int)$month , (int)$day , (int)$year);
											$oggetto->data_inizio=date ("d-m-Y",mktime (0,0,0,(int)$month , (int)$day , (int)$year));
											@list($year,$month,$day) = explode("-",$oggetto->data_fine);
											$oggetto->data_fine=date ("d-m-Y",mktime (0,0,0,(int)$month , (int)$day , (int)$year));
											$data_fine=mktime (0,0,0,(int)$month , (int)$day , (int)$year);




											$query = $this->db->get_where('lavori',array('commessa'=> $oggetto->commessa));
											foreach ($query->result() as $commessa);
											$query = $this->db->get_where('componenti',array('id_componente'=> $oggetto->oggetto));
											foreach ($query->result() as $componente);
											if(isset($oggetto->id_trattamento)){
												$query = $this->db->get_where('trattamenti',array('id_trattamento'=> $oggetto->id_trattamento));
												foreach ($query->result() as $trattamento);
											}else{
												$trattamento = new stdClass();
												$trattamento->sigla_trattamento="";
											}
											$query = $this->db->get_where('clienti',array('id_cliente'=> $commessa->cliente));
											foreach ($query->result() as $cliente);
												$query = $this->db->get_where('materiali',array('id_materiale'=> $oggetto->id_materiale));
												if($query->num_rows() > 0){
													foreach ($query->result() as $materiale);
												}else{
													$materiale->nome_materiale="";
												}
											if($commessa->consegna_tassativa=='S')$oggetto->data_fine='<div style="background-color:red;">'.$oggetto->data_fine.'</div>';


											if(($this->session->userdata('nome_operatore')!="ILARIO")&&($this->session->userdata('nome_operatore')!="MISTER X")){
											$this->table->add_row($oggetto->commessa,$commessa->n_stampo,$commessa->tipo_lavoro,$cliente->ragione_soc,$commessa->nome_sapone,$componente->nome_componente,
																$oggetto->quantita,$materiale->nome_materiale,$trattamento->sigla_trattamento,$oggetto->data_inizio,$oggetto->stato_oggetto,$azioni);
											}else{
												$this->table->add_row("<div class='comm_div'>".$oggetto->commessa."<div class='quick_view'></div></div>",$commessa->n_stampo,$commessa->tipo_lavoro,$cliente->ragione_soc,$commessa->nome_sapone,$componente->nome_componente,
																$oggetto->quantita,$materiale->nome_materiale,$trattamento->sigla_trattamento,$oggetto->data_inizio,$this->stato_lavoro_column($oggetto->commessa),$azioni);
											}

					}else{//se e' una fase
						$azioni_fasi='<ul id="nav">
									<li id="azione" ><a href="#" class="bottone">Azioni</a>
										<ul>
												<li class="tasto"><a href="http://192.168.2.231:8000/index.php/operatori/elenco_bolle_produzione/fasetempi/'.$oggetto->primary_key.'"   role="button" >
												TEMPI					</a></li>
												<li class="tasto"><a href="http://192.168.2.231:8000/index.php/operatori/fase_completata/'.$oggetto->commessa.'/'.$oggetto->primary_key.'"  role="button" >
											COMPLETA					</a></li>
											<li class="tasto"><a href="http://192.168.2.231:8000/index.php/omis/dettaglioop/fasi/'.$oggetto->commessa.'"  role="button" >
																			DETTAGLIO						</a></li>
										</ul>
									</li>
									</ul>';


						@list($year,$month,$day) = explode("-",$oggetto->data_inizio);
						$data_inizio=mktime (0,0,0,(int)$month , (int)$day , (int)$year);
						$oggetto->data_inizio=date ("d-m-Y",mktime (0,0,0,(int)$month , (int)$day , (int)$year));
						@list($year,$month,$day) = explode("-",$oggetto->data_fine);
						$oggetto->data_fine=date ("d-m-Y",mktime (0,0,0,(int)$month , (int)$day , (int)$year));
						$data_fine=mktime (0,0,0,(int)$month , (int)$day , (int)$year);



						$query = $this->db->get_where('lavori',array('commessa'=> $oggetto->commessa));
						foreach ($query->result() as $commessa);
						$query = $this->db->get_where('clienti',array('id_cliente'=> $commessa->cliente));
						foreach ($query->result() as $cliente);

						$query = $this->db->get_where('trattamenti', array('id_trattamento' => $commessa->id_trattam_matrice));
						if($query->num_rows()>0){
							foreach ($query->result() as $row);
							$sigla=$row->sigla_trattamento;
						}
						else $sigla='';

						if($commessa->consegna_tassativa=='S') $oggetto->data_fine='<div style="background-color:red;">'.$oggetto->data_fine.'</div>';
						if(($this->session->userdata('nome_operatore')!="ILARIO")&&($this->session->userdata('nome_operatore')!="MISTER X")){
							$this->table->add_row($oggetto->commessa,$commessa->n_stampo,$commessa->tipo_lavoro,$cliente->ragione_soc,$commessa->nome_sapone,$oggetto->oggetto,
											"","","",$oggetto->data_inizio,$oggetto->stato_oggetto,$azioni_fasi);
						}else{
							$this->table->add_row("<div class='comm_div'>".$oggetto->commessa."<div class='quick_view'></div></div>",$commessa->n_stampo,$commessa->tipo_lavoro,$cliente->ragione_soc,$commessa->nome_sapone,$oggetto->oggetto,
											"","",$sigla,$oggetto->data_inizio,$this->stato_lavoro_column($oggetto->commessa),$azioni_fasi);
						}

					}

			}
			$output= new stdClass();
			$output->output=$this->table->generate();

		}else{$output->output="NON CI SONO LAVORI PER TEEEEE!";}






		$output->footer=$footer;
		$output->header="<br> SCADENZARIO ".$this->session->userdata('nome_operatore');
		$output->nome_op=$this->session->userdata('nome_operatore');

		$output->menu=$this->build_menu();

		$this->load->view('bolle_produzione_op_view.php',$output);
	}

	function fase_completata()
	{
		$this->db->where('id_fase_commessa', $this->uri->segment(4));
		$this->db->update('fasi_commessa', array('stato'=>'COMPLETATO'));
		$this->commessa_completata($this->uri->segment(3));
		redirect('/operatori/elenco_bolle_produzione', 'refresh');
	}


	function stato_lavoro_column($commessa){
		$datrattare=false;
		$totale=0;
		$completato=0;
		$this->db->select('bolle_prod.stato, bolle_prod.id_trattamento,componenti.id_categoria');
		$this->db->from('bolle_prod');
		$this->db->join('componenti', 'bolle_prod.id_componente = componenti.id_componente');
		$where = "bolle_prod.commessa =".$commessa;
		$this->db->where($where);
		$que = $this->db->get();
		foreach($que->result() as $bolla)
		{
			switch($bolla->id_categoria){
				case 1: //5
				case 9:
				case 10:
				case 11:
				case 13:
					$totale+=5;
					if(($bolla->stato=='DA COMPLETARE')||($bolla->stato=='COSTRUITO')||($bolla->stato=='COMPLETATO')){
						$completato+=5;

					}
				break;
				case 2: //4
				case 3:
				case 4:
					$totale+=4;
					if(($bolla->stato=='DA COMPLETARE')||($bolla->stato=='COSTRUITO')||($bolla->stato=='COMPLETATO'))
					$completato+=4;
				break;
				case 14://3
					$totale+=3;
					if(($bolla->stato=='DA COMPLETARE')||($bolla->stato=='COSTRUITO')||($bolla->stato=='COMPLETATO'))
					$completato+=3;
				break;
				case NULL:
				break;
				default:
					$totale+=1;
					if(($bolla->stato=='DA COMPLETARE')||($bolla->stato=='COSTRUITO')||($bolla->stato=='COMPLETATO'))
					$completato+=1;
				break;

			}
			if(($bolla->id_trattamento!=NULL)&&($bolla->id_trattamento!=1)&&($bolla->id_trattamento!=10)) //se esiste un trattamento e non è nè sabbiatura nè lucidatura
			{

				$datrattare=true;
			}
		}
		if($datrattare)
		{
			$totale+=1;

			$lavoro = $this->db->get_where('lav_esterne_commessa', array('commessa'=> $commessa));
			if($lavoro->num_rows()>0){
				$dentro=true;
				foreach ($lavoro->result() as $lavorazione){
						if($lavorazione->stato_lav_est=="FUORI")
						$dentro=false;
				}
				if($dentro)
				{
					$completato+=1;
				}
			}

		}
		if($totale>0)
			return (int)((100*$completato)/($totale))."%";
		else
			return "no componenti";
	}

function salvaTempiFase()			//----------------------GESTIONE SALVATAGGIO TEMPI DI SINGOLA FASE-------------------------//
	{
		date_default_timezone_set('Europe/Rome');

		$nuovaLav=array(
					'id_fase_commessa' => $_POST['id_fase_commessa'],
					't_operatore' => $_POST['t_operatore'],
					't_macchina' => $_POST['t_macchina'],
					'id_operatore'=>$_POST['id_operatore'],
	               	'id_centro_costo' => $_POST['id_centro_costo'],
					'data_lavorazione' => date('Y-m-d'),
					'commessa' => $_POST['commessa']

					);
		($this->session->userdata('tipo_op')!="DIS") ? $stato='IN CORSO' : $stato='INIZIARE';
		$this->db->insert('lavorazioni',$nuovaLav);
		$this->db->where('id_fase_commessa', $_POST['id_fase_commessa']);
		$this->db->update('fasi_commessa', array('stato'=> $stato));
		redirect('/operatori/elenco_bolle_produzione', 'refresh');
	}



function footerFaseTempi() // invocata da dettaglioop se devo modificare tempi fase-----> invoca salvaTempiFase------------------//
	{
		date_default_timezone_set('Europe/Rome');

		$query = $this->db->get('centri_di_costo');
		foreach ($query->result() as $row)
		{
		    $centricosto[$row->id_centro_costo]=$row->nome_centro_costo;
		}
			$lavorazioni = $this->db->get_where('lavorazioni',array('id_fase_commessa'=>$this->uri->segment(4), 'id_operatore' => $this->session->userdata('id_operatore')));
			$this->load->library('table');

			$this->table->set_heading('Tempo Operatore ', 'Tempo Macchina', 'Data Lavorazione');
			foreach ($lavorazioni->result() as $lavorazione){
				list($year,$month,$day) = explode("-",$lavorazione->data_lavorazione);
				$value = date ("d-m-Y",mktime (0,0,0,(int)$month , (int)$day , (int)$year));
				$this->table->add_row($lavorazione->t_operatore, $lavorazione->t_macchina, $value);
			}
			$tabula_lavorazioni=$this->table->generate();

		$query = $this->db->get_where('fasi_commessa',array('id_fase_commessa'=>$this->uri->segment(4)));
		foreach ($query->result() as $row);
			$footer="
			<table align='center'>
				<tr>
			<td><fieldset>
			<legend><font size='4' color='red'>TEMPI INSERITI</font></legend>".$tabula_lavorazioni."</fieldset></td>
			<td width='370'><fieldset>
			<form method='POST' action='/index.php/operatori/salvaTempiFase'>
			<input type='hidden' name='id_fase_commessa' value='".$row->id_fase_commessa."'>
			<input type='hidden' name='commessa' value='".$row->commessa."'>
			<input type='hidden' name='id_operatore' value='".$this->session->userdata('id_operatore')."'>

	    	<legend><font size='4' color='red'>$row->nome_fase</font></legend>
	<font size='4'>
			<table>
			<tr><td align='left'>Centro di costo: </td><td>".form_dropdown('id_centro_costo', $centricosto, $this->session->userdata('id_centro_costo') )."</td></tr>
			<tr><td align='left'>Operatore: </td><td>".$this->session->userdata('nome_operatore')."</td></tr>
			<tr><td width='170' align='left'>Tempo Operatore: </td><td width='170'><input style='height:30px;font-size:12pt;' name='t_operatore' type='text' size='10' value='0.0'/> ore</td></tr>
			<tr><td width='170' align='left'>Tempo Macchina: </td><td><input style='height:30px;font-size:12pt;' name='t_macchina' type='text' size='10' value='0.0' />  ore</td></tr>
			<tr><td align='right'><input  style='font-size:15pt;' type='submit' onclick='internalLink=true;' value='SALVA'></td>
			<td><a href='/index.php/operatori/elenco_bolle_produzione/' ><input style='font-size:15pt;' type='button' value='ANNULLA' /></a></td></tr>
			</table>
			</font>
	  		</fieldset></td>
			</tr>
			</table>
			";

		return $footer;
	}


	function footerComponenteTempi() // invocata da dettaglioop se devo modificare tempi componente-----> invoca salvaTempiComponente------------------//
	{
		date_default_timezone_set('Europe/Rome');

		$query = $this->db->get('centri_di_costo');
		foreach ($query->result() as $row)
		{
		    $centricosto[$row->id_centro_costo]=$row->nome_centro_costo;
		}
			$query = $this->db->get_where('bolle_prod',array('cod_bolla'=>$this->uri->segment(4)));
			foreach ($query->result() as $row);
			$compon = $this->db->get_where('componenti',array('id_componente'=>$row->id_componente));
			foreach ($compon->result() as $componente);
			$lavorazioni = $this->db->get_where('lavorazioni',array('cod_bolla'=>$this->uri->segment(4)));



			$this->load->library('table');

			$this->table->set_heading('Nome Operatore','Tempo Operatore ', 'Tempo Macchina', 'Data Lavorazione', 'Centro di Costo');
			foreach ($lavorazioni->result() as $lavorazione){
				@list($year,$month,$day) = explode("-",$lavorazione->data_lavorazione);
				$value = date ("d-m-Y",mktime (0,0,0,(int)$month , (int)$day , (int)$year));
				$centro = $this->db->get_where('centri_di_costo',array('id_centro_costo'=>$lavorazione->id_centro_costo));
				foreach ($centro->result() as $centro_costo);
				$operatore = $this->db->get_where('operatori',array('id_operatore'=>$lavorazione->id_operatore));
				foreach ($operatore->result() as $nome_op);
				$this->table->add_row($nome_op->nome_op,$lavorazione->t_operatore, $lavorazione->t_macchina, $value, $centro_costo->nome_centro_costo);
			}
		$tabula_lavorazioni=$this->table->generate();

			$footer="
			<table align='center'>
			<tr>

			<td><fieldset>
			<legend><font size='4' color='red'>TEMPI INSERITI IN QUESTO COMPONENTE</font></legend>".$tabula_lavorazioni."</fieldset></td>
			<td width='370'><fieldset>
			<form method='POST' action='/index.php/operatori/salvaTempiComponente'>
			<input type='hidden' name='cod_bolla' value='".$row->cod_bolla."'>
			<input type='hidden' name='commessa' value='".$row->commessa."'>
			<input type='hidden' name='id_operatore' value='".$this->session->userdata('id_operatore')."'>


	    	<legend><font size='4' color='red'>$componente->nome_componente</font></legend>
	<font size='4'>
			<table>
			<tr><td align='left'>Centro di costo: </td><td>".form_dropdown('id_centro_costo', $centricosto, $this->session->userdata('id_centro_costo') )."</td></tr>
			<tr><td align='left'>Operatore: </td><td>".$this->session->userdata('nome_operatore')."</td></tr>
			<tr><td width='170' align='left'>Tempo Operatore: </td><td width='170'><input style='height:30px;font-size:12pt;' name='t_operatore' type='text' size='10' value='0.0'/> ore</td></tr>
	    	<tr><td width='170' align='left'>Tempo Macchina: </td><td><input style='height:30px;font-size:12pt;' name='t_macchina' type='text' size='10' value='0.0' />  ore</td></tr>

			<tr><td align='right'><input  style='font-size:15pt;' type='submit' onclick='internalLink=true;' value='SALVA'></td>
			<td><a href='/index.php/operatori/elenco_bolle_produzione' onclick='internalLink=true;' ><input style='font-size:15pt;' type='button' value='ANNULLA' /></a></td></tr>
			</table>
			</font>
	  		</fieldset></td>
			<td><fieldset>
			<legend><font size='4' color='red'>NOTE COMPONENTE</font></legend>".$row->note."</fieldset></td>
			</tr>
			</table>
			";

		return $footer;
	}

	function salvaTempiComponente()			//----------------------GESTIONE SALVATAGGIO TEMPI DI SINGOLA BOLLA DI PRODUZIONE(COMPONENTE)-------------------------//
	{
		date_default_timezone_set('Europe/Rome');

		$que = $this->db->get_where('bolle_prod',array('cod_bolla' => $_POST['cod_bolla']));  //---quando inserisco dei tempi relativi a una bolla: se era gi� completata la lavorazione, questa rimane tale
		foreach($que->result() as $row);														//se era da iniziare metto stato:IN CORSO dato che sto inserendo dei tempi e quindi ci ho lavorato.
		if($this->session->userdata('tipo_op')!="DIS")
		{
			if($row->stato=='INIZIARE')
			 	$stato='IN CORSO';
			else $stato=$row->stato;
			}

		else
			$stato='INIZIARE';


		$data = array(
		               'stato'    => $stato
		            );


		$nuovaLav=array(
					'cod_bolla' => $_POST['cod_bolla'],
					't_operatore' => $_POST['t_operatore'],
					't_macchina' => $_POST['t_macchina'],
		           /* 't_macch_extra' => $_POST['t_macch_extra'],*/
					'id_operatore'=>$_POST['id_operatore'],
	               	'id_centro_costo' => $_POST['id_centro_costo'],
					'data_lavorazione' => date('Y-m-d'),
					'commessa' => $_POST['commessa']
					);

		$this->db->insert('lavorazioni',$nuovaLav);
		$this->db->where('cod_bolla', $_POST['cod_bolla']);
		$this->db->update('bolle_prod', $data);
		redirect('/operatori/elenco_bolle_produzione', 'refresh');
	}


	function assegna_bolla()
	{

			$this->db->where('cod_bolla', $this->uri->segment(3));
			$this->db->update('bolle_prod', array('id_operatore'=>$this->uri->segment(4) ));
			redirect('/operatori/controllo_produzione_bolle', 'refresh');

	}
	function assegna_fase()
	{

			$this->db->where('id_fase_commessa', $this->uri->segment(3));
			$this->db->update('fasi_commessa', array('id_operatore'=>$this->uri->segment(4) ));
			redirect('/operatori/controllo_produzione_bolle', 'refresh');

	}


	function bolla_completata()
	{
		$bolla = $this->db->get_where('bolle_prod',array('cod_bolla'=>$this->uri->segment(3)));
		foreach($bolla->result() as $row);

			$stato='COMPLETATO';
			$this->db->where('cod_bolla', $this->uri->segment(3));
			$this->db->update('bolle_prod', array('stato'=>$stato ,'completati'=> $row->quantita));
			$this->commessa_completata($row->commessa);
			redirect('/operatori/elenco_bolle_produzione', 'refresh');

	}

	function commessa_completata($commessa) //verifica che tutti componenti e 'assemblaggio, finitura' siano completati, se S�->commessa COMPLETATA
	{
			$tuttoCompletato=TRUE;
			$query = $this->db->get_where('bolle_prod',array('commessa'=>$commessa));
			foreach($query->result() as $row){													//Controllo se tutto i componenti di un lavoro sono completati
				if($row->stato!='COMPLETATO' && $row->stato!='COSTRUITO')													//Se s� cambio stato commessa al lavoro in COMPLETATO
					$tuttoCompletato=FALSE;
			}
			$fase = $this->db->get_where('fasi_commessa',array('commessa' => $commessa, 'nome_fase' => 'IMBALLAGGIO'));
			foreach($fase->result() as $faseCorrente);
			if($faseCorrente->stato != 'COMPLETATO')
					$tuttoCompletato=FALSE;
		($tuttoCompletato==TRUE) ? $data=array('stato_comm' => 'COMPLETATO') : $data=array('stato_comm' => 'IN CORSO'); //---------Aggiorno lo stato della commessa nei lavori---------//
		$this->db->where('commessa', $commessa);
		$this->db->update('lavori', $data);
	}

	function footerComponenteDati() // invocata da dettaglioop se devo modificare dati componente-----> invoca salvaComponente------------------//
	{


		$query = $this->db->get_where('bolle_prod',array('cod_bolla'=>$this->uri->segment(4)));
		foreach ($query->result() as $row);
		if($row->id_materiale!="")
		{$query = $this->db->get_where('materiali',array('id_materiale'=>$row->id_materiale));
				foreach ($query->result() as $nome_mat);
				$compon = $this->db->get_where('componenti',array('id_componente'=>$row->id_componente));
				foreach ($compon->result() as $componente);
				$lista_forme=array("PIATTO"=>"PIATTO",
									'TONDO'=>'TONDO',
									'TUBO'=>'TUBO',
									'ESAGONO'=>'ESAGONO');
				$this->db->select('sezione_materiale');
				$this->db->from('magazzino_materiali');
				$where = "id_materiale = ".$row->id_materiale." AND forma_materiale = 'PIATTO' ";
				$this->db->where($where);
				$this->db->order_by("sezione_materiale", "desc");
				$query = $this->db->get();

				if($query->num_rows()>0){
					foreach ($query->result() as $materiale){
						$lista_sezioni[$materiale->sezione_materiale]=$materiale->sezione_materiale;
					}
				}else{
					$lista_sezioni['RITAGLIO']="RITAGLIO";

				}
				if($row->codice_materiale_interno!=NULL){
					$query = $this->db->get_where('magazzino_materiali',array('codice_materiale_interno'=>$row->codice_materiale_interno));
					foreach ($query->result() as $materiale);
					$forma=$materiale->forma_materiale;
					$sezione=$materiale->sezione_materiale;
				}
				else{
					$forma="PIATTO";
					$sezione="";
				}

					$footer="
					<table align='center'>
					<form method='POST' action='/index.php/operatori/salvaComponente'>
					<input type='hidden' name='cod_bolla' value='".$row->cod_bolla."'>
					<input type='hidden' name='id_materiale' value='".$row->id_materiale."'>
					<input type='hidden' name='redirect'  value='C'>

					<tr><td width='500'><fieldset>
			    	<legend><font size='4' color='red'>$componente->nome_componente</font></legend>
			<font size='4'>
					<table>
					<tr><td width='180' align='left'>Quantita': </td><td><input style='height:30px;font-size:12pt;' type='text' size='10' value='".$row->quantita."' readonly/></td><td></td></tr>
					<tr><td width='180' align='left'>Materiale: </td><td>".$nome_mat->nome_materiale."</td><td></td></tr>

					<tr><td align='left'>Forma: </td><td>".form_dropdown('forma', $lista_forme, $forma )."</td><td></td></tr>
			    	<tr><td align='left'>Sezione: </td><td>".form_dropdown('sezione', $lista_sezioni,$sezione)."</td><td></td></tr>
					<tr class='lunghezza'><td width='200' align='left'><div class='lunghezza_sezione'>Lunghezza:</div> </td><td><input class='lunghezza' style='height:30px;font-size:12pt;' name='lunghezza' type='text' size='10' value='' /></td><td width='200'><div class='ritaglio'>Ritaglio: <input style='height:30px;font-size:12pt;' name='misura_ritaglio' type='text' size='10' value='".$row->misure_ritaglio."' /></div></td></tr>

			    	<tr class='kili'><td width='150' align='left'>Kg: </td><td><input style='height:30px;font-size:12pt;' class='kili' name='kg' type='text' size='10' value='' /></td></tr>
					<tr><td align='left'>Non Conformi: </td><td> <input style='height:30px;font-size:12pt;' name='non_conformi' type='text' size='10' value='".$row->non_conformi."' /></td></tr>
					<tr><td align='right'><input  style='font-size:15pt;' type='submit' onclick='internalLink=true;'' value='SALVA'></td>
					<td><a href='/index.php/omis/dettaglioop/componenti/".$this->uri->segment(4)."' onclick='internalLink=true;'><input style='font-size:15pt;' type='button' value='ANNULLA' /></a></td></tr>
					</table>
					</font>

			  		</fieldset></td>
					<td><fieldset>
					<legend><font size='4' color='red'>NOTE COMPONENTE</font></legend>".$row->note."</fieldset></td>
					</tr>
					</table>
					";}
				else{
					$footer="Non e' stato specificato nessun materiale per il componente selezionato.";
				}

		return $footer;
	}

	function pagina_mobile(){
		$this->db->select("commessa");
		$lav = $this->db->get_where('lavori',array('stato'=>'C'));
		$commesse=array();
		foreach($lav->result() as $lavCorrente){
			$commesse[$lavCorrente->commessa]=$lavCorrente->commessa;
		}
		$output = new stdClass();
		$output->lista_commesse=form_dropdown('commessa', $commesse,$this->uri->segment(3));
		$output->nome_op=$this->session->userdata('nome_operatore');

		$this->load->view('omis_mobile.php',$output);
	}

	function salvaComponente()				//----------------------GESTIONE SALVATAGGIO DATI DI SINGOLA BOLLA DI PRODUZIONE(COMPONENTE)-------------------------//
	{
		$lav = $this->db->get_where('bolle_prod',array('cod_bolla'=>$_POST['cod_bolla']));
		foreach($lav->result() as $lavCorrente);
	/*	if($lavCorrente->quantita==$_POST['completati'])
		{							//	----------	Se COMPLETATI = QUANTITA' lo stato della bolla diventa COMPLETATO ---
		 	$stato='COMPLETATO';
			($_POST['kg'] != '0.00') ? $stato='IN CORSO' : $stato='INSERIRE KG';
		}
		else*/

		if(($_POST['forma']=="PIATTO")&&($_POST['sezione']!="RITAGLIO"))
		{
			$mater =$this->db->get_where('magazzino_materiali',array('id_materiale'=>$_POST['id_materiale'],'forma_materiale'=>$_POST['forma'],'sezione_materiale'=>$_POST['sezione']));
			foreach($mater->result() as $cod_mat_interno);
			$sezione=str_replace("*","x",$_POST['sezione']);
			$sezione=str_replace("X","x",$sezione);
			@list($valore1,$valore2,$valore3) = explode("x",$sezione);
			$_POST['sezione']=$sezione;
			$peso = $this->db->get_where('materiali',array('id_materiale'=>$_POST['id_materiale']));
			foreach($peso->result() as $peso_spec);
			$kg=($valore1*$valore2*$_POST['lunghezza']*$peso_spec->peso_specifico_materiale)/1000000;
			/*$que=$this->db->get_where('bolle_prod',array('cod_bolla'=> $_POST['cod_bolla']));
			foreach($que->result() as $bolla);
			$vecchi_kg=$bolla->kg;*/

			if($_POST['misura_ritaglio']!="")
			{
				$mater=$this->db->get_where('magazzino_materiali',array('id_materiale'=>$_POST['id_materiale'],'forma_materiale'=>$_POST['forma'],
					'sezione_materiale'=>'RITAGLIO'));
				foreach($mater->result() as $cod_mat);

				$nuovi_kg=($valore1*$valore2*$_POST['misura_ritaglio']*$peso_spec->peso_specifico_materiale)/1000000;
				$nuova_qta_disponibile=$cod_mat->qta_disponibile+$nuovi_kg-($valore1*$valore2*($bolla->misure_ritaglio)*$peso_spec->peso_specifico_materiale)/1000000;

				/*$this->db->where('codice_materiale_interno', $cod_mat->codice_materiale_interno);
				$this->db->update('magazzino_materiali', array('qta_disponibile'=>$nuova_qta_disponibile));
				$this->db->where('cod_bolla', $_POST['cod_bolla']);
				$this->db->update('bolle_prod', array('misure_ritaglio'=>$_POST['misura_ritaglio']));*/
				$kg_ritaglio=$nuovi_kg;
				$misure_ritaglio=$valore1."x".$valore2."x".$_POST['misura_ritaglio'];
			}else{
				$kg_ritaglio=0;
			}
			$lunghezza_barra=$_POST['lunghezza'];

		}
		if(($_POST['forma']=="TONDO")&&($_POST['sezione']!="RITAGLIO"))
		{
			$mater=$this->db->get_where('magazzino_materiali',array('id_materiale'=>$_POST['id_materiale'],'forma_materiale'=>$_POST['forma'],'sezione_materiale'=>$_POST['sezione']));
			foreach($mater->result() as $cod_mat_interno);
			$peso = $this->db->get_where('materiali',array('id_materiale'=>$_POST['id_materiale']));
			foreach($peso->result() as $peso_spec);
			$kg=(($_POST['sezione']/2)*($_POST['sezione']/2)*3.1415926535*$_POST['lunghezza']*$peso_spec->peso_specifico_materiale)/1000000;
			/*$que=$this->db->get_where('bolle_prod',array('cod_bolla'=> $_POST['cod_bolla']));
			foreach($que->result() as $bolla);
			$vecchi_kg=$bolla->kg;*/

			if($_POST['misura_ritaglio']!="")
			{
				$mater=$this->db->get_where('magazzino_materiali',array('id_materiale'=>$_POST['id_materiale'],'forma_materiale'=>$_POST['forma'],
					'sezione_materiale'=>'RITAGLIO'));
				foreach($mater->result() as $cod_mat);

				$nuovi_kg=(($_POST['sezione']/2)*($_POST['sezione']/2)*3.1415926535*$_POST['misura_ritaglio']*$peso_spec->peso_specifico_materiale)/1000000;
				//$nuova_qta_disponibile=$cod_mat->qta_disponibile+
				//$nuovi_kg-((($_POST['sezione']/2)*($_POST['sezione']/2)*3.1415926535*($bolla->misure_ritaglio)*$peso_spec->peso_specifico_materiale)/1000000);
				$misure_ritaglio=$_POST['sezione']."x".$_POST['misura_ritaglio'];
				/*$this->db->where('codice_materiale_interno', $cod_mat->codice_materiale_interno);
				$this->db->update('magazzino_materiali', array('qta_disponibile'=>$nuova_qta_disponibile));
				$this->db->where('cod_bolla', $_POST['cod_bolla']);
				$this->db->update('bolle_prod', array('misure_ritaglio'=>$_POST['misura_ritaglio']));*/
				$kg_ritaglio=$nuovi_kg;

			}	else{
					$kg_ritaglio=0;
				}
			$lunghezza_barra=$_POST['lunghezza'];
		}
		if(($_POST['forma']=="PIATTO")&&($_POST['sezione']=="RITAGLIO"))
		{
			$mater=$this->db->get_where('magazzino_materiali',array('id_materiale'=>$_POST['id_materiale'],'forma_materiale'=>$_POST['forma'],'sezione_materiale'=>$_POST['sezione']));
			foreach($mater->result() as $cod_mat_interno);
			$peso = $this->db->get_where('materiali',array('id_materiale'=>$_POST['id_materiale']));
			foreach($peso->result() as $peso_spec);
			$que=$this->db->get_where('bolle_prod',array('cod_bolla'=> $_POST['cod_bolla']));
			foreach($que->result() as $bolla);
			$vecchi_kg=$bolla->kg;
			if(isset($_POST['lunghezza'])&&($_POST['lunghezza']!="")){
				$lunghezza=str_replace("*","x",$_POST['lunghezza']);
				$lunghezza=str_replace("X","x",$lunghezza);
				@list($valore1,$valore2,$valore3) = explode("x",$lunghezza);
				$kg=($valore1*$valore2*$valore3*$peso_spec->peso_specifico_materiale)/1000000;
				$misure_ritaglio=$lunghezza;
			}else{
				if($_POST['kg']!=""){
					$kg= $_POST['kg'];
					$misure_ritaglio="";
				}
			}
			$kg_ritaglio=0;

		}
		date_default_timezone_set('Europe/Rome');

		/*$this->db->where('codice_materiale_interno', $cod_mat_interno->codice_materiale_interno);
		$this->db->update('magazzino_materiali', array('qta_disponibile'=>($cod_mat_interno->qta_disponibile)+$vecchi_kg-$kg-$kg_ritaglio));*/
			$nuovoConsumo=array(
					'cod_bolla' => $_POST['cod_bolla'],
					'commessa' => $lavCorrente->commessa,
					'data_consumo_mat' => date('Y-m-d'),
					'kg_consumati'=>$kg,
					'kg_ritaglio'=>$kg_ritaglio,
	               	'codice_materiale_interno' => $cod_mat_interno->codice_materiale_interno,
					'lunghezza_barra' => $lunghezza_barra,
					'misure_ritaglio' => $misure_ritaglio,
					'id_operatore' => $this->session->userdata('id_operatore')
					);

		$this->db->insert('consumi_materiali',$nuovoConsumo);
		$stato='IN CORSO';
		$data = array(																//------Aggiorno stato bolla produzione-------//
		               //'lunghezza_misure' => $_POST['lunghezza'],
						'codice_materiale_interno' => $cod_mat_interno->codice_materiale_interno,
		               //'completati' => $_POST['completati'],
		               'non_conformi' => $_POST['non_conformi'],
					   'stato' => $stato

		            );
		$this->db->where('cod_bolla', $_POST['cod_bolla']);
		$this->db->update('bolle_prod', $data);
		$this->commessa_completata($_POST['commessa']);
		redirect('/operatori/elenco_bolle_produzione', 'refresh');
	}


}
