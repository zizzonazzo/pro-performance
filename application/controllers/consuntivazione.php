<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/////////////////////////////////////////////////////////////////count($ci->uri->segments)

class Consuntivazione extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		
		/* Standard Libraries */
		$this->load->database();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('date');
		$this->load->helper('security');
		$this->load->library('session');
		
		
		/* ------------------ */	
		
		$this->load->library('grocery_CRUD');	
		if(!$this->session->userdata('logged_in'))
			redirect('/login/show', 'refresh');

		
	}
	
	
	
	
	function tabella_consuntivo()
	{
			$crud = new grocery_CRUD();
			$crud->set_theme('datatables');
			$crud->set_table('lavori');
			$crud->where('stato','E');
			//where ddt_data in between 1gennaio $this->uri->segment(4) e 31 dicembre $this->uri->segment(4)
			$crud->columns('ddt_data','ddt_n','commessa','n_stampo','tipo_lavoro','cliente','nome_sapone','Costo','Costo con CF','Prezzo');
			$crud->callback_column('Costo',array($this,'costo_column'));
			$crud->callback_column('Costo con CF',array($this,'costo_cf_column'));
			$crud->callback_column('Prezzo',array($this,'prezzo_column'));

			$crud->set_relation('cliente','clienti','ragione_soc');
			$output = $crud->render();
			$output->header="CONSUNTIVAZIONE";
			$output->footer="";
			$output->nome_op=$this->session->userdata('nome_operatore');
			
			$this->load->view('anagrafiche.php',$output);
	}
	
	function costo_column($value, $row)
	{
		$costo=0;

		
			$year_ddt=substr($row->ddt_data, -4);
		
				$query = $this->db->get_where('lavorazioni',array('commessa'=>$row->commessa));
				foreach ($query->result() as $lavorazione){
					$queryInterna = $this->db->get_where('costi_cdc_annui',array('id_centro_costo'=>$lavorazione->id_centro_costo,'anno_cdc'=>$year_ddt));
					foreach ($queryInterna->result() as $cdc);
					$costo+=($lavorazione->t_operatore)*($cdc->costo_uomo_cdc);
		
				}
		
		return $costo;
	}

	function costo_cf_column($value, $row)
	{
		
	}

	function prezzo_column($value, $row)
	{
		
	}
	
	
	
	
	
	


}