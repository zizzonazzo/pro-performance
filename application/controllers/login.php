<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		
		/* Standard Libraries */
		$this->load->database();
		$this->load->helper('url');
		$this->load->helper('security');
		
		/* ------------------ */	
		
		$this->load->library('grocery_CRUD');	
	}
	
	function show()
	{

		$this->load->view('login_view.php');	
	}
	
	function valida()
	{
		$query = $this->db->get_where('operatori',array('nome_op'=>$_POST['user'],'password_op'=>$_POST['pass']));
		if($query->num_rows() > 0)
		{	
			foreach ($query->result() as $operatore);
			$this->load->library('session');
			$cookie=array(
				'nome_operatore'=> $operatore->nome_op,
				'id_operatore'=> $operatore->id_operatore,
				'id_centro_costo'=> $operatore->id_centro_costo,
				'logged_in'=> 'TRUE',
				'tipo_op'=> $operatore->tipo_op,
				'id_cliente'=>$operatore->id_cliente,
				'id_produttore'=>$operatore->id_produttore
			);

			
			$this->session->set_userdata($cookie);
			switch ($operatore->tipo_op){
				case "UFF":
					redirect('/omis/lavori/progetti', 'refresh');
				break;
				case "OPE":
					redirect('/omis/lavorazioni/incorso', 'refresh');
				break;
				case "DIS":
					redirect('/omis/lavorazioni/progetti', 'refresh');
				break;
				case "CLI":
					redirect('/clienti/elenco', 'refresh');
				break;
			}
		}
		else
		{

			$this->load->view('login_view.php');	
		}
	}
}	
