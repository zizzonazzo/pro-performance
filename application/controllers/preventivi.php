<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/////////////////////////////////////////////////////////////////count($ci->uri->segments)

class Preventivi extends CI_Controller {

	function __construct()
	{
		parent::__construct();

		/* Standard Libraries */
		$this->load->database();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('date');
		$this->load->helper('security');
		$this->load->library('session');


		/* ------------------ */

		$this->load->library('grocery_CRUD');
		if(!$this->session->userdata('logged_in'))
			redirect('/login/show', 'refresh');


	}

	function build_menu(){
		$vars=array();
		$vars['tipo_op']=$this->session->userdata('tipo_op');
		$vars['nome_op']=$this->session->userdata('nome_operatore');


		extract($vars);
			#region buffering...
			ob_start();

			include(getcwd().'/application/views/menu_builder.php');

			$buffer = ob_get_contents();
		@ob_end_clean();
		return $buffer;
	}

	function aggiungi_comm(){
		date_default_timezone_set('Europe/Rome');

		$this->popola_preventivo($this->uri->segment(3), $this->uri->segment(4),$this->uri->segment(5));
		$query=$this->db->get_where('preventivi',array('id_preventivo'=>$this->uri->segment(3)));
		foreach ($query->result() as $preventivo);
		$this->db->where('commessa', $this->uri->segment(5));
		$this->db->update('lavori', array('offerta_n'=> $preventivo->num_preventivo."/".$this->uri->segment(4),'offerta_data'=>date('Y-m-d')));

		redirect('/preventivi/ricarica_prev/'.$this->uri->segment(3).'/'.$this->uri->segment(4), 'refresh');


	}

	function elimina_comm()
	{
		$this->db->delete('preventivi_commesse', array('id_preventivo'=> $this->uri->segment(3), 'num_revisione' => $this->uri->segment(4),'commessa' => $this->uri->segment(5)));
		$this->db->delete('righe_preventivi', array('id_preventivo'=> $this->uri->segment(3), 'num_revisione' => $this->uri->segment(4),'commessa' => $this->uri->segment(5)));
		$this->db->order_by("num_revisione", "desc");
		$query=$this->db->get_where('preventivi_commesse',array('id_preventivo'=>$this->uri->segment(3),'commessa'=>$this->uri->segment(5)));
		if($query->num_rows>0){
			foreach ($query->result() as $prevcomm);
			$query=$this->db->get_where('preventivi',array('id_preventivo'=>$this->uri->segment(3)));
			foreach ($query->result() as $preventivo);
			$this->db->where('commessa', $this->uri->segment(5));
			$this->db->update('lavori', array('offerta_n'=> $preventivo->num_preventivo."/".$prevcomm->num_revisione,'offerta_data'=>$prevcomm->data_offerta));
		}else{
			$this->db->where('commessa', $this->uri->segment(5));
			$this->db->update('lavori', array('offerta_n'=> NULL,'offerta_data'=>NULL));
		}

		redirect('/preventivi/ricarica_prev/'.$this->uri->segment(3).'/'.$this->uri->segment(4), 'refresh');

	}

	function ricarica_prev(){

		date_default_timezone_set('Europe/Rome');

		$query = $this->db->get_where('preventivi',array('id_preventivo'=>$this->uri->segment(3)));
		foreach ($query->result() as $preventivo);
		$query = $this->db->get_where('preventivi_commesse',array('id_preventivo'=>$this->uri->segment(3),'num_revisione'=>$this->uri->segment(4)));
		foreach ($query->result() as $preventivo_commessa);
		$query = $this->db->get_where('clienti',array('id_cliente'=>$preventivo->id_cliente));
		foreach ($query->result() as $cliente);

		@list($year,$month,$day) = explode("-",$preventivo->data_creazione_prev);
		$output = new stdClass();
		$output->data_offerta_file=date ("dmY",mktime (0,0,0,(int)$month , (int)$day , (int)$year));
		$output->lista_revisioni="<select class='revisioni'>";
		$this->db->order_by("num_revisione", "asc");
		$this->db->select('num_revisione');
		$this->db->distinct();
		$query = $this->db->get_where('preventivi_commesse',array('id_preventivo'=>$this->uri->segment(3)));
		foreach ($query->result() as $row){
			if($row->num_revisione==$this->uri->segment(4))
			{
				$selected='selected';
			}else{
				$selected='';
			}
			$output->lista_revisioni.="<option value='".$row->num_revisione."' ".$selected.">&nbsp;&nbsp;&nbsp;".$row->num_revisione."</option>";
		}

		$output->lista_revisioni.="</select>";
		$output->ultimo_num_revisione=$row->num_revisione;
		switch($this->session->userdata('nome_operatore')){
			case "ANDREA":
				$user='Andrea Piatto';
			break;
			case "FRANZ":
				$user='Franz Piatto';
			break;
			case "GIANNI":
				$user='Gianni Piatto';
			break;
			case "LORI":
				$user='Lori';
			break;
			default:
				$user="";
			break;
		}
		if($cliente->lingua=='ITA'){
			$query = $this->db->get_where('lavori',array('commessa'=>$preventivo_commessa->commessa));
			foreach ($query->result() as $lavoro);
			if($lavoro->comm_cliente!=NULL){
				$aggiunta=". Vostra Commessa: ".$lavoro->comm_cliente;
			}else{
				$aggiunta="";
			}
			$output->oggetto_email="Nostra offerta ".$preventivo->num_preventivo."/".$preventivo_commessa->num_revisione." del ".$preventivo_commessa->data_offerta.$aggiunta;
			$output->signature="Buongiorno, \xA\xA"."In allegato la nostra offerta come in oggetto."."\xA\xA"."In attesa di vostra conferma, \xA".'Cordiali saluti. '."\xA\xA".$user."\xA".'www.omis.it '."\xA".'Tel. +39 0331 776097 '."\xA".'Fax  +39 0331 772290';
		}
		if($cliente->lingua=='FRA'){
			$output->oggetto_email="Notre devis ".$preventivo->num_preventivo."/".$preventivo_commessa->num_revisione." du ".$preventivo_commessa->data_offerta;
			$output->signature="Bonjour, \xA\xA"."Veuillez trouver ci-joint notre devis en objet."."\xA\xA"."En attente de votre confirmation, \xA".'Cordialement. '."\xA\xA".$user."\xA".'www.omis.it'."\xA".'Tel. +39 0331 776097 '."\xA".'Fax  +39 0331 772290';
		}
		if($cliente->lingua=='ENG'){
			$output->oggetto_email="Our offer ".$preventivo->num_preventivo."/".$preventivo_commessa->num_revisione." of ".$preventivo_commessa->data_offerta;
			$output->signature="Good morning, \xA\xA"."Here attached the offer of the subject."."\xA\xA"."Waiting for your confirmation, \xA".'Best Regards. '."\xA\xA".$user."\xA".'www.omis.it '."\xA".'Tel. +39 0331 776097 '."\xA".'Fax  +39 0331 772290';
		}
		if($this->session->userdata('nome_operatore')=="ANDREA"){
			$output->ccns_email="andrea.piatto@omis.it; gianni.piatto@omis.it;";
		}else{
			$output->ccns_email="andrea.piatto@omis.it; franz.piatto@omis.it; gianni.piatto@omis.it;";
		}

		if($preventivo->num_revisione_confermata!=$this->uri->segment(4))
		{
			//HEADER
		$output->riferimenti_cliente=$preventivo->riferimenti_prev;//da leggere da preventivi
		$output->num_revisione=$preventivo_commessa->num_revisione;//da leggere da preventivi
		$output->id_cliente_prev=$preventivo->id_cliente;
		$output->nome_cliente=$cliente->nome_cliente_documenti;
		$output->indirizzo_cliente=$cliente->indirizzo;
		$output->citta_cliente=$cliente->citta;
		$output->nazione_cliente=$cliente->nazione;
		$output->email_cliente=$cliente->email_cliente;
		$output->nome_destinatario='<select class="chosen-select" name="id_destinatario">';
		$this->db->order_by("ragione_soc", "asc");
		$query = $this->db->get('clienti');
		foreach ($query->result() as $clienti){
				if($clienti->id_cliente==$preventivo->id_dest){//da leggere da preventivo
					$selected='selected="selected"';

				}else{$selected="";}

				$output->nome_destinatario.='<option value="'.$clienti->id_cliente.'"'.$selected.' >'.$clienti->nome_cliente_documenti.'</option>';
		}
		$output->nome_destinatario.='</select>';
		$output->num_preventivo=$preventivo->num_preventivo;//da leggere da preventivo
		@list($year,$month,$day) = explode("-",$preventivo->data_creazione_prev);
		$output->data_offerta=date ("d-m-Y",mktime (0,0,0,(int)$month , (int)$day , (int)$year));

		$output->id_preventivo=$preventivo->id_preventivo;

					$output->tabella_descrizione="";
					$output->disegni_allegati="";
					$query = $this->db->get_where('preventivi_commesse',array('id_preventivo'=>$preventivo->id_preventivo,'num_revisione'=>$this->uri->segment(4)));


					foreach ($query->result() as $preventivocomm){


						$query = $this->db->get_where('lavori',array('commessa'=>$preventivocomm->commessa));
						foreach ($query->result() as $commessa);

						if($commessa->dis_sapone!=NULL)
						{
							$nomefile=$commessa->dis_sapone;
						}else{
							$nomefile=$commessa->n_stampo;
						}
						$output->disegni_allegati.="<div><a class='allegato' target='_blank' href='ftp://".$_SERVER["SERVER_NAME"].":2222/".$cliente->ragione_soc."/PDF/".$nomefile.".pdf' >Allegato Disegno &nbsp;</a><a class='eliminaallegato'> <span class='glyphicon glyphicon-remove' aria-hidden='true'></span></a></div>";


						$prendi_righe = $this->db->get_where('righe_preventivi',array('commessa'=>$preventivocomm->commessa,
																				 'id_preventivo'=>$preventivo->id_preventivo,
																				 'num_revisione'=>$this->uri->segment(4)));

						if($commessa->comm_cliente!=NULL){$comm_cliente=" - Vs. Commessa: ".$commessa->comm_cliente;}
						else{$comm_cliente='';}
						// if($commessa->tipo_lavoro!='STA'){
						// 	if($commessa->n_stampo!=NULL){$n_stampo=" - N° Stampo: ".$commessa->n_stampo;}
						// 	else{$n_stampo='';}
						// }else{
						// 	$n_stampo='';
						// }
							$output->tabella_descrizione.='
							<div class="row" style="margin-top:20px;">
											<div class="col-sm-6">Ns. Riferimento: <b>'.$commessa->commessa.$comm_cliente.'</b> <input type="button" style="margin-right:2px;" class="elimina_comm" value="ELIMINA COMMESSA" commessa="'.$commessa->commessa.'" /><a href="'.site_url("omis/dettaglio/componenti/".$commessa->commessa).'"><input type="button" value="DETTAGLIO"/></a></div>
											<div class="col-sm-1"></div>
											<div class="col-sm-1"></div>
											<div class="col-sm-1"></div>
											<div class="col-sm-1"></div>
											<div class="col-sm-2"></div>
							</div>
							';
							if($commessa->tipo_lavoro!='STA'){
								$output->tabella_descrizione.='
							<div class="row" style="margin-top:10px; margin-bottom:30px;">
											<div class="col-sm-6"><textarea style="width:100%; font-weight: bold;" rows="1" commessa="'.$preventivocomm->commessa.'" class="stampo_nome_peso">'.str_replace('<br>', "\xA", $preventivocomm->stampo_nome_peso).'</textarea></div>
											<div class="col-sm-1">------</div>
											<div class="col-sm-1">------</div>
											<div class="col-sm-1">------</div>
											<div class="col-sm-1">------</div>
											<div class="col-sm-2">--------</div>
							</div>
							';
							}
							if($prendi_righe->num_rows()>0){

								foreach ($prendi_righe->result() as $riga)
								{
									$lista_imposte='<small><select name="classe_iva_riga" id_riga="'.$riga->id_riga_preventivo.'">';
									$queryoz = $this->db->get('classi_iva');
									foreach ($queryoz->result() as $classe){
											if($classe->id_classe_iva==$riga->id_classe_iva){//da leggere da preventivo
												$selected='selected="selected"';

											}else{$selected="";}

											$lista_imposte.='<option value="'.$classe->id_classe_iva.'" valore="'.$classe->valore_classe.'"'.$selected.' >'.$classe->nome_classe_iva.'</option>';
									}
									$lista_imposte.='</select></small>';
									if($riga->riga_aggiunta=='S'){
										$elimina_riga="<small><input type='button' class='elimina_riga' id_riga='".$riga->id_riga_preventivo."' value='X'></small>";
										$quantita_campo="<input type='text' style='text-align:right; width:50%;' value='".$riga->quantita."' id_riga='".$riga->id_riga_preventivo."' class='qta_riga'>";
									}
									else{
										$elimina_riga="";
										$quantita_campo="<input type='text' style='text-align:right; width:50%;' value='".$riga->quantita."' id_riga='".$riga->id_riga_preventivo."' class='qta_riga' disabled>";
									}
									$output->tabella_descrizione.='
										<div class="row" style="margin-top:10px;">
											<div class="col-sm-6"><textarea style="width:100%;" id_riga="'.$riga->id_riga_preventivo.'" class="descrizione">'.str_replace('<br>', "\xA", $riga->descrizione).'</textarea></div>
											<div class="col-sm-1">'.$quantita_campo.'</div>
											<div class="col-sm-1" style="padding-right:5px;"><input class="prezzo_riga" id_riga="'.$riga->id_riga_preventivo.'"" style="text-align:right; width:80%;" value="'.$riga->prezzo.'"" type="text"> &#8364; </div>
											<div class="col-sm-1" style="padding-right:5px;"><input class="sconto_riga" id_riga="'.$riga->id_riga_preventivo.'"" style="text-align:right; width:50%;" value="'.$riga->sconto.'"" type="text"> % </div>
											<div class="col-sm-1"><input class="totale_riga" id_riga="'.$riga->id_riga_preventivo.'"" type="text" value="0.00" style="width:80%;" disabled> &#8364;</div>
											<div class="col-sm-2" style="padding-left:0px;">'.$lista_imposte.$elimina_riga.'</div>
										</div>
									';




								}
							}
						if($commessa->tipo_lavoro=='STA')
						$output->tabella_descrizione.='<div class="row" style="margin-top:10px;">
											<div class="col-sm-6"><input tipo_lavoro="STA" commessa="'.$commessa->commessa.'" type="button" value="DESCRIZIONE" style="width:50%; font-size:20px; background-color:#33FFFF;" class="nuovariga"><input commessa="'.$commessa->commessa.'" type="button" value="COMPONENTE" style="width:50%; font-size:20px; background-color:#33FFFF;" class="nuovarigacomponente"></div>
											<div class="col-sm-1"></div>
											<div class="col-sm-1"></div>
											<div class="col-sm-1" ></div>
											<div class="col-sm-1"></div>
											<div class="col-sm-2" style="padding-left:0px;"></div>
										</div>';
						else
						$output->tabella_descrizione.='<div class="row" style="margin-top:10px;">
											<div class="col-sm-6"><input tipo_lavoro="NS" commessa="'.$commessa->commessa.'" type="button" value="DESCRIZIONE" style="width:100%; font-size:20px; background-color:#33FFFF;" class="nuovariga"></div>
											<div class="col-sm-1"></div>
											<div class="col-sm-1"></div>
											<div class="col-sm-1" ></div>
											<div class="col-sm-1"></div>
											<div class="col-sm-2" style="padding-left:0px;"></div>
										</div>';



					}
					if($preventivo->num_revisione_confermata==$this->uri->segment(4))
					{
						$output->button_conferma_revisione="REVISIONE CONFERMATA!";
					}else{
						$output->button_conferma_revisione="<a href='".site_url('preventivi/conferma_revisione/'.$output->id_preventivo.'/'.$output->num_revisione)."'><button class='btn btn-default' style='width:100%; padding-left:5px;' role='button'/><span class='glyphicon glyphicon-thumbs-up' aria-hidden='true'></span> Conferma Rev</button></a>";
					}

					//FOOTER

					$query = $this->db->get_where('banca_appoggio_cliente',array('id_banca_cliente'=>$preventivo->id_banca_cliente));
					if($query->num_rows() > 0){
						foreach ($query->result() as $banca);
						$output->banca_appoggio=$banca->nome_banca_cliente;

					}else{
						$output->banca_appoggio="Non specificato in anagrafica";

					}
					$query = $this->db->get_where('condizioni_di_pagamento',array('id_condiz_pagam'=>$cliente->id_condiz_pagam));
					if($query->num_rows() > 0){
						foreach ($query->result() as $cond);
						$output->condizione_pagamento=$cond->nome_condiz_pagam;

					}else{
						$output->condizione_pagamento="Non specificato in anagrafica";
					}
					$query = $this->db->get('tipo_trasporto');
					foreach ($query->result() as $row){
								$tipi_trasporto[$row->id_tipo_trasp]=$row->nome_tipo_trasp;
					}
					$output->tipo_trasporto=form_dropdown('tipo_trasporto', $tipi_trasporto, $preventivo->id_tipo_trasp);
						$dataconsegna=array(
							'1' => '1 settimana',
							'2' => '2 settimane',
							'3' => '3 settimane',
							'4' => '4 settimane',
							'5' => '5 settimane',
							'6' => '6 settimane'


						);
					$output->data_cons_lav_prevista=form_dropdown('data_cons_lav_prevista', $dataconsegna, $preventivo->data_cons_lav_prevista);
					$query = $this->db->get('tipo_porto');
					foreach ($query->result() as $row){
								$tipi_porto[$row->id_tipo_porto]=$row->nome_tipo_porto;
					}
					$output->tipo_porto=form_dropdown('tipo_porto', $tipi_porto, $preventivo->id_tipo_porto);
					//$output->sconto="0";
					$output->costo_trasp=$preventivo->costo_trasp;
					$output->costo_trasp1=$preventivo->costo_trasp1;
					if($preventivo->costo_trasp1_attivo=='N'){
						$output->attiva_secondo_costo_trasp=0;
						$output->secondo_costo_tr_class='disabled';
					}else{
						$output->attiva_secondo_costo_trasp=1;
						$output->secondo_costo_tr_class='enabled';

					}

					$output->imposte='<select name="classe_iva">';
					$query = $this->db->get('classi_iva');
					foreach ($query->result() as $classe){
							if($classe->id_classe_iva==$preventivo->id_classe_iva_cost_trasp){//da leggere da preventivo
								$selected='selected="selected"';

							}else{$selected="";}

							$output->imposte.='<option value="'.$classe->id_classe_iva.'" valore="'.$classe->valore_classe.'"'.$selected.' >'.$classe->nome_classe_iva.'</option>';
					}
					$output->imposte.='</select>';

					$output->imposte1='<select name="classe_iva1">';
					$query = $this->db->get('classi_iva');
					foreach ($query->result() as $classe){
							if($classe->id_classe_iva==$preventivo->id_classe_iva_cost_trasp1){//da leggere da preventivo
								$selected='selected="selected"';

							}else{$selected="";}

							$output->imposte1.='<option value="'.$classe->id_classe_iva.'" valore="'.$classe->valore_classe.'"'.$selected.' >'.$classe->nome_classe_iva.'</option>';
					}
					$output->imposte1.='</select>';
					$output->num_preventivo=$preventivo->num_preventivo;
		$output->nome_op=$this->session->userdata('nome_operatore');
		$output->base_url="http://".$_SERVER["SERVER_NAME"].":8000/";

		$output->menu=$this->build_menu();
		$this->load->view('preventivi_view.php',$output);

		}else{ //se revisione confermata carico la view non editabile
					//HEADER
		$output->riferimenti_cliente=$preventivo->riferimenti_prev;//da leggere da preventivi
		$output->num_revisione=$preventivo_commessa->num_revisione;//da leggere da preventivi
		$output->id_cliente_prev=$preventivo->id_cliente;
		$output->nome_cliente=$cliente->nome_cliente_documenti;
		$output->indirizzo_cliente=$cliente->indirizzo;
		$output->citta_cliente=$cliente->citta;
		$output->nazione_cliente=$cliente->nazione;
		$output->email_cliente=$cliente->email_cliente;
		$query = $this->db->get_where('clienti',array('id_cliente'=>$preventivo->id_dest));
		foreach ($query->result() as $destinatario);
		$output->nome_destinatario=$destinatario->nome_cliente_documenti;
		$output->indirizzo_dest=$destinatario->indirizzo;
		$output->citta_dest=$destinatario->citta;
		$output->nazione_dest=$destinatario->nazione;
		$output->num_preventivo=$preventivo->num_preventivo;//da leggere da preventivo
		@list($year,$month,$day) = explode("-",$preventivo_commessa->data_offerta);
		$output->data_offerta=date ("d-m-Y",mktime (0,0,0,(int)$month , (int)$day , (int)$year));

		$output->id_preventivo=$preventivo->id_preventivo;
					$output->tabella_descrizione="";
				if($cliente->lingua=='ITA'){
					$nsriferimento="Ns. Riferimento";
					}
					if($cliente->lingua=='ENG'){
					$nsriferimento="Our Reference n";
					}
					if($cliente->lingua=='FRA'){
					$nsriferimento="Notre Réf. n";
					}

				$query = $this->db->get_where('preventivi_commesse',array('id_preventivo'=>$preventivo->id_preventivo,'num_revisione'=>$this->uri->segment(4)));


				foreach ($query->result() as $preventivocomm){


					$query = $this->db->get_where('lavori',array('commessa'=>$preventivocomm->commessa));
					foreach ($query->result() as $commessa);

					if($commessa->dis_sapone!=NULL)
						{
							$nomefile=$commessa->dis_sapone;
						}else{
							$nomefile=$commessa->n_stampo;
						}
						$output->disegni_allegati="";
						$output->disegni_allegati.="<div><a class='allegato' target='_blank' href='ftp://".$_SERVER["SERVER_NAME"].":2222/".$cliente->ragione_soc."/PDF/".$nomefile.".pdf' >Allegato Disegno &nbsp;</a><a class='eliminaallegato'> <span class='glyphicon glyphicon-remove' aria-hidden='true'></span></a></div>";
					$prendi_righe = $this->db->get_where('righe_preventivi',array('commessa'=>$preventivocomm->commessa,
																			 'id_preventivo'=>$preventivo->id_preventivo,
																			 'num_revisione'=>$this->uri->segment(4)));

						if($prendi_righe->num_rows()>0){

							if($commessa->comm_cliente!=NULL){$comm_cliente=" - Vs. Commessa: ".$commessa->comm_cliente;}
							else{$comm_cliente='';}
							if($commessa->tipo_lavoro!='STA'){
								if($commessa->n_stampo!=NULL){
									if($cliente->lingua=='ITA'){
										$n_stampo=" - N° Disegno: <b>".$commessa->n_stampo."</b>";
										}
									if($cliente->lingua=='ENG'){
										$n_stampo=" - Drawing n° <b>".$commessa->n_stampo."</b>";
										}
									if($cliente->lingua=='FRA'){
									$n_stampo=" - Dessin n° <b>".$commessa->n_stampo."</b>";
									}
								}
								else{$n_stampo='';}
							}else{
								$n_stampo='';
							}
								$output->tabella_descrizione.='
							<div class="row" style="margin-top:10px;">
											<div class="col-sm-6">'.$nsriferimento.' <b>'.$commessa->commessa.$comm_cliente.'</b> <a href="'.site_url("omis/dettaglio/componenti/".$commessa->commessa).'"><input type="button" value="DETTAGLIO"/></a></div>
											<div class="col-sm-1"></div>
											<div class="col-sm-1"></div>
											<div class="col-sm-1"></div>
											<div class="col-sm-1"></div>
											<div class="col-sm-2"></div>
							</div>
							';

							if($commessa->tipo_lavoro!='STA'){
								$output->tabella_descrizione.='
							<div class="row" style="margin-top:10px;">
											<div class="col-sm-6">'.$preventivocomm->stampo_nome_peso.'</div>
											<div class="col-sm-1"></div>
											<div class="col-sm-1"></div>
											<div class="col-sm-1"></div>
											<div class="col-sm-1"></div>
											<div class="col-sm-2"></div>
							</div>
							';
							}
							foreach ($prendi_righe->result() as $riga)
							{
								$lista_imposte='<select name="classe_iva_riga" id_riga="'.$riga->id_riga_preventivo.'">';
								$query = $this->db->get_where('classi_iva',array('id_classe_iva'=>$riga->id_classe_iva));
								foreach ($query->result() as $classe);
								if($riga->sconto=='0') $sconto='';
								else $sconto=$riga->sconto."%";

								$output->tabella_descrizione.='
										<div class="row" style="margin-top:10px;">
											<div class="col-sm-7" id_riga="'.$riga->id_riga_preventivo.'" class="descrizione">'.$riga->descrizione.'</div>
											<div class="col-sm-1">Nr</div>
											<div class="col-sm-1 qta_riga" value="'.$riga->quantita.'">'.$riga->quantita.'</div>
											<div class="col-sm-1 prezzo_riga" style="padding-right:5px;" prezzo="'.$riga->prezzo.'" valore_iva="'.$classe->valore_classe.'">'.number_format($riga->prezzo,2).' &#8364; </div>
											<div class="col-sm-1 sconto_riga" style="padding-right:5px;"  value="'.$riga->sconto.'">'.$sconto.'&nbsp;</div>
											<div class="col-sm-1 totale_riga" id_riga="'.$riga->id_riga_preventivo.'"> </div>
										</div>
									';





							}
						}
						$output->tabella_descrizione.='
										<div class="row" style="margin-top:15px;">
											<div class="col-sm-1"></div>
											<div class="col-sm-6" style="border-bottom:1px solid black;"></div>
											<div class="col-sm-1" style="border-bottom:1px solid black;"></div>
											<div class="col-sm-1" style="border-bottom:1px solid black;"></div>
											<div class="col-sm-1" style="border-bottom:1px solid black;"></div>
											<div class="col-sm-1" style="border-bottom:1px solid black;"></div>
											<div class="col-sm-1"> </div>
										</div>
									';
				}
				//FOOTER

				$query = $this->db->get_where('banca_appoggio_cliente',array('id_banca_cliente'=>$cliente->id_banca_cliente));
				if($query->num_rows() > 0){
					foreach ($query->result() as $banca);
					$output->banca_appoggio=$banca->nome_banca_cliente;

				}else{
					$output->banca_appoggio="";

				}
				$query = $this->db->get_where('condizioni_di_pagamento',array('id_condiz_pagam'=>$cliente->id_condiz_pagam));
				if($query->num_rows() > 0){
					foreach ($query->result() as $cond);
					$output->condizione_pagamento=$cond->nome_condiz_pagam;

				}else{
					$output->condizione_pagamento="";
				}
				if($preventivo->id_tipo_trasp!=NULL){
					$query = $this->db->get_where('tipo_trasporto',array('id_tipo_trasp'=>$preventivo->id_tipo_trasp));
					foreach ($query->result() as $tipo);
					$output->tipo_trasporto=$tipo->nome_tipo_trasp;

				}else{
					$output->tipo_trasporto="";
				}
				if($preventivo->id_tipo_porto!=NULL){
					$query = $this->db->get_where('tipo_porto',array('id_tipo_porto'=>$preventivo->id_tipo_porto));
					foreach ($query->result() as $tipo);
					$output->tipo_porto=$tipo->nome_tipo_porto;

				}else{
					$output->tipo_porto="";
				}
				if($preventivo->data_cons_lav_prevista>1){
					if($cliente->lingua=='ITA'){
					$dataconsegna=$preventivo->data_cons_lav_prevista." settimane";
					}
					if($cliente->lingua=='ENG'){
					$dataconsegna=$preventivo->data_cons_lav_prevista." weeks";
					}
					if($cliente->lingua=='FRA'){
					$dataconsegna=$preventivo->data_cons_lav_prevista." semaines";
					}
				}
				else{
					if($cliente->lingua=='ITA'){
					$dataconsegna=$preventivo->data_cons_lav_prevista." settimana";
					}
					if($cliente->lingua=='ENG'){
					$dataconsegna=$preventivo->data_cons_lav_prevista." week";
					}
					if($cliente->lingua=='FRA'){
					$dataconsegna=$preventivo->data_cons_lav_prevista." semaine";
					}
				}
					if($preventivo->num_revisione_confermata==$this->uri->segment(4))
					{
						$output->button_conferma_revisione="REVISIONE CONFERMATA!";
					}else{
						$output->button_conferma_revisione="<a href='".site_url('preventivi/conferma_revisione/'.$output->id_preventivo.'/'.$output->num_revisione)."'><img width='200' src='/application/views/icons/confermarevisione.png' /></a>";
					}

				$output->data_cons_lav_prevista=$dataconsegna;
				$output->sconto="0";
				$output->costo_trasp=$preventivo->costo_trasp;
				$output->costo_trasp1=$preventivo->costo_trasp1;
				if($preventivo->costo_trasp1_attivo=='N'){
					$output->attiva_secondo_costo_trasp=0;
					$output->secondo_costo_tr_class='disabled';
				}else{
					$output->attiva_secondo_costo_trasp=1;
					$output->secondo_costo_tr_class='enabled';

				}

				$query = $this->db->get_where('classi_iva',array('id_classe_iva'=>$preventivo->id_classe_iva_cost_trasp));
				foreach ($query->result() as $classe);
				$output->imposte=$classe->nome_classe_iva;
				$output->costo_trasp_classe_iva=$classe->valore_classe;

				if($preventivo->costo_trasp1_attivo=='S'){
					$query = $this->db->get_where('classi_iva',array('id_classe_iva'=>$preventivo->id_classe_iva_cost_trasp1));
					foreach ($query->result() as $classe);
					$imposte1=$classe->nome_classe_iva;
					$output->costo_trasp_classe_iva1=$classe->valore_classe;

					$output->imposte1= "<strong>Costo trasporto:</strong> ".$preventivo->costo_trasp1." &#8364; - <small> ".$imposte1."</small>";
				}else{
					$output->imposte1="&nbsp;";
					$output->costo_trasp_classe_iva1='0';
				}
				$output->num_preventivo=$preventivo->num_preventivo;
		$output->nome_op=$this->session->userdata('nome_operatore');
				$output->base_url="http://".$_SERVER["SERVER_NAME"].":8000/";


		$output->menu=$this->build_menu();
		$this->load->view('preventivi_view_non_editabile.php',$output);

		}
	}

	function preventivo_inizio(){

		date_default_timezone_set('Europe/Rome');

		$query = $this->db->get_where('lavori',array('commessa'=>$this->uri->segment(3)));
		foreach ($query->result() as $commessa);
		$query = $this->db->get_where('clienti',array('id_cliente'=>$commessa->cliente));
		foreach ($query->result() as $cliente);

		$revisione='0';
		$query = $this->db->get_where('preventivi_commesse',array('commessa'=>$commessa->commessa));
		foreach ($query->result() as $ellabella);
		if($query->num_rows()==0){ //controllo che effettivamente non esista già un preventivo relativo a questa commessa
				$this->db->select_max('num_preventivo');
				$this->db->where("data_creazione_prev BETWEEN '".date('Y')."-01-01' AND '".date('Y')."-12-31'");
				$query = $this->db->get('preventivi');

				foreach ($query->result() as $preventivomax);
				$this->db->where('commessa', $commessa->commessa);
				$this->db->update('lavori', array('offerta_n'=> (($preventivomax->num_preventivo)+1)."/0",'offerta_data'=>date('Y-m-d')));

				$nuovoPrev=array(
					'num_preventivo'=> ($preventivomax->num_preventivo)+1,
					'ultimo_num_revisione' => 0,
					'id_cliente' => $commessa->cliente,
					'riferimenti_prev' => $cliente->riferimenti_cliente,
					'id_dest' => $commessa->cliente,
					'data_creazione_prev' => date('Y-m-d'),
					'id_tipo_trasp'=>$cliente->id_tipo_trasp,
					'id_banca_cliente'=>$cliente->id_banca_cliente,
					'id_classe_iva_cost_trasp' => $cliente->id_classe_iva,
					'id_tipo_porto' => $cliente->id_tipo_porto,
					'id_classe_iva_cost_trasp1'=> $cliente->id_classe_iva
				);


				$this->db->insert('preventivi',$nuovoPrev);
				$id_preventivo=$this->db->insert_id();

				$this->popola_preventivo($id_preventivo,0,$commessa->commessa);


		}else{

			$id_preventivo=$ellabella->id_preventivo;
			@list($numprev,$revisione) = explode("/",$commessa->offerta_n);



		}
		redirect('/preventivi/ricarica_prev/'.$id_preventivo.'/'.$revisione, 'refresh');
	}


	function stampa_prev(){

		date_default_timezone_set('Europe/Rome');
		$query = $this->db->get_where('preventivi',array('id_preventivo'=>$this->uri->segment(3)));
		foreach ($query->result() as $preventivo);
		$query = $this->db->get_where('preventivi_commesse',array('id_preventivo'=>$this->uri->segment(3),'num_revisione'=>$this->uri->segment(4)));
		foreach ($query->result() as $preventivo_commessa);
		$query = $this->db->get_where('clienti',array('id_cliente'=>$preventivo->id_cliente));
		foreach ($query->result() as $cliente);

		//HEADER
		$output = new stdClass();
		$output->riferimenti_cliente=$preventivo->riferimenti_prev;//da leggere da preventivi
		$output->id_preventivo=$preventivo->id_preventivo;
		$output->num_revisione=$preventivo_commessa->num_revisione;//da leggere da preventivi
		$output->id_cliente_prev=$preventivo->id_cliente;
		$output->nome_cliente=$cliente->nome_cliente_documenti;
		$output->indirizzo_cliente=$cliente->indirizzo;
		$output->citta_cliente=$cliente->citta;
		$output->nazione_cliente=$cliente->nazione;
		$query = $this->db->get_where('clienti',array('id_cliente'=>$preventivo->id_dest));
		foreach ($query->result() as $destinatario);
		$output->nome_destinatario=$destinatario->nome_cliente_documenti;
		$output->indirizzo_dest=$destinatario->indirizzo;
		$output->citta_dest=$destinatario->citta;
		$output->nazione_dest=$destinatario->nazione;
		$output->num_preventivo=$preventivo->num_preventivo;//da leggere da preventivo
		@list($year,$month,$day) = explode("-",$preventivo_commessa->data_offerta);
		$output->data_offerta=date ("d-m-Y",mktime (0,0,0,(int)$month , (int)$day , (int)$year));

		$output->id_preventivo=$preventivo->id_preventivo;
		$output->tabella_descrizione="";
		if($cliente->lingua=='ITA'){
			$nsriferimento="Ns. Riferimento";
			}
			if($cliente->lingua=='ENG'){
			$nsriferimento="Our Reference n";
			}
			if($cliente->lingua=='FRA'){
			$nsriferimento="Notre Réf. n";
			}

		$query = $this->db->get_where('preventivi_commesse',array('id_preventivo'=>$preventivo->id_preventivo,'num_revisione'=>$this->uri->segment(4)));


		foreach ($query->result() as $preventivocomm){


			$query = $this->db->get_where('lavori',array('commessa'=>$preventivocomm->commessa));
			foreach ($query->result() as $commessa);
			// $js="comm='".$preventivocomm->commessa."'";
			// $selettoreandor=array(
			// 	'A' => '---------------------------------------',
			// 	'O' => 'In Alternativa'
			// );
			// $select=form_dropdown('andor', $selettoreandor, $preventivocomm->andor,$js);
			$prendi_righe = $this->db->get_where('righe_preventivi',array('commessa'=>$preventivocomm->commessa,
																	 'id_preventivo'=>$preventivo->id_preventivo,
																	 'num_revisione'=>$this->uri->segment(4)));

				if($prendi_righe->num_rows()>0){
					$output->tabella_descrizione.="<tr><td> &nbsp; </td>
					<td align='center'>&nbsp;</td><td align='center'>&nbsp;</td><td>&nbsp;</td></tr>";
					if($commessa->comm_cliente!=NULL){$comm_cliente=" - Vs. Commessa: ".$commessa->comm_cliente;}
					else{$comm_cliente='';}
					if($commessa->tipo_lavoro!='STA'){
						if($commessa->n_stampo!=NULL){
							if($cliente->lingua=='ITA'){
								$n_stampo=" - N° Disegno: <b>".$commessa->n_stampo."</b>";
								}
							if($cliente->lingua=='ENG'){
								$n_stampo=" - Drawing n° <b>".$commessa->n_stampo."</b>";
								}
							if($cliente->lingua=='FRA'){
							$n_stampo=" - Dessin n° <b>".$commessa->n_stampo."</b>";
							}
						}
						else{$n_stampo='';}
					}else{
						$n_stampo='';
					}
						$output->tabella_descrizione.="<tr><td> &nbsp; </td>
							<td align='center'>&nbsp;</td><td align='center'>&nbsp;</td><td>&nbsp;</td></tr>";
							$output->tabella_descrizione.="<tr><td>".$nsriferimento.": <b>".$commessa->commessa."</b>".$comm_cliente."</td>
							<td align='center'></td><td align='center'></td>
							<td></td></tr>";
					if($commessa->tipo_lavoro!='STA'){
							$output->tabella_descrizione.="<tr><td>".$preventivocomm->stampo_nome_peso."</td>
							<td align='center'></td><td align='center'></td>
							<td></td></tr>";
							$output->tabella_descrizione.="<tr><td> &nbsp; </td>
							<td align='center'>&nbsp;</td><td align='center'>&nbsp;</td><td>&nbsp;</td></tr>";
					}
					foreach ($prendi_righe->result() as $riga)
					{
						$lista_imposte='<select name="classe_iva_riga" id_riga="'.$riga->id_riga_preventivo.'">';
						$query = $this->db->get_where('classi_iva',array('id_classe_iva'=>$riga->id_classe_iva));
						foreach ($query->result() as $classe);
						if($riga->sconto=='0') $sconto='';
						else $sconto=$riga->sconto."%";
						if($this->session->userdata('nome_operatore')!='DAVID'){
							$output->tabella_descrizione.="<tr>
						<td class='descrizione'><div class='descrizione'>".$riga->descrizione."</div></td>
						<td align='center'>Nr</td><td align='center' class='qta_riga' value='".$riga->quantita."'>".$riga->quantita."</td>
						<td><div class='prezzo_riga' style='text-align:right;' prezzo='".$riga->prezzo."' valore_iva='".$classe->valore_classe."'>".number_format($riga->prezzo,2)." &#8364; &nbsp; </div></td>
					 <td><div class='sconto_riga' style='text-align:right;' value='".$riga->sconto."'>".$sconto."&nbsp;</div></td>
					 <td><div align='right' class='totale_riga' id_riga='".$riga->id_riga_preventivo."' ></div> </td>
					 </tr>";
						}
						else{
							$output->tabella_descrizione.="<tr>
						<td class='descrizione'><div class='descrizione'>".$riga->descrizione."</div></td>
						<td align='center'>Nr</td><td align='center' class='qta_riga' value='".$riga->quantita."'>".$riga->quantita."</td>
						<td></td>
					 <td></td>
					 <td></div> </td>
					 </tr>";
						}
					}
				}
				$output->tabella_descrizione.="<tr>
						<td class='last'></td>
						<td class='last'></td><td class='last'></td><td class='last'></td><td class='last'></td><td class='last'></td>
					 </tr>";







		}


		//FOOTER

		$query = $this->db->get_where('banca_appoggio_cliente',array('id_banca_cliente'=>$cliente->id_banca_cliente));
		if($query->num_rows() > 0){
			foreach ($query->result() as $banca);
			$output->banca_appoggio=$banca->nome_banca_cliente;

		}else{
			$output->banca_appoggio="";

		}
		$query = $this->db->get_where('condizioni_di_pagamento',array('id_condiz_pagam'=>$cliente->id_condiz_pagam));
		if($query->num_rows() > 0){
			foreach ($query->result() as $cond);
			$output->condizione_pagamento=$cond->nome_condiz_pagam;

		}else{
			$output->condizione_pagamento="";
		}
		if($preventivo->id_tipo_trasp!=NULL){
			$query = $this->db->get_where('tipo_trasporto',array('id_tipo_trasp'=>$preventivo->id_tipo_trasp));
			foreach ($query->result() as $tipo);
			$output->tipo_trasporto=$tipo->nome_tipo_trasp;

		}else{
			$output->tipo_trasporto="";
		}
		if($preventivo->id_tipo_porto!=NULL){
			$query = $this->db->get_where('tipo_porto',array('id_tipo_porto'=>$preventivo->id_tipo_porto));
			foreach ($query->result() as $tipo);
			$output->tipo_porto=$tipo->nome_tipo_porto;

		}else{
			$output->tipo_porto="";
		}
		if($preventivo->data_cons_lav_prevista>1){
			if($cliente->lingua=='ITA'){
			$dataconsegna=$preventivo->data_cons_lav_prevista." settimane";
			}
			if($cliente->lingua=='ENG'){
			$dataconsegna=$preventivo->data_cons_lav_prevista." weeks";
			}
			if($cliente->lingua=='FRA'){
			$dataconsegna=$preventivo->data_cons_lav_prevista." semaines";
			}
		}
		else{
			if($cliente->lingua=='ITA'){
			$dataconsegna=$preventivo->data_cons_lav_prevista." settimana";
			}
			if($cliente->lingua=='ENG'){
			$dataconsegna=$preventivo->data_cons_lav_prevista." week";
			}
			if($cliente->lingua=='FRA'){
			$dataconsegna=$preventivo->data_cons_lav_prevista." semaine";
			}
		}


		$output->data_cons_lav_prevista=$dataconsegna;
		$output->sconto="0";
		$output->costo_trasp=$preventivo->costo_trasp;
		$output->costo_trasp1=$preventivo->costo_trasp1;
		if($preventivo->costo_trasp1_attivo=='N'){
			$output->attiva_secondo_costo_trasp=0;
			$output->secondo_costo_tr_class='disabled';
		}else{
			$output->attiva_secondo_costo_trasp=1;
			$output->secondo_costo_tr_class='enabled';

		}

		$query = $this->db->get_where('classi_iva',array('id_classe_iva'=>$preventivo->id_classe_iva_cost_trasp));
		foreach ($query->result() as $classe);
		$output->imposte=$classe->nome_classe_iva;
		$output->costo_trasp_classe_iva=$classe->valore_classe;

		if($preventivo->costo_trasp1_attivo=='S'){
			$query = $this->db->get_where('classi_iva',array('id_classe_iva'=>$preventivo->id_classe_iva_cost_trasp1));
			foreach ($query->result() as $classe);
			$imposte1=$classe->nome_classe_iva;
			$output->costo_trasp_classe_iva1=$classe->valore_classe;

			$output->imposte1= "<strong>Costo trasporto:</strong> ".$preventivo->costo_trasp1." &#8364; - <small> ".$imposte1."</small>";
		}else{
			$output->imposte1="&nbsp;";
			$output->costo_trasp_classe_iva1='0';
		}
		if($cliente->lingua=='ITA'){
		$this->load->view('/views_stampa/stampa_preventivi_view.php',$output);
		}
		if($cliente->lingua=='ENG'){
		$this->load->view('/views_stampa/stampa_preventivi_view_eng.php',$output);
		}
		if($cliente->lingua=='FRA'){
		$this->load->view('/views_stampa/stampa_preventivi_view_fra.php',$output);
		}
	}


	function nuova_revisione(){
		date_default_timezone_set('Europe/Rome');
		$quiri = $this->db->get_where('preventivi',array('id_preventivo'=>$this->uri->segment(3)));
		foreach ($quiri->result() as $asd);
		$num_revisione=$this->uri->segment(4)+1;
		$query = $this->db->get_where('preventivi_commesse',array('id_preventivo'=>$this->uri->segment(3),'num_revisione'=>$this->uri->segment(4)));
		foreach ($query->result() as $row){
			$this->popola_preventivo($this->uri->segment(3),$num_revisione,$row->commessa);
			$this->db->where('commessa', $row->commessa);
			$this->db->update('lavori', array('offerta_n'=> $asd->num_preventivo."/".$num_revisione,'offerta_data'=>date('Y-m-d')));

		}
		$query = $this->db->get_where('righe_preventivi',array('id_preventivo'=>$this->uri->segment(3),'num_revisione'=>$this->uri->segment(4),'riga_aggiunta'=>'S'));
		foreach ($query->result() as $row){
			$nuovaRigaPrev=array(
					'id_preventivo' => $row->id_preventivo,
					'num_revisione' => $row->num_revisione+1,
					'commessa' => $row->commessa,
					'descrizione'=>$row->descrizione,
					'quantita'=>$row->quantita,
					'prezzo'=>$row->prezzo,
					'sconto'=>$row->sconto,
					'id_classe_iva'=>$row->id_classe_iva,
					'riga_aggiunta'=>$row->riga_aggiunta
				);
				$this->db->insert('righe_preventivi',$nuovaRigaPrev);
		}
		$this->db->where('id_preventivo', $this->uri->segment(3));
		$this->db->update('preventivi', array('ultimo_num_revisione'=> $num_revisione));


		redirect('/preventivi/ricarica_prev/'.$this->uri->segment(3).'/'.$num_revisione, 'refresh');
	}

	function conferma_revisione(){
		$this->db->where('id_preventivo', $this->uri->segment(3));
		$this->db->update('preventivi', array('num_revisione_confermata'=> $this->uri->segment(4)));

		$query = $this->db->get_where('preventivi',array('id_preventivo'=>$this->uri->segment(3)));
		foreach ($query->result() as $prev);
		$query = $this->db->get_where('preventivi_commesse',array('id_preventivo'=>$this->uri->segment(3),'num_revisione'=>$this->uri->segment(4)));
		foreach ($query->result() as $row){
			$this->db->where('commessa', $row->commessa);
			$this->db->update('lavori', array('offerta_n'=> $prev->num_preventivo."/".$row->num_revisione,'offerta_data'=>$row->data_offerta));
		}
		redirect('/preventivi/ricarica_prev/'.$this->uri->segment(3).'/'.$this->uri->segment(4), 'refresh');

	}


	function popola_preventivo($id_preventivo, $revisione,$commessaArg){
				date_default_timezone_set('Europe/Rome');

		$query = $this->db->get_where('lavori',array('commessa'=>$commessaArg));
		foreach ($query->result() as $commessa);
		$query = $this->db->get_where('clienti',array('id_cliente'=>$commessa->cliente));
		foreach ($query->result() as $cliente);
		//popolo il campo stampo_nome_peso in ogni caso, ma se è STA non verrà visualizzato a lato preventivo!
		if($cliente->lingua=='ITA'){
			$stampo_tradottto="N° Disegno: ";
		}
		if($cliente->lingua=='ENG'){
			$stampo_tradottto="Drawing number: ";
		}
		if($cliente->lingua=='FRA'){
			$stampo_tradottto="Dessin num: ";
		}

		if($commessa->n_stampo!=NULL){
				$n_stampo=$stampo_tradottto.$commessa->n_stampo;}
		else{ $n_stampo='';}
		if($commessa->nome_sapone!=NULL){
				$nome_sapone="     -    '".$commessa->nome_sapone."'";}
		else{ $nome_sapone='';}
		if($commessa->peso!=NULL){
				$peso="    -    ".$commessa->peso." gr.";}
		else{ $peso='';}


					$nuovoPrevComm=array(
					'id_preventivo' => $id_preventivo,
					'num_revisione' => $revisione,
					'commessa' => $commessa->commessa,
					'data_offerta' => date('Y-m-d'),
					'stampo_nome_peso' => $n_stampo.$nome_sapone.$peso

				);
				$this->db->insert('preventivi_commesse',$nuovoPrevComm);
			if($commessa->tipo_lavoro=='STA'){
				$query = $this->db->get_where('materiali',array('id_materiale'=>$commessa->id_materiale_matrice));
				foreach ($query->result() as $mat_matrice);
				if($commessa->id_trattam_matrice!=NULL){
									$query = $this->db->get_where('trattamenti',array('id_trattamento'=>$commessa->id_trattam_matrice));
									foreach ($query->result() as $trattam);
									if($cliente->lingua=='ITA'){
										$trattamento=" con trattamento ".$trattam->nome_trattamento;
									}
									if($cliente->lingua=='ENG'){
										$trattamento=" with ".$trattam->nome_trattamento." coating";
									}
									if($cliente->lingua=='FRA'){
										$trattamento=" avec ".$trattam->nome_trattamento;
									}
								}else{
									$trattamento="";
								}
				$query = $this->db->get_where('listini_stampi',array('id_cliente'=>$commessa->cliente,
									'n_cavita'=>$commessa->n_cavita,
									'id_materiale'=>$commessa->id_materiale_matrice,
									'tipo_stampo'=>$commessa->tipo_stampo,
									'stampatrice'=>$commessa->stampatrice,
									'id_trattamento'=>$commessa->id_trattam_matrice));
								foreach ($query->result() as $prezzo);
								if($query->num_rows()==0)
								{
									$prezzo=new stdClass();
									$prezzo->prezzo_listino_stampo='0.00';
								}
				if($cliente->lingua=='ITA'){
				$descrizione="Stampo numero ".$commessa->n_stampo." '".$commessa->nome_sapone."' da ".$commessa->peso." gr per ".$commessa->stampatrice." a ".$commessa->n_cavita." cavita' in ".$mat_matrice->nome_materiale.$trattamento." compreso di incisioni e accessori standard";
				}
				if($cliente->lingua=='ENG'){
				$descrizione="Mould number ".$commessa->n_stampo." '".$commessa->nome_sapone."', ".$commessa->peso." gr for  ".$commessa->stampatrice." with ".$commessa->n_cavita." cavities in ".$mat_matrice->nome_materiale.$trattamento." including engravings and standard accessories";
				}
				if($cliente->lingua=='FRA'){
				$descrizione="Moule num ".$commessa->n_stampo." '".$commessa->nome_sapone."', ".$commessa->peso." gr pour  ".$commessa->stampatrice." avec ".$commessa->n_cavita." cavités en ".$mat_matrice->nome_materiale.$trattamento." complet des gravures et accessoires standard ";
				}
				$nuovaRigaPrev=array(
					'id_preventivo' => $id_preventivo,
					'num_revisione' => $revisione,
					'commessa' => $commessa->commessa,
					'descrizione' => $descrizione,
					'quantita' => 1,
					'prezzo'=>$prezzo->prezzo_listino_stampo,
					'sconto'=>0,
					'id_classe_iva'=>$cliente->id_classe_iva

				);
				$this->db->insert('righe_preventivi',$nuovaRigaPrev);
			}else{
					$this->db->select('*');
					$this->db->from('bolle_prod');
					$this->db->join('componenti', 'bolle_prod.id_componente = componenti.id_componente');
					$this->db->where('commessa', $commessaArg);
					$this->db->order_by("nome_componente", "asc");
					$prendi_compo = $this->db->get();
					if($prendi_compo->num_rows()>0){
						foreach ($prendi_compo->result() as $componente)
						{
							$query = $this->db->get_where('materiali',array('id_materiale'=>$componente->id_materiale));
							foreach ($query->result() as $materialo);
							$query = $this->db->get_where('categorie_componenti',array('id_categoria'=>$componente->id_categoria));
							foreach ($query->result() as $categoria);

							if($componente->escludi_ddt=='N'){

								if($componente->id_trattamento!=NULL){
									$query = $this->db->get_where('trattamenti',array('id_trattamento'=>$componente->id_trattamento));
									foreach ($query->result() as $trattam);
									if($cliente->lingua=='ITA'){
										$trattamento=" con trattamento ".$trattam->nome_trattamento;
									}
									if($cliente->lingua=='FRA'){
										$trattamento=" avec ".$trattam->nome_trattamento_fra;
									}
									if($cliente->lingua=='ENG'){
										$trattamento=" with ".$trattam->nome_trattamento_eng;
									}
								}else{
									$trattamento="";
								}
								$query = $this->db->get_where('listino_componenti',array('id_cliente'=>$commessa->cliente,
									'id_categoria'=>$componente->id_categoria,
									'id_materiale'=>$componente->id_materiale,
									'nome_stamp'=>$commessa->stampatrice,
									'id_trattamento'=>$componente->id_trattamento));
								foreach ($query->result() as $prezzo);
								if($query->num_rows()==0)
									{$prezzo = new stdClass();
										$prezzo->prezzo_componente='0.00';}
								if($cliente->lingua=='ITA'){
									$descrizione=$categoria->nome_categoria." in ".$materialo->nome_materiale.$trattamento;
								}
								if($cliente->lingua=='FRA'){
									$descrizione=$categoria->nome_categoria_fra." en ".$materialo->nome_materiale.$trattamento;
								}
								if($cliente->lingua=='ENG'){
									$descrizione=$categoria->nome_categoria_eng." in ".$materialo->nome_materiale.$trattamento;
								}
							 	$nuovaRigaPrev=array(
									'id_preventivo' => $id_preventivo,
									'num_revisione' => $revisione,
									'commessa' => $commessa->commessa,
									'descrizione' => $descrizione,
									'quantita' => $componente->quantita,
									'sconto' => '0',
									'id_classe_iva'=>$cliente->id_classe_iva,
									'prezzo'=>$prezzo->prezzo_componente
								);
								$this->db->insert('righe_preventivi',$nuovaRigaPrev);

							}

						}
					}
				}
	}

function tabella()
	{
			$crud = new grocery_CRUD();
			$crud->set_theme('datatables');
			$crud->set_table('preventivi');
			$crud->display_as('riferimenti_prev',"All'attenzione di:");
			$crud->display_as('id_dest','Destinatario');
			$crud->display_as('id_cliente','Cliente');
			$crud->add_action('Visualizza', '', '','ui-icon-plus',array($this,'apri_preventivo'));
			$crud->set_relation('id_dest','clienti','nome_cliente_documenti');
			$crud->set_relation('id_cliente','clienti','nome_cliente_documenti');
			$crud->columns('num_preventivo','data_creazione_prev','id_cliente','ultimo_num_revisione','num_revisione_confermata');
			$crud->unset_add();
			$crud->unset_edit();
			$crud->unset_delete();
			$crud->callback_after_delete(array($this,'after_delete_preventivo'));
			$crud->order_by('data_creazione_prev desc, num_preventivo','desc');

			$output = $crud->render();
			$output->header="PREVENTIVI";
			$output->footer="";

			$output->menu=$this->build_menu();
			$this->load->view('omis_view.php',$output);
	}

	function elimina_preventivo(){

		$this->db->delete('preventivi', array('id_preventivo'=> $this->uri->segment(3)));
		$this->after_delete_preventivo($this->uri->segment(3));
		redirect('/omis/lavori/incorso', 'refresh');
	}


	function after_delete_preventivo($primary_key)
	{
		$query = $this->db->get_where('preventivi_commesse',array('id_preventivo'=>$primary_key));
		foreach ($query->result() as $prev){
			$this->db->where('commessa', $prev->commessa);
			$this->db->update('lavori', array('offerta_n'=> NULL,'offerta_data'=>NULL));
		}
    	$this->db->delete('preventivi_commesse', array('id_preventivo'=> $primary_key));
		$this->db->delete('righe_preventivi', array('id_preventivo'=> $primary_key));
	}

	function apri_preventivo($primary_key , $row)
	{
		$query = $this->db->get_where('preventivi',array('id_preventivo'=>$primary_key));
		foreach ($query->result() as $prev);
		if($prev->num_revisione_confermata==NULL) $revisione=$prev->ultimo_num_revisione;
		else $revisione=$prev->num_revisione_confermata;
		return site_url('preventivi/ricarica_prev/'.$primary_key.'/'.$revisione);
	}


	function raccogli_tempi($id_macchina){


		switch($id_macchina){

			case '1':
				$macchina="M1015S"; //M1010?
			break;
			case '2':
				$macchina="Yep3"; //M1015?
			break;
			case '3':
				$macchina="EC1220-PC";
			break;
		}
		$i=0;
		// if(!is_dir('\\\\'.$macchina.'\\WorkingTimes')){
		// 	return "MACCHINA SPENTA";
		// }

		if ($handle = opendir('\\\\'.$macchina.'\\WorkingTimes')) {


			date_default_timezone_set('Europe/Rome');
			@list($month,$day,$year) = explode("-",date('m-d-Y'));
			$data_odierna = mktime (0,0,0,(int)$month , (int)$day , (int)$year);

			$dataUltimaImp = $this->db->get('importazione_tempi');// leggo dal DB quando è stata l'ultima volta che ho importato i tempi
			foreach ($dataUltimaImp->result() as $row);
			switch($id_macchina){

				case '1':
				@list($year,$month,$day) = explode("-",$row->data_ultima_importazione_1);
				break;
				case '2':
				@list($year,$month,$day) = explode("-",$row->data_ultima_importazione_2);
				break;
				case '3':
				@list($year,$month,$day) = explode("-",$row->data_ultima_importazione_3);
				break;
			}
			$ultima_importazione = mktime (0,0,0,(int)$month , (int)$day , (int)$year);

			$filename = readdir($handle);//scarto i primi due file nasosti della directory
			$filename = readdir($handle);


		    while (false !== ($filename = readdir($handle))) {
				$dataFile="20".substr($filename,0,2)."-".substr($filename,2,2)."-".substr($filename,4,2);
				@list($year,$month,$day) = explode("-",$dataFile);
				$data_file = mktime (0,0,0,(int)$month , (int)$day , (int)$year);

				if(($data_file>=$ultima_importazione)&&($data_file<$data_odierna)){ //controlla che il
													//file sia generato dopo l'ultimo checkpoint(controllo che data sia > di data ultima importaz.)
					$file=fopen('\\\\'.$macchina.'\\WorkingTimes\\'.$filename,"r");
					fgets($file); //prime due righe contengono codici che non interessano
					fgets($file);
					$SecTot=0;


					while(!feof($file)){ //scorro riga per riga fino alla fine

						$riga=fgets($file);
						$parametri=explode(",",$riga);

						if(($riga!="")&&($parametri[2]=='1'||$parametri[2]=='2')){//alcuni file hanno righe vuote

							@list($ore,$minuti,$secondi)=explode(".",$parametri[0]);
							$ore=substr($ore,1,2);
							$SecInizio=intval($ore)*3600+intval($minuti)*60+intval($secondi);

							do{
								$rigatmp=$riga;
								$riga=fgets($file);
								$parametri=explode(",",$riga);
								if(feof($file)){
									$parametri=explode(",",$rigatmp);//genio
									$parametri[2]='4';}//se un file finisce con i due vuol dire che finisce a cavallo tra due files durante la mezzanotte

							}while($parametri[2]=='2'); //appena becco un 3 o un 4 mi fermo e torno su. Prima di tornare su mi scrivo i tempi

							@list($ore,$minuti,$secondi)=explode(".",$parametri[0]);
							$ore=substr($ore,1,2);
							$SecFine=intval($ore)*3600+intval($minuti)*60+intval($secondi);

							$SecTot=$SecTot+($SecFine-$SecInizio);

							if($parametri[2]=='4'||$parametri[2]=='3'){
								$righe[$i]=new stdClass();
								(isset($parametri[4]))?$righe[$i]->commessa=substr($parametri[4],1,strlen($parametri[4])-2):$righe[$i]->commessa="";
								$righe[$i]->data=$dataFile;
								$righe[$i]->SecTot=$SecTot;

								$i++;
								$SecTot=0;

							}
						}
					}


					fclose($file);
		       	}
		    }
		    closedir($handle);
		//raggruppo i tempi per commessa e data-----------------------------------------
			if(isset($righe)){	// faccio il secondo raggruppamento solo se ho trovato delle righe da importare
				$commCorrente=$righe[0]->commessa;
				$dataCorrente=$righe[0]->data;
				$i=0;
				$tempoTot=0;
				$tempoTotGiorno=0;
				$lavorazioni = array();
				foreach($righe as $row){
					if(($row->commessa!=$commCorrente)||($row->data!=$dataCorrente)){
						$lavorazioni[$i]=new stdClass();
					//salvo totale
						$lavorazioni[$i]->data=$dataCorrente;
						$lavorazioni[$i]->commessa=$commCorrente;

						$lavorazioni[$i]->tempo_macchina=round(intval($tempoTot)/3600 , 2);
					/*if($row->data!=$dataCorrente){				//salvo totale giornaliero DA DECOMMENTARE PER DEBUGGING
						$i++;
						$lavorazioni[$i]->data=$dataCorrente;
						$lavorazioni[$i]->commessa="------- TOTALE GIORNALIERO: ";
						$oreTot=floor(intval($tempoTotGiorno)/3600);
						$minutiTot=floor((intval($tempoTotGiorno)-($oreTot*3600))/60);
						if(intval($minutiTot)<10){
							$minutiTot="0".$minutiTot;
						}
						$lavorazioni[$i]->tempo_macchina =$oreTot.".".$minutiTot;
						$tempoTotGiorno=0;
					}*/

						$commCorrente=$row->commessa;
						$dataCorrente=$row->data;
						$tempoTot=0;
						$i++;
					}
					$tempoTot+=$row->SecTot;
					$tempoTotGiorno+=$row->SecTot;

				}
				$lavorazioni[$i]=new stdClass();
				$lavorazioni[$i]->data=$dataCorrente;
				$lavorazioni[$i]->commessa=$commCorrente;

				$lavorazioni[$i]->tempo_macchina=round(intval($tempoTot)/3600 , 2);
			}else{ $lavorazioni=false;}
		}
		else{
			$lavorazioni="DIRECTORY NON DISPONIBILE";
		}


		return $lavorazioni;
	}


	function prepara_import(){

			$output = new stdClass();
			$output->header="IMPORTA TEMPI DA REMOTO";
				switch($this->uri->segment(3)){
					case '1':
						$macchina="M1015S"; //M1010?
						$operat="NICO";
					break;
					case '2':
					$macchina="Yep3"; //M1015?
					$operat="NICO";
					break;
					case '3':
					$macchina="EC1220";
					$operat="AGO";
					break;
				}
				$output->output="&nbsp;&nbsp;&nbsp;&nbsp;LAVORAZIONI MACCHINA ".$macchina.":";

				$this->load->library('table');
				$tmpl = array (
				                    'table_open'          => '<table align="center" style="text-align: center; font-size: 16px;" border="1" cellpadding="8"  cellspacing="0">',

				                    'heading_row_start'   => '<tr>',
				                    'heading_row_end'     => '</tr>',
				                    'heading_cell_start'  => '<th><font color="blue">',
				                    'heading_cell_end'    => '</font></th>',

				                    'row_start'           => '<tr>',
				                    'row_end'             => '</tr>',
				                    'cell_start'          => '<td>',
				                    'cell_end'            => '</td>',

				                    'row_alt_start'       => '<tr>',
				                    'row_alt_end'         => '</tr>',
				                    'cell_alt_start'      => '<td>',
				                    'cell_alt_end'        => '</td>',

				                    'table_close'         => '</table>'
				              );

				$this->table->set_template($tmpl);
				$this->table->set_heading(' COMMESSA ', ' DATA ', " TEMPO MACCHINA", "OPE", "MACCHINA");
				$i=0;

			//se $this->uri->segment(3) vale 1 -> agostino  vale 2 ->nicola   vale 3 -> nicola su EC68

				$righe=$this->raccogli_tempi($this->uri->segment(3));
				if(is_string($righe)){
							$output->footer=$righe;
				}else{
						if($righe!=false){
							foreach($righe as $row){
								$this->table->add_row($row->commessa, $row->data, $row->tempo_macchina,$operat,$macchina);
							}
							$i++;
						}
						if($i>0){ // se almeno una delle 3 raccogli_tempi() torna qualche riga
							$output->footer=$this->table->generate();
							$output->footer.="<br> <div align='center'><a href='/index.php/preventivi/importa_tempi/".$this->uri->segment(3)."'>
									<input  type='button' style='font-size:25px;' value='IMPORTA TEMPI'></a></div>";
						}
						else{
								$output->footer="<div align='center'>--------Non esistono nuove lavorazioni da importare--------</div>";
							}
				}
			$output->menu=$this->build_menu();

			$this->load->view('importa_tempi.php',$output);
	}


	function importa_tempi(){
		$output= new stdClass();
		$output->header="IMPORTA TEMPI DA REMOTO";
		$output->output="LAVORAZIONI INSERITE NEL DATABASE:";

		switch($this->uri->segment(3)){

			case '1':
				$macchina="M1015S"; //si riferisce alle M1010?
				$operat="NICO";
				$id_oper="8";
				$id_centro_costo="7";
			break;
			case '2':
				$macchina="Yep3";
				$operat="NICO";
				$id_oper="8";
				$id_centro_costo="6";

			break;
			case '3':
				$macchina="EC1220-PC";
				$operat="AGO";
				$id_oper="3";
				$id_centro_costo="3";

			break;


		}
		$output->footer="";


		$righe=$this->raccogli_tempi($this->uri->segment(3));
		foreach($righe as $row){


			if($row->tempo_macchina>0.00){
				if($row->commessa==""){
					$this->db->order_by("data_inser_comm", "desc");
					$query = $this->db->get_where('lavori',array('id_produttore'=>92),1,0);
					foreach ($query->result() as $lavoro);
					$query = $this->db->get_where('fasi_commessa',array('commessa'=>$lavoro->commessa, 'nome_fase'=>'TEMPO MACCHINA COMMESSA'));
				}else{
									$query = $this->db->get_where('fasi_commessa',array('commessa'=>$row->commessa, 'nome_fase'=>'TEMPO MACCHINA COMMESSA'));

				}
				foreach ($query->result() as $fase);
							if((intval($row->commessa)==0)||($row->commessa=="")){//se commessa è un nome attribuisco il tuo tempo macchina all'ultima commessa OMIS INTERNA
								$this->db->order_by("data_inser_comm", "desc");
								$query = $this->db->get_where('lavori',array('id_produttore'=>92),1,0);
								foreach ($query->result() as $lavoro);
								$row->commessa=$lavoro->commessa;
							}
				$nuovaLavorazione=array(
					'id_fase_commessa' => $fase->id_fase_commessa,
					't_macchina' => $row->tempo_macchina,
					'data_lavorazione' => $row->data,
					'id_operatore' => $id_oper,  //da passare per parametro il numero operatore a raccogli_tempi().
					'id_centro_costo' => $id_centro_costo, //da sistemare anche questo
					'commessa' => $row->commessa
						);
					$this->db->insert('lavorazioni',$nuovaLavorazione);
					$output->footer.="Commessa: ".$nuovaLavorazione['commessa'].
						" Data: ".$nuovaLavorazione['data_lavorazione']." Tempo macchina: ".$nuovaLavorazione['t_macchina']."Id lav:".$this->db->insert_id()."<br>";
			}


		}
		$data = array('data_ultima_importazione_'.$this->uri->segment(3) => date('Y-m-d'));
		$this->db->update('importazione_tempi', $data);
		$output->menu=$this->build_menu();
		$this->load->view('importa_tempi.php',$output);
	}
}
