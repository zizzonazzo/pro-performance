<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/////////////////////////////////////////////////////////////////count($ci->uri->segments)

class Clienti extends CI_Controller {

	function __construct()
	{
		parent::__construct();

		/* Standard Libraries */
		$this->load->database();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('date');
		$this->load->helper('security');
		$this->load->library('session');


		/* ------------------ */

		$this->load->library('grocery_CRUD');
		if(!$this->session->userdata('logged_in'))
		redirect('/login/show', 'refresh');


	}


	function elencovecchio()
	{
		$crud = new grocery_CRUD();
		$crud->set_theme('datatables');
		$crud->set_table('lavori');
		$where = "(stato='E' OR stato='EV') AND (id_produttore='".$this->session->userdata('id_produttore')."' OR cliente='".$this->session->userdata('id_cliente')."') AND tipo_lavoro <> 'VAR' AND tipo_lavoro <> 'ACC' AND tipo_lavoro <> 'MOD'";
		$crud->where($where);

		$nazionalita = $this->db->get_where('clienti', array('id_cliente'=> $this->session->userdata('id_cliente')));
		foreach ($nazionalita->result() as $row);
		switch($row->nazione){
			case "Francia":
			$legenda="
			<br> <br> Légende:<br><br>
			STA = Moule <br>
			ESP = Goujons <br>
			RO/RV/ROV  = Rouleaux <br>
			";
			$lav_eseguiti="TRAVEAUX RÉALISÉS POUR";
			$n_stampo="N° Moule";
			$vs_commessa="Votre Commande";
			$rif_cliente="Votre Réf";
			$nome_sapone="Nom Savon";
			$peso="Poids (gr)";
			$stampatrice="Mouleuse";
			$n_cavita="N° Cavité";
			$tipo_lavoro="Type";
			$ddt_data="Date DDT";
			$ddt_n="N° DDT";
			$disegno="DESSIN";
			$nazio_op="FRA";
			break;
			case "Italia":
			$legenda="
			<br> <br> Legenda:<br><br>
			STA = Stampo <br>
			ESP = Tasselli <br>
			RO/RV/ROV  = Rulli <br>
			";
			$lav_eseguiti="LAVORI ESEGUITI PER";
			$n_stampo="N° Stampo";
			$vs_commessa='Vs. Commessa';
			$rif_cliente="Rif Cliente";
			$nome_sapone="Nome Sapone";
			$peso="Peso (gr)";
			$stampatrice="Stampatrice";
			$n_cavita="N° Cavita'";
			$tipo_lavoro="Tipo";
			$ddt_data='Data invio DDT';
			$ddt_n="N° DDT";
			$disegno="DISEGNO";
			$nazio_op="ITA";

			break;
		}




		$crud->display_as('peso',$peso);
		$crud->display_as('comm_cliente', $vs_commessa);
		$crud->display_as('ddt_data', $ddt_data);
		$crud->display_as('n_stampo', $n_stampo);
		$crud->display_as('rif_cliente', $rif_cliente);
		$crud->display_as('nome_sapone', $nome_sapone);
		$crud->display_as('peso', $peso);
		$crud->display_as('stampatrice', $stampatrice);
		$crud->display_as('n_cavita', $n_cavita);
		$crud->display_as('tipo_lavoro', $tipo_lavoro);
		$crud->display_as('ddt_n', $ddt_n);
		$crud->columns('n_stampo','comm_cliente','rif_cliente','nome_sapone','peso','stampatrice','n_cavita','tipo_lavoro','ddt_n','ddt_data');
		$crud->add_action($disegno, '', '','ui-icon-plus',array($this,'apri_link'));

		$crud->unset_delete();
		$crud->unset_add();
		$crud->unset_edit();


		$output = $crud->render();
		$output->nome_op=$this->session->userdata('nome_operatore');
		$output->legenda=$legenda;
		$output->lav_eseguiti=$lav_eseguiti;
		$this->load->view('cliente_view.php',$output);
	}

	function apri_link($primary_key , $row)
	{
		// return site_url('smb://srv02/officina2/Immagini tech omis/albatros/pdf/ST455-S-REV-2.pdf');
		$lavoro = $this->db->get_where('clienti', array('id_cliente'=> $row->cliente));
		foreach ($lavoro->result() as $cliente);
		if($row->dis_sapone!=NULL)
		{
			$nomefile=$row->dis_sapone;
		}else{
			$nomefile=$row->n_stampo;
		}
		if(substr(current_url(),7,1)=='1')
		return "ftp://192.168.2.231:2222/".$cliente->ragione_soc."/PDF/".$nomefile.".pdf";
		else
		return "ftp://78.134.1.160/".$cliente->ragione_soc."/PDF/".$nomefile.".pdf";

		//return "file:///I:/disegni_saponi/".$cliente->ragione_soc."/PDF/";
		//return "file:///C:/apri_server.bat";
	}



	function elenco()
	{

		date_default_timezone_set('Europe/Rome');

		$this->load->library('table');
		$tmpl = array (
		'table_open'          => '<table align="center" width="100%"  cellpadding="0"  cellspacing="0" class="display" id="groceryCrudTable">',

		'heading_row_start'   => '<tr>',
		'heading_row_end'     => '</tr>',
		'heading_cell_start'  => '<th class="ui-state-default">',
		'heading_cell_end'    => '</font></th>',

		'row_start'           => '<tr>',
		'row_end'             => '</tr>',
		'cell_start'          => '<td>',
		'cell_end'            => '</td>',

		'row_alt_start'       => '<tr>',
		'row_alt_end'         => '</tr>',
		'cell_alt_start'      => '<td>',
		'cell_alt_end'        => '</td>',

		'table_close'         => '</table>'
	);
	$this->table->set_template($tmpl);
	$nazionalita = $this->db->get_where('clienti', array('id_cliente'=> $this->session->userdata('id_cliente')));
	foreach ($nazionalita->result() as $row);
	switch($row->nazione){
		case "Francia":
		$legenda="
		<br> <br> Légende:<br><br>
		STA = Moule <br>
		ESP = Goujons <br>
		RO/RV/ROV  = Rouleaux <br>
		";
		$lav_eseguiti="TRAVEAUX RÉALISÉS POUR";
		$disegno="DESSIN";
		$this->table->set_heading("N° Moule","Votre Commande","Votre Réf","Nom Savon","Poids (gr)","Mouleuse","N° Cavité","Type","Date DDT","N° DDT","Actions");
		break;
		case "Italia":
		$legenda="
		<br> <br> Legenda:<br><br>
		STA = Stampo <br>
		ESP = Tasselli <br>
		RO/RV/ROV  = Rulli <br>
		";
		$lav_eseguiti="LAVORI ESEGUITI PER";
		$this->table->set_heading('N° Stampo','Vs. Commessa',"Rif Cliente","Nome Sapone","Peso (gr)","Stampatrice","N° Cavita'","Tipo",'Data invio DDT',"N° DDT","Azioni");

		$disegno="DISEGNO";
		$nazio_op="ITA";

		break;
	}


	$query=$this->db->query("SELECT n_stampo ,comm_cliente, rif_cliente, nome_sapone, peso, stampatrice, n_cavita, tipo_lavoro, ddt_n, ddt_data, cliente, dis_sapone
		FROM lavori
		WHERE ((stato='E' OR stato='EV') AND (id_produttore='".$this->session->userdata('id_produttore')."' OR cliente='".$this->session->userdata('id_cliente')."') AND tipo_lavoro <> 'VAR' AND tipo_lavoro <> 'ACC' AND tipo_lavoro <> 'MOD')
		UNION DISTINCT
		SELECT n_stampo ,comm_cliente, rif_cliente, nome_sapone, peso, stampatrice, n_cavita, tipo_lavoro, ddt_n, ddt_data, cliente, dis_sapone
		FROM storico_saponi
		WHERE tipo_storico ='C' AND cliente ='".$this->session->userdata('id_cliente')."'");

		foreach ($query->result() as $lav){
			$lavoro = $this->db->get_where('clienti', array('id_cliente'=> $lav->cliente));
			foreach ($lavoro->result() as $cliente);
			if($lav->dis_sapone!=NULL) {

				$nomefile=$lav->dis_sapone;
			}else{
				$nomefile=$lav->n_stampo;
			}
			if(substr(current_url(),7,1)=='1')
			$link="ftp://192.168.2.233/".str_replace(" ","%20",$cliente->ragione_soc)."/PDF/".$nomefile.".pdf";
			else
			$link= "ftp://78.134.1.160/".str_replace(" ","%20",$cliente->ragione_soc)."/PDF/".$nomefile.".pdf";
			$azioni='<ul id="nav"><li id="azione"><a href="#" class="bottone">'.$disegno.'</a>
			<ul>
			<li class="tasto"><a href='.$link.' role="button">'.$disegno.'</a></li>
			</ul>
			</li></ul>';
			if($lav->ddt_data!=NULL){
				@list($year,$month,$day) = explode("-",$lav->ddt_data);
				$lav->ddt_data=date ("d M Y",mktime (0,0,0,(int)$month , (int)$day , (int)$year));
			}
			$this->table->add_row($lav->n_stampo ,$lav->comm_cliente, $lav->rif_cliente, $lav->nome_sapone, $lav->peso, $lav->stampatrice, $lav->n_cavita, $lav->tipo_lavoro, $lav->ddt_n, $lav->ddt_data, $azioni);
		}

		$output->output=$this->table->generate();


		$output->nome_op=$this->session->userdata('nome_operatore');
		$output->legenda=$legenda;
		$output->lav_eseguiti=$lav_eseguiti;
		if(substr(current_url(),7,1)=='1')
		$this->load->view('cliente_view_locale.php',$output);
		else
		$this->load->view('cliente_view.php',$output);

	}













}
