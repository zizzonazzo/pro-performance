<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/////////////////////////////////////////////////////////////////count($ci->uri->segments)

class Omisdue extends CI_Controller {

	function __construct()
	{
		parent::__construct();

		/* Standard Libraries */
		$this->load->database();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('date');
		$this->load->helper('security');
		$this->load->library('session');


		/* ------------------ */

		$this->load->library('grocery_CRUD');
		if(!$this->session->userdata('logged_in'))
			redirect('/login/show', 'refresh');


	}

	function storico(){

				date_default_timezone_set('Europe/Rome');
				@list($month,$day,$year) = explode("-",date('m-d-Y'));
				$oggi = mktime (0,0,0,(int)$month , (int)$day , (int)$year);
				$this->load->library('table');
				$tmpl = array (
				                    'table_open'          => '<table align="center" width="100%" style="text-align: center; font-size: 13px;"  cellpadding="4"  cellspacing="0" class="groceryCrudTable">',

				                    'heading_row_start'   => '<tr>',
				                    'heading_row_end'     => '</tr>',
				                    'heading_cell_start'  => '<th><font color="blue">',
				                    'heading_cell_end'    => '</font></th>',

				                    'row_start'           => '<tr>',
				                    'row_end'             => '</tr>',
				                    'cell_start'          => '<td>',
				                    'cell_end'            => '</td>',

				                    'row_alt_start'       => '<tr>',
				                    'row_alt_end'         => '</tr>',
				                    'cell_alt_start'      => '<td>',
				                    'cell_alt_end'        => '</td>',

				                    'table_close'         => '</table>'
				              );
					$this->table->set_template($tmpl);

					$this->table->set_heading('Progressivo','Num Stampo', 'Rif Cliente',  'Tipo' ,'Cliente',
						'Nome Sapone','Forma','Peso(gr)','F/B','Tipo Lavoro','N Matrici','Cavita','Materiale','Stampatrice','Comm Cliente','Commessa','Note','Lungh.','Largh.','Altezza','Diametro');

				$this->db->select('*');
				$this->db->from('storico_saponi');
				$this->db->join('clienti', 'storico_saponi.cliente = clienti.id_cliente');

				$query = $this->db->get();
						foreach ($query->result() as $commessa){

						$this->table->add_row($commessa->num_progressivo,$commessa->n_stampo,$commessa->rif_cliente,$commessa->tipo_storico,$commessa->ragione_soc,
							$commessa->nome_sapone,$commessa->forma,$commessa->peso,$commessa->FB,$commessa->tipo_lavoro,$commessa->n_matrici,$commessa->n_cavita,$commessa->mat_matrice
							,$commessa->stampatrice,$commessa->comm_cliente,$commessa->commessa,$commessa->note,$commessa->lunghezza_sapone,$commessa->larghezza_sapone,$commessa->altezza_sapone,$commessa->diam_spessore_filiera);
					}
					$output->output=$this->table->generate();
					$output->nome_op=$this->session->userdata('nome_operatore');
					$this->load->view('storico_view.php',$output);
		}


		function ajax_call(){
			$this->setHeader=TRUE;
			$crud = new grocery_CRUD();
			$crud->set_theme('datatables');
			$crud->set_table('lavori');
			$this->db->where('stato','C');
		  	$crud->columns('commessa','comm_cliente','cliente','n_stampo','dis_sapone','nome_sapone',
			 'stampatrice','peso','tipo_lavoro','ddt_n','ddt_data','stato_comm');

			$output = $crud->render_ajax();

			echo $output->output;
		}

}
