<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/////////////////////////////////////////////////////////////////count($ci->uri->segments)

class Magazzino extends CI_Controller {

	function __construct()
	{
		parent::__construct();

		/* Standard Libraries */
		$this->load->database();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('date');
		$this->load->helper('security');
		$this->load->library('session');


		/* ------------------ */

		$this->load->library('grocery_CRUD');
		if(!$this->session->userdata('logged_in'))
			redirect('/login/show', 'refresh');


	}

	function build_menu(){
		$vars=array();
		$vars['tipo_op']=$this->session->userdata('tipo_op');
		$vars['nome_op']=$this->session->userdata('nome_operatore');


		extract($vars);
			#region buffering...
			ob_start();

			include(getcwd().'/application/views/menu_builder.php');

			$buffer = ob_get_contents();
		@ob_end_clean();
		return $buffer;
	}

	function lista_ordini_accessori()
	{


				$crud = new grocery_CRUD();
				$crud->set_theme('datatables');
				$crud->set_table('ordini_accessori');
				$crud->unset_add();
				$crud->unset_edit();
				$crud->unset_delete();
				$crud->set_subject('Ordine Accessorio');
				$crud->set_relation('id_accessorio','anagrafica_accessori','nome_accessorio');

				$crud->display_as('id_accessorio','Nome Accessorio');

				$crud->callback_add_field('id_accessorio',array($this,'ListaAccessoriCodice'));
			    $crud->callback_edit_field('id_accessorio',array($this,'ListaAccessoriCodice'));
				$crud->unset_add_fields('ddt_evasione_ordine','data_evasione_ordine','stato_ordine');
				$crud->change_field_type('stato_ordine', 'hidden');
				$crud->change_field_type('id_ordine_fornitore', 'hidden',$this->uri->segment(3));
				$crud->columns('stato_ordine','ns_riferimento','id_accessorio','codice_accessorio_fornitore','quantita_ordinata','data_consegna_richiesta','ddt_evasione_ordine',
				'data_evasione_ordine');
			$crud->callback_column('codice_accessorio_fornitore',array($this,'codice_accessorio_column'));
					$crud->callback_before_update(array($this,'cambiaStatoOrdineSingoloAccessorio'));
				$crud->order_by('stato_ordine');
				$output = $crud->render();
				$output->header="ORDINI ACCESSORI";
				$output->menu=$this->build_menu();


				$this->load->view('omis_view.php',$output);
	}
	function lista_ordini_materiali()
	{


				$crud = new grocery_CRUD();
				$crud->set_theme('datatables');
				$crud->set_table('ordini_materiali');
				$crud->unset_add();
				$crud->unset_edit();
				$crud->unset_delete();

				$crud->display_as('codice_materiale_interno','Materiale');
				$crud->callback_column('codice_materiale_interno',array($this,'ListaMaterialiFornitoreColumn'));
				$crud->set_js('assets/themes/datatables/js/ordini_materiali.js');
				$crud->columns('stato_ordine','ns_riferimento','codice_materiale_interno','lunghezza_ordinata','misure_ritaglio_ordinato','kg_ordine','quantita_ordinata','data_consegna_richiesta','ddt_evasione_ordine','data_evasione_ordine');
				$crud->set_subject('Ordine Materiale');

				$crud->order_by('stato_ordine');

				$output = $crud->render();
				$output->header="ORDINI MATERIALI";
				$output->menu=$this->build_menu();


				$this->load->view('omis_view.php',$output);
	}
	function lista_ordini_semilavorati()
	{


				$crud = new grocery_CRUD();
				$crud->set_theme('datatables');
				$crud->unset_add();
				$crud->unset_edit();
				$crud->unset_delete();
				$crud->set_table('ordini_semilavorati');
				$crud->set_relation('id_semilav_ana','anagrafica_semilavorati','nome_semilavorato');
				$crud->display_as('id_semilav_ana','Semilavorato');
				$crud->change_field_type('id_ordine_fornitore', 'hidden',$this->uri->segment(3));
				$crud->unset_add_fields('ddt_evasione_ordine','data_evasione_ordine','id_fornitore',
					'stato_ordine','costo_semilav','ddt_evasione_ordine','data_evasione_ordine');
				$crud->unset_edit_fields('id_fornitore');
				$crud->change_field_type('stato_ordine', 'hidden');
				//$crud->callback_before_insert(array($this, 'before_insert_ordine_semilav'));
				$crud->unset_columns('id_ordine_fornitore','id_fornitore','note_semilav','costo_semilav');
				$crud->unset_texteditor('note_semilav');
				$crud->columns('stato_ordine','commessa','id_semilav_ana','disegno','quantita_semilav','data_consegna_richiesta','ddt_evasione_ordine','data_evasione_ordine');


				$crud->callback_add_field('id_semilav_ana',array($this,'ListaSemilavoratiFornitore'));
				$crud->callback_edit_field('id_semilav_ana',array($this,'ListaSemilavoratiFornitore'));
				$crud->callback_edit_field('commessa',array($this,'lista_commesse'));
				$crud->callback_add_field('commessa',array($this,'lista_commesse'));
				$crud->callback_before_update(array($this,'cambiaStatoOrdineSingoloSemilavorato'));

				$crud->set_subject('Ordine Semilavorato');

				$crud->order_by('stato_ordine');


				$output = $crud->render();
				$output->header="ORDINI SEMILAVORATI";
				$output->menu=$this->build_menu();


				$this->load->view('omis_view.php',$output);
	}

	function lav_esterne()
	{


				$crud = new grocery_CRUD();
				$crud->set_theme('datatables');
				$crud->unset_add();
				$crud->unset_edit();
				$crud->unset_delete();
				$this->db->limit(300,0);
				$crud->order_by('stato_lav_est asc, id_lav_esterna','desc');
				$crud->set_table('lav_esterne_commessa');
				$crud->set_relation('id_fornitore','fornitori','ragione_soc');
				$crud->set_relation('id_tipo_lav','tipi_lav_esterne','nome_lav_esterna');
				$crud->set_relation('id_categoria','categorie_componenti','nome_categoria');
				$crud->display_as('id_categoria',"Componente");
				$crud->display_as('id_fornitore',"Fornitore");
				$crud->display_as('id_tipo_lav',"Nome lavorazione");

				$crud->columns('id_fornitore','stato_lav_est','id_categoria','quantita','note','ddt_omis','data_ddt_omis','ddt_fornitore','data_ddt_fornitore','commessa');



				$output = $crud->render();
				$output->header="LAVORAZIONI ESTERNE";
				$output->menu=$this->build_menu();


				$this->load->view('omis_view.php',$output);
	}

	function crea_ordine()
	{
		date_default_timezone_set('Europe/Rome');
		$query = $this->db->get_where('associa_ordini',array('id_acc_magazz'=>$this->uri->segment(3)));
		if($query->num_rows==0){
			$query = $this->db->get_where('ordini_accessori',array('id_acc_magazz'=>$this->uri->segment(3)));
			foreach ($query->result() as $accessorio);
			$query = $this->db->get_where('fornitori', array('id_fornitore'=>$accessorio->id_fornitore));
			foreach ($query->result() as $fornitore);


			$this->db->select_max('n_ordine');
			$this->db->where("data_creazione_ordine BETWEEN '".date('Y')."-01-01' AND '".date('Y')."-12-31'");
			$query = $this->db->get('ordini_fornitori');
			if($query->num_rows() > 0){
				foreach ($query->result() as $ordinemax);
				$num_ordine=($ordinemax->n_ordine)+1;
			}else{
				$num_ordine=1;
			}


			$nuovoOrdine=array(
				'n_ordine'=> $num_ordine,
				'id_fornitore' => $fornitore->id_fornitore,
				'data_creazione_ordine' => date('Y-m-d'),
				);


			$this->db->insert('ordini_fornitori',$nuovoOrdine);




			//insert nuovo ordine
			$output->id_ordine_fornitore=$this->db->insert_id();

			$nuovaAssoc=array(
				'id_ordine_fornitore'=>$output->id_ordine_fornitore,
				'id_acc_magazz'=>$accessorio->id_acc_magazz
			);
			$this->db->insert('associa_ordini',$nuovaAssoc);

			$output->id_fornitore=$fornitore->id_fornitore;
			$output->nome_fornitore=$fornitore->nome_fornitore;
			$output->citta_fornitore=$fornitore->citta_fornitore;
			$output->indirizzo_fornitore=$fornitore->indirizzo_fornitore;

			$query = $this->db->get_where('accessori',array('id_accessorio'=>$accessorio->id_accessorio));
			foreach ($query->result() as $accessorio_amico);

			$output->tabella_accessori="<td >".$accessorio_amico->codice_accessorio."</td><td>".$accessorio_amico->nome_accessorio."</td>
				<td align='center'>NR</td><td align='center'>".$accessorio->quantita_ordinata."</td>";




			$output->note_ordine="";
			$output->riferimenti_ordine="Riferimenti di prova";


			$output->num_ordine_fornitore=$num_ordine;
			$output->data_odierna=date('d-m-Y');
			$this->load->view('/views_stampa/stampa_ordini_acc_view.php',$output);
		}
		else{
			foreach ($query->result() as $ordine);
			redirect('/magazzino/ricarica_ordine/'.$ordine->id_ordine_fornitore, 'refresh');

		}


	}


	function aggiungi_acc()
	{
			$nuovaAssoc=array(
				'id_ordine_fornitore'=>$this->uri->segment(3),
				'id_acc_magazz'=>$this->uri->segment(4)
			);
			$this->db->insert('associa_ordini',$nuovaAssoc);


			redirect('/magazzino/ricarica_ordine/'.$this->uri->segment(3), 'refresh');

	}


	function ricarica_ordine()
	{
		date_default_timezone_set('Europe/Rome');


		$query = $this->db->get_where('ordini_fornitori',array('id_ordine_fornitore'=>$this->uri->segment(3)));
		foreach ($query->result() as $ordine);
		$query = $this->db->get_where('fornitori', array('id_fornitore'=>$ordine->id_fornitore));
		foreach ($query->result() as $fornitore);



		//insert nuovo ordine
		$output->id_ordine_fornitore=$ordine->id_ordine_fornitore;


		$output->id_fornitore=$fornitore->id_fornitore;
		$output->nome_fornitore=$fornitore->nome_fornitore;
		$output->citta_fornitore=$fornitore->citta_fornitore;
		$output->indirizzo_fornitore=$fornitore->indirizzo_fornitore;
		$query = $this->db->get_where('associa_ordini',array('id_ordine_fornitore'=>$ordine->id_ordine_fornitore));
		foreach ($query->result() as $accessorio_ass){

			$query = $this->db->get_where('ordini_accessori',array('id_acc_magazz'=>$accessorio_ass->id_acc_magazz));
			foreach ($query->result() as $accessorio);

			$query = $this->db->get_where('accessori',array('id_accessorio'=>$accessorio->id_accessorio));
			foreach ($query->result() as $accessorio_amico);

			$output->tabella_accessori.="<tr><td >".$accessorio_amico->codice_accessorio."</td><td>".$accessorio_amico->nome_accessorio."</td>
				<td align='center'>NR</td><td align='center'>".$accessorio->quantita_ordinata."</td></tr>";

		}


		$output->note_ordine=$ordine->note_ordine;
		$output->riferimenti_ordine=$ordine->riferimenti_ordine;


		$output->num_ordine_fornitore=$ordine->n_ordine;
		$output->data_odierna=$ordine->data_creazione_ordine;
		$this->load->view('/views_stampa/stampa_ordini_acc_view.php',$output);

	}

	function magazzino_accessori()
	{
			$crud = new grocery_CRUD();
			$crud->set_theme('datatables');
			$crud->set_table('magazzino_accessori');
			$crud->set_subject('Codice Accessorio');

			$crud->unset_texteditor('note_accessorio');

			$crud->unset_add_fields('qta_disponibile','valore_magazzino','prezzo_medio_codice','stampatrici');
			$crud->edit_fields('codice_accessorio_interno','nome_accessorio_interno','ubicazione','quantita_minima_magazzino','note_accessorio','obsoleto','qta_disponibile');
			$crud->unset_columns('valore_magazzino','prezzo_medio_codice','stampatrici');
			$crud->required_fields('codice_accessorio_interno');
			$crud->callback_column('qta_disponibile',array($this,'qta_disponibile_accessori_column'));

			$crud->add_action('Immagine', '', '','image-button',array($this,'apri_accessorio'));
			$crud->set_js('assets/js/immagine_accessorio.js');
			if($this->session->userdata('tipo_op')=="OPE"){
				$crud->unset_add();
				$crud->unset_delete();
				$crud->unset_edit();
			}
			if($this->uri->segment($this->uri->total_segments())!='ajax_call' ){
				$output = $crud->render_ajax();
				$output->header="MAGAZZINO ACCESSORI";
				$output->footer="";
				$output->menu=$this->build_menu();


				$this->load->view('omis_view.php',$output);

			}else{
				$output = $crud->render_ajax();
				echo $output->output;

			}


	}

	function deposito()
	{
			$crud = new grocery_CRUD();
			$crud->set_theme('datatables');
			$crud->set_table('deposito');
			$crud->set_subject('Codice Deposito');

			$crud->unset_texteditor('note_accessorio');

			$crud->edit_fields('codice_accessorio_interno','nome_accessorio_interno','ubicazione','note_accessorio','qta_disponibile');
			$crud->unset_columns('valore_magazzino','prezzo_medio_codice','stampatrici');
			$crud->required_fields('codice_accessorio_interno');

			$crud->add_action('Immagine', '', '','image-button',array($this,'apri_accessorio'));
			$crud->set_js('assets/js/immagine_accessorio.js');

			if($this->uri->segment($this->uri->total_segments())!='ajax_call' ){
				$output = $crud->render_ajax();
				$output->header="DEPOSITO";
				$output->footer="";
				$output->menu=$this->build_menu();


				$this->load->view('omis_view.php',$output);

			}else{
				$output = $crud->render_ajax();
				echo $output->output;

			}


	}

	function apri_accessorio($primary_key , $row)
	{

		return "ftp://192.168.2.231:2222/accessori/".$row->codice_accessorio_interno.".jpg";
	}





	function magazzino_materiali()
	{
			$crud = new grocery_CRUD();
			$crud->set_theme('datatables');
			$crud->set_table('magazzino_materiali');
			$crud->set_subject('Codice Materiale');

			$crud->set_relation('id_materiale','materiali','nome_materiale');
			$crud->display_as('id_materiale','Materiale');
			$crud->callback_column('qta_disponibile',array($this,'qta_disponibile_column'));
			$crud->unset_columns('prezzo_medio_codice');
			$crud->unset_add_fields('qta_disponibile','valore_magazzino');
			$crud->unset_edit_fields('valore_magazzino');

			if($this->session->userdata('tipo_op')=="OPE"){
				$crud->unset_add();
				$crud->unset_delete();
				$crud->unset_edit();
			}

			$output = $crud->render();
			$output->header="MAGAZZINO MATERIALI";
			$output->footer="";
			$output->menu=$this->build_menu();

			$this->load->view('omis_view.php',$output);


	}


	function qta_disponibile_column($value,$row)
	{
		if(($value!="")&&($row->sezione_materiale!="")&&($row->sezione_materiale!="RITAGLIO")){
			if($row->forma_materiale=="PIATTO"){
				$query = $this->db->get_where('materiali',array('id_materiale'=>$row->id_materiale));
				foreach ($query->result() as $materiale);
				@list($value1,$value2) = explode("x",$row->sezione_materiale);
				$lunghezza_rimanente=intval((1000000*$value)/($value1*$value2*$materiale->peso_specifico_materiale));
				if($row->qta_minima_magazzino>$lunghezza_rimanente)
				{
					return "<div class='tr_red'>".$value." kg  [".$lunghezza_rimanente." mm]</div>";
				}else{
					return $value." kg  [".$lunghezza_rimanente." mm]";
				}
			}
			if($row->forma_materiale=="TONDO"){
				$query = $this->db->get_where('materiali',array('id_materiale'=>$row->id_materiale));
				foreach ($query->result() as $materiale);
				$lunghezza_rimanente=intval((1000000*$value)/(($row->sezione_materiale/2)*($row->sezione_materiale/2)*3.1415926535*$materiale->peso_specifico_materiale));
				return $value." kg  [".$lunghezza_rimanente." mm]";
			}
		}else{
			return $value;
		}

	}

	function qta_disponibile_accessori_column($value,$row)
	{
		if($row->qta_disponibile<$row->quantita_minima_magazzino)
		{
			return "<div class='tr_red'>".$value." </div>";
		}else{
			return $value;
		}
	}

	function qta_disponibile_semilav_column($value,$row)
	{
		if($row->qta_disponibile<$row->qta_minima_magazzino)
		{
			return "<div class='tr_red'>".$value." </div>";
		}else{
			return $value;
		}
	}

	function magazzino_semilavorati()
	{
			$crud = new grocery_CRUD();
			$crud->set_theme('datatables');
			$crud->set_table('magazzino_semilavorati');
			$crud->set_subject('Codice Semilavorato');
			$crud->unset_add_fields('qta_disponibile','valore_magazzino','prezzo_medio_codice','riferimento_commessa');
			$crud->unset_edit_fields('qta_disponibile','valore_magazzino','prezzo_medio_codice','riferimento_commessa');
			$crud->unset_columns('riferimento_commessa, prezzo_medio_codice');
			$crud->callback_column('qta_disponibile',array($this,'qta_disponibile_semilav_column'));
			if($this->session->userdata('tipo_op')=="OPE"){
				$crud->unset_add();
				$crud->unset_delete();
				$crud->unset_edit();
			}
			$output = $crud->render();
			$output->header="MAGAZZINO SEMILAVORATI";
			$output->footer="";
			$output->menu=$this->build_menu();

			$this->load->view('omis_view.php',$output);


	}
	function lista()
	{
			$crud = new grocery_CRUD();
			$crud->set_theme('datatables');
			$crud->set_table('magazzino_accessori');
			$crud->set_subject('Codice Accessorio');

			$output = $crud->render();
			$output->output="SCEGLI UN MAGAZZINO";
			$output->header="MAGAZZINI";
			$output->footer="";
			$output->menu=$this->build_menu();


			$this->load->view('omis_view.php',$output);

	}
	function ordini_lista()
	{
			$crud = new grocery_CRUD();
			$crud->set_theme('datatables');
			$crud->set_table('ordini_fornitori');
			$crud->set_subject('Ordine');
			$crud->set_relation('id_fornitore','fornitori','nome_fornitore');
			$crud->add_action('Dettaglio', '', '','ui-icon-plus',array($this,'apri_dettaglio'));
			//$crud->add_action('Stampa', '', 'magazzino/stampa_ordine','ui-icon-plus');
			$crud->unset_texteditor('note_ordine');
			$crud->edit_fields('id_fornitore','riferimenti_ordine','note_ordine');
			$crud->add_fields('id_fornitore','data_creazione_ordine','n_ordine','riferimenti_ordine','note_ordine');
			$crud->display_as('id_fornitore','Fornitore');
			$crud->display_as('riferimenti_ordine',"All'attenzione di");

			$crud->change_field_type('n_ordine', 'invisible');
			$crud->columns('data_creazione_ordine','n_ordine','id_fornitore','note_ordine','stato');

			//$crud->change_field_type('data_creazione_ordine', 'invisible',date('d/m/Y'));

		    $crud->callback_add_field('data_creazione_ordine',array($this,'data_creazione_ordine_nascosta'));
			$crud->callback_after_insert(array($this,'note_ordine_fornitore'));

			$crud->callback_before_insert(array($this,'before_insert_ordine'));
			$crud->callback_before_update(array($this,'before_update_ordine'));
			$crud->callback_after_update(array($this,'evasione_ordine'));
			$crud->callback_after_delete(array($this,'ordine_after_delete'));
			$crud->order_by('data_creazione_ordine desc, n_ordine', 'desc');

			$output = $crud->render();
			$output->header="ORDINI FORNITORI";
			$output->menu=$this->build_menu();


			$this->load->view('omis_view.php',$output);

	}
	function ordine_after_delete($primary_key){
		$this->db->delete('ordini_materiali', array('id_ordine_fornitore' => $primary_key));
		$this->db->delete('ordini_accessori', array('id_ordine_fornitore' => $primary_key));
		$this->db->delete('ordini_semilavorati', array('id_ordine_fornitore' => $primary_key));

	}

	function note_ordine_fornitore($post_array,$primary_key){

		$query = $this->db->get_where('fornitori',array('id_fornitore'=>$post_array['id_fornitore']));
		foreach ($query->result() as $fornitore);
		$this->db->where('id_ordine_fornitore', $primary_key);
		$this->db->update('ordini_fornitori', array('note_ordine' => $fornitore->note_fornitore_ordine));

	}

	function before_update_ordine($post_array, $primary_key){
		if($post_array['ddt_ultima_evasione']!=NULL){
			$query = $this->db->get_where('ordini_fornitori',array('id_ordine_fornitore'=>$primary_key));
			foreach ($query->result() as $ordine);
			if($ordine->ddt_ultima_evasione==NULL){
					@list($day,$month,$year) = explode("/",$post_array['data_ultima_evasione']);
					$data_ultima_evasione=date("Y-m-d", mktime(0,0,0,$month,$day,$year));


				$query = $this->db->get_where('ordini_accessori',array('id_ordine_fornitore'=>$primary_key,'ddt_evasione_ordine'=>NULL));
				foreach ($query->result_array() as $ordine_acc){
					$ordine_acc['ddt_evasione_ordine']=$post_array['ddt_ultima_evasione'];
					$this->cambiaStatoOrdineSingoloAccessorio($ordine_acc,$ordine_acc['id_ordine_accessorio']);
					$this->db->where('id_ordine_accessorio', $ordine_acc['id_ordine_accessorio']);
					$this->db->update('ordini_accessori', array('ddt_evasione_ordine' => $post_array['ddt_ultima_evasione'],
						'data_evasione_ordine'=>$data_ultima_evasione, 'stato_ordine'=>'EVASO'));


				}
				$query = $this->db->get_where('ordini_materiali',array('id_ordine_fornitore'=>$primary_key,'ddt_evasione_ordine'=>NULL));
				foreach ($query->result_array() as $ordine_mat){
					$ordine_mat['ddt_evasione_ordine']=$post_array['ddt_ultima_evasione'];
					$this->cambiaStatoOrdineSingoloMateriale($ordine_mat,$ordine_mat['id_ordine_materiale']);
					$this->db->where('id_ordine_materiale', $ordine_mat['id_ordine_materiale']);
					$this->db->update('ordini_materiali', array('ddt_evasione_ordine' => $post_array['ddt_ultima_evasione'],
						'data_evasione_ordine'=>$data_ultima_evasione,
						'stato_ordine'=>'EVASO'));

				}
				$query = $this->db->get_where('ordini_semilavorati',array('id_ordine_fornitore'=>$primary_key,'ddt_evasione_ordine'=>NULL));
				foreach ($query->result_array() as $ordine_sem){
					$ordine_sem['ddt_evasione_ordine']=$post_array['ddt_ultima_evasione'];
					$this->cambiaStatoOrdineSingoloSemilavorato($ordine_sem,$ordine_sem['id_semilav_comm']);
					$this->db->where('id_semilav_comm', $ordine_sem['id_semilav_comm']);
					$this->db->update('ordini_semilavorati', array('ddt_evasione_ordine' => $post_array['ddt_ultima_evasione'],
																  'data_evasione_ordine' => $data_ultima_evasione,
																          'stato_ordine' => 'EVASO'));
				}
			$this->db->update('ordini_fornitori', array('stato'=>'EVASIONE TOTALE'),'id_ordine_fornitore = '.$primary_key);
			}
		}
		if($post_array['ddt_ultima_evasione']==NULL){
			$query = $this->db->get_where('ordini_fornitori',array('id_ordine_fornitore'=>$primary_key));
			foreach ($query->result() as $ordine);
			if($ordine->ddt_ultima_evasione!=NULL){

				$query = $this->db->get_where('ordini_accessori',array('id_ordine_fornitore'=>$primary_key,'ddt_evasione_ordine'=>$ordine->ddt_ultima_evasione));
					foreach ($query->result_array() as $ordine_acc){
					$ordine_acc['ddt_evasione_ordine']=NULL;
					$this->cambiaStatoOrdineSingoloAccessorio($ordine_acc,$ordine_acc['id_ordine_accessorio']);
					$this->db->where('id_ordine_accessorio', $ordine_acc['id_ordine_accessorio']);
					$this->db->update('ordini_accessori', array('ddt_evasione_ordine' => NULL,
						'data_evasione_ordine'=>NULL, 'stato_ordine'=>'ORDINATO'));

				}
				$query = $this->db->get_where('ordini_materiali',array('id_ordine_fornitore'=>$primary_key,'ddt_evasione_ordine'=>$ordine->ddt_ultima_evasione));
				foreach ($query->result_array() as $ordine_mat){
					$ordine_mat['ddt_evasione_ordine']=NULL;
					$this->cambiaStatoOrdineSingoloMateriale($ordine_mat,$ordine_mat['id_ordine_materiale']);
					$this->db->where('id_ordine_materiale', $ordine_mat['id_ordine_materiale']);
					$this->db->update('ordini_materiali', array('ddt_evasione_ordine' => NULL,
						'data_evasione_ordine'=>NULL,
						'stato_ordine'=>'ORDINATO'));

				}
				$query = $this->db->get_where('ordini_semilavorati',array('id_ordine_fornitore'=>$primary_key,'ddt_evasione_ordine'=>$ordine->ddt_ultima_evasione));
				foreach ($query->result_array() as $ordine_sem){
					$ordine_sem['ddt_evasione_ordine']=NULL;
					$this->cambiaStatoOrdineSingoloSemilavorato($ordine_sem,$ordine_sem['id_semilav_comm']);
					$this->db->update('ordini_semilavorati', array('ddt_evasione_ordine' => NULL,
																  'data_evasione_ordine' => NULL,
																          'stato_ordine' => 'ORDINATO'));
				}
			$this->db->update('ordini_fornitori', array('stato'=>'ORDINATO'),'id_ordine_fornitore = '.$primary_key);
			}
		}

		return $post_array;
	}



	function data_creazione_ordine_nascosta(){
		date_default_timezone_set('Europe/Rome');
		return "<input name='data_creazione_ordine' type='text' value='".date("d/m/Y")."' readonly >";
	}

	function before_insert_ordine($post_array)
	{
		date_default_timezone_set('Europe/Rome');


		$this->db->select_max('n_ordine');
		$this->db->where("data_creazione_ordine BETWEEN '".date('Y')."-01-01' AND '".date('Y')."-12-31'");
		$query = $this->db->get('ordini_fornitori');
		if($query->num_rows() > 0){
			foreach ($query->result() as $ordinemax);
			$num_ordine=($ordinemax->n_ordine)+1;
		}else{
			$num_ordine=1;
		}
		$post_array['n_ordine']=$num_ordine;

		return $post_array;
	}






	function apri_dettaglio($primary_key , $row)
	{
		$pagina="accessori";
		$materiali=0;
		$accessori=0;
		$semilavorati=0;
		$query = $this->db->get_where('ordini_accessori',array('id_ordine_fornitore'=>$row->id_ordine_fornitore));
		$accessori=$query->num_rows();

		$query = $this->db->get_where('ordini_materiali',array('id_ordine_fornitore'=>$row->id_ordine_fornitore));
		$materiali=$query->num_rows();

		$query = $this->db->get_where('ordini_semilavorati',array('id_ordine_fornitore'=>$row->id_ordine_fornitore));
		$semilavorati=$query->num_rows();


		if(($materiali==0)&&($accessori==0)&&($semilavorati==0)){
			$query = $this->db->get_where('anagrafica_accessori',array('id_fornitore'=>$row->id_fornitore));
			$accessori=$query->num_rows();

			$query = $this->db->get_where('anagrafica_materiali',array('id_fornitore'=>$row->id_fornitore));
			$materiali=$query->num_rows();

			$query = $this->db->get_where('anagrafica_semilavorati',array('id_fornitore'=>$row->id_fornitore));
			$semilavorati=$query->num_rows();
		}
		if($materiali>$accessori) {
			$pagina="materiali";
			if($semilavorati>$materiali) $pagina="semilavorati";
		}else{
			if($semilavorati>$accessori) $pagina="semilavorati";
		}

		return ("/index.php/magazzino/dettaglio_ordine/".$primary_key."/".$pagina);
	}
	function dettaglio_ordine()
	{

		date_default_timezone_set('Europe/Rome');

		$crud = new grocery_CRUD();
		$crud->set_theme('datatables');
		$this->db->select('*');
		$this->db->from('ordini_fornitori');
		$this->db->join('fornitori', 'ordini_fornitori.id_fornitore = fornitori.id_fornitore');
		$this->db->where('id_ordine_fornitore', $this->uri->segment(3));
		$prendi_ordine = $this->db->get();
		foreach ($prendi_ordine->result() as $ordine);


		switch($this->uri->segment(4)){
			case "accessori":
				$crud->set_table('ordini_accessori');
				$crud->where('id_ordine_fornitore',$this->uri->segment(3));

				$crud->set_subject('Ordine Accessorio');
				$crud->set_relation('id_accessorio','anagrafica_accessori','nome_accessorio');
				$crud->display_as('id_accessorio','Nome Accessorio');
				$crud->callback_add_field('id_accessorio',array($this,'ListaAccessoriCodice'));
			    $crud->callback_edit_field('id_accessorio',array($this,'ListaAccessoriCodice'));
				$crud->unset_add_fields('ddt_evasione_ordine','data_evasione_ordine','stato_ordine');
				$crud->change_field_type('stato_ordine', 'hidden');
				$crud->change_field_type('id_ordine_fornitore', 'hidden',$this->uri->segment(3));
				$crud->columns('ns_riferimento','id_accessorio','codice_fornitore_interno_ubicazione','quantita_ordinata','data_consegna_richiesta','stato_ordine','ddt_evasione_ordine',
				'data_evasione_ordine');
				$crud->callback_column('codice_fornitore_interno_ubicazione',array($this,'codice_accessorio_column'));
				$crud->display_as('codice_fornitore_interno_ubicazione',"Fornitore | Interno | Ubicazione");
				$crud->callback_before_update(array($this,'cambiaStatoOrdineSingoloAccessorio'));

			break;
			case "materiali":
				$crud->set_table('ordini_materiali');
				$crud->where('id_ordine_fornitore',$this->uri->segment(3));
				$crud->callback_add_field('codice_materiale_interno',array($this,'ListaMaterialiFornitore'));
				$crud->callback_edit_field('codice_materiale_interno',array($this,'ListaMaterialiFornitore'));
				$crud->display_as('codice_materiale_interno','Materiale');
				$crud->unset_add_fields('ddt_evasione_ordine','data_evasione_ordine','stato_ordine');
				$crud->change_field_type('id_ordine_fornitore', 'hidden',$this->uri->segment(3));
				$crud->callback_column('codice_materiale_interno',array($this,'ListaMaterialiFornitoreColumn'));
				$crud->set_js('assets/themes/datatables/js/ordini_materiali.js');
				$crud->change_field_type('stato_ordine', 'hidden');
				$crud->change_field_type('kg_ordine', 'hidden');
				$crud->unset_columns('id_ordine_fornitore');
				$crud->set_subject('Ordine Materiale');
				$crud->callback_before_update(array($this,'cambiaStatoOrdineSingoloMateriale'));
			break;
			case "semilavorati":
				$crud->set_table('ordini_semilavorati');
				$crud->where('id_ordine_fornitore',$this->uri->segment(3));
				$crud->set_relation('id_semilav_ana','anagrafica_semilavorati','nome_semilavorato');
				$crud->display_as('id_semilav_ana','Semilavorato');
				$crud->change_field_type('id_ordine_fornitore', 'hidden',$this->uri->segment(3));
				$crud->unset_add_fields('ddt_evasione_ordine','data_evasione_ordine','id_fornitore',
					'stato_ordine','costo_semilav','ddt_evasione_ordine','data_evasione_ordine');
				$crud->unset_edit_fields('id_fornitore');
				$crud->change_field_type('stato_ordine', 'hidden');
				//$crud->callback_before_insert(array($this, 'before_insert_ordine_semilav'));
				$crud->unset_columns('id_ordine_fornitore','id_fornitore','note_semilav','costo_semilav');
				$crud->unset_texteditor('note_semilav');


				$crud->callback_add_field('id_semilav_ana',array($this,'ListaSemilavoratiFornitore'));
				$crud->callback_edit_field('id_semilav_ana',array($this,'ListaSemilavoratiFornitore'));
				$crud->callback_edit_field('commessa',array($this,'lista_commesse'));
				$crud->callback_add_field('commessa',array($this,'lista_commesse'));
				$crud->callback_before_update(array($this,'cambiaStatoOrdineSingoloSemilavorato'));

				$crud->set_subject('Ordine Semilavorato');

			break;
		}
		$crud->callback_after_update(array($this,'evasione_ordine'));
		$output = $crud->render();
		$output->nome_op=$this->session->userdata('nome_operatore');
		$output->id_ordine_fornitore=$this->uri->segment(3);
		$output->titolo="Ordine n°".$ordine->n_ordine;
		@list($year,$month,$day) = explode("-",$ordine->data_creazione_ordine);
		$output->anno_ordine=$year;
		$ordine->data_creazione_ordine=date("d-m-Y", mktime(0,0,0,$month,($day),$year));
		$output->header="ORDINE n°".$ordine->n_ordine." del ".$ordine->data_creazione_ordine." per ".$ordine->nome_fornitore;
		$output->num_ordine=$ordine->n_ordine;
		switch($this->session->userdata('nome_operatore')){
			case "ANDREA":
				$user='Andrea Piatto';
				$mail='andrea.piatto@omis.it';

			break;
			case "FRANZ":
				$user='Franz Piatto';
				$mail='franz.piatto@omis.it';

			break;
			case "GIANNI":
				$user='Gianni Piatto';
				$mail='gianni.piatto@omis.it';

			break;
			case "LORI":
				$user='Lori';
				$mail='ufficio@omis.it';

			break;
			default:
				$user="";
			break;
		}
		$output->signature="Buongiorno, \xA\xA"."In allegato il nostro ordine come in oggetto."."\xA\xA"."Prego di confermare la data di consegna richiesta. \xA".'Grazie e saluti. '."\xA\xA".$user."\xA".'www.omis.it '."\xA".'Tel. +39 0331 776097 '."\xA".'Fax  +39 0331 772290';
		$output->oggetto_email="Nostro ordine ".$ordine->n_ordine." del ".$ordine->data_creazione_ordine;
		$output->email_fornitore=$ordine->email_fornitore;
		$output->ccn=$mail;

		$output->menu=$this->build_menu();

		$this->load->view('dettaglio_ordine.php',$output);

	}
	/*function stampa_ordine(){

	}*/
	function cambiaStatoOrdineSingoloMateriale($post_array,$primary_key)
	{

		$kg=0;
		if($post_array['ddt_evasione_ordine'] != NULL){
				$post_array['stato_ordine']='EVASO';
				$query = $this->db->get_where('ordini_materiali',array('id_ordine_materiale'=>$primary_key));
				foreach ($query->result() as $ordine_materiale);
				if($ordine_materiale->stato_ordine!='EVASO'){
					$mater = $this->db->get_where('magazzino_materiali',array('codice_materiale_interno'=>$post_array['codice_materiale_interno']));
					foreach($mater->result() as $materiozzo);
					$peso = $this->db->get_where('materiali',array('id_materiale'=>$materiozzo->id_materiale));
					foreach($peso->result() as $peso_spec);
					if($post_array['lunghezza_ordinata']!= NULL){
						@list($valore1,$valore2) = explode("x",$materiozzo->sezione_materiale);
						$kg=($valore1*$valore2*$post_array['lunghezza_ordinata']*$peso_spec->peso_specifico_materiale)/1000000;

					}else{
						@list($valore1,$valore2,$valore3) = explode("x",$post_array['misure_ritaglio_ordinato']);
						$kg=($valore1*$valore2*$valore3*$peso_spec->peso_specifico_materiale)/1000000;
					}
					$this->db->where('codice_materiale_interno', $post_array['codice_materiale_interno']);
					$this->db->update('magazzino_materiali', array('qta_disponibile'=>$materiozzo->qta_disponibile+($kg*$post_array['quantita_ordinata'])));
				}
			}
			else{
					$post_array['stato_ordine']='ORDINATO';
					$query = $this->db->get_where('ordini_materiali',array('id_ordine_materiale'=>$primary_key));
					foreach ($query->result() as $ordine_materiale);
					if($ordine_materiale->stato_ordine!='ORDINATO'){
						$mater = $this->db->get_where('magazzino_materiali',array('codice_materiale_interno'=>$post_array['codice_materiale_interno']));
						foreach($mater->result() as $materiozzo);
						$peso = $this->db->get_where('materiali',array('id_materiale'=>$materiozzo->id_materiale));
						foreach($peso->result() as $peso_spec);
						if($post_array['lunghezza_ordinata']!= NULL){
							@list($valore1,$valore2) = explode("x",$materiozzo->sezione_materiale);
							$kg=($valore1*$valore2*$post_array['lunghezza_ordinata']*$peso_spec->peso_specifico_materiale)/1000000;

						}else{
							@list($valore1,$valore2,$valore3) = explode("x",$post_array['misure_ritaglio_ordinato']);
							$kg=($valore1*$valore2*$valore3*$peso_spec->peso_specifico_materiale)/1000000;
						}
						$this->db->where('codice_materiale_interno', $post_array['codice_materiale_interno']);
						$this->db->update('magazzino_materiali', array('qta_disponibile'=>$materiozzo->qta_disponibile-($kg*$post_array['quantita_ordinata'])));
					}
				}

		if($kg>0){
			$post_array['kg_ordine']=($kg*$post_array['quantita_ordinata']);
		}

		return $post_array;
	}
	function cambiaStatoOrdineSingoloAccessorio($post_array,$primary_key)
	{
		if($post_array['ddt_evasione_ordine'] != NULL){
			$post_array['stato_ordine']='EVASO';
			$query = $this->db->get_where('ordini_accessori',array('id_ordine_accessorio'=>$primary_key));
			foreach ($query->result() as $ordine_accessorio);
			if($ordine_accessorio->stato_ordine!='EVASO'){
					$acc = $this->db->get_where('anagrafica_accessori',array('id_accessorio'=>$post_array['id_accessorio']));
					foreach($acc->result() as $accessorio);
					$acc = $this->db->get_where('magazzino_accessori',array('codice_accessorio_interno'=>$accessorio->codice_accessorio_interno));
					foreach($acc->result() as $accessorio_magazz);
					$this->db->where('codice_accessorio_interno', $accessorio->codice_accessorio_interno);
					$this->db->update('magazzino_accessori', array('qta_disponibile'=>$accessorio_magazz->qta_disponibile+($post_array['quantita_ordinata'])));
				}
		}else{
			$post_array['stato_ordine']='ORDINATO';
			$query = $this->db->get_where('ordini_accessori',array('id_ordine_accessorio'=>$primary_key));
			foreach ($query->result() as $ordine_accessorio);
			if($ordine_accessorio->stato_ordine!='ORDINATO'){
					$acc = $this->db->get_where('anagrafica_accessori',array('id_accessorio'=>$post_array['id_accessorio']));
					foreach($acc->result() as $accessorio);
					$acc = $this->db->get_where('magazzino_accessori',array('codice_accessorio_interno'=>$accessorio->codice_accessorio_interno));
					foreach($acc->result() as $accessorio_magazz);
					$this->db->where('codice_accessorio_interno', $accessorio->codice_accessorio_interno);
					$this->db->update('magazzino_accessori', array('qta_disponibile'=>$accessorio_magazz->qta_disponibile-($post_array['quantita_ordinata'])));
				}
		}
		return $post_array;
	}
	function cambiaStatoOrdineSingoloSemilavorato($post_array,$primary_key)
	{
		if($post_array['ddt_evasione_ordine'] != ""){
			$post_array['stato_ordine']='EVASO';
			$query = $this->db->get_where('ordini_semilavorati',array('id_semilav_comm'=>$primary_key));
			foreach ($query->result() as $ordine_semilav);
			if($ordine_semilav->stato_ordine!='EVASO'){
					if($post_array['commessa']==NULL)
					{
						$acc = $this->db->get_where('anagrafica_semilavorati',array('id_semilav_ana'=>$post_array['id_semilav_ana']));
						foreach($acc->result() as $semilavorato);
						$acc = $this->db->get_where('magazzino_semilavorati',array('codice_semilavorato_interno'=>$semilavorato->codice_semilavorato_interno));
						foreach($acc->result() as $semilavorato_magazz);
						$this->db->where('codice_semilavorato_interno', $semilavorato->codice_semilavorato_interno);
						$this->db->update('magazzino_semilavorati', array('qta_disponibile'=>$semilavorato_magazz->qta_disponibile+($post_array['quantita_semilav'])));
					}
			}
		}else{
			$post_array['stato_ordine']='ORDINATO';
			$query = $this->db->get_where('ordini_semilavorati',array('id_semilav_comm'=>$primary_key));
			foreach ($query->result() as $ordine_semilav);
			if($ordine_semilav->stato_ordine!='ORDINATO'){
					if($post_array['commessa']==NULL)
					{
						$acc = $this->db->get_where('anagrafica_semilavorati',array('id_semilav_ana'=>$post_array['id_semilav_ana']));
						foreach($acc->result() as $semilavorato);
						$acc = $this->db->get_where('magazzino_semilavorati',array('codice_semilavorato_interno'=>$semilavorato->codice_semilavorato_interno));
						foreach($acc->result() as $semilavorato_magazz);
						$this->db->where('codice_semilavorato_interno', $semilavorato->codice_semilavorato_interno);
						$this->db->update('magazzino_semilavorati', array('qta_disponibile'=>$semilavorato_magazz->qta_disponibile-($post_array['quantita_semilav'])));
					}
			}
		}
		return $post_array;
	}
	function evasione_ordine($post_array,$primary_key){
		$evasi=0;
		$query = $this->db->get_where('ordini_accessori',array('id_ordine_fornitore'=>$post_array['id_ordine_fornitore']));
		$totali=$query->num_rows();
		foreach ($query->result_array() as $ordine_acc){
			if($ordine_acc['stato_ordine']==='EVASO') {
				$evasi++;

			}
		}
		$query = $this->db->get_where('ordini_materiali',array('id_ordine_fornitore'=>$post_array['id_ordine_fornitore']));
		$totali+=$query->num_rows();

		foreach ($query->result_array() as $ordine_mat){
			if($ordine_mat['stato_ordine']==='EVASO')
			{
				$evasi++;
			}
		}
		$query = $this->db->get_where('ordini_semilavorati',array('id_ordine_fornitore'=>$post_array['id_ordine_fornitore']));
		$totali+=$query->num_rows();
		foreach ($query->result_array() as $ordine_sem){
			if($ordine_sem['stato_ordine']==='EVASO')
			{
				$evasi++;
			}
		}
		if($evasi>0){
			if($evasi==$totali)
			{
				$stato='EVASIONE TOTALE';
			}else{
				$stato='EVASIONE PARZIALE';
			}
		}else{
			$stato='ORDINATO';
		}
		$this->db->where('id_ordine_fornitore', $post_array['id_ordine_fornitore']);
		$this->db->update('ordini_fornitori', array('stato'=>$stato));

	}

	function lista_commesse($value, $primary_key)
	{
		$class="id='' class='chosen-select' style='width:200px;'";
		$asd['']='';
		if(!(isset($value)))
		{
			$value="";
		}
		$this->db->select('commessa');
		$this->db->from('lavori');
		$this->db->where('stato <>', 'E');
		$query = $this->db->get();
		foreach ($query->result() as $row)
		{
				$asd[$row->commessa]=$row->commessa;
		}
		if(array_search($value, $asd)===FALSE){
			$return="<input name='commessa' type=text value=".$value." disabled>";
		}
		else
		{
			$return=form_dropdown('commessa', $asd, $value,$class);
		}
		return $return;
	}


	function codice_accessorio_column($value,$row){
		$query = $this->db->get_where('anagrafica_accessori',array('id_accessorio'=>$row->id_accessorio));
		foreach ($query->result() as $accessorio);
		$query = $this->db->get_where('magazzino_accessori',array('codice_accessorio_interno'=>$accessorio->codice_accessorio_interno));
		if($query->num_rows()>0)
			foreach ($query->result() as $accessorio_maga);
		else{
			$accessorio_maga=new stdClass();
			$accessorio_maga->ubicazione="";
		}
		return $accessorio->codice_accessorio_fornitore."<b> || </b>".$accessorio->codice_accessorio_interno."<b> || </b>".$accessorio_maga->ubicazione;

	}
	function ListaAccessoriCodice($value, $primary_key)
	{
		$class="id='' class='chosen-select'";
		$asd['']='';
		if(!(isset($value)))
		{
			$value="";
		}
		$query = $this->db->get_where('ordini_fornitori',array('id_ordine_fornitore'=>$this->uri->segment(3)));
		foreach ($query->result() as $ordine);
		$query = $this->db->get_where('anagrafica_accessori',array('id_fornitore'=>$ordine->id_fornitore));

		foreach ($query->result() as $row)
		{
			$asd[$row->id_accessorio]=$row->codice_accessorio_fornitore." - ".$row->nome_accessorio." - ".$row->codice_accessorio_interno;
		}

		return form_dropdown('id_accessorio', $asd, $value,$class);
	}
	function ListaSemilavoratiFornitore($value, $primary_key)
	{
		$class="id='' class='chosen-select'";
		$asd['']='';
		if(!(isset($value)))
		{
			$value="";
		}
		$query = $this->db->get_where('ordini_fornitori',array('id_ordine_fornitore'=>$this->uri->segment(3)));
		foreach ($query->result() as $ordine);
		$query = $this->db->get_where('anagrafica_semilavorati',array('id_fornitore'=>$ordine->id_fornitore));

		foreach ($query->result() as $row)
		{
			$asd[$row->id_semilav_ana]=$row->codice_semilavorato_fornitore." - ".$row->nome_semilavorato;
		}

		return form_dropdown('id_semilav_ana', $asd, $value,$class);
	}
	function ListaMaterialiFornitore($value, $primary_key)
	{
		$class="id='' class='chosen-select'";
		$asd['']='';
		if(!(isset($value)))
		{
			$value="";
		}
		$query = $this->db->get_where('ordini_fornitori',array('id_ordine_fornitore'=>$this->uri->segment(3)));
		foreach ($query->result() as $ordine);
		$query = $this->db->get_where('anagrafica_materiali',array('id_fornitore'=>$ordine->id_fornitore));

		foreach ($query->result() as $row)
		{
			if($row->codice_materiale_interno!=""){
				$query = $this->db->get_where('magazzino_materiali',array('codice_materiale_interno'=>$row->codice_materiale_interno));
				foreach ($query->result() as $mat);
				$query = $this->db->get_where('materiali',array('id_materiale'=>$mat->id_materiale));
				foreach ($query->result() as $nomemat);
				$asd[$row->codice_materiale_interno]=$nomemat->nome_materiale."  ".$mat->forma_materiale." ".$mat->sezione_materiale."  [".$row->lunghezza_codice." mm]";
			}
		}

		return form_dropdown('codice_materiale_interno', $asd, $value,$class);
	}

	function ListaMaterialiFornitoreColumn($value,$row)
	{

			$query = $this->db->get_where('magazzino_materiali',array('codice_materiale_interno'=>$row->codice_materiale_interno));
			foreach ($query->result() as $mat);
			$query = $this->db->get_where('materiali',array('id_materiale'=>$mat->id_materiale));
			foreach ($query->result() as $nomemat);
			$asd=$nomemat->nome_materiale."  ".$mat->forma_materiale." ".$mat->sezione_materiale;

		return $asd;
	}


	function idordinefornitore()
	{
		$return="<input name='commessa' type='text' value=".$this->uri->segment(3)." readonly />";
		return $return;
	}

	function carica_nome_accessorio($value, $row){
		$this->db->select('nome_accessorio');
		$this->db->from('accessori');
		$this->db->where('id_accessorio', $row->id_accessorio);
		$prendi_accessorio = $this->db->get();
		foreach ($prendi_accessorio->result() as $accessorio);

		return $accessorio->nome_accessorio;
	}

}
