<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/////////////////////////////////////////////////////////////////count($ci->uri->segments)

class Fatturazione extends CI_Controller {

	function __construct()
	{
		parent::__construct();

		/* Standard Libraries */
		$this->load->database();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('date');
		$this->load->helper('security');
		$this->load->library('session');


		/* ------------------ */

		$this->load->library('grocery_CRUD');
		if(!$this->session->userdata('logged_in'))
			redirect('/login/show', 'refresh');


	}

	function build_menu(){
		$vars=array();
		$vars['tipo_op']=$this->session->userdata('tipo_op');
		$vars['nome_op']=$this->session->userdata('nome_operatore');


		extract($vars);
			#region buffering...
			ob_start();

			include(getcwd().'/application/views/menu_builder.php');

			$buffer = ob_get_contents();
		@ob_end_clean();
		return $buffer;
	}


	function tabella_fatture()
	{
			$crud = new grocery_CRUD();
			$crud->set_theme('datatables');
			$crud->set_table('fatture_clienti');
			// $crud->display_as('riferimenti_prev',"All'attenzione di:");
			$crud->display_as('id_condiz_pagam','Condizione Pagamento');
			$crud->display_as('id_cliente','Cliente');
			$crud->display_as('id_banca_cliente','Banca');
			$crud->add_action('Visualizza', '', 'fatturazione/ricarica_fattura_non_mod','ui-icon-plus');

			$crud->unset_add_fields('data_creazione_fattura','modificabile');
			$crud->change_field_type('num_fattura', 'hidden');

			$crud->set_relation('id_banca_cliente','banca_appoggio_cliente','nome_banca_cliente');
			$crud->set_relation('id_condiz_pagam','condizioni_di_pagamento','nome_condiz_pagam');
			$crud->set_relation('id_cliente','clienti','nome_cliente_documenti');
			// $crud->columns('num_preventivo','data_creazione_prev','id_cliente','ultimo_num_revisione','num_revisione_confermata');
			// $crud->unset_add();
			// $crud->unset_edit();
			// $crud->unset_delete();
			$crud->callback_before_insert(array($this,'before_insert_fattura_a_cliente'));
			$crud->callback_after_insert(array($this,'after_insert_fattura_a_cliente'));

			// $crud->order_by('num_preventivo','desc');

			$output = $crud->render();
			$output->header="FATTURE A CLIENTI";
			$output->footer="";

			$output->menu=$this->build_menu();

			$this->load->view('omis_view.php',$output);



	}

	function before_insert_fattura_a_cliente($post_array){
				$this->db->select_max('num_fattura');
				$this->db->where("data_creazione_fattura BETWEEN '".date('Y')."-01-01' AND '".date('Y')."-12-31'");
				$query = $this->db->get('fatture_clienti');

				foreach ($query->result() as $num_fattura_max);
				$post_array['num_fattura']=($num_fattura_max->num_fattura+1);

				return $post_array;
	}
	function after_insert_fattura_a_cliente($post_array,$insert_id){
				date_default_timezone_set('Europe/Rome');
				$this->db->where('id_fattura_cliente', $insert_id);
				$this->db->update('fatture_clienti', array('data_creazione_fattura'=>date('Y-m-d'),'modificabile'=>'S'));
				return $post_array;
	}

	function ricarica_fattura_non_mod(){
		$query = $this->db->get_where('fatture_clienti',array('id_fattura_cliente'=>$this->uri->segment(3)));
		foreach ($query->result() as $fattura);
		if($fattura->modificabile=='S')		redirect('/fatturazione/ricarica_fattura/'.$this->uri->segment(3), 'refresh');

		$query = $this->db->get_where('clienti',array('id_cliente'=>$fattura->id_cliente));
		foreach ($query->result() as $cliente);
		$query = $this->db->get_where('banca_appoggio_cliente',array('id_banca_cliente'=>$fattura->id_banca_cliente));
		if($query->num_rows()>0){
					foreach ($query->result() as $banca);
		}else{
			$banca=new stdClass();
			$banca->ABI="";
			$banca->CAB="";
			$banca->nome_banca_cliente="";
		}
		$query = $this->db->get_where('condizioni_di_pagamento',array('id_condiz_pagam'=>$fattura->id_condiz_pagam));
		foreach ($query->result() as $pagamento);


		$output=new stdClass();
		$output->id_fattura=$fattura->id_fattura_cliente;
		$output->id_cliente=$fattura->id_cliente;
		$output->nome_cliente=$cliente->nome_cliente_documenti;
		$output->indirizzo_cliente=$cliente->indirizzo;
		$output->citta_cliente=$cliente->citta;
		$output->nazione_cliente=$cliente->nazione;
		$output->email_cliente=$cliente->email_cliente;
		$output->banca_cliente=$banca->nome_banca_cliente;
		$output->abi_banca=$banca->ABI;
		$output->cab_banca=$banca->CAB;
		$output->tipo_pagamento=$pagamento->nome_condiz_pagam;

		$output->num_fattura=$fattura->num_fattura;
		@list($year,$month,$day) = explode("-",$fattura->data_creazione_fattura);
		$output->data_creazione_fattura=date ("d-m-Y",mktime (0,0,0,(int)$month , (int)$day , (int)$year));
		$output->nome_op=$this->session->userdata('nome_operatore');

		$output->base_url="http://".$_SERVER["SERVER_NAME"].":8000/";

		$query = $this->db->get_where('righe_fatture',array('id_fattura_cliente'=>$this->uri->segment(3)));
		$output->tabella_descrizione="";
		$ddt_corrente="";
		foreach ($query->result() as $riga){
			$query = $this->db->get_where('classi_iva',array('id_classe_iva'=>$riga->id_classe_iva));
			if($query->num_rows()>0){
				foreach ($query->result() as $classe);

			}else{
				$classe=new stdClass();
				$classe->valore_classe=0;
				$classe->codice_articolo="";
			}

			if(($riga->sconto=='0')||($riga->sconto==NULL)) $sconto='';
			else $sconto=$riga->sconto."%";

			if($riga->prezzo==NULL) $prezzo='';
			else $prezzo=number_format($riga->prezzo,2)." &#8364;";

			if($riga->quantita==NULL) $nr='';
			else $nr="Nr";


			if($riga->id_ddt!=$ddt_corrente){
				$ddt_corrente=$riga->id_ddt;
				$output->tabella_descrizione.='
										<div class="row" style="margin-top:10px;">
											<div class="col-sm-6">&nbsp;</div>
											<div class="col-sm-1"></div>
											<div class="col-sm-1></div>
											<div class="col-sm-1></div>
											<div class="col-sm-1></div>
											<div class="col-sm-1></div>
										</div>
									';
				$tasto_dettaglio="  <a href='".site_url("ddt/ricarica_ddt/".$riga->id_ddt)."''><input type='button' value='DETTAGLIO DDT'/></a>";
			}
			else{$tasto_dettaglio="";}
			$output->tabella_descrizione.='
										<div class="row" style="margin-top:10px;">
											<div class="col-sm-6" id_riga="'.$riga->id_riga_fattura.'" class="descrizione">'.$riga->descrizione.$tasto_dettaglio.'</div>
											<div class="col-sm-1">'.$nr.'</div>
											<div class="col-sm-1 qta_riga" value="'.$riga->quantita.'">'.$riga->quantita.'</div>
											<div class="col-sm-1 prezzo_riga" style="padding-right:5px;" prezzo="'.$riga->prezzo.'" valore_iva="'.$classe->valore_classe.'">'.$prezzo.' </div>
											<div class="col-sm-1 sconto_riga" style="padding-right:5px;"  value="'.$riga->sconto.'">'.$sconto.'&nbsp;</div>
											<div class="col-sm-1 totale_riga" id_riga="'.$riga->id_riga_fattura.'"> </div>
											<div class="col-sm-1 " id_riga="'.$riga->id_riga_fattura.'">'.$classe->codice_articolo.'</div>
										</div>
									';
		}
		$output->tabella_descrizione.='
										<div class="row" style="margin-top:10px;">
											<div class="col-sm-6"><b>NOTE AGGIUNTIVE:</b></div>
											<div class="col-sm-1"></div>
											<div class="col-sm-1" ></div>
											<div class="col-sm-1" ></div>
											<div class="col-sm-1" ></div>
											<div class="col-sm-1" > </div>
											<div class="col-sm-1" ></div>
										</div>
									';
		$output->tabella_descrizione.='
										<div class="row" style="margin-top:10px;">
											<div class="col-sm-6"><textarea style="width:100%;" class="note_fatturazione">'.$fattura->note_fatturazione.'</textarea></div>
											<div class="col-sm-1"></div>
											<div class="col-sm-1" ></div>
											<div class="col-sm-1" ></div>
											<div class="col-sm-1" ></div>
											<div class="col-sm-1" > </div>
											<div class="col-sm-1" ></div>
										</div>
									';
		//GENERO TABELLA DA METTERE NEL FOOTER DELLA FATTURA
		$output->tabella_footer="";
		$classe_iva_corrente="";
		$somma=0;
		$this->db->where("id_fattura_cliente = ".$this->uri->segment(3)." AND id_classe_iva IS NOT NULL");
		$this->db->order_by("id_classe_iva", "asc");
		$query = $this->db->get('righe_fatture');
		$totale_documento=0;
		$righe_footer=0;
		foreach ($query->result() as $riga){
			if($riga->id_classe_iva!=$classe_iva_corrente)
			{
				if($classe_iva_corrente!=""){
					$righe_footer++;
					$query = $this->db->get_where('classi_iva',array('id_classe_iva'=>$classe_iva_corrente));
					foreach ($query->result() as $classe);
					$output->tabella_footer.=' <div class="row" style="border-left:1px solid black; border-right:1px solid black; padding:4px;">
	                <div class="col-sm-2">'.$classe->codice_articolo.'</div><div class="col-sm-4">'.number_format($somma,2).'&#8364;</div><div class="col-sm-4">'.$classe->nome_classe_iva.'</div><div class="col-sm-2">'.number_format(($somma*($classe->valore_classe)/100),2).'&#8364;</div>
	              </div>';
	              	$totale_documento+=$somma+($somma*($classe->valore_classe)/100);
				}

				$classe_iva_corrente=$riga->id_classe_iva;
				$somma=0;
			}
			$somma+=$riga->prezzo;

		}
		$query = $this->db->get_where('classi_iva',array('id_classe_iva'=>$classe_iva_corrente));
		foreach ($query->result() as $classe);
		$output->tabella_footer.=' <div class="row" style=" border-left:1px solid black; border-right:1px solid black; padding:4px;">
        <div class="col-sm-2">'.$classe->codice_articolo.'</div><div class="col-sm-4">'.number_format($somma,2).'&#8364;</div><div class="col-sm-4">'.$classe->nome_classe_iva.'</div><div class="col-sm-2">'.number_format(($somma*($classe->valore_classe)/100),2).'&#8364;</div>
      </div>';
	    $totale_documento+=$somma+($somma*($classe->valore_classe)/100);

	    for($i=0;$i<(2-$righe_footer);$i++){
	    	$output->tabella_footer.=' <div class="row" style=" border-left:1px solid black; border-right:1px solid black; padding:4px;">
        <div class="col-sm-2">&nbsp;</div><div class="col-sm-4">&nbsp;</div><div class="col-sm-4">&nbsp;</div><div class="col-sm-2">&nbsp;</div>
      </div>';
	    }
	    $output->tabella_footer.=' <div class="row" style=" border-top:1px solid black; padding:4px;">
        <div class="col-sm-2"></div><div class="col-sm-4"></div><div class="col-sm-4"></div><div class="col-sm-2"></div>
      </div>';

	    $output->totale_documento=number_format($totale_documento,2);
		$output->menu=$this->build_menu();
		$this->load->view('fatture_view.php',$output);


	}

	function calcola_fatturazione_ddt(){
		date_default_timezone_set('Europe/Rome');

		$mesi = array("asd","Gennaio","Febbraio","Marzo","Aprile","Maggio","Giugno","Luglio","Agosto","Settembre","Ottobre","Novembre","Dicembre");

		$month=date("n");
		$year=date("Y");
		if($month==1){
			$month=12;
			$year--;
		}
		else $month--;
		$this->db->where("data_creazione_ddt BETWEEN '".date("Y-m-d", strtotime($year."-".$month."-01"))."' AND '".date("Y-m-t", strtotime($year."-".$month."-01"))."' AND tipo_ddt = 'C' AND ddt_fatturato='N'");
		$query = $this->db->get('tabella_ddt');
		if($query->num_rows()>0)
			echo "Verranno fatturati ".$query->num_rows()." DDT per il mese di ".$mesi[$month]." ".$year;
		else
			echo http_response_code(500);
	}

	function esegui_fatturazione_ddt(){
		date_default_timezone_set('Europe/Rome');
		$month=date("n");
		$year=date("Y");
		if($month==1){
			$month=12;
			$year--;
		}
		else $month--;
		$this->db->where("data_creazione_ddt BETWEEN '".date("Y-m-d", strtotime($year."-".$month."-01"))."' AND '".date("Y-m-t", strtotime($year."-".$month."-01"))."' AND tipo_ddt = 'C' AND ddt_fatturato='N'");
		$this->db->order_by("id_cliente", "asc");
		$query = $this->db->get('tabella_ddt');
		$cliente_attuale="";
		if($query->num_rows()>0){
			$data_fattura_cliente=date("Y-m-t", strtotime($year."-".$month."-01"));
			foreach ($query->result() as $ddt){
				if($ddt->id_cliente!=$cliente_attuale){ //se cambia il cliente creo una nuova fattura
					$cliente_attuale=$ddt->id_cliente;
					//insert nuova riga in fatture_clienti
					$this->db->select_max('num_fattura');
					$this->db->where("data_creazione_fattura BETWEEN '".date('Y')."-01-01' AND '".date('Y')."-12-31'");
					$query = $this->db->get('fatture_clienti');
					foreach ($query->result() as $num_fattura_max);

					$query = $this->db->get_where('clienti',array('id_cliente'=>$ddt->id_cliente));
					foreach ($query->result() as $cliente);

					$num_fatt=($num_fattura_max->num_fattura+1);

					$dati['num_fattura']=$num_fatt;
					$dati['id_cliente']=$ddt->id_cliente;
					$dati['id_banca_cliente']=$cliente->id_banca_cliente;
					$dati['id_condiz_pagam']=$cliente->id_condiz_pagam;
					$dati['data_creazione_fattura']=$data_fattura_cliente;
					$dati['modificabile']='N';
					$dati['note_fatturazione']=$cliente->note_fatturazione;

					$this->db->insert('fatture_clienti',$dati);
					$id_fattura_cliente=$this->db->insert_id();
				}
				@list($year,$month,$day) = explode("-",$ddt->data_creazione_ddt);
				unset($riga);
				$riga = array();
				$riga['id_fattura_cliente']=$id_fattura_cliente;
				$riga['id_ddt']=$ddt->id_ddt;
				$riga['descrizione']="*DDT ".$ddt->num_ddt." del ".date ("d-m-Y",mktime (0,0,0,(int)$month , (int)$day , (int)$year))."*";
				$this->db->insert('righe_fatture',$riga);
				$query = $this->db->get_where('ddt_commesse',array('id_ddt'=>$ddt->id_ddt));
				foreach ($query->result() as $ddt_commessa){//per ogni ddt cerco le commesse che lo compongono
					$query = $this->db->get_where('lavori',array('commessa'=>$ddt_commessa->commessa));
					foreach ($query->result() as $lavoro);

					if(substr($lavoro->ordine_cliente, 0, 4)=="mail") $ordine_nome="mail";
					else $ordine_nome=$lavoro->ordine_cliente;

					if($commessa->data_ordine_cliente!=NULL){
									@list($year,$month,$day) = explode("-",$commessa->data_ordine_cliente);
									$data_ordine_cliente=date ("d-m-Y",mktime (0,0,0,(int)$month , (int)$day , (int)$year));
								}else{$data_ordine_cliente="";}

					if($lavoro->ordine_cliente!=NULL) $ordine="  Vs Ordine: <b>".$ordine_nome."</b> del ".$data_ordine_cliente;
					else $ordine="";

					unset($riga);
					$riga = array();
					$riga['id_fattura_cliente']=$id_fattura_cliente;
					$riga['id_ddt']=$ddt->id_ddt;
					$riga['commessa']=$ddt_commessa->commessa;
					$riga['descrizione']="Ns. Riferimento: <b>".$lavoro->commessa."</b>".$ordine;
					$this->db->insert('righe_fatture',$riga);

					if($ddt_commessa->stampo_nome_peso!=NULL){
						unset($riga);
						$riga = array();
						$riga['id_fattura_cliente']=$id_fattura_cliente;
						$riga['id_ddt']=$ddt->id_ddt;
						$riga['commessa']=$ddt_commessa->commessa;
						$riga['descrizione']=$ddt_commessa->stampo_nome_peso;
						$this->db->insert('righe_fatture',$riga);
					}

					$this->db->order_by("id_riga_ddt", "asc");
					$query = $this->db->get_where('righe_ddt',array('id_ddt'=>$ddt->id_ddt,'commessa'=>$ddt_commessa->commessa));
					foreach ($query->result() as $riga_ddt){
						$riga['id_fattura_cliente']=$id_fattura_cliente;
						$riga['id_ddt']=$ddt->id_ddt;
						$riga['commessa']=$ddt_commessa->commessa;
						$riga['descrizione']=$riga_ddt->descrizione;
						$riga['quantita']=$riga_ddt->quantita;
						$riga['prezzo']=$riga_ddt->prezzo;
						$riga['id_classe_iva']=$riga_ddt->id_classe_iva;
						$riga['sconto']=$riga_ddt->sconto;

						$this->db->insert('righe_fatture',$riga);
					}
				}
				if($ddt->peso_netto!=0.00){
					unset($riga);
					$riga = array();
					$riga['id_fattura_cliente']=$id_fattura_cliente;
					$riga['id_ddt']=$ddt->id_ddt;
					$riga['descrizione']="Peso netto: ".$ddt->peso_netto." kg";
					$this->db->insert('righe_fatture',$riga);
				}



			}

		}

		echo "ok";
	}
	function ricarica_fattura(){

	}

	function tabella_fatture_fornitori()
	{
			date_default_timezone_set('Europe/Rome');

			$crud = new grocery_CRUD();
			$crud->set_theme('datatables');
			$crud->set_table('fatture_fornitori');


			$crud->order_by('id_riga_fatture_fornitori','desc');
			$crud->display_as('id_fornitore', "Fornitore");
			$crud->display_as('tipo_fattura', "TF");
			$crud->display_as('id_riga_fatture_fornitori', "Num");
			$crud->display_as('id_classe_iva_omis', "Classe IVA");
			$crud->display_as('id_modalita_pagamento','Modalità pagamento');

			$crud->set_relation('id_fornitore','fornitori','ragione_soc');
			$crud->set_relation('id_classe_iva_omis','classi_iva','nome_classe_iva');
			$crud->set_relation('id_modalita_pagamento','modalita_pagamento_a_fornitore','nome_modalita_pagamento');
			$crud->columns('id_riga_fatture_fornitori','num_protocollo','tipo_fattura',
							'id_fornitore','id_tipo_pagamento','num_fattura_fornitore','data_fattura_fornitore','data_scadenza_fattura', 'data_effettuato_pagamento','importo','imponibile','importo_iva','note_fattura');
			$crud->unset_texteditor('note_fattura');
			//$crud->unset_add_fields('imponibile','iva','anno_inserimento_fattura');

			$crud->add_fields('id_fornitore','id_tipo_pagamento','tipo_fattura','num_fattura_fornitore',
								'data_fattura_fornitore','id_classe_iva_omis','importo','id_modalita_pagamento','data_effettuato_pagamento','data_scadenza_fattura','note_fattura');

			$crud->unset_edit_fields("importo_iva");
			$crud->callback_after_insert(array($this,'after_insert_fattura'));
			$crud->callback_after_update(array($this,'after_update_fattura'));

			$crud->callback_column('tipo_fattura',array($this,'tipo_fattura_column'));
			$crud->set_relation('id_tipo_pagamento','tipi_pagamento','nome_tipo_pagamento');

			$crud->display_as('id_tipo_pagamento', "Tipo Pagamento");
			if($this->session->userdata('nome_operatore')!="LORI"){
				$crud->unset_add();
				$crud->unset_edit();
				$crud->unset_delete();
			}

			// $crud->order_by('num_ddt','desc');
			$crud->set_css('assets/css/tabella_fatture_fornitori.css');
			$crud->set_js('assets/js/tabella_fatture_fornitori.js');

			$crud->add_action('Documento', '', '','',array($this,'apri_pdf_fattura_fornitore'));
			$crud->add_action('Pagato!', '', '','pagato-button');

			$output = $crud->render();
			$output->header="FATTURE FORNITORI";
			$output->footer="";

			$output->menu=$this->build_menu();

			$this->load->view('omis_view.php',$output);



	}

	function tipo_fattura_column($value,$row){
			if(($row->data_scadenza_fattura!=NULL)&&($row->data_effettuato_pagamento==NULL))
			{
				$date1 = new DateTime($row->data_scadenza_fattura);
				$date2 = new DateTime(date("Y-m-d"));
				$interval = $date2->diff($date1);
					if((($interval->days)<10)&&($date1>$date2)){
						return "<span class='pagare'>".$value."</span>";
					}
					else return $value;
			}else{
				return $value;
			}
	}

	function pagata_singola_fattura(){

		$this->db->where('id_riga_fatture_fornitori', $_POST['id_riga']);
		$this->db->update('fatture_fornitori', array('data_effettuato_pagamento'=> date('Y-m-d')));

		echo date('d M Y');

	}

	function after_insert_fattura($post_array, $primary_key) {
		date_default_timezone_set('Europe/Rome');
		if($post_array['tipo_fattura']!='B'){

			$query = $this->db->get_where('classi_iva',array('id_classe_iva'=>$post_array['id_classe_iva_omis']));
			foreach ($query->result() as $classe_iva);

			$imponibile=$post_array['importo']/(1+(($classe_iva->valore_classe)/100));
			$importo_iva=$imponibile*(($classe_iva->valore_classe)/100);

		}else{
			$imponibile=NULL;
			$importo_iva=NULL;
		}


		//calcolo data scadenza in base al tipo di pagamento che si ha con il fornitore
		if(($post_array['data_scadenza_fattura']==NULL)&&($post_array['id_modalita_pagamento']!=NULL))
		{
			if($post_array['data_fattura_fornitore']!=NULL){


				@list($day,$month,$year) = explode("/",$post_array['data_fattura_fornitore']);

				switch((int)$post_array['id_modalita_pagamento']){
					case 1: //30gg
						$month++;
					break;
					case 2: //60gg
						$month+=2;
					break;
					case 3: //90gg
						$month+=3;
					break;
					case 4: //120gg
						$month+=4;
					break;
					case 5: //60gg al 10 del mese
						$month+=2;
					break;
					case 6: //30gg precisi non a fine mese
						$month++;
					break;
					case 7: //90gg al 10 del mese
						$month+=3;
					break;
				}

				if($month>12){ //se sforo dicembre, ricalcolo mese e anno dopo
					$month=$month-12;
					$year++;
				}
				if(((int)$post_array['id_modalita_pagamento'])<5){ //cioè casistiche in cui calcolo i giorni a fine mese
					if($month==12)//se la data calcolata è dicembe devo spostare in avanti di 1 mese e 10 giorni e 1 anno
					{
						$data_scadenza=date("Y-m-d", strtotime(($year+1)."-1-10"));

					}else{
						if($month==8){//se la data calcolata è agosto devo spostare in avanti di 1 mese e 10 giorni
							$data_scadenza=date("Y-m-d", strtotime($year."-9-10"));
						}
						else{
								$data_scadenza=date("Y-m-t", strtotime($year."-".$month."-".$day));
						}
					}
				}else{
					if((((int)$post_array['id_modalita_pagamento'])==5)||(((int)$post_array['id_modalita_pagamento'])==7)){ //cioè 60gg + 10 giorni o 90 gg + 10 giorni
						$month++;
						if($month>12){ //se sforo dicembre, ricalcolo mese e anno dopo
											$month=$month-12;
											$year++;
										}
						$data_scadenza=date("Y-m-d", strtotime($year."-".$month."-10"));
					}
					if(((int)$post_array['id_modalita_pagamento'])==6){       //cioè 30gg precisi
						if($month>12){ //se sforo dicembre, ricalcolo mese e anno dopo
											$month=$month-12;
											$year++;
										}
						$data_scadenza=date("Y-m-d", strtotime($year."-".$month."-".$day));
					}
				}

				$this->db->where('id_riga_fatture_fornitori', $primary_key);
				$this->db->update('fatture_fornitori', array('data_scadenza_fattura'=> $data_scadenza));

				// if($post_array['id_tipo_pagamento']==10){ //se è una rbi devo copiare le date
				// 	@list($year,$month,$day) = explode("-",$data_scadenza);
				// 	$this->db->where('id_riga_fatture_fornitori', $primary_key);
				// 	$this->db->update('fatture_fornitori', array('data_effettuato_pagamento'=> $year."-".$month."-".$day));
				// }

			}
		}else{
			// if($post_array['id_tipo_pagamento']==10){ //se è una rbi devo copiare le date, quali precisamente?
			// @list($day,$month,$year) = explode("/",$post_array['data_scadenza_fattura']);
			// $this->db->where('id_riga_fatture_fornitori', $primary_key);
			// $this->db->update('fatture_fornitori', array('data_effettuato_pagamento'=> $year."-".$month."-".$day));
		//}
		}

		$anno_inserimento=date('Y');

		$this->db->where('id_riga_fatture_fornitori', $primary_key);
		$this->db->update('fatture_fornitori', array('imponibile'=> $imponibile,'importo_iva'=>$importo_iva,'anno_inserimento_fattura'=>$anno_inserimento));

	}

	function after_update_fattura($post_array, $primary_key) {
		if($post_array['tipo_fattura']!='B'){

			$query = $this->db->get_where('classi_iva',array('id_classe_iva'=>$post_array['id_classe_iva_omis']));
			foreach ($query->result() as $classe_iva);

			$imponibile=$post_array['importo']/(1+(($classe_iva->valore_classe)/100));
			$importo_iva=$imponibile*(($classe_iva->valore_classe)/100);

			$this->db->where('id_riga_fatture_fornitori', $primary_key);
			$this->db->update('fatture_fornitori', array('imponibile'=> $imponibile,'importo_iva'=>$importo_iva));
		}

	}

	function apri_pdf_fattura_fornitore($primary_key , $row){
			$query = $this->db->get_where('fornitori',array('id_fornitore'=>$row->id_fornitore));
			if($query->num_rows()>0){
				foreach ($query->result() as $forn);
				return "ftp://".$_SERVER["SERVER_NAME"].":2222/fatturefornitori/".$forn->ragione_soc."/".substr($row->data_fattura_fornitore, -4)."/".$row->num_fattura_fornitore.".pdf";
			}
			else return "";

	}


}
