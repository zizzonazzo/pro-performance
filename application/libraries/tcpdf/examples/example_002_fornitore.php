<?php

// Include the main TCPDF library (search for installation path).
require_once('tcpdf_include.php');

class MYPDF extends TCPDF {

    // Page header
    public function Header() {
        $this->SetFont('dejavusans', '', 8, '', true);
        $header = file_get_contents("http://".$_SERVER['SERVER_NAME'].":8000/index.php/controllerpdf/header_pdf_ddt/".$_POST['id_ddt']);

        $this->writeHTMLCell(0, 20, 8, 5, $header, 0, 1, 0, true, '', true);

    }

    // Page footer
    public function Footer() {
        $this->SetFont('dejavusans', '', 8, '', true);
        $footer = file_get_contents("http://".$_SERVER['SERVER_NAME'].":8000/index.php/controllerpdf/footer_pdf_ddt/".$_POST['id_ddt']);

        $this->writeHTMLCell(0, 0, 8, 243, $footer, 0, 1, 0, true, '', true);
		$this->Cell(0, 5, 'Pagina '.$this->getAliasNumPage().' di '.$this->getAliasNbPages(), 0, false, 'R', 0, '', 0, false, 'T', 'M');

    }
}
// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('OMIS srl');
$pdf->SetTitle('DDT');


// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);
$pdf->SetAutoPageBreak(TRUE, 55);
// $pdf->SetMargins(0, 20, 0);
$pdf->SetMargins(8, 82);
$pdf->SetFooterMargin(10);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
$pdf->SetFont('dejavusans', '', 8, '', true);

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

// set text shadow effect
//$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

// Set some content to print
// $html = <<<EOD

// EOD;
// Print text using writeHTMLCell()


$html = file_get_contents("http://".$_SERVER['SERVER_NAME'].":8000/index.php/controllerpdf/corpo_pdf_ddt/".$_POST['id_ddt']);

$pdf->writeHTML($html, true, 0, true, 0);



// ---------------------------------------------------------
//$pdf->render();
// Close and output PDF document
// This method has several options, check the source code documentation for more information.
//$pdf->Output('/example_001.pdf', 'F');
// file_put_contents("filename.html", $html);
// $pdf->writeHTML($html, true, 0, true, 0);


$asd=$pdf->Output('example_002.pdf', 'S');
file_put_contents("E:/Ufficio/DDT_Fornitori/".$_POST['num_ddt']."_".$_POST['anno_ddt'].".pdf", $asd);

//============================================================+
// END OF FILE
//============================================================+
