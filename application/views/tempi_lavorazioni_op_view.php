<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>OMIS</title>
	<link type="text/css" rel="stylesheet" href="http://<?php echo $_SERVER['SERVER_NAME'].':8000/assets/css/libraries/jquery-ui.min.css'?>"/>
	<link type="text/css" rel="stylesheet" href="http://<?php echo $_SERVER['SERVER_NAME'].':8000/assets/themes/datatables/css/datatables.css'?>"/>
	<link type="text/css" rel="stylesheet" href="http://<?php echo $_SERVER['SERVER_NAME'].':8000/assets/css/menutendina.css'?>"/>
  <link type="text/css" rel="stylesheet" href="http://<?php echo $_SERVER['SERVER_NAME'].':8000/assets/themes/datatables/css/bootstrap.css'?>"/>

	<script src="http://<?php echo $_SERVER['SERVER_NAME'].':8000/assets/js/libraries/jquery-1.7.1.min.js'?>"></script>

  <script src="http://<?php echo $_SERVER['SERVER_NAME'].':8000/assets/themes/datatables/js/ricerca_globale_op.js'?>"></script>
  <script src="http://<?php echo $_SERVER['SERVER_NAME'].':8000/assets/themes/datatables/js/tabella_tempi_operatore.js'?>"></script>

 <script type='text/javascript'>
  var base_url = '<?php echo $_SERVER['SERVER_NAME'].':8000'?>';

</script>


  </head>
  <body>
    <?php echo $menu; ?>

    <div class="container-fluid" style="margin-top:55px;">
        <div class="row">
          <div class="col-sm-2"></div>
          <div class="col-sm-8" >
              <div  align="center" class="risultato_ric_glob" style="overflow: visible;"></div>
          </div>
            <div class="col-sm-2 tasto_chiudi_ricerca"></div>
        </div>
        <div class="row">
            <div class="col-sm-4"></div>
            <div class="col-sm-4" align="center">
             <font color="red" size="5">  <?php echo $header; ?></font>
            </div>
            <div class="col-sm-4"></div>
        </div>

        <div class="row" style='margin-top:20px;' >
            <div class="col-sm-1"></div>
            <div class="col-sm-10">
              <?php echo $footer; ?>
            </div>
            <div class="col-sm-1"></div>
        </div>
          <div class="row">
            <div class="col-sm-12">
                              <?php echo $output; ?>

            </div>
          </div>
    </div>

<div id="dialog" title="Immagine Accessorio">
  <img src="" style="max-width: 100%; ">
</div>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
  </body>
</html>
