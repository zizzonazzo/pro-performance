<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo $titolo?> DETTAGLIO - OMIS Gestione Schede</title>
<?php
foreach($css_files as $file): ?>
	<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<?php foreach($js_files as $file): ?>
	<script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>
 <link rel="stylesheet" href="http://<?php echo $_SERVER['SERVER_NAME'].':8000/assets/themes/datatables/css/animate.css'?>" type='text/css'>
   <link type="text/css" rel="stylesheet" href="http://<?php echo $_SERVER['SERVER_NAME'].':8000/assets/themes/datatables/css/bootstrap.css'?>" />
  <script src="http://<?php echo $_SERVER['SERVER_NAME'].':8000/assets/themes/datatables/js/gestore_inserimento_dati_componente.js'?>" type="text/javascript"></script>


<style>
  .multiple-select{
    background-color: yellow;
  }
</style>


    <!-- Latest compiled and minified CSS -->
<!-- 	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:300,700"> -->
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
        <!-- Static navbar -->
    <?php echo $menu; ?>

    <div class="container-fluid" style="margin-top:55px;">
        <div class="row">
          <div class="col-sm-2"></div>
          <div class="col-sm-8" >
              <div  align="center" class="risultato_ric_glob" style="overflow: visible;"></div>
          </div>
            <div class="col-sm-2 tasto_chiudi_ricerca"></div>
        </div>
        <div class="row">
            <div class="col-sm-4"></div>  
            <div class="col-sm-4" align="center">
             <font color="red" size="5">  <?php echo $titolo_header; ?></font>
            </div>
            <div class="col-sm-4"></div>  
        </div>
        <div class="row">
        		<div class="col-sm-12">
        			<?php echo $intestazione; ?>
        		</div>
        </div>
        <div class="row" style='margin-top:20px;' >
            <div class="col-sm-1"></div>  
            <div class="col-sm-10">
              <?php echo $footer; ?>
            </div>
            <div class="col-sm-1"></div>  
        </div>
        <div class='row' style="margin-top:10px;" >
              <div class="col-sm-4"></div>
                <div id='title' class="col-sm-4" align="center">
                	<a href='<?php echo site_url('omis/dettaglioop/componenti/'.$commessa)?>'><font size="3">Componenti</font></a> |
          				<a href='<?php echo site_url('omis/dettaglioop/semilavorati/'.$commessa)?>'><font size="3">Semilavorati</font></a> | 
          				<a href='<?php echo site_url('omis/dettaglioop/accessori/'.$commessa)?>'><font size="3">Accessori</font></a> |
          				<a href='<?php echo site_url('omis/dettaglioop/fasi/'.$commessa)?>'><font size="3">Fasi</font></a> |
          				<a href='<?php echo site_url('omis/dettaglioop/lav_esterne/'.$commessa)?>'><font size="3">Lavorazioni esterne</font></a> 
                </div>
                <div class="col-sm-4"></div>
        	</div>
          <div class="row">
            <div class="col-sm-12">
                              <?php echo $output; ?>

            </div>
          </div>
    </div>
    
<div id="dialog" title="Immagine Accessorio">
  <img src="" style="max-width: 100%; ">
</div>
<div class="modal fade" id="myModalDati" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title">INSERIMENTO MULTIPLO DATI</h4>
          </div>
          <div class="modal-body">
            <div class="row" style="padding-left:20px;">
              OPERATORE:<font style="color:red"><b> <?php echo $nome_op; ?></b></font>
            </div>
            <div class="row" style="padding-left:20px;">
               <form method="POST" action="/index.php/omis/salvaComponente">
                  <input type="hidden" name="id_materiale_modal" value="">
                  <input type="hidden" name="non_conformi"  value="0">
                      <div class='row' style="margin-top:10px; border-top:1px solid black; padding-top:20px;">
                        <div class="col-sm-12" ><span style='color:red; font-size:130%;'>DATI COMPONENTE:</span></div>
                      </div>
                      <div class="row" style="margin-top:10px;">
                        <div class="col-xs-6">Materiale </div>
                        <div class="col-xs-6"><span id="span_materiale" style="font-size:120%;"></span></div>            
                      </div>
                      <div class="row" style="margin-top:10px;">
                        <div class="col-xs-6">FORMA</div>
                        <div class="col-xs-6"><span id="span_forma" style="font-size:120%;"></span></div>        
                      </div>
                      <div class="row" style="margin-top:10px;">
                        <div class="col-xs-6">SEZIONE </div>
                        <div class="col-xs-6"><span id="span_sezioni" style="font-size:120%;"></span></div>
                      </div>
                      <div class="row" style="margin-top:10px;">
                        <div class="col-xs-6"><span id="span_11" ></span></div>
                        <div class="col-xs-6"><span id="span_12" style="font-size:120%;"></span></div>  
                      </div>
                       <div class="row" style="margin-top:10px; padding-bottom:20px; border-bottom:1px solid black;">
                        <div class="col-xs-6"><span id="span_21"></span></div>
                        <div class="col-xs-6"><span id="span_22" style="font-size:120%;"></span></div>    
                      </div>
                      <div class="row" style="margin-top:20px; padding-right:30px;">
                        <div class="col-xs-12" align="right"><input class="btn btn-primary" style="font-size:15pt;" type="submit" value="SALVA"></div>
                      </div>
                </form>     
            </div>
          </div>
    </div>
  </div>
</div>
<div id="dialog" title="Immagine Accessorio">
  <img src="" style="max-width: 100%; ">
</div>

<div class="modal fade" id="myModalTempi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title">INSERIMENTO MULTIPLO TEMPI</h4>
          </div>
          <div class="modal-body">
            <div class="row" style="padding-left:20px;">
              OPERATORE:<font style="color:red"><b> <?php echo $nome_op; ?></b></font>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancella</button> 
            <button type="button" class="btn btn-primary inviaofferta"> <span class="glyphicon glyphicon-send" aria-hidden="true"></span> &nbsp; Invia</button>
            
          </div>
    </div>
  </div>
</div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script language="javascript" src="http://<?php echo $_SERVER['SERVER_NAME'].':8000/assets/themes/datatables/js/gestione_inserimenti_multipli.js'?>" type="text/javascript"></script>

  </body>
</html>