<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo $titolo?> DETTAGLIO - OMIS Gestione Schede</title>
<?php
foreach($css_files as $file): ?>
	<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<?php foreach($js_files as $file): ?>
	<script src="<?php echo $file; ?>"></script>

<?php endforeach; ?>
<script src="http://code.jquery.com/ui/1.11.1/jquery-ui.min.js"></script>

 <link rel="stylesheet" href="http://<?php echo $_SERVER['SERVER_NAME'].':8000/assets/themes/datatables/css/animate.css'?>" type='text/css'>

    <!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:300,700">
  <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css" />

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
        <!-- Static navbar -->
     <?php echo $menu; ?>

    <div class="container-fluid" style="margin-top:55px;">
        <div class="row">
          <div class="col-sm-2"></div>
          <div class="col-sm-8" >
              <div  align="center" class="risultato_ric_glob" style="overflow: visible;"></div>
          </div>
            <div class="col-sm-2 tasto_chiudi_ricerca"></div>
        </div>
        <div class="row">
            <div class="col-sm-4"></div>  
            <div class="col-sm-4" align="center">
             <font color="red" size="5">  <?php echo $titolo_header; ?></font>
            </div>
            <div class="col-sm-4"></div>  
        </div>
        <div class="row">
        		<div class="col-sm-12">
        			<?php echo $intestazione; ?>
        		</div>
        </div>
        <div class="row" style='margin-top:20px;' >
            <div class="col-sm-4"></div>  
            <div class="col-sm-4">
              <?php echo $footer; ?>
            </div>
            <div class="col-sm-4"></div>  
        </div>
        <div class='row' style="margin-top:10px;" >
              <div class="col-sm-4"></div>
                <div id='title' class="col-sm-4" align="center">
                	<a href='<?php echo site_url('omis/dettaglio/componenti/'.$commessa)?>'><font size="3">Componenti</font></a> |
          				<a href='<?php echo site_url('omis/dettaglio/semilavorati/'.$commessa)?>'><font size="3">Semilavorati</font></a> | 
          				<a href='<?php echo site_url('omis/dettaglio/accessori/'.$commessa)?>'><font size="3">Accessori</font></a> |
          				<a href='<?php echo site_url('omis/dettaglio/fasi/'.$commessa)?>'><font size="3">Fasi</font></a> |
          				<a href='<?php echo site_url('omis/dettaglio/lav_esterne/'.$commessa)?>'><font size="3">Lavorazioni esterne</font></a> 
                </div>
                <div class="col-sm-4"></div>
        	</div>
          <div class="row">
            <div class="col-sm-3"></div>
             <div class="col-sm-6" align="center">
               <?php echo $ordini_semilavorati; ?>
             </div> 
            <div class="col-sm-3"></div>
          </div>
          <div class="row">
            <div class="col-sm-12">
                              <?php echo $output; ?>

            </div>
          </div>
    </div>
    

<div id="dialog" title="Immagine Accessorio">
  <img src="" style="max-width: 100%; ">
</div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
  </body>
</html>