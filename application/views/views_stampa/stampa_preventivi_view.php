<html lang="en">
<head>
  <meta http-equiv="Content-type" content="text/html; charset=utf-8" />

	<link href='http://fonts.googleapis.com/css?family=Noto+Sans:400Italic' rel='stylesheet' type='text/css'>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>

	<script language="javascript" src="http://<?php echo $_SERVER['SERVER_NAME'].':8000/assets/themes/datatables/js/printer.js'?>" type="text/javascript"></script>
	<link type="text/css" rel="stylesheet" href="http://<?php echo $_SERVER['SERVER_NAME'].':8000/assets/css/preventivi.css'?>" />

	<title>Preventivo</title>
	<meta name="author" content="Marco Scoppetta">
	<!-- Date: 2013-03-25 -->
</head>

<body style="font-family: 'Noto Sans', sans-serif;">

<style type="text/css" media="print">
    #backbutton {
    display:none!important;
}
</style>


<a id="backbutton" href="/index.php/preventivi/ricarica_prev/<?php echo $id_preventivo; ?>/<?php echo $num_revisione; ?>"> TORNA INDIETRO</a>
<table><tr><td>
	<table width="1029px" style="border:1px solid black;" cellpadding="10" >
		<tr><td>
	<table width="100%" style="text-align: left;">

	  <tbody>
	    <tr  >
	    	<td >
				<img src="/application/views/icons/omis_bronzo_sps_09_2.jpg" width="200" height="150">

			</td>
			<td style="line-height:150%" >
				Via Olona 183/B <br>
				21013 GALLARATE (VA)  ITALY <br>
				<br>
				<div style="font-size:13px;">C.F. - P.IVA: 02554680120 <br>
				Cap.Soc. Euro 15.000,00 int.vers. <br>
				C.C.I.A.A. 267346 Reg. Impr. 02554680120 <div>
			</td>
			<td style="line-height:150%" >
				Tel. 0331/776097 <br>
				Fax 0331/772290 <br>
				E-mail: omis@omis.it <br>
				PEC: omis@ticertifica.it <br>
				<br>
				www.omis.it
			</td>
	    </tr>

	  </tbody>
	</table>
	</td></tr>
	</table>
	<table width="1029px" cellpadding="10">
			<tr><td width="31%" valign="top"><br>Alla cortese attenzione di: <br><br><div class='riferimenti_prev'><?php echo $riferimenti_cliente; ?></div> <br><br>
				<br><br><strong> OFFERTA <?php echo $num_preventivo; ?>/<?php echo $num_revisione; ?> del <?php echo $data_offerta; ?></strong></td>
				<td width="35%" style="border:1px solid black;"><strong>CLIENTE </strong><br><br> Spett.le <?php echo $nome_cliente; ?><br><br><?php echo $indirizzo_cliente; ?><br><br> <?php echo $citta_cliente; ?> - <?php echo $nazione_cliente; ?></td>
				<td width="35%" style="border:1px solid black;"><strong>DESTINATARIO </strong><br><br> Spett.le <?php echo $nome_destinatario; ?><br><br><?php echo $indirizzo_dest; ?><br><br> <?php echo $citta_dest; ?> - <?php echo $nazione_dest; ?></td>

			</tr>
	</table>

	<table width="1029px" cellpadding="0" class="footer_preventivo">
	<tr ><td width="70%">
		<table width="98%" cellpadding="5">
			<tr><td style="border:1px solid black;">Data consegna lavoro prevista:<strong> <?php echo $data_cons_lav_prevista; ?> dalla Vs conferma disegno</strong></td></tr>
			<tr><td style="border:1px solid black;">Condizione di pagamento:<strong> <?php echo $condizione_pagamento; ?></strong>	</td></tr>
			<tr><td style="border:1px solid black;">Banca d'appoggio:<strong> <?php echo $banca_appoggio; ?></strong>	</td></tr>
			<tr><td style="border:1px solid black;">Tipologia trasporto:<strong> <?php echo $tipo_trasporto; ?>	</strong></td></tr>
			<tr><td style="border:1px solid black;">Porto:<strong> <?php echo $tipo_porto; ?></strong></td></tr>
		</table>
	</td>
	<td width="30%">
		<table width="100%" cellpadding="5">
			<tr><td style="border:1px solid black;">Imponibile Totale:<strong><a id='imponibile_tot'></a>&#8364;</strong></td></tr>
			<tr><td style="border:1px solid black;">
					<div id='costo_trasp' value='<?php echo $costo_trasp; ?>' classeiva='<?php echo $costo_trasp_classe_iva; ?>'> Costo trasporto:<strong> <?php echo $costo_trasp; ?> &#8364; </strong></div></td></tr>

			<tr class='secondo_costo_tr <?php echo $secondo_costo_tr_class; ?>'><td style="border:1px solid black;" class='optionaltd'>
					<div id='costo_trasp1' value='<?php echo $costo_trasp1; ?>' classeiva='<?php echo $costo_trasp_classe_iva1; ?>' ><?php echo $imposte1; ?></div></td></tr>
			<tr><td style="border:1px solid black;">Imposte:<strong><a id='imposta'></a> &#8364;</strong> </td></tr>
			<tr><td style="border:1px solid black;">Prezzo a pagare:<strong><a id='a_pagare'></a> &#8364;</strong></td></tr>
		</table>
	</td></tr>

	</table>
	<table width="1029px" cellpadding="10" class="sotto_footer_preventivo">
		<tr><td>In attesa di Vostra gradita conferma porgiamo distinti saluti.</td><td><strong>O.M.I.S. s.r.l</strong></td></tr>
	</table>


		<table class="descrizione_prev" style="width:1029px;"cellpadding="2" cellspacing="1">
			<tr>
				<th style="border:1px solid black;" width="64%">DESCRIZIONE</th>
				<th style="border:1px solid black;" width="4%">UM</th>
				<th style="border:1px solid black;" width="4%">Q.TA'</th>
				<th style="border:1px solid black;" width="12%">PREZZO</th>
				<th style="border:1px solid black;" width="6%">SC.</th>
				<th style="border:1px solid black;" width="12%">TOTALE</th>

			</tr>
			<?php echo $tabella_descrizione; ?>

		</table>
		<br>



	 <br>


</tr></table>
</body>
</html>
