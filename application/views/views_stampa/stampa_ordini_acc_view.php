<html lang="en">
<head>
	<link href='http://fonts.googleapis.com/css?family=Noto+Sans:400Italic' rel='stylesheet' type='text/css'>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>

	<script language="javascript" src="http://192.168.2.231:8000/assets/themes/datatables/js/ordini_fornitori.js" type="text/javascript"></script>
	<link type="text/css" rel="stylesheet" href="http://192.168.2.231:8000/assets/css/preventivi.css" />

	<title>Ordini</title>
	<meta name="author" content="Marco Scoppetta">
	<!-- Date: 2013-03-25 -->
</head>

<body style="font-family: 'Noto Sans', sans-serif;">
	<input type='hidden' id='id_ordine_fornitore' value='<?php echo $id_ordine_fornitore; ?>'>
	<input type='hidden' id='id_fornitore' value='<?php echo $id_fornitore; ?>'>

	<table class="footer" width="1029px" cellpadding="0">
	<tr ><td width="70%">
		<table width="98%" cellpadding="5">
			<tr><td style="border:1px solid black;"><strong>Note:</strong><textarea class='note_ordine'> <?php echo $note_ordine; ?></textarea></td></tr>
		</table>
	</td><td>Ufficio Acquisti<br><br><strong>O.M.I.S. s.r.l</strong>
	</td></tr>

	</table>


<table><tr><td>
	<table width="1029px" style="border:1px solid black;" cellpadding="10" >
		<tr><td>
	<table width="100%" style="text-align: left;">

	  <tbody>
	    <tr  >
	    	<td >
				<img src="/application/views/icons/omis_bronzo_sps_09_2.jpg" width="200" height="150">

			</td>
			<td style="line-height:150%" >
				Via Olona 183/B <br>
				21013 GALLARATE (VA)  ITALY <br>
				<br>
				<div style="font-size:13px;">C.F. - P.IVA: 02554680120 <br>
				Cap.Soc. Euro 15.000,00 int.vers. <br>
				C.C.I.A.A. 267346 Reg. Impr. 02554680120 <div>
			</td>
			<td style="line-height:150%" >
				Tel. 0331/776097 <br>
				Fax 0331/772290 <br>
				E-mail: omis@omis.it <br>
				PEC: omis@ticertifica.it <br>
				<br>
				www.omis.it
			</td>
	    </tr>

	  </tbody>
	</table>
	</td></tr>
	</table>
	<table width="1029px" cellpadding="10">
			<tr><td width="60%" valign="top"><br>Alla cortese attenzione di: <br><br><textarea class='riferimenti_ordine'><?php echo $riferimenti_ordine; ?></textarea> <br><br>
				<br><br><strong> ORDINE FORNITORE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?php echo $num_ordine_fornitore; ?>&nbsp;&nbsp;&nbsp;&nbsp; del &nbsp;&nbsp;&nbsp;&nbsp;
				<?php echo $data_odierna; ?></strong></td>
			<td width="40%" style="border:1px solid black;">Spett.le <br><br><?php echo $nome_fornitore; ?><br><br>
			<?php echo $indirizzo_fornitore; ?><br><br> <?php echo $citta_fornitore; ?></td>

	</table>



		<table class="accessori" style="width:1029px;"cellpadding="5" cellspacing="1">
			<tr>
				<th style="border:1px solid black;" width="25%">CODICE</th>
				<th style="border:1px solid black;" width="50%">DESCRIZIONE</th>
				<th style="border:1px solid black;" width="10%">UM</th>
				<th style="border:1px solid black;" width="15%">QUANTITA'</th>
			</tr>
			<?php echo $tabella_accessori; ?>

		</table>
		<br>
		<br>




	 <br>



<td valign='top'>	<a href='<?php echo site_url('omis/lavori/incorso')?>'><font size="3">TORNA A LAVORI</font></a>
	<br><br><br><input type="button" class="agg_acc" value="AGGIUNGI ACCESSORIO" /><div class="elenco_acc"></div>
	<br><br><br><a href='<?php echo site_url('magazzino/stampa_ordine/'.$id_ordine_fornitore)?>'><input type="button"  value="STAMPA ORDINE" /></a>
</div>
</td>
</tr></table>
</body>
</html>
