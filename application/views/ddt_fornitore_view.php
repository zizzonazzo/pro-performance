<html lang="en">
<head>
	<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	<link type="text/css" rel="stylesheet" href="http://<?php echo $_SERVER['SERVER_NAME'].':8000/assets/css/libraries/chosen.min.css'?>" />

	<link href='http://fonts.googleapis.com/css?family=Noto+Sans:400Italic' rel='stylesheet' type='text/css'>
	<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>

	<script src="http://<?php echo $_SERVER['SERVER_NAME'].':8000/assets/js/libraries/jquery_plugins/jquery.chosen.min.js'?>"></script>
	<script src="http://<?php echo $_SERVER['SERVER_NAME'].':8000/assets/js/libraries/jquery_plugins/ajax-chosen.js'?>"></script>


	<script language="javascript" src="http://<?php echo $_SERVER['SERVER_NAME'].':8000/assets/themes/datatables/js/gestione_ddt.js'?>" type="text/javascript"></script>
	<script language="javascript" src="http://<?php echo $_SERVER['SERVER_NAME'].':8000/assets/themes/datatables/js/ricerca_globale.js'?>" type="text/javascript"></script>



	<link type="text/css" rel="stylesheet" href="http://<?php echo $_SERVER['SERVER_NAME'].':8000/assets/css/preventivi.css'?>" />
	<link type="text/css" rel="stylesheet" href="http://<?php echo $_SERVER['SERVER_NAME'].':8000/assets/css/menutendina.css'?>" />
	<link type="text/css" rel="stylesheet" href="http://<?php echo $_SERVER['SERVER_NAME'].':8000/assets/themes/datatables/css/bootstrap.css'?>" />

	<!-- Latest compiled and minified CSS -->

<!-- Optional theme -->

<!-- Latest compiled and minified JavaScript -->

	<title>DDT</title>
	<meta name="author" content="Marco Scoppetta">
	 <script type='text/javascript'>
		var base_url = '<?php echo $base_url; ?>';

	</script>
	<style>
	#omis_nav{
		margin-bottom: 0px;
	}
	</style>
	<!-- Date: 2013-03-25 -->
</head>

<body style="font-family: 'Noto Sans', sans-serif;" class="compile">
	<input type='hidden' id='id_ddt' value='<?php echo $id_ddt; ?>'>
	<input type='hidden' id='anno_ddt' value='<?php echo $anno_ddt; ?>'>
	<input type='hidden' id='data_ddt' value='<?php echo $data_ddt_db; ?>'>
	<input type='hidden' id='num_ddt' value='<?php echo $num_ddt; ?>'>
	<input type='hidden' id='nome_operatore' value='<?php echo $nome_op; ?>'>
<!-- INIZIO BODY NUOVO-->

      <?php echo $menu ?>



    <div class="container-fluid" style="margin-top:2%;">
    <div class="row">
        <div class="col-sm-2"></div>
      	<div class="col-sm-8" >
      			<div  align="center" class="risultato_ric_glob" style="overflow: visible;"></div>
      	</div>
          <div class="col-sm-2 tasto_chiudi_ricerca_fornitore"></div>
      </div>
 		 <div class='row'>
            	<div class="col-lg-1"> </div>
            	<div class="col-lg-10 col-md-11" align="left" id="central" style="padding:2%;">
	            	<div class="row"  style="border:1px solid black;">
		            		<div class="col-sm-4"  style="padding-left:5px; padding-top:5px;">
		            			<img src="/application/views/icons/omis_bronzo_sps_09_2.jpg" width="200" height="150">
		            		</div>
		            		<div class="col-sm-4" style="line-height:150%; padding-top:20px;" >
		            					Via Olona 183/B <br>
										21013 GALLARATE (VA)  ITALY <br>
										<br>
										<div style="font-size:13px;">C.F. - P.IVA: 02554680120 <br>
										Cap.Soc. Euro 15.000,00 int.vers. <br>
										C.C.I.A.A. 267346 Reg. Impr. 02554680120</div>
		            		</div>
		            		<div class="col-sm-4" style="line-height:150%; padding-top:20px;" >
		            					Tel. 0331/776097 <br>
										Fax 0331/772290 <br>
										E-mail: omis@omis.it <br>
										PEC: omis@ticertifica.it <br>
										<br>
										www.omis.it
		            		</div>
	            	</div>
	            	<div class="row" style="border-left:1px solid black; border-right:1px solid black; ">
		            		<div class="col-sm-4"  style="padding-left:5px; padding-top:5px;">
		            			<br><br><strong> D.d.T n° <?php echo $num_ddt; ?> del <?php echo $data_ddt; ?></strong><small>
		            			<strong><br><br>DOCUMENTO DI TRASPORTO (D.P.R 472 14/08/96)</strong></small>
		            		</div>
		            		<div class="col-sm-4"  style="border-left:1px solid black; padding-left:5px; " >
		            			<br><br><strong>LUOGO DESTINAZIONE MERCE</strong><br><br>
		            			<div class='luogo_destinazione_fornitore' ><?php echo $luogo_destinazione; ?></div>
		            			<br>
		            		</div>
		            		<div class="col-sm-4" style="border-left:1px solid black; padding-left:5px;" >
		            			<br><br><strong>FORNITORE</strong><br><br>
		            			<?php echo $fornitori; ?><br><br>
		            		</div>
	            	</div>
	            	<div class="row" >
	            		<div class="col-sm-6" style="border:1px solid black;">
	            			<strong>DESCRIZIONE</strong>
	            		</div>
	            		<div class="col-sm-3" style="border:1px solid black;">
	            			<strong>LAVORAZIONE</strong>
	            		</div>
	            		<div class="col-sm-1" style="border:1px solid black;">
	            			<strong>Q.TA'</strong>
	            		</div>
	            		<div class="col-sm-2" style="border:1px solid black;">
	            			<strong>RIGA</strong>
	            		</div>
	            	</div>
	            	<div class="row">
	            		<?php echo $tabella_descrizione; ?>
	            	</div>


					<div class="row" style="margin-top:30px;">
						<div class="col-sm-3" style="border:1px solid black; padding:4px;">
							<strong>Trasporto a mezzo</strong>
							<br> <?php echo $tipo_trasporto ?>
						</div>
						<div class="col-sm-3" style="border:1px solid black; padding:4px;">
							<strong>Causale del Trasporto</strong>
							<br> <?php echo $causali_trasporto ?>
						</div>
						<div class="col-sm-3" style="border:1px solid black; padding:4px;">
							<strong>Porto</strong>
							<br> <?php echo $tipo_porto ?>
						</div>
						<div class="col-sm-3" style="border:1px solid black; padding:4px;">
							<strong>Aspetto dei beni</strong>
							<br> <?php echo $aspetto_beni ?>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-7" style="border-left:1px solid black; border-bottom:1px solid black; border-right:1px solid black; padding:4px;">
							<div class="col-sm-12"><strong>Vettore</strong></div>
							<div class="col-sm-12" style="margin-top:3px;"><?php echo $vettori ?></div>
						</div>

						<div class="col-sm-5" style="border-left:1px solid black; border-bottom:1px solid black; border-right:1px solid black; padding:4px;">
							<div class="row">
								<div class="col-sm-3" ><strong>Dimensioni</strong>
									<br> <input class='dimensioni' type="text" value="<?php echo $dimensioni ?>" style="width:100%; text-align:right;">
								</div>
								<div class="col-sm-3" ><strong>N. Colli</strong>
									<br> <input class='n_colli' type="text" value="<?php echo $n_colli ?>" style="width:60%; text-align:right;">
								</div>
								<div class="col-sm-3" ><strong>Peso Lordo</strong>
									<br> <input class='peso_lordo' type="text" value="<?php echo $peso_lordo ?>" style="width:60%; text-align:right;">
								</div>
								<div class="col-sm-3" ><strong>Peso Netto</strong>
									<br> <input class='peso_netto' type="text" value="<?php echo $peso_netto ?>" style="width:60%; text-align:right;">
								</div>
							</div>
						</div>
					</div>
					<div class="row" style="margin-top:10px;">
						<div class="col-sm-4" style="border:1px solid black; padding:4px;">
							<strong>Firma Conducente</strong>
							<br><br><br>
						</div>
						<div class="col-sm-4" style="border:1px solid black; padding:4px;">
							<strong>Firma Vettore</strong>
							<br><br><br>
						</div>
						<div class="col-sm-4" style="border:1px solid black; padding:4px;">
							<strong>Firma Destinatario</strong>
							<br><br><br>
						</div>
					</div>


            	</div>
    			<div class="col-sm-1" id="right" style="padding-left:0; padding-top:30px;">
    				<a href='#'><button style="width: 100%;" class='agg_comm_fornitore btn btn-default' role='button'/><span class="glyphicon glyphicon-list" aria-hidden="true"></span> Agg. Comm</button></a><div class="elenco_comm" style="height:600px; overflow-y:scroll;"></div>
						<br><br><br><a href='#'><button style="width: 100%;" class='stampa_fornitore btn btn-default' role='button'/><span class="glyphicon glyphicon-print" aria-hidden="true"></span> Stampa</button></a>
						<br><br><a href='#'><button style="width: 100%;" class='creapdf_fornitore btn btn-default' role='button'/><span class="glyphicon glyphicon-floppy-save" aria-hidden="true"></span> Crea PDF</button></a>
						<br><br><a type="button" data-toggle="modal" data-target="#myModal" class="aprimodal_fornitore"><button style="width: 100%;" class='btn btn-default' role='button'/><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span> Invia DDT</button></a>
    			 		<br><br><br><br><a href='#'><button style="width: 100%;" class='elimina_ddt btn btn-default' role='button'/><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Elimina</button></a>
    			 </div>
    		</div>
    </div>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

    <!-- FINE BODY NUOVO -->



<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title">Invia offerta</h4>
          </div>
          <div class="modal-body">
            <form role="form" class="form-horizontal">
                <div class="form-group">
                  <label class="col-sm-2" for="inputTo">A:</label>
                  <div class="col-sm-10"><input  class="form-control" id="inputToFornitore" value=""></div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2" for="inputCc">CC:</label>
                  <div class="col-sm-10"><input  class="form-control" id="inputCc" value=""></div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2" for="inputCcn">CCn:</label>
                  <div class="col-sm-10"><input  class="form-control" id="inputCcn" value="<?php echo $ccns_email ?>"></div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2" for="inputSubject">Oggetto:</label>
                  <div class="col-sm-10"><input type="text" class="form-control" id="inputSubject" value="<?php echo $oggetto_email ?>"></div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2" for="inputSubject">Allegato:</label>
                  <div class="col-sm-10"><a href="ftp://192.168.2.231:2222/ddtfornitori/<?php echo $num_ddt ?>_<?php echo $anno_ddt ?>.pdf" target="_blank">Allegato DDT</a><br></div>
                </div>
                <div class="form-group">
                  <label class="col-sm-12" for="inputBody">Messaggio</label>
                  <div class="col-sm-12"><textarea id="inputBody" cols="80" rows="18"><?php echo $signature ?></textarea></div>
                </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancella</button>
            <button type="button" class="btn btn-primary invia_ddt_fornitore"> <span class="glyphicon glyphicon-send" aria-hidden="true"></span> &nbsp; Invia</button>

          </div>
    </div>
  </div>
</div>
<div class="modal fade" id="myModalLoading" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title">Invio E-mail in corso..</h4>
          </div>
         <div class="row">
                <div class="col-sm-4"></div>
                <div class="col-sm-4"><img src="/application/views/icons/loading.gif"></div>
                <div class="col-sm-4"></div>
              </div>
    </div>
  </div>
</div>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>




</body>
</html>
