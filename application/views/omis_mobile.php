<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>OMIS</title>
  <link type="text/css" rel="stylesheet" href="http://<?php echo $_SERVER['SERVER_NAME'].':8000/assets/themes/datatables/css/bootstrap.css'?>" />

  </head>
  <body>
        <!-- Static navbar -->
    <nav class="navbar navbar-default navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <span class="navbar-brand" href="#"><img src="/application/views/icons/omis_logo_2.png" width="80" height="50" style='margin-top:-13px;'></span>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Lavori <span class="caret"></span></a>
              <?php if($nome_op!='DAVID'){ ?>
                  <ul class="dropdown-menu">
                    <li><a href="<?php echo site_url('omis/lavorazioni/progetti')?>">In progetto</a></li>
                    <li><a href="<?php echo site_url('omis/lavorazioni/incorso')?>">In Corso</a></li>
                    <li><a href="<?php echo site_url('omis/lavorazioni/eseguiti')?>">Eseguiti</a></li>
                 </ul>
              <?php } else {?>
                <ul class="dropdown-menu">
                    <li><a href="<?php echo site_url('omis/lavori/progetti')?>">In progetto</a></li>
                    <li><a href="<?php echo site_url('omis/lavori/incorso')?>">In Corso</a></li>
                    <li><a href="<?php echo site_url('omis/lavori/eseguiti')?>">Eseguiti</a></li>
                 </ul>
                 <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Anagrafiche <span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li><a href="<?php echo site_url('omis/categorie_comp')?>">Categorie componenti</a></li>
                    <li><a href="<?php echo site_url('omis/clienti')?>">Clienti</a></li>
                    <li><a href="<?php echo site_url('omis/componenti')?>">Componenti</a></li>
                    <li><a href="<?php echo site_url('omis/fasi')?>">Fasi</a></li>
                    <li><a href="<?php echo site_url('omis/lavesterne')?>">Lav. Esterne</a></li>
                    <li><a href="<?php echo site_url('omis/materiali')?>">Materiali</a></li>
                    <li><a href="<?php echo site_url('omis/note_produttori')?>">Note produttori</a></li>
                    <li><a href="<?php echo site_url('omis/produttori')?>">Produttori</a></li>
                    <li><a href="<?php echo site_url('omis/stampatrici')?>">Stampatrici</a></li>
                    <li><a href="<?php echo site_url('omis/trattamenti')?>">Trattamenti</a></li>
                  </ul>
              </li>

              <?php }?>

            </li>
            <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Magazzino<span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li><a href="<?php echo site_url('magazzino/magazzino_accessori')?>">Accessori</a></li>
                    <li><a href="<?php echo site_url('magazzino/magazzino_materiali')?>">Materiali</a></li>
                    <li><a href="<?php echo site_url('magazzino/magazzino_semilavorati')?>">Semilavorati</a></li>
                  </ul>
            </li>

             <li><a href="<?php echo site_url('operatori/elenco_bolle_produzione')?>">Scadenzario</a></li>
             <li><a href="<?php echo site_url('operatori/elenco_tempi_operatore')?>">Tempi inseriti</a></li>
             <?php if(($nome_op!='DAVID')&&($nome_op!='CLAUDIO')){ ?>
             	<li><a href="<?php echo site_url('operatori/elenco_consumi_materiali')?>">Consumi Materiali</a></li>
             <?php } ?>
              <?php if($nome_op=='FRANCESCO'){ ?>
              <li><a href="<?php echo site_url('operatori/pagina_mobile')?>">Mobile</a></li>
             <?php } ?>
          </ul>

        <ul class="nav navbar-nav navbar-right">



			      <li >
              <a style="margin-top:-5px;">
                  <small>  <select class="select_ric_globale">
                        <option value=""></option>
                        <option value="commessa">Commessa</option>
                        <option value="n_stampo">N° Stampo</option>
                        <option value="nome_sapone">Nome Sapone</option>
                        <option value="ordine_cliente">Ordine Cliente</option>
                        <option value="comm_cliente">Commessa Cliente</option>
                      </select>
                    </small>
		            		<input type="text" class="ric_globale" name="search" id="search" placeholder="Ricerca Globale">
	            </a>
            </li>
			     <li class="dropdown">
	              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><font color="red"> <?php echo $nome_op; ?></font><span class="caret"></span></a>
	              <ul class="dropdown-menu">
	                <li><a href='<?php echo site_url('omis/logout')?>'><font >LOGOUT</font></a></li>
	              </ul>
	            </li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-2"></div>
      	<div class="col-sm-8" >
      			<div  align="center" class="risultato_ric_glob" style="overflow: visible;"></div>
      	</div>
          <div class="col-sm-2 tasto_chiudi_ricerca"></div>
      </div>
      <form method="POST" action="/index.php/omis/salvaComponente">
      <input type="hidden" name="id_materiale" value="">
      <input type="hidden" name="non_conformi"  value="0">
      <input type="hidden" name="redirect"  value="M">

      	<div class='row' >
          <div class="col-sm-12" align="center"> <span style='font-size:180%;'><?php echo $lista_commesse ?></span></div>
      	</div>
        <div class='row' style="margin-top:10px;">
          <div class="col-sm-12" align="center" ><span style=' font-size:110%;' id="lista_componenti"></span></div>
        </div>
          <div class='row' style="margin-top:10px; border-top:1px solid black; padding-top:20px;">
            <div class="col-sm-12" ><span style='color:red; font-size:130%;'>DATI COMPONENTE:</span></div>
          </div>
          <div class="row" style="margin-top:10px;">
            <div class="col-xs-6">Materiale </div>
            <div class="col-xs-6"><span id="span_materiale" style="font-size:120%;"></span></div>
          </div>
          <div class="row" style="margin-top:10px;">
            <div class="col-xs-6">FORMA</div>
            <div class="col-xs-6"><span id="span_forma" style="font-size:120%;"></span></div>
          </div>
          <div class="row" style="margin-top:10px;">
            <div class="col-xs-6">SEZIONE </div>
            <div class="col-xs-6"><span id="span_sezioni" style="font-size:120%;"></span></div>
          </div>
          <div class="row" style="margin-top:10px;">
            <div class="col-xs-6"><span id="span_11" ></span></div>
            <div class="col-xs-6"><span id="span_12" style="font-size:120%;"></span></div>
          </div>
           <div class="row" style="margin-top:10px; padding-bottom:20px; border-bottom:1px solid black;">
            <div class="col-xs-6"><span id="span_21"></span></div>
            <div class="col-xs-6"><span id="span_22" style="font-size:120%;"></span></div>
          </div>
          <div class="row" style="margin-top:20px;">
            <div class="col-xs-12" align="right"><input style="font-size:15pt;" type="submit" value="SALVA"></div>
          </div>
    </form>

    </div>

  <script src="http://192.168.2.231:8000/assets/js/libraries/jquery-1.11.3.min.js"></script>
  <script src="http://192.168.2.231:8000/assets/themes/datatables/js/gestione_mobile.js"></script>
  <script src="http://192.168.2.231:8000/assets/js/libraries/bootstrap.min.js"></script>



    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- Latest compiled and minified JavaScript -->
  </body>
</html>
