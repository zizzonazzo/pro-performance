<html lang="en" ng-app>
<head>
  <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
  <link type="text/css" rel="stylesheet" href="http://<?php echo $_SERVER['SERVER_NAME'].':8000/assets/css/libraries/chosen.min.css'?>" />

  <link href='http://fonts.googleapis.com/css?family=Noto+Sans:400Italic' rel='stylesheet' type='text/css'>



  <link type="text/css" rel="stylesheet" href="http://<?php echo $_SERVER['SERVER_NAME'].':8000/assets/css/preventivi.css'?>" />
  <link type="text/css" rel="stylesheet" href="http://<?php echo $_SERVER['SERVER_NAME'].':8000/assets/css/menutendina.css'?>" />
  <link type="text/css" rel="stylesheet" href="http://<?php echo $_SERVER['SERVER_NAME'].':8000/assets/themes/datatables/css/bootstrap.css'?>" />


  <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
  <script language="javascript" src="http://<?php echo $_SERVER['SERVER_NAME'].':8000/assets/themes/datatables/js/ricerca_globale.js'?>" type="text/javascript"></script>
  <script language="javascript" src="http://<?php echo $_SERVER['SERVER_NAME'].':8000/assets/js/gestione_fatture.js'?>" type="text/javascript"></script>

  <script src="http://<?php echo $_SERVER['SERVER_NAME'].':8000/assets/js/libraries/jquery_plugins/jquery.chosen.min.js'?>"></script>
  <script src="http://<?php echo $_SERVER['SERVER_NAME'].':8000/assets/js/libraries/jquery_plugins/ajax-chosen.js'?>"></script>

	<!-- Latest compiled and minified CSS -->

<!-- Optional theme -->

<!-- Latest compiled and minified JavaScript -->

	<title>Fattura</title>
	<meta name="author" content="Marco Scoppetta">
	 <script type='text/javascript'>
		//var base_url = '<?php echo $base_url; ?>';
	</script>
	<!-- Date: 2013-03-25 -->
</head>

<body style="font-family: 'Noto Sans', sans-serif;" class="compile">
	<input type='hidden' id='id_cliente' value='<?php echo $id_cliente; ?>'>
	<input type='hidden' id='id_fattura' value='<?php echo $id_fattura; ?>'>
	<input type='hidden' id='num_fattura' value='<?php echo $num_fattura; ?>'>
	<input type='hidden' id='data_creazione_fattura' value='<?php echo $data_creazione_fattura; ?>'>
	<input type='hidden' id='nome_operatore' value='<?php echo $nome_op; ?>'>
<!-- INIZIO BODY NUOVO-->

   <?php echo $menu ?>

<div class="container-fluid" style="margin-top:2%;">
    <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-8" >
            <div  align="center" class="risultato_ric_glob" style="overflow: visible;"></div>
        </div>
          <div class="col-sm-2 tasto_chiudi_ricerca"></div>
      </div>
     <div class='row'>
              <div class="col-lg-1"> </div>
              <div class="col-lg-10 col-md-11" align="left" id="central" style="padding:2%;">
                <div class="row"  style="border:1px solid black;">
                    <div class="col-sm-4"  style="padding-left:5px; padding-top:5px;">
                      <img src="/application/views/icons/omis_bronzo_sps_09_2.jpg" width="200" height="150">
                    </div>
                    <div class="col-sm-4" style="line-height:150%; padding-top:20px;" >
                          Via Olona 183/B <br>
                    21013 GALLARATE (VA)  ITALY <br>
                    <br>
                    <div style="font-size:13px;">C.F. - P.IVA: 02554680120 <br>
                    Cap.Soc. Euro 15.000,00 int.vers. <br>
                    C.C.I.A.A. 267346 Reg. Impr. 02554680120</div>
                    </div>
                    <div class="col-sm-4" style="line-height:150%; padding-top:20px;" >
                          Tel. 0331/776097 <br>
                    Fax 0331/772290 <br>
                    E-mail: omis@omis.it <br>
                    PEC: omis@ticertifica.it <br>
                    <br>
                    www.omis.it
                    </div>
                </div>
                <div class="row" style="border-left:1px solid black; border-right:1px solid black; ">
                    <div class="col-sm-6"  style="padding-left:15px; padding-top:5px;">
                      <strong>Sede Legale </strong><br><br><?php echo $nome_cliente; ?><br><?php echo $indirizzo_cliente; ?><br>
                          <br> <?php echo $citta_cliente; ?> - <?php echo $nazione_cliente; ?><br><br>
                    </div>
                    <div class="col-sm-6"  style="border-left:1px solid black; padding-left:15px;" >
                          <strong> Spett.le  </strong><br><br><?php echo $nome_cliente; ?><br><?php echo $indirizzo_cliente; ?><br>
                          <br> <?php echo $citta_cliente; ?> - <?php echo $nazione_cliente; ?><br><br>
                    </div>
                </div>
                <div class="row" style="border-top:1px solid black; border-left:1px solid black; border-right:1px solid black; ">
                    <div class="col-sm-6"  style="padding-left:15px; padding-bottom:5px;padding-top:5px;">
                          <strong> FATTURA <?php echo $num_fattura; ?> del <?php echo $data_creazione_fattura; ?></strong>
                    </div>
                    <div class="col-sm-6"  style="border-left:1px solid black; padding-left:5px;" >
                      &nbsp;
                    </div>
                </div>
                <div class="row" style="border-top:1px solid black; border-left:1px solid black; border-right:1px solid black; ">
                    <div class="col-sm-12"  style="padding-left:15px; padding-bottom:5px;padding-top:5px;">
                          <div class="row"><div class="col-sm-12"> Pagamento: <?php echo $tipo_pagamento; ?></div></div>
                          <div class="row"><div class="col-sm-12"> Banca: <?php echo $banca_cliente; ?></div></div>
                          <div class="row"><div class="col-sm-3">ABI: <?php echo $abi_banca; ?></div><div class="col-sm-3">CAB: <?php echo $cab_banca; ?></div><div class="col-sm-3">P.Iva:</div><div class="col-sm-3">CF:</div></div>
                    </div>
                </div>
                <div class="row" >
                  <div class="col-sm-6" style="border:1px solid black;">
                    <strong>DESCRIZIONE</strong>
                  </div>
                  <div class="col-sm-1" style="border:1px solid black;">
                    <strong>UM</strong>
                  </div>
                  <div class="col-sm-1" style="border:1px solid black;">
                    <strong>Q.TA'</strong>
                  </div>
                  <div class="col-sm-1" style="border:1px solid black;">
                    <strong>PREZZO</strong>
                  </div>
                  <div class="col-sm-1" style="border:1px solid black;">
                    <strong>SC.</strong>
                  </div>
                  <div class="col-sm-1" style="border:1px solid black;">
                    <strong>TOTALE</strong>
                  </div>
                  <div class="col-sm-1" style="border:1px solid black;">
                    <strong>C.I.</strong>
                  </div>
                </div>
                <div class="row">
                  <?php echo $tabella_descrizione; ?>
                </div>


          <div class="row" style="margin-top:30px;">
            <div class="col-sm-8">
              <div class="row" style="border:1px solid black; padding:4px;">
                <div class="col-sm-6">Spese trasporto: </div><div class="col-sm-6">Spese Incasso:</div>
              </div>
               <div class="row" style="border-left:1px solid black; border-right:1px solid black; padding:4px;">
                <div class="col-sm-2">C.I. </div><div class="col-sm-4">Imponibile</div><div class="col-sm-4">Descrizione IVA</div><div class="col-sm-2">IVA</div>
              </div>
              <?php echo $tabella_footer; ?>
            </div>
            <div class="col-sm-1"></div>
            <div class="col-sm-3">
              <div class="row" style="border:1px solid black; padding:4px;"><strong>Totale Documento: <?php echo $totale_documento; ?>&#8364;</strong>
              </div>

            </div>
          </div>
          <div class="row" style="margin-top:10px;">
            <div class="col-sm-8"> In attesa di Vostra gradita conferma porgiamo distinti saluti.
            </div>
            <div class="col-sm-4"> <strong>O.M.I.S. s.r.l</strong>
            </div>
          </div>


              </div>
          <div class="col-sm-1" id="right" style="padding-left:0; padding-top:30px;">
<!--             <a href='#'><button style="width: 100%;" class='agg_comm btn btn-default' role='button'/><span class="glyphicon glyphicon-list" aria-hidden="true"></span> Agg. Comm</button></a><div class="elenco_comm" style="height:600px; overflow-y:scroll;"></div>
 -->            <br><br><br><a href='#'><button style="width: 100%;" class='stampa btn btn-default' role='button'/><span class=" glyphicon glyphicon-print" aria-hidden="true"></span> Stampa</button></a>
            <br><br><a href='#'><button style="width: 100%;" class='creapdf btn btn-default' role='button'/><span class="glyphicon glyphicon-floppy-save" aria-hidden="true"></span> Crea PDF</button></a>
            <br><br><a type="button" data-toggle="modal" data-target="#myModal" class="aprimodal"><button style="width: 100%;" class='btn btn-default' role='button'/><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span> Invia Fattura</button></a>

           </div>
        </div>
    </div>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

    <!-- FINE BODY NUOVO -->



<!-- Modal -->
<!-- <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title">Invia offerta</h4>
          </div>
          <div class="modal-body">
            <form role="form" class="form-horizontal">
                <div class="form-group">
                  <label class="col-sm-2" for="inputTo">A:</label>
                  <div class="col-sm-10"><input  class="form-control" id="inputTo" value="<?php echo $email_cliente ?>"></div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2" for="inputCc">CC:</label>
                  <div class="col-sm-10"><input  class="form-control" id="inputCc" value=""></div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2" for="inputCcn">CCn:</label>
                  <div class="col-sm-10"><input  class="form-control" id="inputCcn" value="<?php echo $ccns_email ?>"></div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2" for="inputSubject">Oggetto:</label>
                  <div class="col-sm-10"><input type="text" class="form-control" id="inputSubject" value="<?php echo $oggetto_email ?>"></div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2" for="inputSubject">Allegato:</label>
                  <div class="col-sm-10"><a href="ftp:192.168.2.231:2222/ufficio/<?php echo $num_preventivo ?>-<?php echo $num_revisione ?>_<?php echo $data_offerta_file ?>.pdf" target="_blank">Allegato Offerta</a><br><?php echo $disegni_allegati ?></div>
                </div>
                <div class="form-group">
                  <label class="col-sm-12" for="inputBody">Messaggio</label>
                  <div class="col-sm-12"><textarea id="inputBody" cols="80" rows="18"><?php echo $signature ?></textarea></div>
                </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancella</button>
            <button type="button" class="btn btn-primary inviaofferta"> <span class="glyphicon glyphicon-send" aria-hidden="true"></span> &nbsp; Invia</button>

          </div>
    </div>
  </div>
</div>
<div class="modal fade" id="myModalLoading" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title">Invio E-mail in corso..</h4>
          </div>
         <div class="row">
                <div class="col-sm-4"></div>
                <div class="col-sm-4"><img src="/application/views/icons/loading.gif"></div>
                <div class="col-sm-4"></div>
              </div>
    </div>
  </div>
</div> -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>




</body>
</html>
