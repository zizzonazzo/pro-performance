<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>OMIS</title>
	<link type="text/css" rel="stylesheet" href="http://<?php echo $_SERVER['SERVER_NAME'].':8000/assets/css/libraries/jquery-ui.min.css'?>"/>
	<link type="text/css" rel="stylesheet" href="http://<?php echo $_SERVER['SERVER_NAME'].':8000/assets/themes/datatables/css/datatables.css'?>"/>
	<link type="text/css" rel="stylesheet" href="http://<?php echo $_SERVER['SERVER_NAME'].':8000/assets/css/menutendina.css'?>"/>
	<script src="http://<?php echo $_SERVER['SERVER_NAME'].':8000/assets/js/libraries/jquery-1.7.1.min.js'?>"></script>
	<script src="http://<?php echo $_SERVER['SERVER_NAME'].':8000/assets/themes/datatables/js/jquery-ui-1.8.10.custom.min.js'?>"></script>
	<script src="http://<?php echo $_SERVER['SERVER_NAME'].':8000/assets/themes/datatables/js/jquery.dataTables.min.js'?>"></script>
	<script src="http://<?php echo $_SERVER['SERVER_NAME'].':8000/assets/themes/datatables/js/datatables_dettaglio.js'?>"></script>
  <script src="http://<?php echo $_SERVER['SERVER_NAME'].':8000/assets/themes/datatables/js/ricerca_globale.js'?>"></script>

<script type='text/javascript'>
	var base_url = '<?php echo $base_url; ?>';
	var subject = 'Record';

	var displaying_paging_string = "Visualizzati elementi da _START_ a _END_ di _TOTAL_ totali";
	var filtered_from_string 	= "(filtrato dal _MAX_ voci totali)";
	var show_entries_string 	= "Visualizza _MENU_ voci";
	var search_string 			= "Ricerca";
	var list_no_items 			= "No elementi da visualizzare";
	var list_zero_entries 			= "Visualizzazione 0 a 0 di 0 elementi";

	var list_loading 			= "Caricamento in corso...";

	var paging_first 	= "Primo";
	var paging_previous = "Precedente";
	var paging_next 	= "Prossimo";
	var paging_last 	= "Ultimo";

	var message_alert_delete = "Sei sicuro di voler eliminare questo record?";

	var default_per_page = '50';

</script>


    <!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:300,700">
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
        <!-- Static navbar -->
   <?php echo $menu ?>
    <div class="container-fluid" style="margin-top:3%;">
      <div class="row">
        <div class="col-sm-2"></div>
      	<div class="col-sm-8" >
      			<div  align="center" class="risultato_ric_glob" style="overflow: visible;"></div>
      	</div>
          <div class="col-sm-2 tasto_chiudi_ricerca"></div>
      </div>
    	<div class='row'>
        <div class="col-sm-4"></div>
        <div id='title' class="col-sm-4" align="center"> <span style='color:red; font-size:180%;'><?php echo $header ?></span></div>
        <div class="col-sm-4"></div>
    	</div>
      <div class='row'>
        <div class="col-sm-12">
            <?php echo $output; ?>
        </div>
      </div>
      <div class='row'>
        <div class="col-sm-12">
            <?php echo $footer; ?>
        </div>
      </div>
    </div>

</head>
<body>

<div id="dialog" title="Immagine Accessorio">
  <img src="" style="max-width: 100%; ">
</div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
  </body>
</html>
