<style>
 th {
    border: 1px solid black;
}
</style>
<table width="100%" style="border:1px solid black;" cellpadding="10" >
		<tr><td>
			<table width="100%" style="text-align: left;">

			  <tbody>
			    <tr  >
			    	<td width="30%">
						<img src="/application/views/icons/omis_bronzo_sps_09_2.jpg" width="125" height="90">
				
					</td>
					<td width="35%" style="line-height:120%" >
						<br>
						Via Olona 183/B <br>
						21013 GALLARATE (VA)  ITALY <br>
						<br>
						<div><font size="7">C.F. - P.IVA: 02554680120 <br>
						Cap.Soc. Euro 15.000,00 int.vers. <br>
						C.C.I.A.A. 267346 Reg. Impr. 02554680120</font> </div>
					</td>
					<td width="35%" style="line-height:120%" >
					<br>
						Tel. 0331/776097 <br>
						Fax 0331/772290 <br>
						E-mail: omis@omis.it <br>
						PEC: omis@ticertifica.it <br>
						<br>
						www.omis.it
					</td>
			    </tr>

			  </tbody>
			</table>
		</td></tr>
	</table>
	<table width="100%" cellpadding="10">
			<tr>
				<td width="60%">
				<br>Alla cortese attenzione di: <br><br><?php echo $riferimenti_ordine; ?><br><br>
				<br><br><strong> ORDINE FORNITORE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?php echo $num_ordine_fornitore; ?>&nbsp;&nbsp;&nbsp;&nbsp; del &nbsp;&nbsp;&nbsp;&nbsp;
				<?php echo $data_odierna; ?></strong></td>
				<td width="40%" style="border:1px solid black;">
					<br><br>Spett.le <br><br><?php echo $nome_fornitore; ?><br><br>
					<?php echo $indirizzo_fornitore; ?><br><br> <?php echo $citta_fornitore; ?>
				</td>
			</tr>	
	</table>
	<br><br>
		<?php if($stampa_tabella_accessori==true) {?>
		<table class="accessori" width="100%" cellpadding="2" cellspacing="1">
			<tr>
				<th style="border:1px solid black;" width="18%">CODICE</th>
				<th style="border:1px solid black;" width="12%">NS. RIF</th>
				<th style="border:1px solid black;" width="46%">DESCRIZIONE</th>
				<th style="border:1px solid black;" width="4%">UM</th>
				<th style="border:1px solid black;" width="8%">Q.TA'</th>
				<th style="border:1px solid black;" width="12%">CONSEGNA</th>
			</tr>
			<?php echo $tabella_accessori; ?>		
						
		</table>
		<br>
		<br>
		<?php }?>
		<?php if($stampa_tabella_materiali==true) {?>
		<table class="accessori" width="100%" cellpadding="2" cellspacing="1">
			<tr>
				<th style="border:1px solid black;" width="15%">CODICE</th>
				<th style="border:1px solid black;" width="10%">NS. RIF</th>
				<th style="border:1px solid black;" width="24%">DESCRIZIONE</th>
				<th style="border:1px solid black;" width="11%">FORMA</th>
				<th style="border:1px solid black;" width="21%">MISURE</th>
				<th style="border:1px solid black;" width="4%">UM</th>
				<th style="border:1px solid black;" width="5%">Q.TA'</th>
				<th style="border:1px solid black;" width="10%">CONSEGNA</th>
			</tr>
			<?php echo $tabella_materiali; ?>		
						
		</table>
		<br>
		<br>
		<?php }?><?php if($stampa_tabella_semilavorati==true) {?>
		<table class="accessori" width="100%" cellpadding="2" cellspacing="1">
			<tr>
				<th style="border:1px solid black;" width="12%">NS. RIF</th>
				<th style="border:1px solid black;" width="48%">DESCRIZIONE</th>
				<th style="border:1px solid black;" width="18%">DISEGNO</th>
				<th style="border:1px solid black;" width="4%">UM</th>
				<th style="border:1px solid black;" width="7%">Q.TA'</th>
				<th style="border:1px solid black;" width="11%">CONSEGNA</th>
			</tr>
			<?php echo $tabella_semilavorati; ?>		
						
		</table>
		<br>
		<br>
		<?php }?>	
