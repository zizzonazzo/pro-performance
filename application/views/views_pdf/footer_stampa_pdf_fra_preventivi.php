<table width="100%" cellpadding="0" class="footer_preventivo" >
	<tr >
	<td width="65%">
		<table width="98%" cellpadding="5">
			<tr><td style="border:1px solid black;">Délai de livraison:<strong> <?php echo $data_cons_lav_prevista; ?> après votre confirmation du dessin</strong></td></tr>
			<tr><td style="border:1px solid black;">Modalités de paiement:<strong> <?php echo $condizione_pagamento; ?>	</strong></td></tr>
			<tr><td style="border:1px solid black;">Banque:<strong> <?php echo $banca_appoggio; ?>	</strong></td></tr>
			<tr><td style="border:1px solid black;">Transport:<strong> <?php echo $tipo_trasporto; ?>	</strong></td></tr>
			<tr><td style="border:1px solid black;">Port:<strong> <?php echo $tipo_porto; ?></strong></td></tr>
		</table>	
	</td>
	<td width="35%">
		<table width="100%" cellpadding="5">
			<tr><td style="border:1px solid black;">Montant: <strong><?php echo $imponibile_totale; ?> &#8364;</strong></td></tr>
			<tr><td style="border:1px solid black;">
					<div id='costo_trasp' value='0.00' classeiva='0'>Frais de livraison:<strong> <?php echo $costo_trasp; ?> &#8364;</strong></div></td></tr>
			
			<tr class='secondo_costo_tr disabled'><td style="border:1px solid black;" class='optionaltd'>
					<div id='costo_trasp1' value='0.00' classeiva='0' >&nbsp;</div></td></tr>
			<tr><td style="border:1px solid black;">Taxes: <strong><?php echo $imposte_tot; ?> &#8364;</strong> </td></tr>
			<tr><td style="border:1px solid black;">Montant à payer: <strong><?php echo $prezzo_a_pagare; ?> &#8364;</strong></td></tr>

		</table>
	</td>
	</tr>
	
	</table>
	<table width="100%" cellpadding="10" class="sotto_footer_preventivo">
		<tr><td width="80%">En attente de votre confirmation, Cordialement.</td><td width="20%"><strong>O.M.I.S. s.r.l</strong></td></tr>
	</table>