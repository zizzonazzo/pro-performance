<table width="100%" cellpadding="5" class="footer_ddt">
		<tr >
			<td width="20%" style="border:1px solid black; padding:5px;" valign="top" >
				<strong>Trasporto a mezzo</strong><br> <div style="margin-top:3px;"><?php echo $tipo_trasporto ?></div>
			</td>
			<td width="30%" style="border:1px solid black; padding:5px;" valign="top" >
				<strong>Causale del Trasporto</strong><br><div style="margin-top:3px;"><?php echo $causale_trasporto ?></div>
			</td>
			<td width="30%" style="border:1px solid black; padding:5px;" valign="top">
				<strong>Porto</strong><br><div style="margin-top:3px;"><?php echo $tipo_porto ?></div>
			</td>
			<td width="20%" style="border:1px solid black; padding:5px;" valign="top">
				<strong>Aspetto dei beni</strong><br> <div style="margin-top:3px;"><?php echo $aspetto_beni ?></div>
			</td>
		</tr>
	
</table>
<table width="100%" cellpadding="5" class="sotto_footer_ddt">
		<tr >
			<td width="50%" style="border:1px solid black; padding:5px;">
				<table width="100%">
					<tr>
						<td width="30%" valign="top"><strong>Vettore</strong></td>
						<td width="70%"><?php echo $vettori ?></td>
					</tr>
				</table>
			</td>
			<td width="17%" style="border:1px solid black; padding:5px 10px 5px 3px; text-align:right;" valign="top"><strong>Dimensioni</strong><br><br><?php echo $dimensioni ?></td>
			<td width="11%" style="border:1px solid black; padding:5px 10px 5px 3px; text-align:right;" valign="top"><strong>N. Colli</strong><br><br> <?php echo $n_colli ?></td>
			<td width="11%" style="border:1px solid black; padding:5px 10px 5px 3px; text-align:right;" valign="top"><strong>Peso Lordo</strong><br><br> <?php echo $peso_lordo ?></td>
			<td width="11%" style="border:1px solid black; padding:5px 10px 5px 3px; text-align:right;" valign="top"><strong>Peso Netto</strong><br><br> <?php echo $peso_netto ?></td>
		</tr>
</table>
<table width="100%" cellpadding="5" class="sotto_footer_ddt2">
		<tr><td width="33%" style="border:1px solid black; padding:5px 10px 5px 5px;"><strong>Firma Conducente</strong>
							<br><br><br>
			</td>
			<td width="33%" style="border:1px solid black; padding:5px 10px 5px 5px;"><strong>Firma Vettore</strong>
							<br><br><br>
			</td>
			<td width="34%" style="border:1px solid black; padding:5px 10px 5px 5px;"><strong>Firma Destinatario</strong>
							<br><br><br>
			</td>
			</tr>
</table>