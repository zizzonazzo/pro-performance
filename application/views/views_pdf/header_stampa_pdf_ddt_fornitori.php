<style>
 th {
    border: 1px solid black;
}
</style>
<head>
	<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
</head>
<table width="100%" style="border:1px solid black;" cellpadding="10" >
		<tr><td>
			<table width="100%" style="text-align: left;">

			  <tbody>
			    <tr  >
			    	<td width="30%">
						<img src="/application/views/icons/omis_bronzo_sps_09_2.jpg" width="125" height="90">
				
					</td>
					<td width="35%" style="line-height:120%" >
						<br>
						Via Olona 183/B <br>
						21013 GALLARATE (VA)  ITALY <br>
						<br>
						<div><font size="7">C.F. - P.IVA: 02554680120 <br>
						Cap.Soc. Euro 15.000,00 int.vers. <br>
						C.C.I.A.A. 267346 Reg. Impr. 02554680120</font> </div>
					</td>
					<td width="35%" style="line-height:120%" >
					<br>
						Tel. 0331/776097 <br>
						Fax 0331/772290 <br>
						E-mail: omis@omis.it <br>
						PEC: omis@ticertifica.it <br>
						<br>
						www.omis.it
					</td>
			    </tr>

			  </tbody>
			</table>
		</td></tr>
	</table>
	<table width="100%" cellpadding="10">
			<tr><td width="30%">
					<br><br><br><br><br><br><br>
					<big><strong>D.d.T N° <?php echo $num_ddt; ?> del <?php echo $data_ddt; ?></strong></big>
					<small><small><strong><br><br>DOCUMENTO DI TRASPORTO (D.P.R 472 14/08/96)</strong></small></small>
				</td>
				<td width="35%" style="border:1px solid black;">
				<br><br><strong>LUOGO DESTINAZIONE MERCE</strong><br><br>
						<?php echo $nome_fornitore; ?><br><?php echo $indirizzo_fornitore; ?><br><?php echo $citta_fornitore; ?>
		        </td>
				<td width="35%" style="border:1px solid black;">
				<br><br>Spett.le <br><br><?php echo $nome_fornitore; ?><br><?php echo $indirizzo_fornitore; ?><br><?php echo $citta_fornitore; ?><br><br>P. IVA: <?php echo $partita_iva_fornitore; ?>
				</td>
					
			</tr>
	</table>
	<br><br>
	<table class="descrizione_prev" width="100%" cellpadding="2" cellspacing="1">
			<tr>
				<th style="border:1px solid black;" width="60%">DESCRIZIONE</th>
				<th style="border:1px solid black;" width="25%">LAVORAZIONE</th>
				<th style="border:1px solid black;" width="5%">UM</th>
				<th style="border:1px solid black;" width="10%">Q.TA'</th>
			</tr>						
		</table>