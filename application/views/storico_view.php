<!DOCTYPE html>
<html lang="en">
<head>
	<title>OMIS Gestione Schede</title>
	<meta charset="utf-8" />

<script src="http://192.168.2.231:8000/assets/js/libraries/jquery-1.7.1.min.js"></script>
<script src="http://192.168.2.231:8000/assets/themes/datatables/js/jquery-ui-1.8.10.custom.min.js"></script>
<script src="http://192.168.2.231:8000/assets/themes/datatables/js/jquery.dataTables.min.js"></script>
<script src="http://192.168.2.231:8000/assets/themes/datatables/js/datatables.js"></script>
<script src="http://192.168.2.231:8000/assets/themes/datatables/js/eseguiti_vecchi.js"></script>
<script src="http://192.168.2.231:8000/assets/themes/datatables/js/ricerca_globale.js"></script>


<link type="text/css" rel="stylesheet" href="http://192.168.2.231:8000/assets/themes/datatables/css/demo_table_jui.css" />
<link type="text/css" rel="stylesheet" href="http://192.168.2.231:8000/assets/css/libraries/jquery-ui.min.css" />
<link type="text/css" rel="stylesheet" href="http://192.168.2.231:8000/assets/themes/datatables/css/datatables.css" />
<link rel="stylesheet" type="text/css" href="http://192.168.2.231:8000/assets/css/menutendina.css">
<script type='text/javascript'>
	var base_url = 'http://192.168.2.231:8000/';
	var subject = 'Record';

	var displaying_paging_string = "Visualizzati elementi da _START_ a _END_ di _TOTAL_ totali";
	var filtered_from_string 	= "(filtrato dal _MAX_ voci totali)";
	var show_entries_string 	= "Visualizza _MENU_ voci";
	var search_string 			= "Ricerca";
	var list_no_items 			= "No elementi da visualizzare";
	var list_zero_entries 			= "Visualizzazione 0 a 0 di 0 elementi";

	var list_loading 			= "Caricamento in corso...";

	var paging_first 	= "Primo";
	var paging_previous = "Precedente";
	var paging_next 	= "Prossimo";
	var paging_last 	= "Ultimo";

	var message_alert_delete = "Sei sicuro di voler eliminare questo record?";

	var default_per_page = '50';

</script>
<style type='text/css'>
body
{
	font-family: Arial;
	font-size: 14px;
}
a {
    color: blue;
    text-decoration: none;
    font-size: 14px;
}
a:hover
{
	text-decoration: underline;
}
</style>
</head>
<body>
	<table width="100%">
	<tr>
		<td width="30%">
			SEI LOGGATO COME:<font size="4" color="red"> <?php echo $nome_op; ?></font><br>
			<a href='<?php echo site_url('omis/logout')?>'><font size="3">LOGOUT</font></a>
		</td>
		<td width="50%">
			<div width="50%" align=center>
			<a href='<?php echo site_url('omis/lavori/incorso')?>'><font size="3">LAVORI</font></a> |
			<a href='<?php echo site_url('omis/preset')?>'><font size="3">PRE-SET</font></a> |
			<a href='<?php echo site_url('omis/anagrafiche')?>'><font size="3">ANAGRAFICHE</font></a> |
			<a href='<?php echo site_url('preventivi/tabella')?>'><font size="3">PREVENTIVI</font></a> |
			<a href='<?php echo site_url('magazzino/ordini_accessori')?>'><font size="3">ORDINI ACCESSORI</font></a> |
			<a href='<?php echo site_url('operatori/controllo_produzione_bolle')?>'><font size="3">SCADENZIARI</font></a>

				<a href='<?php echo site_url('omis/listini/stampi')?>'onclick="internalLink=true;"><font size="3">| LISTINI</font></a> |


		</div>
		<div align=center>
			<font size="6" color="red">   <!-- DA METTERE NEL CSS -->
			STORICO SAPONI
			</font>
	    </div>
		</br>
		<div>
	    </div>
		</td>
		<td width="30%">
		</td>
	</tr>
	</table>

	</br></br>
	<div style='height:10px;'></div>
    <div>
		<table width="100%">
		<tr><td width="50%">


		</td>
		<td style="text-align: right;" width="50%">
		Ricerca globale:	<select class="select_ric_globale">
			  <option value=""></option>
			  <option value="commessa">Commessa</option>
			  <option value="n_stampo">N° Stampo</option>
			  <option value="nome_sapone">Nome Sapone</option>
			  <option value="ordine_cliente">Ordine Cliente</option>
			  <option value="comm_cliente">Commessa Cliente</option>

			</select><input class='ric_globale' size='18' type='text' value=""/></td>
		</tr>
		</table>
		<table>
		<tr><td width="20%"></td><td width="60%" ><div  align="center" class="risultato_ric_glob"></div></td><td class="tasto_chiudi_ricerca" width="20%" style="vertical-align:top;"></td>
		</table>
		<?php echo $output; ?>
    </div>



</body>
</html>
