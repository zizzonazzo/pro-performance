<?php  
	if (!defined('BASEPATH')) exit('No direct script access allowed');
?>
		<div class='row'>
			<div class="col-lg-1 visible-lg-1">
				<div class="row" align="right"><button class="btn btn-default" role='button'><a href='/index.php/omis/tornaprogS/<?php echo $row->commessa; ?>'>Progetto</a></button></div>
				<div class="row" align="right"><button class="btn btn-default" role='button'><a href='/index.php/omis/sospendi/progetti/<?php echo $row->commessa; ?>'> Sospeso</a></button></div>
			</div>
			<div class="col-md-12 col-lg-10">
				<div class="col-sm-3">
					<div class="row"><small>CLIENTE:</small></div>
					<div class="row" style='border-bottom:1px solid black;'> <font size= '5' color=blue><?php echo $cliente->ragione_soc; ?></font></div>
				</div>
				<div class="col-sm-3">
					<div class="row"><small>NOME SAPONE:</small></div>
					<div class="row" style='border-bottom:1px solid black;'><font size= '5' color=blue><?php echo ($row->nome_sapone!=NULL ? $row->nome_sapone : '&nbsp;') ?></font></div>
				</div>
				<div class="col-sm-3">
					<div class="row"><small>DISEGNO SAPONE:</small></div>
					<div class="row" style='border-bottom:1px solid black;'><font size='5' color=blue><?php echo ($row->dis_sapone!=NULL ? $row->dis_sapone : '&nbsp;') ?></font></div>
				</div>
				<div class="col-sm-3">
					<div class="row"><small>NUMERO STAMPO:</small></div>
					<div class="row" style='border-bottom:1px solid black;'><font size='5' color=blue><?php echo ($row->n_stampo!=NULL ? $row->n_stampo : '&nbsp;') ?></font></div>
				</div>
			</div>
			<div class="col-lg-1 visible-lg-1">
				 <div class="row"><a href='/index.php/omis/lavori/<?php echo $stato_url; ?>/edit/<?php echo $row->commessa; ?>'><button class="btn btn-default" role='button'> Modifica</button></a></div>
				<div class="row"><a href='<?php echo $row->indirizzo_disegno; ?>'><button class="btn btn-default" role='button'> DISEGNO</button></a></div>
			</div>
		</div>
		<div class='row'>
			<div class="col-lg-1 visible-lg-1">
				<div class="row" align="right"><button class="btn btn-default" role='button'><a href='/index.php/omis/mandaincorso/<?php echo $row->commessa; ?>'>In corso&nbsp;</a></button></div>
				<div class="row" align="right"><button class="btn btn-default" role='button'><a href='/index.php/omis/clona_commessa/<?php echo $row->commessa; ?>'>Clona &nbsp;</a></button></div>

			</div>
			<div class="col-md-12 col-lg-10">
				<div class="col-sm-3">
					<div class="row"><small>PRODUTTORE:</small></div>
					<div class="row" style='border-bottom:1px solid black;'><font  size='5' color=blue><?php echo $produttore->ragione_soc; ?></font></div>
				</div>
				<div class="col-sm-3">
					<div class="row"><small>PESO:</small></div>
					<div class="row" style='border-bottom:1px solid black;'><font size='5' color=blue><?php echo ($row->peso!=NULL ? $row->peso.' gr' : '&nbsp;') ?></font></div>
				</div>
				<div class="col-sm-3">
					<div class="row"><small>STAMPATRICE:</small></div>
					<div class="row" style='border-bottom:1px solid black;'><font size= '5' color=blue><?php echo ($row->stampatrice!=NULL ? $row->stampatrice : '&nbsp;') ?></font></div>
				</div>
				<div class="col-sm-3">
					<div class="row"><small>COMMESSA:</small></div> 
					<div class="row" style='border-bottom:1px solid black;'><font size='5' color=blue><?php echo $row->commessa; ?></font>
					<a href="#"><big><span class='glyphicon glyphicon-hand-down mostraTendina' style="float:right;"></span></big></a></div>
				</div>	
			</div>
			<div class="col-lg-1 visible-lg-1" style="float:left;">
							<div class="row">
								<div class="col-sm-4" style="padding:0px;">
									<a href='/index.php/omis/stampa/<?php echo $row->commessa; ?>'><button class="btn btn-default" role='button'> <span class='glyphicon glyphicon-folder-open' ></span></button></a>
								</div>
								<div class="col-sm-4" style="padding:0px;">
									<a href='/index.php/omis/stampa_componenti/<?php echo $row->commessa; ?>'><button class="btn btn-default" role='button'> <span class='glyphicon glyphicon-list-alt'></span></button></a>
								</div>
								<div class="col-sm-4" style="padding:0px;">								
									<a href='/index.php/omis/stampa_accessori/<?php echo $row->commessa; ?>'><button class="btn btn-default" role='button'> <span class='glyphicon glyphicon-align-justify'></span></button></a>
								</div>
							</div>
							<div class="row"><a <?php echo $preventivo_row ?> ><button class='btn btn-default'>Preventivo</button></a></div>
			</div>
		</div>
		<div class='tendina' style='display:none;'>
			<div class="row">
				<div class="col-lg-1 visible-lg-1">
					<div class="row" align="right"><a class="ddt_fornitore_button" commessa='<?php echo $row->commessa; ?>'><button class='btn btn-default'>DDT Fornitore</button></a></div> 
				</div>
				<div class="col-md-12 col-lg-10">
					<!-- <div class="col-sm-3">
						<div class="row"><small>ORIENTAMENTO SCARICO:</small></div> 
						<div class="row" style='border-bottom:1px solid black;'><font color=blue><?php echo ($row->orientam_scarico!=NULL ? $row->orientam_scarico : '&nbsp;') ?></font></div>
					</div> -->
					<div class="col-sm-3">
						<div class="row" style="border-left:1px solid black; border-right:1px solid black; padding-left:5px;"><small>DATA INSERIMENTO COMMESSA:</small></div>  
						<div class="row" style="border-left:1px solid black; border-right:1px solid black; padding-left:5px;"><font color=blue><?php echo $row->data_inser_comm; ?></font></div> 
					</div>
						<div class="col-sm-3">
						<div class="row" style="padding-left:5px;"><small>PESO SPECIFICO:</small></div>
						<div class="row" style='border-bottom:1px solid black; padding-left:5px;'><font color=blue><?php echo ($row->peso_specifico!=NULL ? $row->peso_specifico : '&nbsp;') ?> </font></div>
					</div> 
					<div class="col-sm-3">
						<div class="row"><small>TIPO STAMPO:</small></div>
						<div class="row" style='border-bottom:1px solid black;'><font color=blue><?php echo ($row->tipo_stampo!=NULL ? $row->tipo_stampo : '&nbsp;') ?></font></div>
					</div>
					<div class="col-sm-3">
						<div class="row" style="border-left:1px solid black; border-right:1px solid black; padding-left:5px;"><small>N° OFFERTA:</small></div>
						<div class="row" style='border-left:1px solid black; border-right:1px solid black; padding-left:5px;'><font color=blue><?php echo ($row->offerta_n!=NULL ? $row->offerta_n : '&nbsp;') ?></font></div>
					</div> 	 	
					 	 	 	 	 	
				</div>	 
				<div class="col-lg-1 visible-lg-1">
				<div class="row">	<a <?php echo $ddt_cliente_row ?> ><button class='btn btn-default'>DDT Cliente</button></a></div>
				</div>
			</div>
			<div class="row">	
				<div class="col-lg-1 visible-lg-1"></div> 	 	 	 	 	
				<div class="col-md-12 col-lg-10">
					<div class="col-sm-3">
						<div class="row" style="border-left:1px solid black; border-right:1px solid black; padding-left:5px;"><small>&nbsp;</small></div>  
						<div class="row" style="border-left:1px solid black; border-right:1px solid black; padding-left:5px; border-bottom:1px solid black;">&nbsp;</div> 
					</div>
					<div class="col-sm-3">
						<div class="row" style="padding-left:5px;"><small>N° CAVITA':</small></div>
						<div class="row" style='border-bottom:1px solid black;padding-left:5px;'><font color=blue><?php echo ($row->n_cavita!=NULL ? $row->n_cavita : '&nbsp;') ?></font></div>
					</div>
					
					<div class="col-sm-3">
						<div class="row"><small>CONTO LAVORAZIONE:</small></div>
						<div class="row" style='border-bottom:1px solid black;'><a href='ftp://192.168.2.231:2222/ddt_cl/<?php echo $row->commessa; ?>.pdf' ><font color=blue><?php echo $row->c_lavorazione; ?></font></a></div>
					</div>	 	
					<div class="col-sm-3">
						<div class="row" style="border-left:1px solid black; border-right:1px solid black; padding-left:5px;"><small>DATA OFFERTA:</small></div>
						<div class="row" style='border-bottom:1px solid black; border-left:1px solid black; border-right:1px solid black; padding-left:5px;'><font color=blue><?php echo ($row->offerta_data!=NULL ? $row->offerta_data : '&nbsp;') ?></font></div>
					</div>
				</div>
				<div class="col-lg-1 visible-lg-1"></div>
			</div>
			<div class="row">
				<div class="col-lg-1 visible-lg-1"></div>
			 	<div class="col-md-12 col-lg-10">
			 		<div class="col-sm-3">
						<div class="row" style="border-left:1px solid black; border-right:1px solid black; padding-left:5px;"><small>DATA INVIO DISEGNO:</small></div> 
						<div class="row" style="border-left:1px solid black; border-right:1px solid black; padding-left:5px;"><font color=blue><?php echo ($row->data_invio_dis!=NULL ? $row->data_invio_dis : '&nbsp;') ?></font></div> 
					</div>	
					<div class="col-sm-3">
						<div class="row" style="padding-left:5px;"><small>COMMESSA CLIENTE:</small></div>
						<div class="row" style="padding-left:5px;"><font color=blue><?php echo ($row->comm_cliente!=NULL ? $row->comm_cliente : '&nbsp;') ?></font></div>
					</div>
					
					<div class="col-sm-3">
						<div class="row"><small>N° ORDINE CLIENTE:</small></div>
						<div class="row" ><a href='ftp://192.168.2.231:2222/ordiniclienti/<?php echo $cliente->ragione_soc; ?>/<?php echo $row->ordine_cliente; ?>.pdf'><?php echo ($row->ordine_cliente!=NULL ? $row->ordine_cliente : '&nbsp;') ?></a></div>
					</div>
					
					
					
					<div class="col-sm-3">
						<div class="row" style="border-left:1px solid black; border-right:1px solid black; padding-left:5px;"><small>N° DDT</small></div>
						<div class="row" style='border-left:1px solid black; border-right:1px solid black; padding-left:5px;'><font color=blue><?php echo ($row->ddt_n!=NULL ? $row->ddt_n : '&nbsp;') ?></font></div>
					</div>
				</div>
				<div class="col-lg-1 visible-lg-1"></div>
			</div>
			<div class="row">
				<div class="col-lg-1 visible-lg-1"></div>
				<div class="col-md-12 col-lg-10">
					<div class="col-sm-3">
						<div class="row" style="border-left:1px solid black; border-right:1px solid black; padding-left:5px;"><small>DATA APPROVAZIONE DISEGNO:</small></div> 
						<div class="row" style="border-left:1px solid black; border-right:1px solid black; padding-left:5px; border-bottom:1px solid black;"><font color=blue><?php echo ($row->data_approv_dis!=NULL ? $row->data_approv_dis : '&nbsp;') ?></font></div>
					</div>

					<div class="col-sm-3">
						<div class="row" style="padding-left:5px;"><small>RIFERIMENTO CLIENTE:</small></div>
						<div class="row" style='border-bottom:1px solid black; padding-left:5px;'><font color=blue><?php echo ($row->rif_cliente!=NULL ? $row->rif_cliente : '&nbsp;') ?></font></div>
					</div>
					
					<div class="col-sm-3">
						<div class="row"><small>DATA ORDINE CLIENTE:</small></div>
						<div class="row" style='border-bottom:1px solid black;'><font color=blue><?php echo ($row->data_ordine_cliente!=NULL ? $row->data_ordine_cliente : '&nbsp;') ?></font></div>
					</div>
					<div class="col-sm-3">
						<div class="row" style="border-left:1px solid black; border-right:1px solid black; padding-left:5px;"><small>DATA DDT:</small></div>
						<div class="row" style='border-bottom:1px solid black; border-left:1px solid black; border-right:1px solid black; padding-left:5px;'><font color=blue><?php echo ($row->ddt_data!=NULL ? $row->ddt_data : '&nbsp;') ?></font></div>
					</div>
				</div>
				<div class="col-lg-1 visible-lg-1"></div>
			</div>
			<div class="row">
				<div class="col-lg-1 visible-lg-1"></div>
				<div class="col-md-12 col-lg-10">	
					<div class="col-sm-3">
						<div class="row" style="border-left:1px solid black; border-right:1px solid black; padding-left:5px;"><small>DATA CONSEGNA INTERNA:</small></div>  
						<div class="row" style="border-left:1px solid black; border-right:1px solid black; padding-left:5px;"><font color=blue><?php echo ($row->data_consegna_interna!=NULL ? $row->data_consegna_interna : '&nbsp;') ?></font></div>
					</div>
					<div class="col-sm-3">
						<div class="row" style="padding-left:5px;"><small>MATERIALE MATRICE:</small></div>
						<div class="row" style="padding-left:5px;"><font color=blue><?php echo $materiale_matrice->nome_materiale; ?></font></div>
					</div>
					<div class="col-sm-3">
						<div class="row"><small>MATERIALE PIASTRE:</small></div>
						<div class="row" ><font color=blue><?php echo $materiale_piastre->nome_materiale; ?></font></div>
					</div>
					
					<div class="col-sm-3">
						<div class="row" style="border-left:1px solid black; border-right:1px solid black; padding-left:5px;"><small>N° FATTURA:</small></div>
						<div class="row" style="border-left:1px solid black; border-right:1px solid black; padding-left:5px;"><font color=blue><?php echo ($row->fattura_n!=NULL ? $row->fattura_n : '&nbsp;') ?></font></div>
					</div>	
				</div>
				<div class="col-lg-1 visible-lg-1"></div>
			</div>
			<div class="row">
				<div class="col-lg-1 visible-lg-1"></div>
				<div class="col-md-12 col-lg-10">
					<div class="col-sm-3">
						<div class="row" style="border-left:1px solid black; border-right:1px solid black; padding-left:5px;"><small>DATA CONSEGNA AL CLIENTE:</small></div> 
						<div class="row" style="border-left:1px solid black; border-right:1px solid black; padding-left:5px; border-bottom:1px solid black;"><font color=blue style='border:4px solid red;'><?php echo ($row->data_consegna_cliente!=NULL ? $row->data_consegna_cliente : '&nbsp;') ?></font></div>
					</div>
					<div class="col-sm-3">
						<div class="row" style="padding-left:5px;"><small>TRATTAM MATRICE:</small></div>
						<div class="row" style='border-bottom:1px solid black; padding-left:5px;'><font color=blue><?php echo $tratt_mat->nome_trattamento; ?></font> </div>
					</div>
					<div class="col-sm-3">
						<div class="row"><small>TRATTAM PIASTRE:</small></div>
						<div class="row" style='border-bottom:1px solid black;'><font color=blue><?php echo $tratt_piastre->nome_trattamento; ?></font> </div>
					</div>
					
					<div class="col-sm-3">
						<div class="row" style="border-left:1px solid black; border-right:1px solid black; padding-left:5px;"><small>DATA FATTURA:</small></div>
						<div class="row" style="border-left:1px solid black; border-right:1px solid black; padding-left:5px; border-bottom:1px solid black;"><font color=blue><?php echo ($row->fattura_data!=NULL ? $row->fattura_data : '&nbsp;') ?></font></div>
					</div>	
				</div>
				<div class="col-lg-1 visible-lg-1"></div>
			</div>
			<div class="row">
				<div class="col-lg-1 visible-lg-1"></div>
				<div class="col-md-12 col-lg-10">
					<div class="col-md-6">
						<div class="row"><small>NOTE CLIENTE:</small></div>
						<div class="rowm " style='border:7px solid #CCFF33;'><font color=red size= '4'><?php echo ($cliente->note_cliente!=NULL ? $cliente->note_cliente : '&nbsp;') ?></font></div>
					</div>
					<div class="col-md-6" >
						<div class="row"><small>NOTE PRODUTTORE: </small></div>
						<div class="row " style='border:7px solid #00FFFF;'><font color=red size= '4'><?php echo ($produttore->note_produttore!=NULL ? $produttore->note_produttore : '&nbsp;') ?></font></div>
					</div>
				</div>
				<div class="col-lg-1 visible-lg-1"></div>
			</div>
	
			<div class="row">
				<div class="col-lg-1 visible-lg-1"></div>
				<div class="col-md-12 col-lg-10">
					<div class="col-sm-12">
						<div class="row"><small>DESCRIZIONE PROGETTO:</small></div>
						<div class="row"><textarea style="width:100%;" rows="6" disabled><?php echo $row->descrizione; ?></textarea></div>
					</div>
					
				</div>
				<div class="col-lg-1 visible-lg-1"></div>
			</div>
			<div class="row" style='margin-bottom:30px;'>
				<div class="col-lg-1 visible-lg-1"></div>
				<div class="col-md-12 col-lg-10">
					<div class="col-sm-12 ">
						<div class="row"><small>NOTE PRODUZIONE:</small></div>
						<div class="row"><input type="text" style="width:100%; height:200%; text-align:center;" value="<?php echo $row->note; ?>" disabled></div>
					</div>
					
				</div>
				<div class="col-lg-1 visible-lg-1"></div>
			</div>
		</div>
	
