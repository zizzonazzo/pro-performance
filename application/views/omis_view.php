<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>OMIS</title>
<?php
foreach($css_files as $file): ?>
	<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>


  <link type="text/css" rel="stylesheet" href="http://<?php echo $_SERVER['SERVER_NAME'].':8000/assets/themes/datatables/css/bootstrap.css'?>" />
  <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css" />


    <!-- Latest compiled and minified CSS -->
<!--   <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:300,700">
 -->	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
   <?php echo $menu; ?>

    <div class="container-fluid" style="margin-top:60px;">
      <div class="row">
        <div class="col-sm-2"></div>
      	<div class="col-sm-8" >
      			<div  align="center" class="risultato_ric_glob" style="overflow: visible;"></div>
      	</div>
          <div class="col-sm-2 tasto_chiudi_ricerca"></div>
      </div>
    	<div class='row'>
        <div class="col-sm-4"></div>
        <div id='title' class="col-sm-4" align="center"> <span style='color:red; font-size:180%;'><?php echo $header ?></span></div>
        <div class="col-sm-4"></div>
    	</div>
      <div class='row'>
        <div class="col-sm-12">
            <?php echo $output; ?>
        </div>
      </div>
    </div>

    <?php foreach($js_files as $file): ?>
    	<script src="<?php echo $file; ?>"></script>
    <?php endforeach; ?>
<div id="dialog" title="Immagine Accessorio">
  <img src="" style="max-width: 100%; ">
</div>
<script>
  var pathArray = window.location.pathname.split( '/' );

  if(pathArray[3]=="operazioni_cassa"){
      $( "tr td:nth-child(6)" ).each(function() {
        if($(this).html()=='S') $(this).addClass("tobepaid");
        $(".tobepaid").siblings().css( "background-color", "#FF606E" );
        $(".tobepaid").css( "background-color", "#FF606E" );
      });

  }

</script>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

  </body>
</html>
