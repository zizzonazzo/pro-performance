
        <!-- Static navbar -->
    <nav class="navbar navbar-default navbar-fixed-top" id="omis_nav">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <span class="navbar-brand" href="#"><img src="/application/views/icons/omis_logo_2.png" width="80" height="50" style='margin-top:-13px;'></span>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
          <?php if($tipo_op=='UFF'){ ?>
                    <?php if($nome_op!='DAVID'){ ?>
                          <li class="dropdown">               <!------------------------------ MENU UFFICIO ---------------------- -->
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Lavori <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                              <li><a href="<?php echo site_url('omis/lavori/progetti')?>">In progetto</a></li>
                              <li><a href="<?php echo site_url('omis/lavori/sospesi')?>">Sospesi</a></li>
                              <li><a href="<?php echo site_url('omis/lavori/incorso')?>">In Corso</a></li>
                              <li><a href="<?php echo site_url('omis/lavori/eseguiti')?>">Eseguiti</a></li>
                            </ul>
                          </li>
                          <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Anagrafiche <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                              <li><a href="<?php echo site_url('omis/accessori')?>">Accessori</a></li>
                              <li><a href="<?php echo site_url('omis/banche')?>">Banche</a></li>
                              <li><a href="<?php echo site_url('omis/categorie_comp')?>">Categorie componenti</a></li>
                              <li><a href="<?php echo site_url('omis/centricosto')?>">Centri di costo</a></li>
                              <li><a href="<?php echo site_url('omis/clienti')?>">Clienti</a></li>
                              <li><a href="<?php echo site_url('omis/componenti')?>">Componenti</a></li>
                              <li><a href="<?php echo site_url('omis/condizioni_pagamento')?>">Condizioni Pagamento</a></li>
                              <li><a href="<?php echo site_url('omis/costi_cdc_annui')?>">Costi cdc annui</a></li>
                              <li><a href="<?php echo site_url('omis/fasi')?>">Fasi</a></li>
                              <li><a href="<?php echo site_url('omis/fasi_arretramento')?>">Fasi Arretramento</a></li>
                              <li><a href="<?php echo site_url('omis/fornitori')?>">Fornitori</a></li>
                              <li><a href="<?php echo site_url('omis/lavesterne')?>">Lav. Esterne</a></li>
                              <li><a href="<?php echo site_url('omis/materiali')?>">Materiali</a></li>
                              <li><a href="<?php echo site_url('omis/materiali_fornitori')?>">Materiali Fornitori</a></li>
                              <li><a href="<?php echo site_url('omis/note_produttori')?>">Note produttori</a></li>
                              <li><a href="<?php echo site_url('omis/operatori')?>">Operatori</a></li>
                              <li><a href="<?php echo site_url('omis/piano_dei_conti')?>">Piano dei conti</a></li>
                              <li><a href="<?php echo site_url('omis/produttori')?>">Produttori</a></li>
                              <li><a href="<?php echo site_url('omis/semilavorati')?>">Semilavorati</a></li>
                              <li><a href="<?php echo site_url('omis/stampatrici')?>">Stampatrici</a></li>
                              <li><a href="<?php echo site_url('omis/tipi_porto')?>">Tipi Porto</a></li>
                              <li><a href="<?php echo site_url('omis/tipi_trasporto')?>">Tipi Trasporto</a></li>
                              <li><a href="<?php echo site_url('omis/trattamenti')?>">Trattamenti</a></li>
                            </ul>
                          </li>
                          <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Magazzino<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                              <li><a href="<?php echo site_url('magazzino/magazzino_accessori')?>">Accessori</a></li>
                              <li><a href="<?php echo site_url('magazzino/deposito')?>">Deposito</a></li>
                              <li><a href="<?php echo site_url('magazzino/magazzino_materiali')?>">Materiali</a></li>
                              <li><a href="<?php echo site_url('magazzino/magazzino_semilavorati')?>">Semilavorati</a></li>
                            </ul>
                          </li>
                           <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Amministrazione<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                              <li><a href="<?php echo site_url('contabilita/operazioni_cassa')?>">Cassa</a></li>
                              <li><a href="<?php echo site_url('ddt/tabella_ddt')?>">DDT</a></li>
                              <li><a href="<?php echo site_url('fatturazione/tabella_fatture')?>">Fatture</a></li>
                              <li><a href="<?php echo site_url('fatturazione/tabella_fatture_fornitori')?>">Fatture Fornitori</a></li>
                              <li class="dropdown-submenu">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Importa tempi</a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?php echo site_url('preventivi/prepara_import/1')?>">M1015</a></li>
                                    <li><a href="<?php echo site_url('preventivi/prepara_import/2')?>">M1010</a></li>
                                    <li><a href="<?php echo site_url('preventivi/prepara_import/3')?>">EC68</a></li>
                                </ul>
                              </li>
                              <li><a href="<?php echo site_url('omis/listini/componenti')?>">Listini Componenti</a></li>
                              <li><a href="<?php echo site_url('omis/listini/stampi')?>">Listini Stampi</a></li>
                              <li><a href="<?php echo site_url('preventivi/tabella')?>">Preventivi</a></li>

                            </ul>
                          </li>
                           <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Scadenzari<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                              <li class="dropdown-submenu">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Operatori</a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?php echo site_url('operatori/controllo_produzione_bolle/3')?>">Agostino</a></li>
                                    <li><a href="<?php echo site_url('operatori/controllo_produzione_bolle/5')?>">Fabio</a></li>
                                    <li><a href="<?php echo site_url('operatori/controllo_produzione_bolle/10')?>">Ilario</a></li>
                                    <li><a href="<?php echo site_url('operatori/controllo_produzione_bolle/8')?>">Nicola</a></li>
                                    <li><a href="<?php echo site_url('operatori/controllo_produzione_bolle/7')?>">Salvatore</a></li>
                                </ul>
                              </li>
                              <li><a href="<?php echo site_url('operatori/controllo_produzione_fasi')?>">Uff. Tecnico</a></li>
                            </ul>
                          </li>
                           <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Ordini<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                              <li><a href="<?php echo site_url('magazzino/lav_esterne')?>">Lav Esterne</a></li>
                              <li><a href="<?php echo site_url('magazzino/ordini_lista')?>">Ordini fornitori</a></li>
                              <li role="separator" class="divider"></li>
                              <li><a href="<?php echo site_url('magazzino/lista_ordini_accessori')?>">Accessori</a></li>
                              <li><a href="<?php echo site_url('magazzino/lista_ordini_materiali')?>">Materiali</a></li>
                              <li><a href="<?php echo site_url('magazzino/lista_ordini_semilavorati')?>">Semilavorati</a></li>
                            </ul>
                          </li>


                        <?php }else{ ?>             <!------------------------------ MENU DAVID ---------------------- -->
                             <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Lavori <span class="caret"></span></a>
                              <ul class="dropdown-menu">
                                <li><a href="<?php echo site_url('omis/lavori/progetti')?>">In progetto</a></li>
                                <li><a href="<?php echo site_url('omis/lavori/sospesi')?>">Sospesi</a></li>
                                <li><a href="<?php echo site_url('omis/lavori/incorso')?>">In Corso</a></li>
                                <li><a href="<?php echo site_url('omis/lavori/eseguiti')?>">Eseguiti</a></li>
                              </ul>
                            </li>
                            <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Magazzino<span class="caret"></span></a>
                              <ul class="dropdown-menu">
                                <li><a href="<?php echo site_url('magazzino/magazzino_accessori')?>">Accessori</a></li>
                                <li><a href="<?php echo site_url('magazzino/magazzino_materiali')?>">Materiali</a></li>
                                <li><a href="<?php echo site_url('magazzino/magazzino_semilavorati')?>">Semilavorati</a></li>
                              </ul>
                            </li>
                             <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Anagrafiche <span class="caret"></span></a>
                              <ul class="dropdown-menu">
                                <li><a href="<?php echo site_url('omis/categorie_comp')?>">Categorie componenti</a></li>
                                <li><a href="<?php echo site_url('omis/clienti')?>">Clienti</a></li>
                                <li><a href="<?php echo site_url('omis/componenti')?>">Componenti</a></li>
                                <li><a href="<?php echo site_url('omis/fasi')?>">Fasi</a></li>
                                <li><a href="<?php echo site_url('omis/lavesterne')?>">Lav. Esterne</a></li>
                                <li><a href="<?php echo site_url('omis/materiali')?>">Materiali</a></li>
                                <li><a href="<?php echo site_url('omis/note_produttori')?>">Note produttori</a></li>
                                <li><a href="<?php echo site_url('omis/produttori')?>">Produttori</a></li>
                                <li><a href="<?php echo site_url('omis/stampatrici')?>">Stampatrici</a></li>
                                <li><a href="<?php echo site_url('omis/trattamenti')?>">Trattamenti</a></li>
                              </ul>
                          </li>


                             <li><a href="<?php echo site_url('operatori/elenco_bolle_produzione')?>">Scadenzario</a></li>
                             <li><a href="<?php echo site_url('operatori/elenco_tempi_operatore')?>">Tempi inseriti</a></li>
                           <?php } ?>
                  <?php }else{ ?>    <!------------------------------ MENU OPERATORI ---------------------- -->
                            <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Lavori <span class="caret"></span></a>
                              <ul class="dropdown-menu">
                                <li><a href="<?php echo site_url('omis/lavorazioni/progetti')?>">In progetto</a></li>
                                <li><a href="<?php echo site_url('omis/lavorazioni/incorso')?>">In Corso</a></li>
                                <li><a href="<?php echo site_url('omis/lavorazioni/eseguiti')?>">Eseguiti</a></li>
                             </ul>
                            </li>
                            <li class="dropdown">
                                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Magazzino<span class="caret"></span></a>
                                  <ul class="dropdown-menu">
                                    <li><a href="<?php echo site_url('magazzino/magazzino_accessori')?>">Accessori</a></li>
                                    <li><a href="<?php echo site_url('magazzino/magazzino_materiali')?>">Materiali</a></li>
                                    <li><a href="<?php echo site_url('magazzino/magazzino_semilavorati')?>">Semilavorati</a></li>
                                  </ul>
                            </li>
                             <li><a href="<?php echo site_url('operatori/elenco_bolle_produzione')?>">Scadenzario</a></li>
                             <li><a href="<?php echo site_url('operatori/elenco_tempi_operatore')?>">Tempi inseriti</a></li>
                             <?php if(($nome_op!='DAVID')&&($nome_op!='CLAUDIO')){ ?>
                              <li class="dropdown">
                                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Consumi<span class="caret"></span></a>
                                  <ul class="dropdown-menu">
                                    <li><a href="<?php echo site_url('operatori/elenco_consumi_materiali')?>">Materiali</a></li>
                                    <li><a href="<?php echo site_url('operatori/elenco_materiali_mancanti')?>">Materiali non inseriti</a></li>
                                    <li><a href="<?php echo site_url('operatori/elenco_scarico_accessori')?>">Accessori</a></li>
                                    <li><a href="<?php echo site_url('operatori/elenco_scarico_mancanti')?>">Accessori non scaricati</a></li>

                                  </ul>
                              </li>
                             <?php } ?>
                      <?php } ?>

          </ul>

        <ul class="nav navbar-nav navbar-right">
			      <li >
              <a class="hidden-md" style="margin-top:-5px; padding: 15px 3px;">
                  <small>  <select class="select_ric_globale">
                        <option value=""></option>
                        <option value="commessa">Commessa</option>
                        <option value="n_stampo">N° Stampo</option>
                        <option value="nome_sapone">Nome Sapone</option>
                        <option value="ordine_cliente">Ordine Cliente</option>
                        <option value="comm_cliente">Commessa Cliente</option>
                        <option value="dis_sapone">Disegno Sapone</option>
                      </select>
                    </small>
		            		<input type="text" class="ric_globale" name="search" id="search" placeholder="Ricerca Globale">
	            </a>
            </li>
			     <li class="dropdown">
	              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="padding: 15px 3px;"><font color="red"> <?php echo $nome_op; ?></font><span class="caret"></span></a>
	              <ul class="dropdown-menu">
	                <li><a href='<?php echo site_url('omis/logout')?>'><font >LOGOUT</font></a></li>
	              </ul>
	            </li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
