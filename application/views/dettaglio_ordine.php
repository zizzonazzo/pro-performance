<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo $titolo?> - OMIS Gestione Schede</title>

<?php
foreach($css_files as $file): ?>
	<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<?php foreach($js_files as $file): ?>
	<script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>
    <!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:300,700">
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  <input type='hidden' id='id_ordine' value='<?php echo $id_ordine_fornitore; ?>'>
  <input type='hidden' id='num_ordine' value='<?php echo $num_ordine; ?>'>
  <input type='hidden' id='anno_ordine' value='<?php echo $anno_ordine; ?>'>
  <input type='hidden' id='nome_operatore' value='<?php echo $nome_op; ?>'>

        <!-- Static navbar -->
    <?php echo $menu; ?>
    <div class="container-fluid" style="margin-top:55px;">
    <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-8" >
            <div  align="center" class="risultato_ric_glob" style="overflow: visible;"></div>
        </div>
          <div class="col-sm-2 tasto_chiudi_ricerca"></div>
      </div>
    <div class="row" style='margin-top:75px;'>
        <div class="col-sm-3"></div>
        <div class="col-sm-6" align="center">
         <font color="red" size="5">  <?php echo $header; ?></font>
        </div>
        <div class="col-sm-3">
          <div class="row">
            <a href='<?php echo site_url('magazzino/ordini_lista/edit/'.$id_ordine_fornitore)?>'><button class="btn btn-default" role='button'> <span class='glyphicon glyphicon-pencil' ></span> Modifica Ordine</button></a>
          </div>
          <br>
          <div class="row">
            <a href='#'><button class="crea_pdf_ordine btn btn-default" role='button'> <span class='glyphicon glyphicon-floppy-save' ></span> Crea PDF</button></a>
          </div>
          <br>
          <div class="row">
            <a href='#'><button class="stampa_ordine btn btn-default" role='button'> <span class='glyphicon glyphicon-print' ></span> Stampa Ordine</button></a>
          </div>
          <br>
          <div class="row">
            <a type="button" data-toggle="modal" data-target="#myModal" class="aprimodal"><button class="btn btn-default" role='button'> <span class='glyphicon glyphicon-envelope' ></span> Invia Ordine</button></a>
          </div>
        </div>
      </div>
    	<div class='row' style="margin-top:10px;" >
    		<div class="col-sm-12">
            <div  class="col-sm-4" ></div>
            <div id='title' class="col-sm-4" align="center">
            	<a href='<?php echo site_url('magazzino/dettaglio_ordine/'.$id_ordine_fornitore.'/accessori')?>'><font size="3">Accessori</font></a> |
				      <a href='<?php echo site_url('magazzino/dettaglio_ordine/'.$id_ordine_fornitore.'/materiali')?>'><font size="3">Materiali</font></a> |
				      <a href='<?php echo site_url('magazzino/dettaglio_ordine/'.$id_ordine_fornitore.'/semilavorati')?>'><font size="3">Semilavorati</font></a>
            </div>
            <div  class="col-sm-4" >
            </div>
    		</div>
    	</div>
      <div class='row'>
        <div class="col-sm-12">
            <?php echo $output; ?>
        </div>
      </div>
    </div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title">Invia ordine</h4>
          </div>
          <div class="modal-body">
            <form role="form" class="form-horizontal">
                <div class="form-group">
                  <label class="col-sm-2" for="inputTo">A:</label>
                  <div class="col-sm-10"><input  class="form-control" id="inputTo" value="<?php echo $email_fornitore ?>"></div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2" for="inputCc">CC:</label>
                  <div class="col-sm-10"><input  class="form-control" id="inputCc" value=""></div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2" for="inputCcn">CCn:</label>
                  <div class="col-sm-10"><input  class="form-control" id="inputCcn" value="<?php echo $ccn ?>"></div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2" for="inputSubject">Oggetto:</label>
                  <div class="col-sm-10"><input type="text" class="form-control" id="inputSubject" value="<?php echo $oggetto_email ?>"></div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2" for="inputSubject">Allegato:</label>
                  <div class="col-sm-10"><a href="ftp:192.168.2.231:2222/ordinifornitori/Ordine_<?php echo $num_ordine ?>_<?php echo $anno_ordine ?>.pdf" target="_blank">Allegato Ordine</a><br></div>
                </div>
                <div class="form-group">
                  <label class="col-sm-12" for="inputBody">Messaggio</label>
                  <div class="col-sm-12"><textarea id="inputBody" cols="80" rows="18"><?php echo $signature ?></textarea></div>
                </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancella</button>
            <button type="button" class="btn btn-primary invia_ordine"> <span class="glyphicon glyphicon-send" aria-hidden="true"></span> &nbsp; Invia</button>

          </div>
    </div>
  </div>
</div>
<div class="modal fade" id="myModalLoading" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title">Invio E-mail in corso..</h4>
          </div>
         <div class="row">
                <div class="col-sm-4"></div>
                <div class="col-sm-4"><img src="/application/views/icons/loading.gif"></div>
                <div class="col-sm-4"></div>
              </div>
    </div>
  </div>
</div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="http://<?php echo $_SERVER['SERVER_NAME'].':8000/assets/js/gestione_ordini.js'?>"></script>

  </body>
</html>
