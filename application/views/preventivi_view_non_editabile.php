<html lang="en">
<head>
	<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	<link href='http://fonts.googleapis.com/css?family=Noto+Sans:400Italic' rel='stylesheet' type='text/css'>
	<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
	<script language="javascript" src="http://<?php echo $_SERVER['SERVER_NAME'].':8000/assets/js/gestione_preventivi_confermati.js'?>" type="text/javascript"></script>
	<script language="javascript" src="http://<?php echo $_SERVER['SERVER_NAME'].':8000/assets/themes/datatables/js/ricerca_globale.js'?>" type="text/javascript"></script>

	  <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:300,700">


	<link type="text/css" rel="stylesheet" href="http://<?php echo $_SERVER['SERVER_NAME'].':8000/assets/css/preventivi.css'?>" />
	<link type="text/css" rel="stylesheet" href="http://<?php echo $_SERVER['SERVER_NAME'].':8000/assets/css/menutendina.css'?>" />
	<link type="text/css" rel="stylesheet" href="http://<?php echo $_SERVER['SERVER_NAME'].':8000/assets/themes/datatables/css/bootstrap.css'?>" />

	<!-- Latest compiled and minified CSS -->

<!-- Optional theme -->

<!-- Latest compiled and minified JavaScript -->

	<title>Preventivo</title>
	<meta name="author" content="Marco Scoppetta">
	 <script type='text/javascript'>
		var base_url = '<?php echo $base_url; ?>';
	</script>
	<style>
	#omis_nav{
		margin-bottom: 0px;
	}
	</style>
	<!-- Date: 2013-03-25 -->
</head>

<body style="font-family: 'Noto Sans', sans-serif;"  class="compile">
	<input type='hidden' id='id_cliente' value='<?php echo $id_cliente_prev; ?>'>
	<input type='hidden' id='id_preventivo' value='<?php echo $id_preventivo; ?>'>
	<input type='hidden' id='num_revisione' value='<?php echo $num_revisione; ?>'>
	<input type='hidden' id='num_preventivo' value='<?php echo $num_preventivo; ?>'>
	<input type='hidden' id='secondo_costo_trasp' value='<?php echo $attiva_secondo_costo_trasp; ?>'>
	<input type='hidden' id='data_offerta_file' value='<?php echo $data_offerta_file; ?>'>
	<input type='hidden' id='nome_operatore' value='<?php echo $nome_op; ?>'>
<!-- INIZIO BODY NUOVO-->
   <?php echo $menu ?>



    <div class="container-fluid" style="margin-top:2%;">
    <div class="row">
        <div class="col-sm-2"></div>
      	<div class="col-sm-8" >
      			<div  align="center" class="risultato_ric_glob" style="overflow: visible;"></div>
      	</div>
          <div class="col-sm-2 tasto_chiudi_ricerca"></div>
      </div>
 		 <div class='row'>
            	<div class="col-lg-1"> </div>
            	<div class="col-lg-10 col-md-11" align="left" id="central" style="padding:2%;">
	            	<div class="row"  style="border:1px solid black;">
		            		<div class="col-sm-4"  style="padding-left:5px; padding-top:5px;">
		            			<img src="/application/views/icons/omis_bronzo_sps_09_2.jpg" width="200" height="150">
		            		</div>
		            		<div class="col-sm-4" style="line-height:150%; padding-top:20px;" >
		            					Via Olona 183/B <br>
										21013 GALLARATE (VA)  ITALY <br>
										<br>
										<div style="font-size:13px;">C.F. - P.IVA: 02554680120 <br>
										Cap.Soc. Euro 15.000,00 int.vers. <br>
										C.C.I.A.A. 267346 Reg. Impr. 02554680120</div>
		            		</div>
		            		<div class="col-sm-4" style="line-height:150%; padding-top:20px;" >
		            					Tel. 0331/776097 <br>
										Fax 0331/772290 <br>
										E-mail: omis@omis.it <br>
										PEC: omis@ticertifica.it <br>
										<br>
										www.omis.it
		            		</div>
	            	</div>
	            	<div class="row" style="border-left:1px solid black; border-right:1px solid black; ">
		            		<div class="col-sm-4"  style="padding-left:5px; padding-top:5px;">
		            			<br>Alla cortese attenzione di: <br><br><div class='riferimenti_prev'><?php echo $riferimenti_cliente; ?></div> <br><br>
									<strong> OFFERTA <?php echo $num_preventivo; ?>/<?php echo $num_revisione; ?> del <?php echo $data_offerta; ?></strong>
		            		</div>
		            		<div class="col-sm-4"  style="border-left:1px solid black; padding-left:5px;" >
		            					<strong>CLIENTE </strong><br><br> Spett.le <?php echo $nome_cliente; ?><br><br><?php echo $indirizzo_cliente; ?><br>
		            					<br> <?php echo $citta_cliente; ?> - <?php echo $nazione_cliente; ?><br><br>
		            		</div>
		            		<div class="col-sm-4" style="border-left:1px solid black; padding-left:5px;" >
		            					<strong>DESTINATARIO </strong><br><br> Spett.le <?php echo $nome_destinatario; ?>
										<br><br><?php echo $indirizzo_dest; ?><br><br> <?php echo $citta_dest; ?> - <?php echo $nazione_dest; ?><br><br>
		            		</div>
	            	</div>
	            	<div class="row" >
	            		<div class="col-sm-7" style="border:1px solid black;">
	            			<strong>DESCRIZIONE</strong>
	            		</div>
	            		<div class="col-sm-1" style="border:1px solid black;">
	            			<strong>UM</strong>
	            		</div>
	            		<div class="col-sm-1" style="border:1px solid black;">
	            			<strong>Q.TA'</strong>
	            		</div>
	            		<div class="col-sm-1" style="border:1px solid black;">
	            			<strong>PREZZO</strong>
	            		</div>
	            		<div class="col-sm-1" style="border:1px solid black;">
	            			<strong>SC.</strong>
	            		</div>
	            		<div class="col-sm-1" style="border:1px solid black;">
	            			<strong>TOTALE</strong>
	            		</div>
	            	</div>
	            	<div class="row">
	            		<?php echo $tabella_descrizione; ?>
	            	</div>


					<div class="row" style="margin-top:30px;">
						<div class="col-sm-7">
							<div class="row" style="border:1px solid black; padding:4px;"><strong>Data consegna lavoro prevista:</strong> <?php echo $data_cons_lav_prevista; ?> dalla Vs conferma disegno</div>
							<div class="row" style="border:1px solid black; padding:4px;"><strong>Condizione di pagamento:</strong> <?php echo $condizione_pagamento; ?>	</div>
							<div class="row" style="border:1px solid black; padding:4px;"><strong>Banca d'appoggio:</strong> <?php echo $banca_appoggio; ?>	</div>
							<div class="row" style="border:1px solid black; padding:4px;"><strong>Tipologia trasporto:</strong> <?php echo $tipo_trasporto; ?>	</div>
							<div class="row" style="border:1px solid black; padding:4px;"><strong>Porto:</strong> <?php echo $tipo_porto; ?></div>
						</div>
						<div class="col-sm-1"></div>
						<div class="col-sm-4">
							<div class="row" style="border:1px solid black; padding:4px;"><strong>Imponibile Totale:</strong><a id='imponibile_tot'></a>&#8364;</div>
							<div class="row" style="border:1px solid black; padding:4px;" id='costo_trasp' value='<?php echo $costo_trasp; ?>' classeiva='<?php echo $costo_trasp_classe_iva; ?>'>
								<strong>Costo trasporto:</strong><?php echo $costo_trasp; ?> &#8364;
							</div>
							<div class="row secondo_costo_tr <?php echo $secondo_costo_tr_class; ?>" style="border:1px solid black; padding:4px;"><span class='optionaltd' id='costo_trasp1' value='<?php echo $costo_trasp1; ?>' classeiva='<?php echo $costo_trasp_classe_iva1; ?>'>
								<strong>Costo trasporto:</strong></span><?php echo $imposte1; ?>
								</div>
							<div class="row" style="border:1px solid black; padding:4px;"><strong>Imposte:</strong><a id='imposta'></a> &#8364; </div>
							<div class="row" style="border:1px solid black; padding:4px;"><strong>Prezzo a pagare:<a id='a_pagare'></a> &#8364;</strong></div>
						</div>
					</div>
					<div class="row" style="margin-top:10px;">
						<div class="col-sm-8"> In attesa di Vostra gradita conferma porgiamo distinti saluti.
						</div>
						<div class="col-sm-4"> <strong>O.M.I.S. s.r.l</strong>
						</div>
					</div>
            	</div>
    			<div class="col-sm-1" id="right" style="padding-left:0; padding-top:30px;">
						<br><br><br><a href=''><button style="width: 100%;" class='stampa btn btn-default' role='button'/><span class="glyphicon glyphicon-print" aria-hidden="true"></span> Stampa</button></a>
						<br><br><a href='#'><button style="width: 100%;" class='creapdf btn btn-default' role='button'/><span class="glyphicon glyphicon-floppy-save" aria-hidden="true"></span> Crea PDF</button></a>
						<br><br><a type="button" data-toggle="modal" data-target="#myModal" class="aprimodal"><button style="width: 100%;" class='btn btn-default' role='button'/><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span> Invia Offerta</button></a>
						<br><br><br>REVISIONE: <?php echo $lista_revisioni ?>
						<br><br><a href='<?php echo site_url('preventivi/nuova_revisione/'.$id_preventivo.'/'.$ultimo_num_revisione)?>'><button style="width: 100%;" class='btn btn-default' role='button'/><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Nuova Rev</button></a>
						<br><br><?php echo $button_conferma_revisione ?>
    			 </div>
    		</div>
    </div>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

    <!-- FINE BODY NUOVO -->



<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title">Invia offerta</h4>
          </div>
          <div class="modal-body">
            <form role="form" class="form-horizontal">
                <div class="form-group">
                  <label class="col-sm-2" for="inputTo">A:</label>
                  <div class="col-sm-10"><input  class="form-control" id="inputTo" value="<?php echo $email_cliente ?>"></div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2" for="inputCc">CC:</label>
                  <div class="col-sm-10"><input  class="form-control" id="inputCc" value=""></div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2" for="inputCcn">CCn:</label>
                  <div class="col-sm-10"><input  class="form-control" id="inputCcn" value="<?php echo $ccns_email ?>"></div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2" for="inputSubject">Oggetto:</label>
                  <div class="col-sm-10"><input type="text" class="form-control" id="inputSubject" value="<?php echo $oggetto_email ?>"></div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2" for="inputSubject">Allegato:</label>
                  <div class="col-sm-10"><a href="ftp:192.168.2.231:2222/ufficio/<?php echo $num_preventivo ?>-<?php echo $num_revisione ?>_<?php echo $data_offerta_file ?>.pdf" target="_blank">Allegato Offerta</a><br><?php echo $disegni_allegati ?></div>
                </div>
                <div class="form-group">
                  <label class="col-sm-12" for="inputBody">Messaggio</label>
                  <div class="col-sm-12"><textarea id="inputBody" cols="80" rows="18"><?php echo $signature ?></textarea></div>
                </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancella</button>
            <button type="button" class="btn btn-primary inviaofferta"> <span class="glyphicon glyphicon-send" aria-hidden="true"></span> &nbsp; Invia</button>

          </div>
    </div>
  </div>
</div>
<div class="modal fade" id="myModalLoading" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title">Invio E-mail in corso..</h4>
          </div>
         <div class="row">
                <div class="col-sm-4"></div>
                <div class="col-sm-4"><img src="/application/views/icons/loading.gif"></div>
                <div class="col-sm-4"></div>
              </div>
    </div>
  </div>
</div>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>




</body>
</html>
